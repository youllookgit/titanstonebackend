function readxlsx(inpdata, fmt) {
    //讀取xlsx檔

    //參數
    //inpdata為由input file讀入之data
    //fmt為讀取格式，可有"json"或"csv"，csv格式之分欄符號為逗號，分行符號為[\n]

    //說明
    //所使用函式可參考js-xlsx的GitHub文件[https://github.com/SheetJS/js-xlsx]


    //to_json
    function to_json(workbook) {
        var result = {};
        workbook.SheetNames.forEach(function (sheetName) {
            var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            if (roa.length > 0) {
                result[sheetName] = roa;
            }
        });
        return result;
    }


    //to_csv
    function to_csv(workbook) {
        var result = [];
        workbook.SheetNames.forEach(function(sheetName) {
            var csv = XLSX.utils.sheet_to_csv(workbook.Sheets[sheetName]);
            if(csv.length > 0){
                result.push('SHEET: ' + sheetName);
                result.push('\n');
                result.push(csv);
            }
        });
        return result;
    }


    //讀檔
    var workbook = XLSX.read(inpdata, { type: 'binary'});
    // ws = workbook.Sheets[workbook.SheetNames[0]];
    // data = XLSX.utils.sheet_to_row_object_array(ws, {'date_format':'yyyy/mm/dd'});
    // console.log('xmls',data);
    //轉為json物件回傳
    if (fmt === 'json') {
        return to_json(workbook);
    }
    else {
        return to_csv(workbook);
    }


}