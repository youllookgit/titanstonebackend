import { NgModule,Injector } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpModule } from '@angular/http';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import {FormsModule,ReactiveFormsModule} from '@angular/forms';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

//Import custom Service
import { ApiService } from './share/service/api.service';
import { LitmitService } from './share/service/litmitservice';
//Import custom Module
import { PopupComponentModule } from './share/component/popup/pop.modules';

import { AppComponent } from './app.component';

//Base Module
import {BaseComponent} from './share/component/base.component';
// Base containers
import { DoLogInComponent } from './component/doLogin/doLogin.component';
import { LayoutComponent } from './component/layout/layout.component';
import { DocumentComponent } from './component/_doc/doc.component';
import { EmptyComponent } from './component/empty/empty.component';
//Demo
import { BaseformComponent } from './component/Demo/baseform.component';
//A
import { AccountComponent } from './component/UnitA/A01/account.component';
import { ChannelComponent } from './component/UnitA/A02/channel.component';
//B
import { MemberComponent } from './component/UnitB/B01/member.component';
import { EmaillogComponent } from './component/UnitB/B02/emaillog.component';
import { PreferenceComponent } from './component/UnitB/B03/preference.component';
import { EstateSaleComponent } from './component/UnitB/B04/estatesale.component';
import { CharterComponent } from './component/UnitB/B05/charter.component';
import { PayRecordComponent } from './component/UnitB/B06/payRecord.component';
import { BenefitRecordComponent } from './component/UnitB/B07/benefitRecord.component';
import { RentComponent } from './component/UnitB/B08/rent.component';
import { FundsSaleComponent } from './component/UnitB/B09/fundsSale.component';
import { FundsDetailComponent } from './component/UnitB/B10/fundsDetail.component';
import { InsSaleComponent } from './component/UnitB/B11/insuranceSale.component';
// C
import { EstateObjComponent } from './component/UnitC/C01/estateObj.component';
import { EstateRoomComponent } from './component/UnitC/C02/estateRoom.component';
import { EstateScheduleComponent } from './component/UnitC/C03/estateSchedule.component';
import { ScheduleRecordComponent } from './component/UnitC/C04/scheduleRecord.component';
import { FundsComponent } from './component/UnitC/C05/funds.component';
import { FundsWorthComponent } from './component/UnitC/C06/fundsWorth.component';
import { FundsReportComponent } from './component/UnitC/C07/fundsReport.component';
import { InsuranceComponent } from './component/UnitC/C08/insurance.component';
//D
import { TradeNewsComponent } from './component/UnitD/D01/tradenews.component';
import { ActivityAreaComponent } from './component/UnitD/D02/activityarea.component';
import { ActivityComponent } from './component/UnitD/D03/activity.component';
import { NewsComponent } from './component/UnitD/D04/news.component';
import { FaqSortComponent } from './component/UnitD/D05/faqsort.component';  
import { FaqItemComponent } from './component/UnitD/D06/faqitem.component';
// E
import { PushComponent } from './component/UnitE/E01/push.component';
import { PushLogComponent } from './component/UnitE/E02/pushLog.component';
import { ContactUsComponent } from './component/UnitE/E03/contactUs.component';
import { SeminarComponent } from './component/UnitE/E04/seminar.component';
import { AdminNoticeComponent } from './component/UnitE/E05/adminNotice.component';
//html editor Ngx-Quill
import { QuillModule } from 'ngx-quill';
//Pipe
import { DataFilterPipe } from './share/pipe/datafilterpipe';
// Directuve
import { LitmitDirective } from './share/directive/litmitDirective';
import { DatePickerDirective } from './share/directive/detepickerDirective';
import { DatePickerNgModelDirective } from './share/directive/detepickerModelDirective';
//share
import { PopTransQueryComponent } from './share/component/datepicker/pop-transquery.component';
import { TagInputComponent } from './share/component/tag/tag.component';
import { HtmlEditFormContrlComponent } from './share/component/html-edit/html-edit.component';
import { HtmlTableFormContrlComponent } from './share/component/html-table/html-table.component';
import { FileUploadComponent } from './share/component/fileupload/fileupload.component';
import { AlbumComponent } from './share/component/fileupload/album.component';
import { DataTableContrlComponent } from './share/component/datatable/datatable.component';
import { MultiEditComponent } from './share/component/html-edit/multi-edit.component';
import { ImgUploadComponent } from './share/component/fileupload/Imgupload.component';
import { CheckboxComponent } from './share/component/form/checkbox.component';
import { RadioComponent } from './share/component/form/radio.component';
const APP_CONTAINERS = [
  BaseComponent,
  LayoutComponent,
  DoLogInComponent,
  DocumentComponent,
  EmptyComponent,
  //Demo
  BaseformComponent,
  //A
  AccountComponent,
  ChannelComponent,
  //B
  MemberComponent,
  EmaillogComponent,
  PreferenceComponent,
  EstateSaleComponent,
  CharterComponent,
  PayRecordComponent,
  BenefitRecordComponent,
  RentComponent,
  FundsSaleComponent,
  FundsDetailComponent,
  InsSaleComponent,
  // C
  EstateObjComponent,
  EstateRoomComponent,
  EstateScheduleComponent,
  ScheduleRecordComponent,
  FundsComponent,
  FundsWorthComponent,
  FundsReportComponent,
  InsuranceComponent,
  //D
  TradeNewsComponent,
  ActivityAreaComponent,
  ActivityComponent,
  NewsComponent,
  FaqSortComponent,
  FaqItemComponent,
  // E
  PushComponent,
  PushLogComponent,
  ContactUsComponent,
  SeminarComponent,
  AdminNoticeComponent,
  //share
  PopTransQueryComponent,
  TagInputComponent,
  HtmlEditFormContrlComponent,
  HtmlTableFormContrlComponent,
  FileUploadComponent,
  ImgUploadComponent,
  AlbumComponent,
  DataTableContrlComponent,
  MultiEditComponent,
  CheckboxComponent,
  RadioComponent,
  //directove
  LitmitDirective,
  DatePickerDirective,
  DatePickerNgModelDirective,
  //pipe
  DataFilterPipe,
];

import {
  AppAsideModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
//for test


// Pagination Component
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    FormsModule,
    ReactiveFormsModule,
    
    QuillModule,

    AppAsideModule,
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    ChartsModule,

    PopupComponentModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS
  ],
  providers: [
    ApiService,
    LitmitService,
    {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
    }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  static injector: Injector;//DI 注入
  constructor(
    injector: Injector//DI 注入
  ) {
    AppModule.injector = injector;//DI 注入
  }
}
