import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Base Containers
import { LayoutComponent } from './component/layout/layout.component';
import { DoLogInComponent } from './component/doLogin/doLogin.component';
import { DocumentComponent } from './component/_doc/doc.component';
import { EmptyComponent } from './component/empty/empty.component';
//Demo
import { BaseformComponent } from './component/Demo/baseform.component';
//A
import { AccountComponent } from './component/UnitA/A01/account.component';
import { ChannelComponent } from './component/UnitA/A02/channel.component';
//B
import { MemberComponent } from './component/UnitB/B01/member.component';
import { EmaillogComponent } from './component/UnitB/B02/emaillog.component';
import { PreferenceComponent } from './component/UnitB/B03/preference.component';
import { EstateSaleComponent } from './component/UnitB/B04/estatesale.component';
import { CharterComponent } from './component/UnitB/B05/charter.component';
import { PayRecordComponent } from './component/UnitB/B06/payRecord.component';
import { BenefitRecordComponent } from './component/UnitB/B07/benefitRecord.component';
import { RentComponent } from './component/UnitB/B08/rent.component';
import { FundsSaleComponent } from './component/UnitB/B09/fundsSale.component';
import { FundsDetailComponent } from './component/UnitB/B10/fundsDetail.component';
import { InsSaleComponent } from './component/UnitB/B11/insuranceSale.component';
// C
import { EstateObjComponent } from './component/UnitC/C01/estateObj.component';
import { EstateRoomComponent } from './component/UnitC/C02/estateRoom.component';
import { EstateScheduleComponent } from './component/UnitC/C03/estateSchedule.component';
import { ScheduleRecordComponent } from './component/UnitC/C04/scheduleRecord.component';
import { FundsComponent } from './component/UnitC/C05/funds.component';
import { FundsWorthComponent } from './component/UnitC/C06/fundsWorth.component';
import { FundsReportComponent } from './component/UnitC/C07/fundsReport.component';
import { InsuranceComponent } from './component/UnitC/C08/insurance.component';
//D
import { TradeNewsComponent } from './component/UnitD/D01/tradenews.component';
import { ActivityAreaComponent } from './component/UnitD/D02/activityarea.component';
import { ActivityComponent } from './component/UnitD/D03/activity.component';
import { NewsComponent } from './component/UnitD/D04/news.component';
import { FaqSortComponent } from './component/UnitD/D05/faqsort.component';
import { FaqItemComponent } from './component/UnitD/D06/faqitem.component';
// E
import { PushComponent } from './component/UnitE/E01/push.component';
import { PushLogComponent } from './component/UnitE/E02/pushLog.component';
import { ContactUsComponent } from './component/UnitE/E03/contactUs.component';
import { SeminarComponent } from './component/UnitE/E04/seminar.component';
import { AdminNoticeComponent } from './component/UnitE/E05/adminNotice.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dologin',
    pathMatch: 'full',
  },
  {
    path: '',
    component: LayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      //共用Route设定
      {
        path: 'doc',
        component: DocumentComponent
      },
      {
        path: 'empty',
        component: EmptyComponent
      },

      //单元Route设定
      {
        path: 'demo',
        component: BaseformComponent
      },
      //A
      {
        path: 'A01',
        component: AccountComponent
      },
      {
        path: 'A02',
        component: ChannelComponent
      },
      //B
      {
        path: 'B01',
        component: MemberComponent
      },
      {
        path: 'B02',
        component: EmaillogComponent
      },
      {
        path: 'B03',
        component: PreferenceComponent
      },
      {
        path: 'B04',
        component: EstateSaleComponent
      },
      {
        path: 'B05',
        component: CharterComponent
      },
      {
        path: 'B06',
        component: PayRecordComponent
      },
      {
        path: 'B07',
        component: BenefitRecordComponent
      },
      {
        path: 'B08',
        component: RentComponent
      },
      {
        path: 'B09',
        component: FundsSaleComponent
      },
      {
        path: 'B10',
        component: FundsDetailComponent
      },
      {
        path: 'B11',
        component: InsSaleComponent
      },
      // C
      {
        path: 'C01',
        component: EstateObjComponent
      },
      {
        path: 'C02',
        component: EstateRoomComponent
      },
      {
        path: 'C03',
        component: EstateScheduleComponent
      },
      {
        path: 'C04',
        component: ScheduleRecordComponent
      },
      {
        path: 'C05',
        component: FundsComponent
      },
      {
        path: 'C06',
        component: FundsWorthComponent
      },
      {
        path: 'C07',
        component: FundsReportComponent
      },
      {
        path: 'C08',
        component: InsuranceComponent
      },
      //D
      {
        path:"D01",
        component:TradeNewsComponent
      },
      {
        path:"D02",
        component:ActivityAreaComponent
      },
      {
        path: "D03",
        component:ActivityComponent
      },
      {
        path:"D04",
        component:NewsComponent
      },
      {
        path:"D05",
        component:FaqSortComponent
      },
      {
        path:"D06",
        component:FaqItemComponent
      },
      // E
      {
        path: 'E01',
        component: PushComponent
      },
      {
        path: 'E02',
        component: PushLogComponent
      },
      {
        path: 'E03',
        component: ContactUsComponent
      },
      {
        path: 'E04',
        component: SeminarComponent
      },
      {
        path: 'E05',
        component: AdminNoticeComponent
      }
    ]
  },
  {
    path: 'dologin',
    component: DoLogInComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
