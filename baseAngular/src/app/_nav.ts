export const navItems = 
[
  {
    "name": "0.开发注意事项",
    "url": "/doc",
    "icon": "icon-note"
  },
  {
    "name": "A.系统管理",
    "url": "/empty?A",
    "icon": "icon-flag",
    "children": [
      {
        "name": "A01.管理员管理",
        "url": "/A01"
      },
      {
        "name": "A02.业务及渠道管理",
        "url": "/A02"
      }
    ]
  },
  {
    "name": "B.会员管理",
    "url": "/empty?B",
    "icon": "icon-user",
    "children": [
      {
        "name": "B01.会员资料管理",
        "url": "/B01"
      },
      {
        "name": "B02.会员验证信管理",
        "url": "/B02"
      },
      {
        "name": "B03.投资意向表管理",
        "url": "/B03"
      },
      {
        "name": "B04.地产销售管理",
        "url": "/B04"
      },
      // {
      //   "name": "B05.委託代管紀錄管理",
      //   "url": "/B05"
      // },
      // {
      //   "name": "B06.缴款纪录管理",
      //   "url": "/B06"
      // },
      // {
      //   "name": "B07.每月收益管理",
      //   "url": "/B07"
      // },
      // {
      //   "name": "B08.租賃紀錄管理",
      //   "url": "/B08"
      // },
      {
        "name": "B09.基金销售管理",
        "url": "/B09"
      },
      // {
      //   "name": "B11.基金交易明细管理",
      //   "url": "/B10"
      // }
      {
        "name": "B11.保险销售管理",
        "url": "/B11"
      },
    ]
  },
  {
    "name": "C.资产内容管理",
    "url": "/empty?C",
    "icon": "icon-pie-chart",
    "children": [
      {
        "name": "C01.地产上稿管理",
        "url": "/C01"
      },
      // {
      //   "name": "C02.房型管理",
      //   "url": "/C02"
      // },
      // {
      //   "name": "C03.工程进度管理",
      //   "url": "/C03"
      // },
      // {
      //   "name": "C04.施工纪录管理",
      //   "url": "/empty"
      // },
      {
        "name": "C05.基金商品管理",
        "url": "/C05"
      },
      // {
      //   "name": "C06.单位净值管理",
      //   "url": "/C06"
      // },
      // {
      //   "name": "C07.财务报表管理",
      //   "url": "/C07"
      // }
      {
        "name": "C08.保险商品管理",
        "url": "/C08"
      },
    ]
  },
  {
    "name": "D.APP内容管理",
    "url": "/empty?D",
    "icon": "icon-energy",
    "children": [
      {
        "name": "D01.理财趋势管理",
        "url": "/D01"
      },
      {
        "name": "D02.活动地区管理",
        "url": "/D02"
      },
      {
        "name": "D03.精选活动管理",
        "url": "/D03"
      },
      {
        "name": "D04.官方消息管理",
        "url": "/D04"
      },
      {
        "name": "D05.常见问题分类管理",
        "url": "/D05"
      },
      {
        "name": "D06.常见问题内容管理",
        "url": "/D06"
      }
    ]
  },
  {
    "name": "E.通知管理",
    "url": "/empty?E",
    "icon": "icon-bell",
    "children": [
      {
        "name": "E01.通知设定管理",
        "url": "/E01"
      },
      {
        "name": "E02.通知纪录管理",
        "url": "/E02"
      },
      {
        "name": "E03.联络我们管理",
        "url": "/E03"
      },
      {
        "name": "E04.活动报名管理",
        "url": "/E04"
      },
      {
        "name": "E05.收信人管理",
        "url": "/E05"
      }
    ]
  }
];
