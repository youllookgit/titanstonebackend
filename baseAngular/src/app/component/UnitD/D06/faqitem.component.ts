import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'faqitem.component.html'
})
export class FaqItemComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;

  public Query = {
    keyword: '',
    status: '',
    faqsortid: '',
    startdate: '',
    enddate: ''
  };
  public Isvalided = false;
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit() {
    this.Submit();
  }

  public FaqsortData;

  Submit() {
    this.api.apiPost('FaqSort/GetFaqSort', '').then(
      (res) =>{
          this.FaqsortData = res;
          this.api.apiPost('FaqItem/GetFaqItem', this.Query).then(
            (res) => {
              this.Data = res;
              //set Page
              this.setPage();
            }, (error) => {
              console.log('[Fail]', error);
            });
      },
      (error) => {
        console.log('[Fail]', error);
      }
    )
  }


  public TableSet = [
    {key:'Title',name:'标题'},
    {key:'FaqSortName',name:'分类'},
    {key:'OrderNo',name:'排序'},
    {key:'Status',name:'状态'},
    // {
    //   key:'Rule',
    //   name:'设定权限',
    //   style:{'text-align':'center'},
    //   icon:'fa-gear',
    //   event:(item)=>{
    //     console.log('设定权限',item);
    //   }
    // },
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];


  Reset() {
    this.Query = {
      keyword: '',
      status: '',
      faqsortid: '',
      startdate: '',
      enddate: ''
    };
    this.Submit();
  }


  public IsEditor = false;
  Create() {

    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      FaqSortID: new FormControl(''),
      Title: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      TitleEn: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      Detail: new FormControl('',  [Validators.required]),
      DetailEn: new FormControl('',  [Validators.required]),
      OrderNo: new FormControl('',  [Validators.required]),
      Status: new FormControl(true),
      CreateDate: new FormControl(null)
    });
    console.log('Create', this.mainForm);
  }

  Cancel() { this.IsEditor = false; this.mainForm.reset(); }

  Editor(item) {
    this.IsEditor = true;
    console.log(item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      FaqSortID: new FormControl(item['FaqSortID']),
      Title: new FormControl(item['Title'], [Validators.required]),
      TitleEn: new FormControl(item['TitleEn'], [Validators.required]),
      Detail: new FormControl(item['Detail'], [Validators.required]),
      DetailEn: new FormControl(item['DetailEn'], [Validators.required]),
      OrderNo: new FormControl(item['OrderNo'],  [Validators.required]),
      Status: new FormControl(item['Status']),
      CreateDate: new FormControl(item['CreateDate'])
    });
    console.log('Editor', this.mainForm);
  }

  FormSubmit() {
    var data = this.mainForm.value;
    var SendObj = {
      ID: data.ID,
      Title: data.Title,
      TitleEn: data.TitleEn,
      FaqSortID: data.FaqSortID,
      Detail: data.Detail,
      DetailEn: data.DetailEn,
      OrderNo: data.OrderNo,
      Status: data.Status,
    };
    console.log('Send Obj', SendObj);
    this.api.apiPost('FaqItem/Create', SendObj).then((res) => {
      this.pop.setConfirm(
        {
          content: '储存成功',
          event: () => {
            this.IsEditor = false;
            this.Submit();
          }
        });
    }, (err) => {
      this.pop.setConfirm({ content: err['msg'] });
    });


  }

  Delete() {

    var user = this.mainForm.value;
    this.pop.setConfirm({
      content: '确定删除 标题名称' + user['Title'] + '?', cancelTxt: '取消', event: () => {
        this.api.apiPost('FaqItem/Remove', { Id: user['ID'] }).then(
          (res) => {
            this.pop.setConfirm('删除成功!');
            this.IsEditor = false;
            this.Submit();
          }, (err) => {
            this.pop.setConfirm(err['msg']);
          }
        );
      }
    });
  }



  eventHandler(code) {
    if (code == 13) {
      this.Submit();
    }
  }

  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    console.log('pageChanged!', event);
    var _start = 0;
    if (event.page != 1) {
      _start = Number(this.itemsPerPage) * (event.page - 1);
    }
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start, _end);
    this.start = _start;
    this.end = _end;
  }
  setPage() {
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    this.PageData = this.Data.slice(0, Number(this.itemsPerPage));
    this.start = 0;
    this.end = this.itemsPerPage;
  }
  PerPageSet() {
    this.setPage();
  }
}
