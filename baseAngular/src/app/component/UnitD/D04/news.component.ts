import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'news.component.html'
})
export class NewsComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;

  public Query = {
    keyword: '',
    status: '',
    publicstartdate: '',
    publicenddate: '',
    beginstartdate: '',
    beginenddate: '',
    startdate: '',
    enddate: ''
  };
  public Isvalided = false;
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit() {
    this.Submit();
  }

  Submit() {
    this.api.apiPost('News/GetNews', this.Query).then(
      (res) => {
        this.Data = res;
        //set Page
        this.setPage();
      }, (error) => {
        console.log('[Fail]', error);
      });
  }

  public TableSet = [
    {key:'Title',name:'文章名称'},
    {key:'PublicDate',name:'文章日期'},
    {key:'BeginDate',name:'上架日期'},
    {key:'EndDate',name:'下架日期'},
    {key:'OrderNo',name:'排序'},
    {key:'Status',name:'状态'},
    // {
    //   key:'Rule',
    //   name:'设定权限',
    //   style:{'text-align':'center'},
    //   icon:'fa-gear',
    //   event:(item)=>{
    //     console.log('设定权限',item);
    //   }
    // },
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];

  Reset() {
    this.Query = {
      keyword: '',
      status: '',
      publicstartdate: '',
      publicenddate: '',
      beginstartdate: '',
      beginenddate: '',
      startdate: '',
      enddate: ''
    };
    this.Submit();
  }


  public IsEditor = false;
  Create() {

    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      Title: new FormControl('', [Validators.required]),
      TitleEn: new FormControl('', [Validators.required]),
      PublicDate: new FormControl('', [Validators.required]),
      Brief: new FormControl(''),
      BriefEn: new FormControl(''),
      ImageUrl: new FormControl('', [Validators.required]),
      BeginDate: new FormControl(''),
      EndDate: new FormControl(''),
      Detail: new FormControl('', [Validators.required]),
      DetailEn: new FormControl('', [Validators.required]),
      OrderNo: new FormControl('0'),
      Status: new FormControl(true),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
    console.log('Create', this.mainForm);
  }

  Cancel() { this.IsEditor = false; this.mainForm.reset(); }

  Editor(item) {
    this.IsEditor = true;
    console.log(item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      Title: new FormControl(item['Title'], [Validators.required]),
      TitleEn: new FormControl(item['TitleEn'], [Validators.required]),
      PublicDate: new FormControl(item['PublicDate'], [Validators.required]),
      Brief: new FormControl(item['Brief']),
      BriefEn: new FormControl(item['BriefEn']),
      ImageUrl: new FormControl(item['ImageUrl'], [Validators.required]),
      BeginDate: new FormControl(item['BeginDate']),
      EndDate: new FormControl(item['EndDate']),
      Detail: new FormControl(item['Detail'], [Validators.required]),
      DetailEn: new FormControl(item['DetailEn'], [Validators.required]),
      OrderNo: new FormControl(item['OrderNo']),
      Status: new FormControl(item['Status']),
      CreateDate: new FormControl({value : item['CreateDate'], disabled: true })
    });
    console.log('Editor', this.mainForm);
  }

  FormSubmit() {
    var data = this.mainForm.value;
    var SendObj = {
      ID: data.ID,
      Title: data.Title,
      TitleEn: data.TitleEn,
      PublicDate: data.PublicDate,
      Brief: data.Brief,
      BriefEn: data.BriefEn,
      ImageUrl: data.ImageUrl,
      BeginDate: data.BeginDate,
      EndDate: data.EndDate,
      Detail: data.Detail,
      DetailEn: data.DetailEn,
      OrderNo: data.OrderNo,
      Status: data.Status,
    };
    console.log('Send Obj', SendObj);
    this.api.apiPost('News/Create', SendObj).then((res) => {
      this.pop.setConfirm(
        {
          content: '储存成功',
          event: () => {
            this.IsEditor = false;
            this.Submit();
          }
        });
    }, (err) => {
      this.pop.setConfirm({ content: err['msg'] });
    });


  }

  Delete() {

    var user = this.mainForm.value;
    this.pop.setConfirm({
      content: '确定删除 文章名称' + user['Title'] + '?', cancelTxt: '取消', event: () => {
        this.api.apiPost('News/Remove', { Id: user['ID'] }).then(
          (res) => {
            this.pop.setConfirm('删除成功!');
            this.IsEditor = false;
            this.Submit();
          }, (err) => {
            this.pop.setConfirm(err['msg']);
          }
        );
      }
    });
  }



  eventHandler(code) {
    if (code == 13) {
      this.Submit();
    }
  }

  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    console.log('pageChanged!', event);
    var _start = 0;
    if (event.page != 1) {
      _start = Number(this.itemsPerPage) * (event.page - 1);
    }
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start, _end);
    this.start = _start;
    this.end = _end;
  }
  setPage() {
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    this.PageData = this.Data.slice(0, Number(this.itemsPerPage));
    this.start = 0;
    this.end = this.itemsPerPage;
  }
  PerPageSet() {
    this.setPage();
  }
}
