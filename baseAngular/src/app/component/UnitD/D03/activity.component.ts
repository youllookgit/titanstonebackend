import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'activity.component.html'
})
export class ActivityComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;

  public Query = {
    keyword: '',
    status: '',
    acttypeid:'',
    actareaid:'',
    estateObjID: ''
  };

  public Isvalided = false;
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit() {
    this.Submit();
  }
  public estateObjList = [];
  public Act_Type;
  public Act_Area;
  Submit() {

    this.api.apiPost('Activity/GetActTypeData', '').then(
      (type_res) => {
        this.Act_Type = type_res['ActType'];
        this.Act_Area = type_res['ActArea'];
        
        // 查询地产物件资料
        this.api.apiPost('EstateObj/GetList', {}).then(
          (es_res:any) => {
            this.estateObjList = [];
            es_res.forEach(estate => {
              this.estateObjList.push(
                {name:estate['Name'],value:estate['ID']}
              );
            });
            //查詢列表資料
            this.api.apiPost('Activity/GetActivity', this.Query).then(
              (res:any) => {
                res.forEach(act => {
                  var est = this.estateObjList.find(eitem =>{
                    return eitem.value == act['EstateObjIDs'];
                  });
                  act['esName'] = (est) ? est.name : '';
                });
                this.Data = res;
                //set Page
                this.setPage();
              }, (error) => {
                console.log('[Fail]', error);
              });
            
          }
        );

        
      },
      (error) => {
        console.log('[Fail]', error);
      }
    );

    
  }

  
  public TableSet = [
    {key:'ActTypeName',name:'活动类型'},
    {key:'Name',name:'活动标题'},
    {key:'ActDate',name:'活动時間日期'},
    {key:'ActAreaName',name:'活动地区'},
    {key:'esName',name:'建案'},
    {key:'BeginDate',name:'上架时间'},
    {key:'EndDate',name:'下架时间'},
    {key:'OrderNo',name:'排序'},
    {key:'Status',name:'状态'},
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];


  Reset() {
    this.Query = {
      keyword: '',
      status: '',
      acttypeid:'',
      actareaid:'',
      estateObjID: ''
    };
    this.Submit();
  }


  public IsEditor = false;
  Create() {

    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      ActType: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      Name: new FormControl('', [Validators.required]),
      NameEn: new FormControl('', [Validators.required]),
      ActDate: new FormControl(''),
      ActArea: new FormControl(''),
      ActPosition: new FormControl(''),
      ActPositionEn: new FormControl(''),
      ActAddr: new FormControl(''),
      ActAddrEn: new FormControl(''),
      Brief: new FormControl(''),
      BriefEn: new FormControl(''),
      Img: new FormControl('', [Validators.required]),
      EstateObjIDs: new FormControl(''),
      EstateObjIDsEn: new FormControl(''),
      Detail: new FormControl('', [Validators.required]),
      DetailEn: new FormControl('', [Validators.required]),
      BeginDate: new FormControl(''),
      EndDate: new FormControl(''),
      OrderNo: new FormControl('0'),
      Status: new FormControl(true),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
    console.log('Create', this.mainForm);
  }

  Cancel() { this.IsEditor = false; this.mainForm.reset(); }

  public Proc;
  public ProcEn;
  public Trafic;
  public TraficEn;
  Editor(item) {
    this.IsEditor = true;
    console.log('act',item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      ActType: new FormControl(item['ActType'], [Validators.required]),
      Name: new FormControl(item['Name'], [Validators.required]),
      NameEn: new FormControl(item['NameEn'], [Validators.required]),
      ActDate: new FormControl(item['ActDate']),
      ActArea: new FormControl(item['ActArea']),
      ActPosition: new FormControl(item['ActPosition']),
      ActPositionEn: new FormControl(item['ActPositionEn']),
      ActAddr: new FormControl(item['ActAddr']),
      ActAddrEn: new FormControl(item['ActAddrEn']),
      Brief: new FormControl(item['Brief']),
      BriefEn: new FormControl(item['BriefEn']),
      Img: new FormControl(item['Img'], [Validators.required]),
      EstateObjIDs: new FormControl(item['EstateObjIDs']),
      EstateObjIDsEn: new FormControl(item['EstateObjIDsEn']),
      BeginDate: new FormControl(item['BeginDate']),
      EndDate: new FormControl(item['EndDate']),
      Detail: new FormControl(item['Detail'], [Validators.required]),
      DetailEn: new FormControl(item['DetailEn'], [Validators.required]),
      OrderNo: new FormControl(item['OrderNo']),
      Status: new FormControl(item['Status']),
      CreateDate: new FormControl({value : item['CreateDate'], disabled: true }),

      Traffic: new FormControl(item['Traffic']),
      TrafficEn: new FormControl(item['TrafficEn']),
      Process: new FormControl(item['Process']),
      ProcessEn: new FormControl(item['ProcessEn'])
    });
    //console.log('Editor', this.mainForm);
  }

  FormSubmit() {
    var data = this.mainForm.value;
    var SendObj = {
      ID: data.ID,
      ActType: data.ActType,
      Name: data.Name,
      NameEn: data.NameEn,
      ActDate: data.ActDate,
      ActArea: data.ActArea,
      ActPosition: data.ActPosition,
      ActPositionEn: data.ActPositionEn,
      ActAddr: data.ActAddr,
      ActAddrEn: data.ActAddrEn,
      Brief: data.Brief,
      BriefEn: data.BriefEn,
      Img: data.Img,
      EstateObjIDs: data.EstateObjIDs,
      EstateObjIDsEn: data.EstateObjIDsEn,
      BeginDate: data.BeginDate,
      EndDate: data.EndDate,
      Detail: data.Detail,
      DetailEn: data.DetailEn,
      OrderNo: data.OrderNo,
      Status: data.Status,

      Traffic: this.ObjToStr(data.Traffic),
      TrafficEn: this.ObjToStr(data.TrafficEn),
      Process: this.ObjToStr(data.Process),
      ProcessEn: this.ObjToStr(data.ProcessEn)
    };


    if(typeof SendObj.Traffic == 'object'){

    }

    if(SendObj['EstateObjIDs'] && typeof SendObj['EstateObjIDs'] == 'object'){
      SendObj['EstateObjIDs'] = SendObj['EstateObjIDs'].join(',');
    }
    //console.log('Send Obj', SendObj);
    this.api.apiPost('Activity/Create', SendObj).then((res) => {
      this.pop.setConfirm(
        {
          content: '储存成功',
          event: () => {
            this.IsEditor = false;
            this.Submit();
          }
        });
    }, (err) => {
      this.pop.setConfirm({ content: err['msg'] });
    });


  }

  Delete() {

    var user = this.mainForm.value;
    this.pop.setConfirm({
      content: '确定删除 活动名称' + user['Name'] + '?', cancelTxt: '取消', event: () => {
        this.api.apiPost('Activity/Remove', { Id: user['ID'] }).then(
          (res) => {
            this.pop.setConfirm('删除成功!');
            this.IsEditor = false;
            this.Submit();
          }, (err) => {
            this.pop.setConfirm(err['msg']);
          }
        );
      }
    });
  }



  eventHandler(code) {
    if (code == 13) {
      this.Submit();
    }
  }

  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    
    var _start = 0;
    if (event.page != 1) {
      _start = Number(this.itemsPerPage) * (event.page - 1);
    }
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start, _end);
    this.start = _start;
    this.end = _end;
  }
  setPage() {
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    this.PageData = this.Data.slice(0, Number(this.itemsPerPage));
    this.start = 0;
    this.end = this.itemsPerPage;
  }
  PerPageSet() {
    this.setPage();
  }

  SetProc(param){
    console.log('param:',param,this.mainForm.value[param]);
    this.pop.setMultiEdit({
      data: this.mainForm.value[param],
      icon:false,
      event: (list) => {
        console.log('list',list);
        this.mainForm.value[param] = list;
      }
    });
  }

  Setalbum(param){
    console.log('param:',param,this.mainForm.value[param]);
    this.pop.setMultiEdit({
      data: this.mainForm.value[param],
      event: (list) => {
        console.log('list',list);
        this.mainForm.value[param] = list;
      }
    });
  }

  ObjToStr(value){
    if(typeof value == 'object'){
       return JSON.stringify(value);
    }else{
      return value;
    }
  }
}
