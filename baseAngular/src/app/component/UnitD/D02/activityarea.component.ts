import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'activityarea.component.html'
})
export class ActivityAreaComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;

  public Query = {
    keyword: '',
    status: '',
  };
  public Isvalided = false;
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit() {
    this.Submit();
  }

  Submit() {
    this.api.apiPost('ActivityArea/GetActivityArea', this.Query).then(
      (res) => {
        this.Data = res;
        //set Page
        this.setPage();
      }, (error) => {
        console.log('[Fail]', error);
      });
  }

  public TableSet = [
    {key:'Name',name:'名称'},
    {key:'OrderNo',name:'排序'},
    {key:'Enabled',name:'状态 '},

    // {
    //   key:'Rule',
    //   name:'设定权限',
    //   style:{'text-align':'center'},
    //   icon:'fa-gear',
    //   event:(item)=>{
    //     console.log('设定权限',item);
    //   }
    // },
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];


  Reset() {
    this.Query = {
      keyword: '',
      status: ''
    };
    this.Submit();
  }


  public IsEditor = false;
  Create() {

    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      Name: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      NameEn: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      OrderNo: new FormControl('0'),
      Status: new FormControl(true),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
    console.log('Create', this.mainForm);
  }

  Cancel() { this.IsEditor = false; this.mainForm.reset(); }

  Editor(item) {
    this.IsEditor = true;
    console.log(item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      Name: new FormControl(item['Name'], [Validators.required, Validators.maxLength(10)]),
      NameEn: new FormControl(item['NameEn'], [Validators.required, Validators.maxLength(50)]),
      OrderNo: new FormControl(item['OrderNo']),
      Status: new FormControl(item['Enabled']),
      CreateDate: new FormControl({value : item['CreateDate'], disabled: true })
    });
    console.log('Editor', this.mainForm);
  }

  FormSubmit() {
    var data = this.mainForm.value;
    var SendObj = {
      ID: data.ID,
      Name: data.Name,
      NameEn: data.NameEn,
      OrderNo: data.OrderNo,
      Enabled: data.Status,
    };
    console.log('Send Obj', SendObj);
    this.api.apiPost('ActivityArea/Create', SendObj).then((res) => {
      this.pop.setConfirm(
        {
          content: '储存成功',
          event: () => {
            this.IsEditor = false;
            this.Submit();
          }
        });
    }, (err) => {
      this.pop.setConfirm({ content: err['msg'] });
    });


  }

  Delete() {

    var user = this.mainForm.value;
    this.pop.setConfirm({
      content: '确定删除 名称' + user['Name'] + '?', cancelTxt: '取消', event: () => {
        this.api.apiPost('ActivityArea/Remove', { Id: user['ID'] }).then(
          (res) => {
            this.pop.setConfirm('删除成功!');
            this.IsEditor = false;
            this.Submit();
          }, (err) => {
            this.pop.setConfirm(err['msg']);
          }
        );
      }
    });
  }



  eventHandler(code) {
    if (code == 13) {
      this.Submit();
    }
  }

  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    console.log('pageChanged!', event);
    var _start = 0;
    if (event.page != 1) {
      _start = Number(this.itemsPerPage) * (event.page - 1);
    }
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start, _end);
    this.start = _start;
    this.end = _end;
  }
  setPage() {
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    this.PageData = this.Data.slice(0, Number(this.itemsPerPage));
    this.start = 0;
    this.end = this.itemsPerPage;
  }
  PerPageSet() {
    this.setPage();
  }
}
