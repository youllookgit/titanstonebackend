import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'tradenews.component.html'
})
export class TradeNewsComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;

  public Query = {
    keyword: '',
    status: '',
    displaystartdate: '',
    displayenddate: '',
    beginstartdate: '',
    beginenddate: '',
    startdate: '',
    enddate: '',
    tradetypeid:''
  };
  public Isvalided = false;
  public Data;
  public edit = false;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit() {
    this.Submit();
  }

  public TradeData;
  public SelectList;
  Submit() {

    this.api.apiPost('Option/GetTrade', '').then(
      (res) => {
        this.TradeData = res;
        this.api.apiPost('TradeNews/GetActivity', '').then(
          (res) => {
            this.SelectList = res;
            this.api.apiPost('TradeNews/GetTradeNews', this.Query).then(
              (res) => {
                this.Data = res;
                //set Page
                this.setPage();

              }, (error) => {
                console.log('[Fail]', error);
              });         
          },
          (error) => {
            console.log('[Fail]', error);
          }
        )

      },
      (error) => {
        console.log('[Fail]', error);
      }
    )


  }

  Reset() {
    this.Query = {
      keyword: '',
      status: '',
      displaystartdate: '',
      displayenddate: '',
      beginstartdate: '',
      beginenddate: '',
      startdate: '',
      enddate: '',
      tradetypeid :''
    };
    this.Submit();
  }

  public TableSet = [
    { key: 'TradeTypeName', name: '文章类型' },
    { key: 'Name', name: '文章名称' },
    { key: 'DisplayDate', name: '文章日期' },
    { key: 'Author', name: '作者' }, 
    { key: 'BeginDate', name: '上架时间' },
    { key: 'EndDate', name: '下架时间' },
    { key: 'OrderNo', name: '排序' },
    { key: 'Status', name: '状态' },
    // {
    //   key:'Rule',
    //   name:'设定权限',
    //   style:{'text-align':'center'},
    //   icon:'fa-gear',
    //   event:(item)=>{
    //     console.log('设定权限',item);
    //   }
    // },
    {
      key: 'Edit',
      name: '',
      icon: 'fa-pencil-square-o',
      event: (item) => {
        this.Editor(item);
      }
    }
  ];


  public IsEditor = false;
  Create() {

    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      TradeType: new FormControl('', [Validators.required]),
      Name: new FormControl(''),
      NameEn: new FormControl(''),
      DisplayDate: new FormControl(''),
      Brief: new FormControl(''),
      BriefEn:new FormControl(''),
      Img: new FormControl(''),
      Author: new FormControl(''),
      AuthorEn: new FormControl(''),
      Recommend: new FormControl(''),
      BeginDate: new FormControl(''),
      EndDate: new FormControl(''),
      Detail: new FormControl(''),
      DetailEn: new FormControl(''),
      OrderNo: new FormControl('0'),
      CreateDate: new FormControl(null),
      Status: new FormControl(true)
    });
    console.log('Create', this.mainForm);
  }

  Cancel() { this.IsEditor = false; this.mainForm.reset(); }

  Editor(item) {
    this.IsEditor = true;
    console.log(item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      TradeType: new FormControl(item['TradeType'], [Validators.required, Validators.maxLength(10)]),
      Name: new FormControl(item['Name']),
      NameEn: new FormControl(item['NameEn']),
      DisplayDate: new FormControl(item['DisplayDate']),
      Brief: new FormControl(item['Brief']),
      BriefEn: new FormControl(item['BriefEn']),
      Img: new FormControl(item['Img']),
      Author: new FormControl(item['Author']),
      AuthorEn: new FormControl(item['AuthorEn']),
      Recommend: new FormControl(item['Recommend']),
      BeginDate: new FormControl(item['BeginDate']),
      EndDate: new FormControl(item['EndDate']),
      Detail: new FormControl(item['Detail']),
      DetailEn: new FormControl(item['DetailEn']),
      OrderNo: new FormControl(item['OrderNo']),
      Status: new FormControl(item['Status']),
      CreateDate: new FormControl(item['CreateDate'])
    });
    console.log('Editor', this.mainForm);
  }

  FormSubmit() {
    var data = this.mainForm.value;
    var SendObj = {
      ID: data.ID,
      TradeType: data.TradeType,
      Name: data.Name,
      NameEn: data.NameEn,
      DisplayDate: data.DisplayDate,
      Brief: data.Brief,
      BriefEn: data.BriefEn,
      Img: data.Img,
      Author: data.Author,
      AuthorEn: data.AuthorEn,
      Recommend: '',
      BeginDate: data.BeginDate,
      EndDate: data.EndDate,
      Detail: data.Detail,
      DetailEn: data.DetailEn,
      OrderNo: data.OrderNo,
      Status: data.Status,
    };
    if(data.Recommend){
      SendObj.Recommend = data.Recommend.toString();
    }
    
    console.log('Send Obj', SendObj);
    this.api.apiPost('TradeNews/Create', SendObj).then((res) => {
      this.pop.setConfirm(
        {
          content: '储存成功',
          event: () => {
            this.IsEditor = false;
            this.Submit();
          }
        });
    }, (err) => {
      this.pop.setConfirm({ content: err['msg'] });
    });


  }

  Delete() {

    var user = this.mainForm.value;
    this.pop.setConfirm({
      content: '确定删除 文章名称' + user['Name'] + '?', cancelTxt: '取消', event: () => {
        this.api.apiPost('TradeNews/Remove', { Id: user['ID'] }).then(
          (res) => {
            this.pop.setConfirm('删除成功!');
            this.IsEditor = false;
            this.Submit();
          }, (err) => {
            this.pop.setConfirm(err['msg']);
          }
        );
      }
    });
  }



  eventHandler(code) {
    if (code == 13) {
      this.Submit();
    }
  }

  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    console.log('pageChanged!', event);
    var _start = 0;
    if (event.page != 1) {
      _start = Number(this.itemsPerPage) * (event.page - 1);
    }
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start, _end);
    this.start = _start;
    this.end = _end;
  }
  setPage() {
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    this.PageData = this.Data.slice(0, Number(this.itemsPerPage));
    this.start = 0;
    this.end = this.itemsPerPage;
  }
  PerPageSet() {
    this.setPage();
  }
}
