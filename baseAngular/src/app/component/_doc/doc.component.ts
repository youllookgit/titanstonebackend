import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

declare var $:any; //呼叫第三方js
@Component({
  templateUrl: 'doc.component.html'
})


export class DocumentComponent extends BaseComponent implements OnInit {
  public mainForm: FormGroup;
  constructor(
  ) {
    super();
  }
  //假资料范例 须整理成如下格式 name,value
  public SelectList = [
    {name:'新闻1',value:'1'},
    {name:'新闻2',value:'2'},
    {name:'新闻3',value:'3'},
    {name:'新闻4',value:'4'},
    {name:'新闻5',value:'5'},
    {name:'新闻6',value:'6'}
  ];
  
  public SelectedData = [1,3,'NewAdd'];//假资料范例
  public SelectedData2 = [1,6,4];//假资料范例

  ngOnInit(){
  
    //create form
    this.mainForm = new FormGroup({
      //tag Select 
      tag: new FormControl(this.SelectedData),  //带假资料 型别为阵列
      tag2: new FormControl(this.SelectedData2),//带假资料 型别为阵列
      //html编辑器
      html: new FormControl('<p>t<span style="color:red">htmlcf</span> Demo</p>'),
      htmltable: new FormControl(''),
      demoDate: new FormControl('2019/08/11'),
      img: new FormControl(),
      img2: new FormControl('assets/img/logo.png'),
      pdf: new FormControl('assets/empty.pdf'),
    });

  }

  public display = {};
  FormSubmit(){
    console.log('FormSubmit',this.mainForm.value);
    this.display = this.mainForm.value;
  }

  //DatePicker NgModel
  public ngModelDate = '2019/09/12';
  EditAlbum(){
    this.pop.setAlbum({
       data:[
         'http://localhost:12005/UploadFile/edit/81b3bf5909cc4d5ca8dd58faa68b396c.png',
         'http://localhost:12005/UploadFile/edit/ba65be61adde411bbd8a45f7bfa1c77c.png',
         'http://localhost:12005/UploadFile/edit/291e2cca726e42c68265a4ae8d793e2f.png'
       ],
       event:(list)=>{
         console.log('Album data',list);
       }
    });
  }
  MultiEdit(){
    this.pop.setMultiEdit({
      data:[
        {img:'http://localhost:12005/UploadFile/edit/81b3bf5909cc4d5ca8dd58faa68b396c.png',title:'11',desc:'11desc'},
        {img:'http://localhost:12005/UploadFile/edit/ba65be61adde411bbd8a45f7bfa1c77c.png',title:'22',desc:'22desc'},
        {img:'http://localhost:12005/UploadFile/edit/291e2cca726e42c68265a4ae8d793e2f.png',title:'33',desc:'33desc'}
      ],
      event:(list)=>{
        console.log('MultiEdit data',list);
      }
   });
  }
}
