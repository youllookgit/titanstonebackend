import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopupService } from '../../share/component/popup/popup.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Cinfig } from '../../../assets/config';
import { ApiService } from '../../share/service/api.service';

@Component({
  templateUrl: 'empty.component.html'
})
export class EmptyComponent implements OnInit {


  constructor(
    private router : Router,
    private pop : PopupService,
    private api : ApiService
  ){
  }

  ngOnInit(){ 
   
  }

}
