import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'contactUs.component.html'
})
export class ContactUsComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    country: '',
    subject: '',
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Name', name: '联络人姓名' },
    { key: 'Email', name: 'Email' },
    { key: 'CName', name: '国籍' },
    { key: 'TypeName', name: '联络主题' },
    { key: 'Status', name: '处理状态', HandleType:true},
    { key: 'CreateDate', name: '建立时间' },
    { key: 'ReplyTime', name: '回复时间' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 联络我们纪录资料阵列
  public list = [];
  // 联络我们纪录编辑物件
  public data;

  constructor() {
    super();
  }
  public countries;
  public contactList;
  public timeList;
  ngOnInit() {
    //取得國家列表
    this.api.apiPost('Option/GetByCode', { code:'Country' }).then(
      (res) => {
          this.countries = res;
          //取得說明會項目列表
          this.api.apiPost('Option/GetByCode', { code:'Contact' }).then(
            (res) => {
                this.contactList = res;
                //取得聯絡時段
                this.api.apiPost('Option/GetByCode', { code:'Time' }).then(
                  (res) => {
                      this.timeList = res;
                      // 初始化查询联络我们资料
                      this.getContactUsList();
                  }
                );
            }
          );
      }
    );
    
  }

  Submit() {
    // 执行查询作业
    this.getContactUsList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['country'] = "";
    this.query['subject'] = "";
    this.query['status'] = "";
    // 执行查询作业
    this.getContactUsList();
  }

  private getContactUsList() {
    // 查询联络我们资料
    this.api.apiPost('ContactUs/GetList', this.query).then(
      (res:any) => {
          //加入國別名稱
          res.forEach(item => {
            var d = this.countries.find(c=>{return c['OptionValue'] == item['Country']});
            item['CName'] = (d) ? d['Name'] : '';
          });
          //加入類別名稱
          res.forEach(item => {
            var ctype = this.contactList.find(c=>{return c['OptionValue'] == item['Subject']});
            item['TypeName'] = (ctype) ? ctype['Name'] : '';
          });
          //加入聯絡時段
          res.forEach(item => {
            var ttype = this.timeList.find(c=>{return c['OptionValue'] == item['ContactTime']});
            item['TimeName'] = (ttype) ? ttype['Name'] : '';
          });
          this.list = res;
      }
    );
  }

  Edit(item) {
    
    this.data = item;
    if(this.data['Subject'] == '1'){
      //取得建案選項名稱
      this.api.apiPost('EstateObj/GetNameByID', { id: this.data['SeletedItem'] }).then(
        (res) => {
          this.data['SeletedItem'] = res['Name'];
          // 建立表单栏位内容
          this.mainForm = new FormGroup({
            ID: new FormControl(this.data['ID']),
            Name: new FormControl({ value: this.data['Name'], disabled: true }),
            Email: new FormControl({ value: this.data['Email'], disabled: true }),
            Country: new FormControl({ value: this.data['CName'], disabled: true }),
            Phone: new FormControl({ value: this.data['Phone'], disabled: true }),
            SocialID: new FormControl({ value: this.data['SocialID'], disabled: true }),
            Subject: new FormControl({ value: this.data['TypeName'], disabled: true }),
            SeletedItem: new FormControl({ value: this.data['SeletedItem'], disabled: true }),
            ContactTime: new FormControl({ value: this.data['TimeName'], disabled: true }),
            Detail: new FormControl({ value: this.data['Detail'], disabled: true }),
            Reply: new FormControl({ value: this.data['Reply'], disabled: (this.data['Reply']) }),
            ReplyTime: new FormControl({ value: this.data['ReplyTime'], disabled: true }),
            Status: new FormControl(this.data['Status'], [Validators.required]),
            Handler: new FormControl(this.data['Handler']),
            Note: new FormControl(this.data['Note'])
          });
          this.isEditor = true;
        }
      );
    }else{
      // 建立表单栏位内容
      this.mainForm = new FormGroup({
        ID: new FormControl(this.data['ID']),
        Name: new FormControl({ value: this.data['Name'], disabled: true }),
        Email: new FormControl({ value: this.data['Email'], disabled: true }),
        Country: new FormControl({ value: this.data['CName'], disabled: true }),
        Phone: new FormControl({ value: this.data['Phone'], disabled: true }),
        SocialID: new FormControl({ value: this.data['SocialID'], disabled: true }),
        Subject: new FormControl({ value: this.data['TypeName'], disabled: true }),
        SeletedItem: new FormControl({ value: this.data['SeletedItem'], disabled: true }),
        ContactTime: new FormControl({ value: this.data['TimeName'], disabled: true }),
        Detail: new FormControl({ value: this.data['Detail'], disabled: true }),
        Reply: new FormControl({ value: this.data['Reply'], disabled: (this.data['Reply']) }),
        ReplyTime: new FormControl({ value: this.data['ReplyTime'], disabled: true }),
        Status: new FormControl(this.data['Status'], [Validators.required]),
        Handler: new FormControl(this.data['Handler']),
        Note: new FormControl(this.data['Note'])
      });
      this.isEditor = true;
    }
  }

  Delete() {
    // console.log('[E03] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔联络我们资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定联络我们资料
        this.api.apiPost('ContactUs/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询联络我们资料
            this.getContactUsList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E01][API] ContactUs/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    
    // 取得API参数物件
    let data = this.mainForm.value;
    if(data['Reply']){
      this.pop.setConfirm({
        content:'此回覆内容將會寄送回覆給該使用者，確認送出?',
        cancelTxt: '取消',
        event:()=>{
          // 修改联络我们资料
          this.api.apiPost('ContactUs/Modify', data).then(
            (res) => {
              // console.log('[E03][API] ContactUs/Modify success', res);
              this.pop.setConfirm({ content: '修改成功' });
              // 重新查询联络我们资料
              this.getContactUsList();
              // 返回列表页
              this.isEditor = false;
            }
          );
        }
      });

    }else{
        // 修改联络我们资料
        this.api.apiPost('ContactUs/Modify', data).then(
          (res) => {
            // console.log('[E03][API] ContactUs/Modify success', res);
            this.pop.setConfirm({ content: '修改成功' });
            // 重新查询联络我们资料
            this.getContactUsList();
            // 返回列表页
            this.isEditor = false;
          }
        );
    }
    
  }
  Export(){
    
    window.open( this.baseUrl + 'Export/contactUsCSV', '_blank');
  }
}
