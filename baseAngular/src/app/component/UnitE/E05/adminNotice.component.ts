import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'adminNotice.component.html'
})
export class AdminNoticeComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Email', name: 'Email' },
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 收信人资料阵列
  public list = [];
  // 收信人资料编辑物件
  public data;

  constructor() {
    super();
  }

  ngOnInit() {
    // console.log('[E05] ngOnInit');
    // 初始化查询收信人资料
    this.getAdminNoticeList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getAdminNoticeList(this.query);
  }

  Reset() {
    // 初始化查询条件值
    this.query['status'] = "";
    // 执行查询作业
    this.getAdminNoticeList();
  }

  private getAdminNoticeList(parameter?) {
    // console.log('[E05] getAdminNoticeList');
    // 查询收信人资料
    this.api.apiPost('AdminNotice/GetList', parameter).then(
      (res) => {
        // console.log('[E05][API] AdminNotice/GetList success', res);
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        // console.log('[E05][API] AdminNotice/GetList error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // console.log('[E05] initializeForm');
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      Email: new FormControl('', [Validators.required]),
      ContactUs: new FormControl(false, [Validators.required]),
      Activity: new FormControl(false, [Validators.required]),
      PushLog: new FormControl(false, [Validators.required]),
      Status: new FormControl(true, [Validators.required])
    });
  }

  Create() {
    // console.log('[E05] Create');
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      Email: { value: '', disabled: false },
      ContactUs: { value: false, disabled: false },
      Activity: { value: false, disabled: false },
      PushLog: { value: false, disabled: false },
      Status: true
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    // console.log('[E05] Edit item', item);
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      Email: { value: this.data['Email'], disabled: false },
      ContactUs: { value: this.data['ContactUs'], disabled: false },
      Activity: { value: this.data['Activity'], disabled: false },
      PushLog: { value: this.data['PushLog'], disabled: false },
      Status: this.data['Status']
    });
    
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // console.log('[E05] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔收信人资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定收信人资料
        this.api.apiPost('AdminNotice/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[E05][API] AdminNotice/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询收信人资料
            this.getAdminNoticeList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E01][API] AdminNotice/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // console.log('[E05] SubmitForm');
    // 取得API参数物件
    let data = this.mainForm.value;
    // console.log('[E05] api-data', data);

    if (data['ID']) {
      // 修改收信人资料
      this.api.apiPost('AdminNotice/Modify', data).then(
        (res) => {
          // console.log('[E05][API] AdminNotice/Modify success', res);
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询收信人资料
          this.getAdminNoticeList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[E05][API] AdminNotice/Modify error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增收信人资料
      this.api.apiPost('AdminNotice/Create', data).then(
        (res) => {
          // console.log('[E05][API] AdminNotice/Create success', res);
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询收信人资料
          this.getAdminNoticeList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[E05][API] AdminNotice/Create error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
    
  }
}
