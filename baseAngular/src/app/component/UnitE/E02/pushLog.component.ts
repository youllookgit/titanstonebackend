import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'pushLog.component.html'
})
export class PushLogComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isDetail = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    type: '',
    targettype:''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Name', name: '类型' },
    { key: 'Detail', name: '通知内容' },
    { key: 'Lang', name: '通知语系' },
    { key:'CreateDate', name:'发送时间'},
    {
      key: 'Detail',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Detail(item);
      }
    },

  ];
  // 通知纪录资料阵列
  public list = [];
  // 通知纪录明细物件
  public data;

  constructor() {
    super();
  }

  public notice;
  ngOnInit() {
    this.api.apiPost('Option/GetByCode',{code:'Noitce'}).then((res)=>{
      this.notice = res;
    });
    // console.log('[E02] ngOnInit');
    // 初始化查询通知纪录
    this.getPushLogList();
  }

  Submit() {
    // 执行查询作业
    this.getPushLogList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['type'] = "";
    this.query['targettype'] = "";
    // 执行查询作业
    this.getPushLogList();
  }

  private getPushLogList() {
    // console.log('[E02] getPushLogList');
    // 查询通知纪录
    this.api.apiPost('Push/GetList', this.query).then(
      (res) => {
        //console.log('[E02][API] Push/GetList success', res);
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        // console.log('[E02][API] Push/GetList error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  Detail(item) {
    // console.log('[E02] Detail item', item);
    this.data = item;
    // 建立表单栏位内容
    this.mainForm = new FormGroup({
      PType: new FormControl({ value: this.data['PType'], disabled: true }),
      Detail: new FormControl({ value: this.data['Detail'], disabled: true }),
      Lang: new FormControl({ value: this.data['Lang'], disabled: true }),
      TargetType: new FormControl({ value: this.data['TargetType'], disabled: true }),
      DeviceIDs: new FormControl({ value: this.data['DeviceIDs'], disabled: true }),
      CreateDate: new FormControl({ value: this.data['CreateDate'], disabled: true })
    });
    
    // 切换至明细页
    this.isDetail = true;

    // 查询指定纪录对应通知对象清单
    // this.api.apiPost('Push/GetTargets', { Id: this.data['ID'] }).then(
    //   (res) => {
    //     // console.log('[E02][API] Push/GetTargets success', res);
    //     if (Array.isArray(res) && res.length > 0) {
    //       // 组织装置代码显示值
    //       let devices = res.map(x => x['DeviceID']);
    //       this.data['DeviceIDs'] = devices.join(",");
    //       this.mainForm.get("DeviceIDs").setValue(this.data['DeviceIDs']);
    //     }
    //   },
    //   (err) => {
    //     // console.log('[E02][API] Push/GetTargets error', err);
    //     this.pop.setConfirm({ content: err['msg'] });
    //   }
    // );
  }

  Delete() {
    // console.log('[E02] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔通知纪录?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定通知纪录
        this.api.apiPost('Push/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[E02][API] Push/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询通知纪录
            this.getPushLogList();
            // 返回列表页
            this.isDetail = false;
          },
          (err) => {
            // console.log('[E01][API] Push/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isDetail = false;
  }
}
