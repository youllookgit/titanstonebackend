import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'seminar.component.html'
})
export class SeminarComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startPDate: '',
    endPDate: '',
    startSDate: '',
    endSDate: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Title', name: '活动标题',style:{width:'150px'} },
    { key: 'StartTime', name: '活动时间' },
    { key: 'Addr', name: '地址' },
    { key: 'Cname', name: '国籍' },
    { key: 'Email', name: 'Email' },
    { key: 'CreateDate', name: '建立时间' },
    {
      key: 'Detail',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 活动报名纪录资料阵列
  public list = [];
  // 活动报名纪录编辑物件
  public data;

  constructor() {
    super();
    
  }
  public countries;
  ngOnInit() {
    //取得國家列表
    this.api.apiPost('Option/GetByCode', { code:'Country' }).then(
      (res) => {
          this.countries = res;
          // 初始化查询活动报名资料
          this.getSeminarList();
      }
    );
  }

  Submit() {
    // 执行查询作业
    this.getSeminarList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startPDate'] = "";
    this.query['endPDate'] = "";
    this.query['startSDate'] = "";
    this.query['endSDate'] = "";
    // 执行查询作业
    this.getSeminarList();
  }

  private getSeminarList() {
    // console.log('[E04] getSeminarList');
    // 查询活动报名资料
    this.api.apiPost('Seminar/GetList', this.query).then(
      (res:any) => {
        console.log('GetList',res);
        res.forEach(item => {
          var cItem = this.countries.find(c=>{return c['OptionValue'] == item['Country']});
          item['Cname'] = (cItem) ? cItem['Name'] : '';
        });
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        // console.log('[E04][API] Seminar/GetList error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  Edit(item) {
    // console.log('[E04] Edit item', item);
    this.data = item;
    // 建立表单栏位内容
    this.mainForm = new FormGroup({
      ID: new FormControl(this.data['ID']),
      Title: new FormControl({ value: this.data['Title'], disabled: true }),
      StartTime: new FormControl({ value: this.data['StartTime'], disabled: true }),
      Addr: new FormControl({ value: this.data['Addr'], disabled: true }),
      ContactName: new FormControl({ value: this.data['ContactName'], disabled: true }),
      Country: new FormControl({ value: this.data['Cname'], disabled: true }),
      Phone: new FormControl({ value: this.data['Phone'], disabled: true }),
      Email: new FormControl({ value: this.data['Email'], disabled: true }),
      Count: new FormControl({ value: this.data['Count'], disabled: true }),
      Detail: new FormControl({ value: this.data['Detail'], disabled: true }),
      Handler: new FormControl(this.data['Handler']),
      SendDate: new FormControl({ value: this.data['CreateDate'], disabled: true }),
      Note: new FormControl(this.data['Note'])
    });
    
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // console.log('[E04] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔活动报名资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定活动报名资料
        this.api.apiPost('Seminar/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[E04][API] Seminar/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询活动报名资料
            this.getSeminarList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E04][API] Seminar/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // console.log('[E04] SubmitForm');
    // 取得API参数物件
    let data = this.mainForm.value;
    // console.log('[E04] api-data', data);

    // 修改活动报名资料
    this.api.apiPost('Seminar/Modify', data).then(
      (res) => {
        // console.log('[E04][API] Seminar/Modify success', res);
        this.pop.setConfirm({ content: '修改成功' });
        // 重新查询活动报名资料
        this.getSeminarList();
        // 返回列表页
        this.isEditor = false;
      },
      (err) => {
        // console.log('[E04][API] Seminar/Modify error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  Export(){
    
    window.open( this.baseUrl + 'Export/seminarCSV', '_blank');
  }
}
