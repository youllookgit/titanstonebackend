import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
   templateUrl: 'push.component.html'
})
export class PushComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  constructor() {
    super();
  }
  public notice;
  public HideAll = false;
  ngOnInit() {
    this.api.apiPost('Option/GetByCode',{code:'Noitce'}).then((res)=>{
      this.notice = res;
    });
    this.initializeForm();
      // 设定表单栏位特殊连动性验证
      this.mainForm.get("TargetType").valueChanges.subscribe((value) => {
        if (value == "3"){
          this.mainForm.get("DeviceIDs").setValidators([Validators.required]);
        }
        else{
          this.mainForm.get("DeviceIDs").setValidators(null);
          this.mainForm.get("DeviceIDs").setValue('');
        }
        this.mainForm.get("DeviceIDs").updateValueAndValidity();
      });

      this.mainForm.get("Lang").valueChanges.subscribe((value) => {
        if (value == "zh_CN"){
          this.mainForm.get("Detail").setValidators([Validators.required]);
          this.mainForm.get("DetailEn").setValidators(null);
          this.mainForm.get("DetailEn").setValue('');
        }else{
          this.mainForm.get("DetailEn").setValidators([Validators.required]);
          this.mainForm.get("Detail").setValidators(null);
          this.mainForm.get("Detail").setValue('');
        }
        this.mainForm.get("Detail").updateValueAndValidity();
        this.mainForm.get("DetailEn").updateValueAndValidity();
      });

      this.mainForm.get("PType").valueChanges.subscribe((value) => {
        if(value == '1' || value == '2'){
          this.HideAll = true;
        }else{
          this.HideAll = false;
        }
      });
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    console.log('SubmitForm',data)
    if(data.Lang == 'en_US'){
      data.Detail = data.DetailEn;
    }
    //delete data.DetailEn;

    // 新增推播通知
    this.api.apiPost('Push/Create', data).then(
      (res) => {
        this.pop.setConfirm({ content: '通知发送成功' });
        this.initializeForm();
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }
  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      PType: new FormControl('', [Validators.required]),
      Lang: new FormControl('zh_CN', [Validators.required]),
      Detail: new FormControl('', [Validators.required]),
      DetailEn: new FormControl(''),
      TargetType: new FormControl(null, [Validators.required]),
      DeviceIDs: new FormControl('')
    });
  }
}
