import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare var readxlsx;
@Component({
  templateUrl: 'insuranceSale.component.html'
})
export class InsSaleComponent extends BaseComponent implements OnInit {

  // TODO: 累積申購金額、篩選
  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    Status: '',
    validStart: '',
    validEnd: '',
    startDate: '',
    endDate: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'InsNo', name: '保单号码' },
    { key: 'UserIdentityNo', name: '要保人ID' },
    { key: 'ChannelName', name: '所属渠道',style:{width:'4rem;'} },

    { key: 'BuyerName', name: '要保人' },
    { key: 'BenefitName', name: '被保险人' },
    { key: 'InsName', name: '保险名称' },
    { key: 'StatusName', name: '保单状态' },
    { key: 'ValidDate', name: '保单生效日' },
    { key: 'ValidRange', name: '保障期间' },
    { key: 'InsFee', name: '保费' },
    { key: 'CreateDate', name: '建立时间' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 保險銷售资料阵列
  public list = [];
  // 保險銷售资料编辑物件
  public data;

  public fundsList = [];
  // 申購方式選項
  public buyTypeList = {
    oneTime: false,
    periodic: false
  };

  public Options = {
    channelList : [], //所屬渠道选单
    Insurance: [],     //保險商品選單
    InsStatus: [],   //保單狀態
    PayType: []   //保單繳款類別
  };
  constructor() {
    super();

    //取得渠道選項
    this.Options.channelList = [];
    this.api.apiPost('Channel/GetChannel', {}).then(
      (chres: any) => {
        chres.forEach(citem => {
          var cObj = { name: (citem['Name'] + '(' + citem['Act'] + ')'), value: citem['ID'] };
          this.Options.channelList.push(cObj);
        });
      }
    );

    //保險商品選單
    this.api.apiPost('Insurance/GetList', {}).then(
      (listRes: any) => {
        this.Options.Insurance = listRes;
            //取得保單狀態選項
            this.api.apiPost('Option/GetByCode', { code:'InsuranceValid' }).then(
              (statusRes: any) => {
                this.Options.InsStatus = statusRes;
                 // 初始化查询保險銷售资料
                 this.getSaleList();
              }
            );
      }
    );

    //取得保單繳款類別選項
    this.api.apiPost('Option/GetByCode', { code:'InsurancePay' }).then(
      (TypeRes: any) => {
        this.Options.PayType = TypeRes;
      }
    );
  }

  ngOnInit() {
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getSaleList();
  }

  Reset() {
    // 初始化查询条件值
    this.query = {
      keyword: '',
      Status: '',
      validStart: '',
      validEnd: '',
      startDate: '',
      endDate: ''
    };
    // 执行查询作业
    this.getSaleList();
  }

  private getSaleList() {
    // 查询保險銷售资料
    this.api.apiPost('insuranceSale/GetList', this.query).then(
      (res:any) => {
        this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      InsID: new FormControl(null, [Validators.required]),
      InsSortName: new FormControl(),
      InsNo: new FormControl(null, [Validators.required]),
      UserIdentityNo: new FormControl(null, [Validators.required]),
      Channel: new FormControl(),
      BuyerName: new FormControl(null, [Validators.required]),
      BenefitName: new FormControl(null, [Validators.required]),

      SaleName: new FormControl(null),
      SalePhone: new FormControl(null),
      InsStatus: new FormControl(null, [Validators.required]),
      ValidDate: new FormControl(null),
      ValidStart: new FormControl(null),
      ValidEnd: new FormControl(null),
      PayPeriod: new FormControl(null, [Validators.required]),
      PayType: new FormControl(null, [Validators.required]),
      Currency: new FormControl(null, [Validators.required]),

      CurrencyEn: new FormControl(null, [Validators.required]),
      Amount: new FormControl(null, [Validators.required]),
      InsAmount: new FormControl(null, [Validators.required]),
      InsContent: new FormControl(null),
      InsContentEn: new FormControl(null),
      InsNote: new FormControl(null),
      InsNoteEn: new FormControl(null),

      Recommend: new FormControl(null),
      DealFile: new FormControl(null),

      Note: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true }),
    });

    // 设定表单栏位特殊连动性关系
    this.mainForm.get("InsID").valueChanges.subscribe((value) => {
      var insItem = this.Options.Insurance.find(ins => {return ins.ID == Number(value)});
      if(insItem){
        this.mainForm.get("InsSortName").setValue(insItem['TypeName']);
      }else{
        this.mainForm.get("InsSortName").setValue('');
      }
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      InsNo: { value: '', disabled: false },
      InsID:'',
      InsStatus:'',
      PayType : '',
      Recommend:'',
      DealFile : '',
      InsContent: '',
      InsContentEn:''
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    console.log('edit',item);
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      InsID: this.data['InsID'],
      InsSortName: this.data['InsSortName'],
      InsNo: { value: this.data['InsNo'], disabled: true },
      UserIdentityNo: this.data['UserIdentityNo'],
      Channel: this.data['Channel'],
      BuyerName: this.data['BuyerName'],
      BenefitName: this.data['BenefitName'],
      SaleName: this.data['SaleName'],
      SalePhone: this.data['SalePhone'],
      InsStatus: this.data['InsStatus'],
      ValidDate: this.data['ValidDate'],
      ValidStart: this.data['ValidStart'],
      ValidEnd: this.data['ValidEnd'],
      PayPeriod: this.data['PayPeriod'],
      PayType: this.data['PayType'],
      Currency: this.data['Currency'],
      CurrencyEn: this.data['CurrencyEn'],
      Amount: this.data['Amount'],
      InsAmount: this.data['InsAmount'],
      InsContent: this.data['InsContent'],
      InsContentEn: this.data['InsContentEn'],
      InsNote: this.data['InsNote'],
      InsNoteEn: this.data['InsNoteEn'],
      Recommend: this.data['Recommend'],
      DealFile: this.data['DealFile'],
      Note: this.data['Note'],
      CreateDate: this.data['CreateDate']
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔保險銷售资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定保險銷售资料
        this.api.apiPost('insuranceSale/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询保險銷售资料
            this.getSaleList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    if (data['Channel'] && typeof data['Channel'] == 'object')
      data['Channel'] = data['Channel'].join(',');

    if (data['ID']) {
      // 修改保險銷售资料
      this.api.apiPost('insuranceSale/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询保險銷售资料
          this.getSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增保險銷售资料
      this.api.apiPost('insuranceSale/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询保險銷售资料
          this.getSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }
  SetProc(param){
    console.log('param:',param,this.mainForm.value[param]);
    this.pop.setMultiEdit({
      header:['项目','说明'],
      data: this.mainForm.value[param],
      icon:false,
      event: (list) => {
        console.log('setMultiEdit',list);
        if(typeof list === 'object'){
          var _data = JSON.stringify(list);
          this.mainForm.get(param).setValue(_data
          );
        }else{
          this.mainForm.get(param).setValue(list);
        }
      }
    });
  }

  Export(){
    window.open( this.baseUrl + 'Export/insuranceSaleCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    console.log('ImportHandle',source);
    this.api.apiPost('insuranceSale/Import',{data:source}).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({content:'汇入成功'});
      this.getSaleList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/InsuranceSample.xlsx', '_blank');
  }
}

