import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'emaillog.component.html'
})
export class EmaillogComponent extends BaseComponent implements OnInit {

  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    type: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'CreateDate', name: '建立时间' },
    { key: 'TypeCode', name: '验证信类型' },
    { key: 'Email', name: 'Email' },
    { key: 'Status', name: '验证状态' },
    { key: 'CheckDate', name: '验证时间' },
    // {
    //   key: 'Delete',
    //   name: '',
    //   icon:'fa-pencil-square-o',
    //   event: (item) => {
    //     this.Delete(item);
    //   }
    // }
  ];
  // 会员验证信资料阵列
  public list = [];
  // 验证信类型選項陣列
  public emailTypes;

  constructor(
  ) {
    super();
  }

  ngOnInit() {
    // 取得選項
    this.api.apiPost('Option/GetByCode', { code:'EmailType' }).then(
      (res) => {
        this.emailTypes = res;   
      }
    );
    // 初始化查询会员验证信资料
    this.getEmailLogList();
  }

  Submit() {
    // 执行查询作业
    this.getEmailLogList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['type'] = "";
    // 执行查询作业
    this.getEmailLogList();
  }

  private getEmailLogList() {
    // 查询会员验证信资料
    this.api.apiPost('EmailLog/GetEmailLog', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
        } 
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  // Delete(data) {
  //   // 提示确认删除
  //   this.pop.setConfirm({
  //     content:'确定删除此笔会员验证信资料?',
  //     cancelTxt: '取消', 
  //     event: () => {
  //       // 删除指定会员验证信资料
  //       this.api.apiPost('EmailLog/Delete', { Id: data['ID'] }).then(
  //         (res) => {
  //           this.pop.setConfirm({ content: '删除成功' });
  //           // 重新查询会员验证信资料
  //           this.getEmailLogList();
  //         },
  //         (err) => {
  //           this.pop.setConfirm({ content: err['msg'] });
  //         }
  //       );
  //     }
  //   });
  // }
}
