import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";
declare var readxlsx;
@Component({
  templateUrl: 'fundsDetail.component.html'
})
export class FundsDetailComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    txnType: '',
    txnStartDate: '',
    txnEndDate: '',
    redemptionMin: null,
    redemptionMax: null,
    purchaseMin: null,
    purchaseMax: null,
    FundsSaleID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'dTxnType', name: '交易类型' },
    { key: 'TxnNo', name: '交易单号' },
    { key: 'TxnDate', name: '交易日期' },
    { key: 'Unit', name: '单位数' },
    { key: 'RedemptionVal', name: '赎回淨值' },
    { key: 'PurchaseVal', name: '申购淨值' },
    { key: 'CreateDate', name: '建立时间' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 基金交易明細资料阵列
  public list = [];
  // 基金交易明細资料编辑物件
  public data;
  // 基金主档物件
  public tempFundsSale;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempFundsSale');
    if (tempData) {
      this.query.FundsSaleID = tempData['ID'];
      this.tempFundsSale = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/B09']);
    }
  }

  ngOnInit() {
    // 初始化查询基金交易明細资料
    this.getFundsDetailList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsDetailList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = '';
    this.query['startDate'] = '';
    this.query['endDate'] = '';
    this.query['txnType'] = '';
    this.query['txnStartDate'] = '';
    this.query['txnEndDate'] = '';
    this.query['redemptionMin'] = null;
    this.query['redemptionMax'] = null;
    this.query['purchaseMin'] = null;
    this.query['purchaseMax'] = null;
    // 执行查询作业
    this.getFundsDetailList();
  }

  private getFundsDetailList() {
    // 查询基金交易明細资料
    this.api.apiPost('FundsDetail/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      FundsSaleID: new FormControl(this.query['FundsSaleID']),
      TxnType: new FormControl(null, [Validators.required]),
      TxnNo: new FormControl(),
      TxnDate: new FormControl(null, [Validators.required]),
      Unit: new FormControl(),
      RedemptionVal: new FormControl(),
      RedemptionAmt: new FormControl(),
      PrefFee: new FormControl(),
      PurchaseVal: new FormControl(),
      PurchaseAmt: new FormControl(),
      HandlingFee: new FormControl(),
      DividendAmt: new FormControl(),
      PayStatus: new FormControl(),
      Note: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
    // 设定表单栏位特殊连动性关系
    this.mainForm.get("TxnType").valueChanges.subscribe((value) => {
      console.log('[B10] TxnType changed value =', value);
      // 清除欄位資料
      this.mainForm.get("TxnNo").reset("");
      this.mainForm.get("Unit").reset(null);
      this.mainForm.get("PurchaseVal").reset(null);
      this.mainForm.get("PurchaseAmt").reset(null);
      this.mainForm.get("RedemptionVal").reset(null);
      this.mainForm.get("RedemptionAmt").reset(null);
      this.mainForm.get("DividendAmt").reset(null);
      this.mainForm.get("PayStatus").reset("");
      // 申購
      if (value == "1") {
        this.mainForm.get("TxnNo").setValidators([Validators.required]);
        this.mainForm.get("Unit").setValidators([Validators.required]);
        this.mainForm.get("PayStatus").setValidators([Validators.required]);
        this.mainForm.get("PurchaseVal").setValidators([Validators.required]);
        this.mainForm.get("PurchaseAmt").setValidators([Validators.required]);
        this.mainForm.get("RedemptionVal").setValidators(null);
        this.mainForm.get("RedemptionAmt").setValidators(null);
        this.mainForm.get("DividendAmt").setValidators(null);
      }
      // 贖回
      else if (value == "2") {
        this.mainForm.get("TxnNo").setValidators([Validators.required]);
        this.mainForm.get("Unit").setValidators([Validators.required]);
        this.mainForm.get("PayStatus").setValidators(null);
        this.mainForm.get("PurchaseVal").setValidators(null);
        this.mainForm.get("PurchaseAmt").setValidators(null);
        this.mainForm.get("RedemptionVal").setValidators([Validators.required]);
        this.mainForm.get("RedemptionAmt").setValidators([Validators.required]);
        this.mainForm.get("DividendAmt").setValidators(null);
      }
      // 分紅
      else if (value == "3") {
        this.mainForm.get("TxnNo").setValidators(null);
        this.mainForm.get("Unit").setValidators(null);
        this.mainForm.get("PayStatus").setValidators(null);
        this.mainForm.get("PurchaseVal").setValidators(null);
        this.mainForm.get("PurchaseAmt").setValidators(null);
        this.mainForm.get("RedemptionVal").setValidators(null);
        this.mainForm.get("RedemptionAmt").setValidators(null);
        this.mainForm.get("DividendAmt").setValidators([Validators.required]);
      }
      this.mainForm.get("TxnNo").updateValueAndValidity();
      this.mainForm.get("PayStatus").updateValueAndValidity();
      this.mainForm.get("PurchaseVal").updateValueAndValidity();
      this.mainForm.get("PurchaseAmt").updateValueAndValidity();
      this.mainForm.get("RedemptionVal").updateValueAndValidity();
      this.mainForm.get("RedemptionAmt").updateValueAndValidity();
      this.mainForm.get("DividendAmt").updateValueAndValidity();
      this.mainForm.get("Unit").updateValueAndValidity();
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      FundsSaleID: this.query['FundsSaleID'],
      TxnType: { value: '', disabled: false },
      TxnNo: { value: '', disabled: false },
      TxnDate: '',
      PayStatus: ''
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      FundsSaleID: this.query['FundsSaleID'],
      TxnType: { value: this.data['TxnType'], disabled: true },
      TxnNo: { value: this.data['TxnNo'], disabled: true },
      TxnDate: this.data['TxnDate'],
      Unit: this.data['Unit'],
      RedemptionVal: this.data['RedemptionVal'],
      RedemptionAmt: this.data['RedemptionAmt'],
      PrefFee: this.data['PrefFee'],
      PurchaseVal: this.data['PurchaseVal'],
      PurchaseAmt: this.data['PurchaseAmt'],
      HandlingFee: this.data['HandlingFee'],
      DividendAmt: this.data['DividendAmt'],
      PayStatus: this.data['PayStatus'],
      Note: this.data['Note'],
      CreateDate: this.data['CreateDate']
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔基金交易明細资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定基金交易明細资料
        this.api.apiPost('FundsDetail/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询基金交易明細资料
            this.getFundsDetailList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改基金交易明細资料
      this.api.apiPost('FundsDetail/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询基金交易明細度资料
          this.getFundsDetailList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增基金交易明細资料
      this.api.apiPost('FundsDetail/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询基金交易明細资料
          this.getFundsDetailList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }

  }

  ReturnB09(){
    this.router.navigate(['/B09']);
    this.jslib.RemoveStorage('tempFundsSale');
  }

  Export(){
    window.open( this.baseUrl + 'Export/FundsDetailCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    var _saleid = this.query.FundsSaleID;
    source.forEach(item => {
      item['FundsSaleID'] = _saleid;
    });
    console.log('ImportHandle',source);
    this.api.apiPost('FundsDetail/Import',{data:source}).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({content:'汇入成功'});
      this.getFundsDetailList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/FundDealSample.xlsx', '_blank');
  }
}
