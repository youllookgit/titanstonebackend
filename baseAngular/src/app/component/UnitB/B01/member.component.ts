import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'member.component.html'
})
export class MemberComponent extends BaseComponent implements OnInit {
  
  public mainForm: FormGroup;
  public submitLimit = "";

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    country: '',
    channel: '',
    valid: '',
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Act', name: '帐号' },
    { key: 'Name', name: '姓名' },
    { key: 'IdentityNo', name: '身分证号码' },
    { key: 'CName', name: '国籍' },
    { 
      key: '', 
      name: '投资意向',
      icon:'fa-pencil-square-o',
      style:{'text-align':'center'},
      event: (item) => {
        this.gotoB03(item);
      }
    },
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 会员资料阵列
  public list = [];
  // 会员资料编辑物件
  public data;

  public countries;
  public ChannelList = [];

  constructor(
  ) {
    super();
    // 取得選項
    this.api.apiPost('Option/GetByCode', { code:'Country' }).then(
      (res) => {
          this.countries = res;   
      }
    );
    //取得渠道選項
    this.api.apiPost('Channel/GetChannel',{}).then(
      (res:any)=>{
          console.log('GetChannel',res);
          res.forEach(citem => {
            this.ChannelList.push(
              {name:(citem['Name'] + '(' + citem['Act'] + ')'),value:citem['ID']}
            );
          });

          // 初始化查询会员资料
          this.getUserList();
          // 初始化表单物件
          this.initializeForm();

      },(error)=>{
           console.log('[Fail]',error);
      });
  }

  ngOnInit() {
    
  }

  Submit() {
    // 执行查询作业
    this.getUserList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['country'] = "";
    this.query['channel'] = "";
    this.query['valid'] = "";
    this.query['status'] = "";
    // 执行查询作业
    this.getUserList();
  }

  private getUserList() {
    // 查询会员资料
    this.api.apiPost('User/GetUsers', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
        } 
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      DeviceId: new FormControl({ value: null, disabled: true }),
      Act: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      Pwd: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(16),
        Validators.pattern('^[a-zA-Z0-9]*$')
      ]),
      Valid: new FormControl(false, [Validators.required]),
      Name: new FormControl(null, [Validators.required]),
      IdentityNo: new FormControl(null),
      Birth: new FormControl(),
      Country: new FormControl('', [Validators.required]),
      PhonePrefix: new FormControl(),
      Phone: new FormControl(),
      ImageUrl: new FormControl(),
      Channel: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true }),
      Status: new FormControl(true, [Validators.required])
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      Act: '',
      Pwd: '',
      Name: '',
      Valid: false,
      Country: '',
      Status: true
    });
    this.mainForm.get("Pwd").setValidators([
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
      Validators.pattern('^[a-zA-Z0-9]*$')
    ]);
    this.mainForm.get("Pwd").updateValueAndValidity();

    // 設定表單發送權限
    this.submitLimit = "add";
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    this.mainForm.reset({
      ID: this.data['ID'],
      DeviceId: this.data['DeviceID'],
      Act: this.data['Act'],
      Pwd: '',
      Valid: this.data['Valid'],
      Name: this.data['Name'],
      IdentityNo: this.data['IdentityNo'],
      Birth: this.data['Birth'],
      Country: this.data['Country'],
      PhonePrefix: this.data['PhonePrefix'],
      Phone: this.data['Phone'],
      ImageUrl: this.data['ImageUrl'],
      Channel: this.data['Channel'],
      CreateDate: this.data['CreateDate'],
      Status: this.data['Status']
    });
    this.mainForm.get("Pwd").setValidators([
      Validators.minLength(6),
      Validators.maxLength(16),
      Validators.pattern('^[a-zA-Z0-9]*$')
    ]);
    this.mainForm.get("Pwd").updateValueAndValidity();
    
    // 設定表單發送權限
    this.submitLimit = "edit";
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔会员资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定会员资料
        this.api.apiPost('User/Remove', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询会员资料
            this.getUserList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    if(data['Channel'] && typeof data['Channel'] == 'object'){
      data['Channel'] = data['Channel'].join(',');
    }
    if (data['ID']) {
      // 修改会员资料
      this.api.apiPost('User/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询会员资料
          this.getUserList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增会员资料
      this.api.apiPost('User/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询会员资料
          this.getUserList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoB03(item) {
    // 开启投资意向功能
    this.router.navigate(['/B03']);
    //加入暂存
    this.jslib.SetStorage('tempUser', item);
  }
  Export(){
    window.open( this.baseUrl + 'Export/MemberCSV', '_blank');
  }
  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {
        if (Fileinput.files != null && Fileinput.files[0] != null) {
          //验大小
          console.log('Imageinput.files',Fileinput.files[0]);
          this.api.apiPostCSV('User/MemberImport',Fileinput.files[0]).then((url)=>{
            console.log('Upload res',url);
            this.pop.setConfirm({content:'汇入成功',event:()=>{
              this.Reset();
             }});
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }
  Sample(){
    window.open( window.location.origin + '/Content/MemberSample.xlsx', '_blank');
  }
}
