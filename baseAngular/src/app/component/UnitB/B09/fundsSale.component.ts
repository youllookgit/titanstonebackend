import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare var readxlsx;
@Component({
  templateUrl: 'fundsSale.component.html'
})
export class FundsSaleComponent extends BaseComponent implements OnInit {

  // TODO: 累積申購金額、篩選

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    fundType: '',
    buyType: '',
    startDate: '',
    endDate: '',
    sum:'',
    sumlitmit:''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'FundsNo', name: '基金单号' },
    { key: 'UserIdentityNo', name: '购买人ID' },
    { key: 'BuyerName', name: '购买人姓名' },
    
    { key: 'ChannelName', name: '所属渠道',style:{width:'4rem;'} },
    
    { key: 'FundsName', name: '基金名稱' },
    { key: 'dFundType', name: '基金類型' },
    { key: 'dBuyType', name: '申購方式' },
    { key: 'PurchaseTotal', name: '累積申購金額' },
    {
      key: '',
      name: '基金交易明細',
      icon: 'fa-bar-chart',
      style: { 'text-align': 'center' },
      event: (item) => {
        this.gotoB10(item);
      }
    },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 基金銷售资料阵列
  public list = [];
  // 基金銷售资料编辑物件
  public data;
  // 所屬渠道选单
  public channelList = [];
  // 基金商品選單
  public fundsList = [];
  // 申購方式選項
  public buyTypeList = {
    oneTime: false,
    periodic: false
  };

  constructor() {
    super();
    this.api.apiPost('Channel/GetChannel', {}).then(
      (chres: any) => {
        chres.forEach(citem => {
          var cObj = { name: (citem['Name'] + '(' + citem['Act'] + ')'), value: citem['ID'] };
          this.channelList.push(cObj);
        });
      },
      (error) => {
          console.log('[Fail]',error);
      }
    );
    this.api.apiPost('Funds/GetList', {}).then(
      (res) => {
        if (Array.isArray(res)) {
          this.fundsList = res;
        }
      },
      (err) => {
        this.pop.setConfirm({ content: '无法取得基金名稱选单资料' });
      }
    );
  }

  ngOnInit() {
    // 初始化查询基金銷售资料
    this.getFundsSaleList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsSaleList();
  }

  Reset() {
    // 初始化查询条件值
    this.query = {
      keyword: '',
      fundType: '',
      buyType: '',
      startDate: '',
      endDate: '',
      sum:'',
      sumlitmit:''
    };
    // 执行查询作业
    this.getFundsSaleList();
  }

  private getFundsSaleList() {
    // 查询基金銷售资料
    this.api.apiPost('FundsSale/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
        }
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      FundsNo: new FormControl(null, [Validators.required]),
      UserIdentityNo: new FormControl(null, [Validators.required]),
      Channel: new FormControl(),
      BuyerName: new FormControl(null, [Validators.required]),
      FundsID: new FormControl(null, [Validators.required]),
      FundsName: new FormControl(),
      FundType: new FormControl(),
      BuyType: new FormControl(null, [Validators.required]),
      Note: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true }),
      PurchaseTotal: new FormControl({ value: null, disabled: true })
    });
    // 设定表单栏位特殊连动性关系
    this.mainForm.get("FundsID").valueChanges.subscribe((value) => {
      console.log('[B09] FundsID changed value =', value);
      // 取得指定基金商品資料
      let funds = this.fundsList.find(item => item.ID == value);
      if (funds) {
        this.mainForm.get("FundsName").setValue(funds['Name']);
        this.mainForm.get("FundType").setValue(funds['FundType']);
        // 申購方式選項
        this.buyTypeList['oneTime'] = funds['BuyType'].includes("1") ? true : false;
        this.buyTypeList['periodic'] = funds['BuyType'].includes("2") ? true : false;
        this.mainForm.get("BuyType").setValue("");
      }
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      FundsNo: { value: '', disabled: false },
      Channel: '',
      FundsID: { value: '', disabled: false },
      FundsName: { value: '', disabled: false },
      FundType: { value: '', disabled: false },
      BuyType: ''
    });

    // 取得基金商品选单内容
    //this.getFundsList();

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      FundsNo: { value: this.data['FundsNo'], disabled: true },
      UserIdentityNo: this.data['UserIdentityNo'],
      Channel: this.data['Channel'],
      BuyerName: this.data['BuyerName'],
      FundsID: { value: this.data['FundsID'], disabled: true },
      FundsName: { value: this.data['FundsName'], disabled: true },
      FundType: { value: this.data['FundType'], disabled: true },
      BuyType: this.data['BuyType'],
      Note: this.data['Note'],
      CreateDate: this.data['CreateDate'],
      PurchaseTotal: this.data['PurchaseTotal']
    });

    // 取得基金商品选单内容
    //this.getFundsList();

    // 切换至编辑页
    this.isEditor = true;
  }

  // private getFundsList() {
  //   // 查询基金商品资料
  //   this.api.apiPost('Funds/GetList', {}).then(
  //     (res) => {
  //       if (Array.isArray(res)) {
  //         this.fundsList = res;
  //       }
  //     },
  //     (err) => {
  //       this.pop.setConfirm({ content: '无法取得基金名稱选单资料' });
  //     }
  //   );
  // }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔基金銷售资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定基金銷售资料
        this.api.apiPost('FundsSale/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询基金銷售资料
            this.getFundsSaleList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    if (data['Channel'] && typeof data['Channel'] == 'object')
      data['Channel'] = data['Channel'].join(',');

    if (data['ID']) {
      // 修改基金銷售资料
      this.api.apiPost('FundsSale/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询基金銷售资料
          this.getFundsSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增基金銷售资料
      this.api.apiPost('FundsSale/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询基金銷售资料
          this.getFundsSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoB10(item) {
    // 开启基金交易明細管理功能
    this.router.navigate(['/B10']);
    // 加入暂存
    this.jslib.SetStorage('tempFundsSale', item);
  }

  Export(){
    window.open( this.baseUrl + 'Export/FundsSaleCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    console.log('ImportHandle',source);
    this.api.apiPost('FundsSale/Import',{data:source}).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({content:'汇入成功'});
      this.getFundsSaleList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/FundSample.xlsx', '_blank');
  }
}

