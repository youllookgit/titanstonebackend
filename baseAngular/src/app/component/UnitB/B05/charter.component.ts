import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";
declare var readxlsx;
@Component({
  templateUrl: 'charter.component.html'
})
export class CharterComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    startDate: '',
    endDate: '',
    type: '',
    EstateID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'dManagerType', name: '代管類型' },
    { key: 'dCharterDate', name: '合約期間' },
    {
      key: '',
      name: '租賃管理',
      icon: 'fa-bar-chart',
      style: {'text-align':'center'},
      event: (item) => {
        this.gotoB08(item);
      }
    },
    {
      key: 'Edit',
      name: '',
      icon: 'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 代管類型選項陣列
  public charterType = [];
  // 代管资料阵列
  public list = [];
  // 代管资料编辑物件
  public data;
  // 地產銷售物件
  public tempEstateSale;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempEstateSale');
    if (tempData) {
      this.query.EstateID = tempData['ID'];
      this.tempEstateSale = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/B04']);
    }
  }

  ngOnInit() {
    // 取得代管類型選項
    this.api.apiPost('Option/GetByCode', { code:'CharterType' }).then(
      (res) => {
        if (Array.isArray(res))
          this.charterType = res;
      }
    );
    // 初始化查询代管资料
    this.getCharterList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getCharterList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['type'] = "";
    // 执行查询作业
    this.getCharterList();
  }

  private getCharterList() {
    // 查询代管资料
    this.api.apiPost('Charter/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateID: new FormControl(this.query['EstateID']),
      ManagerType: new FormControl('', [Validators.required]),
      StartDate: new FormControl('', [Validators.required]),
      EndDate: new FormControl('', [Validators.required]),
      ManageDeal: new FormControl('', [Validators.required]),
      CharterAmount: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateID: this.query['EstateID'],
      ManagerType: '',
      StartDate: '',
      EndDate: '',
      ManageDeal: '',
      CreateDate: null
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateID: this.data['EstateID'],
      ManagerType: this.data['ManagerType'],
      StartDate: this.data['StartDate'],
      EndDate: this.data['EndDate'],
      ManageDeal: this.data['ManageDeal'],
      CharterAmount: this.data['CharterAmount'],
      CreateDate: this.data['CreateDate']
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔代管资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定代管資料
        this.api.apiPost('Charter/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询代管资料
            this.getCharterList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改代管资料
      this.api.apiPost('Charter/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询代管度资料
          this.getCharterList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增代管资料
      this.api.apiPost('Charter/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询代管资料
          this.getCharterList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  ReturnB04() {
    this.router.navigate(['/B04']);
    this.jslib.RemoveStorage('tempEstateSale');
  }

  gotoB08(item) {
    // 开启租賃管理功能
    this.router.navigate(['/B08']);
    //加入暂存
    this.jslib.SetStorage('tempCharterData', item);
  }

  Export(){
    window.open( this.baseUrl + 'Export/charterCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    var param = {
      EstateSaleID : this.query['EstateID'],
      data:source
    };
    this.api.apiPost('Charter/Import',param).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({ content: '汇入成功' });
      this.getCharterList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/charterSample.xlsx', '_blank');
  }
}
