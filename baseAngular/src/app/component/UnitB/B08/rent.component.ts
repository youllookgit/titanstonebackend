import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'rent.component.html'
})
export class RentComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    startDate: '',
    endDate: '',
    EstateSaleID: '',
    CharterDataID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'dRentDate', name: '租賃期間' },
    { key: 'RenterName', name: '租客名稱' },
    { key: 'RentAmount', name: '租赁金额' },
    // { key: 'dRentStatus', name: '租賃狀態' },
    {
      key: 'Edit',
      name: '',
      icon: 'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 租賃狀態選項陣列
  public rentStatus = [];
  // 租賃资料阵列
  public list = [];
  // 租賃资料编辑物件
  public data;
  // 地產銷售物件
  public tempEstateSale;
  // 代管資料物件
  public tempCharterData;

  constructor() {
    super();
    // 取得暂存
    var tempDataES = this.jslib.GetStorage('tempEstateSale');
    var tempDataC = this.jslib.GetStorage('tempCharterData');
    if (tempDataES && tempDataC) {
      this.query.EstateSaleID = tempDataES['ID'];
      this.query.CharterDataID = tempDataC['ID'];
      this.tempEstateSale = tempDataES;
      this.tempCharterData = tempDataC;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/B05']);
    }
  }

  ngOnInit() {
    // 取得租賃狀態選項
    this.api.apiPost('Option/GetByCode', { code:'RentStatus' }).then(
      (res) => {
        if (Array.isArray(res)) 
          this.rentStatus = res;   
      }
    );
    // 初始化查询租賃资料
    this.getRentList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getRentList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    // 执行查询作业
    this.getRentList();
  }

  private getRentList() {
    // 查询租賃资料
    this.api.apiPost('Rent/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateSaleID: new FormControl(this.query['EstateSaleID']),
      CharterDataID: new FormControl(this.query['CharterDataID']),
      RentStatus: new FormControl({ value: '1', disabled: true }),
      RentStart: new FormControl('', [Validators.required]),
      RentEnd: new FormControl('', [Validators.required]),
      RenterName: new FormControl('', [Validators.required]),
      RenterGender: new FormControl(),
      RenterIdNo: new FormControl(),
      RenterBirth: new FormControl(),
      RentDeal: new FormControl('', [Validators.required]),
      RentAmount: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
    // 设定表单栏位特殊连动性验证
    this.mainForm.get("RentStatus").valueChanges.subscribe((value) => {
      if (value == "1")
        this.mainForm.get("RenterBirth").setValidators([Validators.required]);
      else
        this.mainForm.get("RenterBirth").setValidators(null);
      this.mainForm.get("RenterBirth").updateValueAndValidity();
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateSaleID: this.query['EstateSaleID'],
      CharterDataID: this.query['CharterDataID'],
      RentStatus: '1',
      RentStart: '',
      RentEnd: '',
      RenterBirth: '',
      RentDeal: '',
      CreateDate: null
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateSaleID: this.data['EstateSaleID'],
      CharterDataID: this.data['CharterDataID'],
      RentStatus: this.data['RentStatus'],
      RentStart: this.data['RentStart'],
      RentEnd: this.data['RentEnd'],
      RenterName: this.data['RenterName'],
      RenterGender: this.data['RenterGender'],
      RenterIdNo: this.data['RenterIdNo'],
      RenterBirth: this.data['RenterBirth'],
      RentDeal: this.data['RentDeal'],
      RentAmount: this.data['RentAmount'],
      CreateDate: this.data['CreateDate']
    });
    
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔租賃资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定租賃資料
        this.api.apiPost('Rent/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询租賃资料
            this.getRentList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    data['RentStatus'] = '1';
    if (data['ID']) {
      // 修改租賃资料
      this.api.apiPost('Rent/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询租賃度资料
          this.getRentList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增租賃资料
      this.api.apiPost('Rent/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询租賃资料
          this.getRentList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  ReturnB05() {
    this.router.navigate(['/B05']);
    this.jslib.RemoveStorage('tempRentData');
  }
}
