import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";
declare var readxlsx;
@Component({
  templateUrl: 'payRecord.component.html'
})
export class PayRecordComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    startDate: '',
    endDate: '',
    type: '',
    EstateObjID: '',
    EstateSaleID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'dSchNo', name: '工期' },
    { key: 'Amount', name: '本期應付金額' },
    { key: 'dPayStatus', name: '繳款狀態' },
    {
      key: 'Edit',
      name: '',
      icon: 'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 繳費狀態選項陣列
  public payStatus = [];
  // 繳費资料阵列
  public list = [];
  // 繳費资料编辑物件
  public data;
  // 地產銷售物件
  public tempEstateSale;
  // 工程進度期數选单
  public estateScheduleList = [];
  public estateScheduleDic;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempEstateSale');
    if (tempData) {
      this.query.EstateSaleID = tempData['ID'];
      this.query.EstateObjID = tempData['EstateObjID']
      this.tempEstateSale = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/B04']);
    }
  }

  ngOnInit() {
    // 取得繳費類型選項
    this.api.apiPost('Option/GetByCode', { code:'PayStatus' }).then(
      (res) => {
        if (Array.isArray(res))
          this.payStatus = res;
      }
    );
    // 初始化查询繳費资料、工程進度工期資料
    this.getEstateScheduleList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getPayRecordList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['type'] = "";
    // 执行查询作业
    this.getPayRecordList();
  }

  private getPayRecordList() {
    // 查询繳費资料
    this.api.apiPost('PayRecord/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
          // 繳費紀錄新增對應工期名稱
          if (this.estateScheduleDic) {
            this.list.forEach(data => {
              data['dSchNo'] = this.estateScheduleDic[data['EstateScheduleID']];
            });
          }
        }
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private getEstateScheduleList() {
    // 查询工程进度资料
    this.api.apiPost('EstateSchedule/GetList', { EstateObjID: this.query['EstateObjID'] }).then(
      (res) => {
        if (Array.isArray(res) && res.length > 0) {
          this.estateScheduleList = res;
          // 將工程進度ID與工期名稱索引化
          this.estateScheduleDic = {};
          this.estateScheduleList.forEach(schedule => {
            this.estateScheduleDic[schedule['ID']] = schedule['SchNo'];
          });
        }
        else
          this.pop.setConfirm({ content: '查無相關工期資料' });

        // 查询繳費资料
        this.getPayRecordList();
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateObjID: new FormControl(this.query['EstateObjID']),
      EstateSaleID: new FormControl(this.query['EstateSaleID']),
      EstateScheduleID: new FormControl('', [Validators.required]),
      Amount: new FormControl('', [Validators.required]),
      PayStatus: new FormControl('', [Validators.required]),
      Note: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateObjID: this.query['EstateObjID'],
      EstateSaleID: this.query['EstateSaleID'],
      EstateScheduleID: '',
      Amount: '',
      PayStatus: '',
      CreateDate: null
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateObjID: this.data['EstateObjID'],
      EstateSaleID: this.data['EstateSaleID'],
      EstateScheduleID: this.data['EstateScheduleID'],
      Amount: this.data['Amount'],
      PayStatus: this.data['PayStatus'],
      Note: this.data['Note'],
      CreateDate: this.data['CreateDate']
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔繳費资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定繳費資料
        this.api.apiPost('PayRecord/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询繳費资料
            this.getPayRecordList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;
    // 工程進度ID
    data['EstateScheduleID'] = parseInt(data['EstateScheduleID']);

    if (data['ID']) {
      // 修改繳費资料
      this.api.apiPost('PayRecord/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询繳費度资料
          this.getPayRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增繳費资料
      this.api.apiPost('PayRecord/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询繳費资料
          this.getPayRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  ReturnB04() {
    this.router.navigate(['/B04']);
    this.jslib.RemoveStorage('tempEstateSale');
  }

  Export(){
    window.open( this.baseUrl + 'Export/payRecordCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    var param = {
      EstateObjID : this.query.EstateObjID,
      EstateSaleID : this.query.EstateSaleID,
      data:source
    };
    this.api.apiPost('PayRecord/Import',param).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({ content: '汇入成功' });
      this.getPayRecordList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/paySample.xlsx', '_blank');
  }
}
