import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
declare var $;
@Component({
  templateUrl: 'preference.component.html'
})
export class PreferenceComponent extends BaseComponent implements OnInit, OnDestroy {

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    UserId: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Email', name: 'Email' },
    { key: 'Name', name: '姓名' },
    { key: 'IdentityNo', name: 'ID/身分证号码' },
    { key: 'UpdateDate', name: '更新时间' },
    {
      key: 'Detail',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Detail(item);
      }
    }
  ];
  // 投资意向资料阵列
  public list = [];
  // 投资意向资料物件
  public data;
  // 会员资料物件
  public tempUser;

  constructor(
  ) {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempUser');
    if (tempData) {
      this.query.UserId = tempData['ID'];
      this.tempUser = tempData;
    }
  }

  ngOnInit() {
    // 初始化查询投资意向资料
    this.getPreferenceList();
  }

  Submit() {
    // 执行查询作业
    this.getPreferenceList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['UserId'] = "";
    // 执行查询作业
    this.getPreferenceList();
  }

  private getPreferenceList() {
    this.api.apiPost('Preference/GetPreference', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;

          if (this.query['UserId'] && this.list.length == 0) {
            this.pop.setConfirm({ 
              content: '查无指定会员之投资意向资料',
              event: () => {
                // 返回会员资料管理
                this.ReturnB01();
              }
            });
          }

        } 
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  Detail(data) {
    this.data = data;
    // 切换至明细页
    this.isEditor = true;
    //設定顯示值 用混的
    setTimeout(()=>{
      var plist = data['ItemData'];
      plist.forEach(pitem => {
        console.log('pitem',pitem);
        if(pitem.Ans != null)
         $('[name=' + pitem.PrefernceName +']').val((pitem.Ans.split(',')));
      });
    },250);
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  ReturnB01(){
    this.router.navigate(['/B01']);
  }

  ngOnDestroy() {
    // 离开此页面，统一删除会员暂存资料
    this.jslib.RemoveStorage('tempUser');
  }
  Export(){
    
    window.open( this.baseUrl + 'Export/PreferenceCSV', '_blank');
  }
}
