import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare var readxlsx;
@Component({
  templateUrl: 'estatesale.component.html'
})
export class EstateSaleComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    type: '',
    room:'',
    floor:'',
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'EstateNo', name: '物件ID',style:{'width':'90px'} },
    { key: 'UserIdentityNo', name: '购买人ID',style:{'width':'110px'} },
    { key: 'BuyerName', name: '购买人姓名',style:{'width':'120px'} },
    { key: 'EstateObjName', name: '物件名称' },
    { key: 'EstateRoomName', name: '房型' },
    { key: 'Floor', name: '楼层',style:{'width':'60px'} },
    { key: 'PriceSum', name: '购买总价' },
    { 
      key: '', 
      name: '缴款纪录',
      icon:'fa-bar-chart',
      style:{'text-align':'center'},
      event: (item) => {
        this.gotoB06(item);
      }
    },
    { key: 'ParkingTypeName', name: '是否有车位' },
    { 
      key: '', 
      name: '收益纪录',
      icon:'fa-bar-chart',
      style:{'text-align':'center'},
      event: (item) => {
        this.gotoB07(item);
      }
    },
    { 
      key: '', 
      name: '委託代管',
      icon:'fa-bar-chart',
      style:{'text-align':'center'},
      event: (item) => {
        this.gotoB05(item);
      }
    },
    { key: 'RentStatus', name: '租赁状态' },
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 地产销售资料阵列
  public list = [];
  // 地产销售资料编辑物件
  public data;
  // 地产物件选单
  public estateObjList = [];
  // 地产物件房型选单
  public estateRoomList = [];
 
  public Channel = [];
  constructor() {
    super();
    this.api.apiPost('Channel/GetChannel',{}).then(
      (chres:any)=>{
        chres.forEach(citem => {
         var cObj = {name:(citem['Name'] + '(' + citem['Act'] + ')'),value:citem['ID']};
         this.Channel.push(cObj);
        });
      },(error)=>{
           console.log('[Fail]',error);
      });
  }

  ngOnInit() {
    // 初始化查询地产销售资料
    this.getEstateSaleList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getEstateSaleList();
  }

  Reset() {
    // 初始化查询条件值
    this.query = {
      keyword: '',
      startDate: '',
      endDate: '',
      type: '',
      room:'',
      floor:'',
      status: ''
    };
    // 执行查询作业
    this.getEstateSaleList();
  }

  private getEstateSaleList() {
    // 查询地产销售资料
    this.api.apiPost('EstateSale/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
        } 
      }, 
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      });
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      IsFinish: new FormControl('2', [Validators.required]),
      EstateNo: new FormControl(null, [Validators.required]),
      UserIdentityNo: new FormControl(null, [Validators.required]),
      Channel: new FormControl(),
      BuyerName: new FormControl(null, [Validators.required]),
      EstateObjID: new FormControl(null, [Validators.required]),
      EstateRoomID: new FormControl(null, [Validators.required]),
      Addr: new FormControl(null, [Validators.required]),
      Floor: new FormControl(null, [Validators.required]),
      UnitArea: new FormControl(),
      RoomArea: new FormControl(),
      PublicArea: new FormControl(),
      PriceSum: new FormControl(),
      DealFile: new FormControl(),
      SignDate: new FormControl(),
      IsPush: new FormControl(true, [Validators.required]),
      ParkingType: new FormControl(null, [Validators.required]),
      ParkingPrice: new FormControl(),
      RentStatus: new FormControl({ value: null, disabled: true }),
      CreateDate: new FormControl({ value: null, disabled: true }),
      Status: new FormControl(true, [Validators.required])
    });
    // 设定表单栏位特殊连动性关系
    this.mainForm.get("EstateObjID").valueChanges.subscribe((value) => {
      // console.log('[B04] EstateObjID changed value =', value);
      // 取得指定地产物件对应房型选单
      let estateObjID = parseInt(value);
      if (estateObjID)
        this.getEstateRoomList(estateObjID);
      else
        this.estateRoomList = [];
    });
    this.mainForm.get("ParkingType").valueChanges.subscribe((value) => {
      console.log('[B04] ParkingType changed value =', value);
      this.mainForm.get("ParkingPrice").reset();
      // 無車位
      if (!value || value == "1")
        this.mainForm.get("ParkingPrice").setValidators(null);
      // 有車位
      else
        this.mainForm.get("ParkingPrice").setValidators([Validators.required]);

      // 含於總價時，價格預設為0
      if (value == "3")
        this.mainForm.get("ParkingPrice").setValue(0);

      this.mainForm.get("ParkingPrice").updateValueAndValidity();
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      EstateNo: { value: '', disabled: false },
      EstateObjID: '',
      EstateRoomID: '',
      DealFile: '',
      IsPush: true,
      ParkingType: '',
      Status: true
    });

    // 取得地产物件选单内容
    this.getEstateObjList();

    // 切换至编辑页
    this.isEditor = true;
  }
  idnolist = [];
  Edit(item) {
    console.log('Edit',item)
    this.data = item;
    this.idnolist = this.data['UserIdentityNo'].split(',');
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateNo: { value: this.data['EstateNo'], disabled: true },
      IsFinish:this.data['IsFinish'],
      UserIdentityNo: this.data['UserIdentityNo'],
      Channel: this.data['Channel'],
      BuyerName: this.data['BuyerName'],
      EstateObjID: this.data['EstateObjID'],
      EstateRoomID: this.data['EstateRoomID'],
      Addr: this.data['Addr'],
      Floor: this.data['Floor'],
      UnitArea: this.data['UnitArea'],
      RoomArea: this.data['RoomArea'],
      PublicArea: this.data['PublicArea'],
      PriceSum: this.data['PriceSum'],
      DealFile: this.data['DealFile'] ? this.data['DealFile'] : '',
      SignDate: this.data['SignDate'],
      IsPush: this.data['IsPush'],
      ParkingType: this.data['ParkingType'],
      ParkingPrice: this.data['ParkingPrice'],
      RentStatus: this.data['RentStatus'],
      CreateDate: this.data['CreateDate'],
      Status: this.data['Status']
    });
    
    // 取得地产物件选单内容
    this.getEstateObjList();

    // 切换至编辑页
    this.isEditor = true;
  }

  private getEstateObjList() {
    // 查询地产物件资料
    this.api.apiPost('EstateObj/GetList', {}).then(
      (res) => {
        if (Array.isArray(res)) {
          this.estateObjList = res;
        } 
      },
      (err) => {
        this.pop.setConfirm({ content: '无法取得物件名称选单资料' });
      }
    );
  }

  private getEstateRoomList(id) {
    // 查询房型资料
    this.api.apiPost('EstateRoom/GetList', { EstateObjID: id }).then(
      (res) => {
        if (Array.isArray(res))
          this.estateRoomList = res;
      },
      (err) => {
        this.pop.setConfirm({ content: '无法取得房型选单资料' });
      }
    );
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔地产销售资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定地产销售资料
        this.api.apiPost('EstateSale/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询地产销售资料
            this.getEstateSaleList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
   
    // 取得API参数物件
    let data = this.mainForm.value;
    console.log('SubmitForm',data);
    if(typeof data['UserIdentityNo'] == 'object' && data['UserIdentityNo'] != null){
      data['UserIdentityNo'] = data['UserIdentityNo'].join(',');
    }
    if(typeof data['Channel'] == 'object' && data['Channel'] != null){
      data['Channel'] = data['Channel'].join(',');
    }
    if (data['ID']) {
      // 修改地产销售资料
      this.api.apiPost('EstateSale/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询地产销售资料
          this.getEstateSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增地产销售资料
      this.api.apiPost('EstateSale/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询地产销售资料
          this.getEstateSaleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoB05(item) {
    // 开启委託代管管理功能
    this.router.navigate(['/B05']);
    //加入暂存
    this.jslib.SetStorage('tempEstateSale', item);
  }

  gotoB06(item) {
    // 开启缴款纪录管理功能
    this.router.navigate(['/B06']);
    //加入暂存
    this.jslib.SetStorage('tempEstateSale', item);
  }

  gotoB07(item) {
    // 开启每月收益管理功能
    this.router.navigate(['/B07']);
    //加入暂存
    this.jslib.SetStorage('tempEstateSale', item);
  }
  Export(){
    window.open( this.baseUrl + 'Export/EstatesaleCSV', '_blank');
  }
  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    this.api.apiPost('EstateSale/Import',{data:source}).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({content:'汇入成功'});
      this.getEstateSaleList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/estateSaleSample.xlsx', '_blank');
  }
}
