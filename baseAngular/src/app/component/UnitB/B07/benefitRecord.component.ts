import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";
declare var readxlsx;
@Component({
  templateUrl: 'benefitRecord.component.html'
})
export class BenefitRecordComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    payStartDate: '',
    payEndDate: '',
    EstateSaleID: '',
    EstateObjID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'IncomeDate', name: '收款日期' },
    { key: 'PayDate', name: '撥款日期' },
    { key: 'Income', name: '收入金額' },
    { key: 'Expense', name: '支出金額' },
    { key: 'dPayStatus', name: '撥款狀態' },
    {
      key: 'Edit',
      name: '',
      icon: 'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 撥款狀態選項陣列
  public payStatus = [];
  // 代管類型選項陣列
  public charterType = [];
  // 每月收益资料阵列
  public list = [];
  // 每月收益资料编辑物件
  public data;
  // 地產銷售物件
  public tempEstateSale;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempEstateSale');
    if (tempData) {
      this.query.EstateSaleID = tempData['ID'];
      this.query.EstateObjID = tempData['EstateObjID'];
      this.tempEstateSale = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/B04']);
    }
  }

  ngOnInit() {
    // 取得繳費類型選項
    this.api.apiPost('Option/GetByCode', { code:'AllotStatus' }).then(
      (res) => {
        if (Array.isArray(res))
          this.payStatus = res;
      }
    );
    // 取得代管類型選項
    this.api.apiPost('Option/GetByCode', { code:'CharterType' }).then(
      (res) => {
        if (Array.isArray(res))
          this.charterType = res;
      }
    );
    // 初始化查询每月收益资料
    this.getBenefitRecordList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getBenefitRecordList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['payStartDate'] = "";
    this.query['payEndDate'] = "";
    // 执行查询作业
    this.getBenefitRecordList();
  }

  private getBenefitRecordList() {
    // 查询每月收益资料
    this.api.apiPost('BenefitRecord/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateObjID: new FormControl(this.query['EstateObjID']),
      EstateSaleID: new FormControl(this.query['EstateSaleID']),
      RentDataID: new FormControl(null),
      CharterType: new FormControl(''),
      RenterName: new FormControl(null),
      RenterGender: new FormControl(null),
      RenterIdNo: new FormControl(null),
      RenterBirth: new FormControl(null),
      Income: new FormControl(null),
      Expense: new FormControl(null),
      PayDate: new FormControl(''),
      IncomeDate: new FormControl(''),
      PdfFile: new FormControl(''),
      Note: new FormControl(null),
      PayStatus: new FormControl(''),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateObjID: this.query['EstateObjID'],
      EstateSaleID: this.query['EstateSaleID'],
      CharterType: '',
      PayDate: '',
      IncomeDate:'',
      PdfFile: '',
      PayStatus: '',
      CreateDate: null
    });

    // 查询租客欄位預設資料
    this.api.apiPost('Rent/GetLatestRenter', { EstateSaleID: this.query['EstateSaleID'] }).then(
      (res) => {
        if (res) {
          this.mainForm.get("RentDataID").setValue(res['RentDataID']);
          this.mainForm.get("RenterName").setValue(res['RenterName']);
          this.mainForm.get("RenterGender").setValue(res['RenterGender']);
          this.mainForm.get("RenterIdNo").setValue(res['RenterIdNo']);
          this.mainForm.get("RenterBirth").setValue(res['RenterBirth']);
        }
        // 切换至编辑页
        this.isEditor = true;
      },
      (err) => {
        // 切换至编辑页
        this.isEditor = true;
      }
    );
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateObjID: this.data['EstateObjID'],
      EstateSaleID: this.data['EstateSaleID'],
      RentDataID: this.data['RentDataID'],
      CharterType: this.data['CharterType'] ? this.data['CharterType'] : "",
      RenterName: this.data['RenterName'],
      RenterGender: this.data['RenterGender'],
      RenterIdNo: this.data['RenterIdNo'],
      RenterBirth: this.data['RenterBirth'],
      Income: this.data['Income'],
      Expense: this.data['Expense'],
      PayDate: this.data['PayDate'],
      IncomeDate: this.data['IncomeDate'],
      PdfFile: this.data['PdfFile'] ? this.data['PdfFile'] : "",
      Note: this.data['Note'],
      PayStatus: this.data['PayStatus'],
      CreateDate: this.data['CreateDate']
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔每月收益资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定每月收益資料
        this.api.apiPost('BenefitRecord/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询每月收益资料
            this.getBenefitRecordList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改每月收益资料
      this.api.apiPost('BenefitRecord/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询每月收益度资料
          this.getBenefitRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增每月收益资料
      this.api.apiPost('BenefitRecord/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询每月收益资料
          this.getBenefitRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  ReturnB04() {
    this.router.navigate(['/B04']);
    this.jslib.RemoveStorage('tempEstateSale');
  }

  Export(){
    window.open( this.baseUrl + 'Export/benefitRecordCSV', '_blank');
  }

  Import(){
    console.log('Import');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','.xlsx');
  
    Fileinput.addEventListener('change', () =>  {

      if (Fileinput.files != null && Fileinput.files[0] != null){
        var file = Fileinput.files[0];
        var reader = new FileReader();
        var name = Fileinput.files[0].name;
        reader.onload =  (e) => {
          var data = e.target.result;
          var retjson = readxlsx(data, 'json');
          var _keys = Object.keys(retjson);
          console.log('retjson',retjson[_keys[0]]);
          var result = retjson[_keys[0]];
          this.ImportHandle(result);
        };
        reader.readAsBinaryString(file);
      }
    });
    Fileinput.click();
  }
  ImportHandle(source){
    var param = {
      EstateObjID : this.query.EstateObjID,
      EstateSaleID : this.query.EstateSaleID,
      data:source
    };
    this.api.apiPost('BenefitRecord/Import',param).then((url)=>{
      console.log('Upload res',url);
      this.pop.setConfirm({ content: '汇入成功' });
      this.getBenefitRecordList();
    },(err)=>{
      this.pop.setConfirm({content:err['msg']});
    });
  }
  Sample(){
    window.open( window.location.origin + '/Content/benefitSample.xlsx', '_blank');
  }
}
