import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cinfig } from '../../../assets/config';
import { PopupService } from '../../share/component/popup/popup.service';
import { ApiService } from '../../share/service/api.service';
// import {navItems} from '../../_nav'
@Component({
  templateUrl: 'doLogin.component.html'
})
export class DoLogInComponent implements OnInit {

  public info :any;
  public userdata = {
    Act : '',
    Pwd : '',
    Type: 'admin',
    keep : false
  }
  public errorMsg = '';
  constructor(
    private router : Router,
    private pop : PopupService,
    private api : ApiService
  ){

    localStorage.removeItem('user');

    if(Cinfig.DebugMode){
      this.userdata = {
        Act : 'SuperAdmin',
        Pwd : 'ts27986788',
        Type: 'admin',
        keep : false
      }
    }
  }

  ngOnInit() {

  }
  DoLogIn(){
    this.errorMsg = '';
      this.api.apiPost('Admin/LogIn',this.userdata).then(
        (res)=>{

            localStorage.setItem('user',JSON.stringify(res));
            var url_index = res['Permission'].findIndex(item => {return item['IsView'] == true});
            if(url_index > -1){
            var url =  res['Permission'][url_index].FuncCode;
            this.router.navigate(['/' + url]);
            }else{
              this.pop.setConfirm({content:'无任何检视权限'})
            }
        },(error)=>{
            console.log('[Fail]',error);
            this.errorMsg = error['msg'];
        }
      );
  }
}
