import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'estateObj.component.html'
})
export class EstateObjComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    status: '',
    startDate: '',
    endDate: '',
    type: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'Name', name: '物件名称' },
    { key: 'TypeName', name: '销售类型' },
    { key: 'Price', name: '价格' },
    { key: 'Country', name: '国家' },
    { 
      key: '', 
      name: '工程进度管理',
      icon:'fa-bar-chart',
      style:{'text-align':'center'},
      event: (item) => {
        this.gotoEstateSchedule(item);
      }
    },
    { 
      key: '', 
      name: '房型管理',
      icon:'fa-cubes',
      event: (item) => {
        this.gotoEstateRoom(item);
      }
    },
    { key: 'OrderNo', name: '排序'},
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 地产物件资料阵列
  public list = [];
  // 地产物件资料编辑物件
  public data;
  // 特色标签选单阵列
  public specTagList = [];
  // 产品形象栏位物件
  public imageAlbum = [];
  // 周围设施栏位物件
  public surroundings = { album: [], item: [] };
  public surroundingsEn = { album: [], item: [] };
  // 设施栏位物件
  public facilities = { album: [], item: [] };
  public facilitiesEn = { album: [], item: [] };
  // 服务栏位物件
  public services = { item: [] };
  public servicesEn = { item: [] };

  constructor() {
    super();
  }

  ngOnInit() {
    // console.log('[C01] ngOnInit');
    // 初始化查询地产物件资料
    this.getEstateObjList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getEstateObjList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['status'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    this.query['type'] = "";
    // 执行查询作业
    this.getEstateObjList();
  }

  private getEstateObjList() {
    // console.log('[C01] getEstateObjList');
    // 查询地产物件资料
    this.api.apiPost('EstateObj/GetList', this.query).then(
      (res) => {
        // console.log('[C01][API] EstateObj/GetList success', res);
        if (Array.isArray(res)) {
          res.forEach(data => {
            data['TypeName'] = (data['SaleType'] == '1') ? '期房' : '现房';
          });
          this.list = res;
        } 
      },
      (err) => {
        // console.log('[C01][API] EstateObj/GetList error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // console.log('[C01] initializeForm');
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      Name: new FormControl(null, [Validators.required]),
      SaleType: new FormControl(null, [Validators.required]),
      MainImg: new FormControl(),
      Brief: new FormControl(),
      Price: new FormControl(null, [Validators.required]),
      SpecTag: new FormControl([]),
      VideoImg: new FormControl(),
      VideoUrl: new FormControl(),
      Country: new FormControl(),
      EstateType: new FormControl(),
      Address: new FormControl(),
      FloorDesc: new FormControl(),
      FloorImg: new FormControl(),
      CompleteDate: new FormControl(),
      CaseSpec: new FormControl(),
      FileUrl: new FormControl(),
      OnShelfDate: new FormControl(),
      OffShelfDate: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true }),
      OrderNo: new FormControl(0, [Validators.required]),
      Status: new FormControl(true, [Validators.required]),

      NameEn: new FormControl(null, [Validators.required]),//EN add
      BriefEn: new FormControl(),//EN add
      SpecTagEn: new FormControl([]),
      CountryEn: new FormControl(),//EN add
      EstateTypeEn: new FormControl(),//EN add
      CaseSpecEn: new FormControl(),//EN add
    });
  }

  Create() {
    // console.log('[C01] Create');
    // 初始化表单栏位内容
    this.mainForm.reset({
      SpecTag: [],
      SpecTagEn: [],
      FloorImg: '',
      CompleteDate: '',
      CaseSpec: '',
      CaseSpecEn: '',
      FileUrl: '',
      OrderNo: 0,
      Status: true
    });
    this.imageAlbum = [];
    this.surroundings = { album: [], item: [] };
    this.surroundingsEn = { album: [], item: [] };
    this.facilities = { album: [], item: [] };
    this.facilitiesEn = { album: [], item: [] };
    this.services = { item: [] };
    this.servicesEn = { item: [] };
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    // console.log('[C01] Edit item', item);
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      Name: this.data['Name'],
      SaleType: this.data['SaleType'],
      MainImg: this.data['MainImg'],
      Brief: this.data['Brief'],
      Price: this.data['Price'],
      SpecTag: this.data['SpecTag'],
      VideoImg: this.data['VideoImg'],
      VideoUrl: this.data['VideoUrl'],
      Country: this.data['Country'],
      EstateType: this.data['EstateType'],
      Address: this.data['Address'],
      FloorDesc: this.data['FloorDesc'],
      FloorImg: this.data['FloorImg'] ? this.data['FloorImg'] : '',
      CompleteDate: this.data['CompleteDate'],
      CaseSpec: this.data['CaseSpec'] ? this.data['CaseSpec'] : '',
      FileUrl: this.data['FileUrl'] ? this.data['FileUrl'] : '',
      OnShelfDate: this.data['OnShelfDate'],
      OffShelfDate: this.data['OffShelfDate'],
      CreateDate: this.data['CreateDate'],
      OrderNo: this.data['OrderNo'],
      Status: this.data['Status'],
      //En add
      NameEn: this.data['NameEn'],
      BriefEn: this.data['BriefEn'],
      SpecTagEn: this.data['SpecTagEn'],
      CountryEn: this.data['CountryEn'],
      EstateTypeEn: this.data['EstateTypeEn'],
      CaseSpecEn: this.data['CaseSpecEn'] ? this.data['CaseSpecEn'] : ''
    });
    this.imageAlbum = this.checkJson(this.data['Album']) ? JSON.parse(this.data['Album']) : [];
    this.surroundings = this.checkJson(this.data['Around']) ? JSON.parse(this.data['Around']) : { album: [], item: [] };
    this.surroundingsEn = this.checkJson(this.data['AroundEn']) ? JSON.parse(this.data['AroundEn']) : { album: [], item: [] };
    this.facilities = this.checkJson(this.data['Device']) ? JSON.parse(this.data['Device']) : { album: [], item: [] };
    this.facilitiesEn = this.checkJson(this.data['DeviceEn']) ? JSON.parse(this.data['DeviceEn']) : { album: [], item: [] };
    this.services = this.checkJson(this.data['Service']) ? JSON.parse(this.data['Service']) : { item: [] };
    this.servicesEn = this.checkJson(this.data['ServiceEn']) ? JSON.parse(this.data['ServiceEn']) : { item: [] };
    
    // 切换至编辑页
    this.isEditor = true;
  }

  private checkJson(jsonStr) {
    if (!jsonStr || typeof jsonStr != "string")
      return false;
    
    jsonStr = jsonStr.trim();
    let first = jsonStr.substr(0, 1);
    let last = jsonStr.substr(-1, 1);
    if ((first == "{" && last == "}") || (first == "[" && last == "]"))
      return true;
    else 
      return false;
  }

  Delete() {
    // console.log('[C01] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔地产物件资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定地产物件资料
        this.api.apiPost('EstateObj/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[C01][API] EstateObj/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询地产物件资料
            this.getEstateObjList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E01][API] EstateObj/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    
    // 取得API参数物件
    let data = this.mainForm.value;
    console.log('[C01] SubmitForm',data);
    // 特色标签
    if (Array.isArray(data['SpecTag']))
      data['SpecTag'] = data['SpecTag'].join();
    if (Array.isArray(data['SpecTagEn']))
      data['SpecTagEn'] = data['SpecTagEn'].join();
    // 产品形象
    data['Album'] = JSON.stringify(this.imageAlbum);
    // 周围设施
    data['Around'] = JSON.stringify(this.surroundings);
    data['AroundEn'] = JSON.stringify(this.surroundingsEn);
    // 设施服务
    data['Device'] = JSON.stringify(this.facilities);
    data['DeviceEn'] = JSON.stringify(this.facilitiesEn);
    // 服务
    data['Service'] = JSON.stringify(this.services);
    data['ServiceEn'] = JSON.stringify(this.servicesEn);

    if (data['ID']) {
      // 修改地产物件资料
      this.api.apiPost('EstateObj/Modify', data).then(
        (res) => {
          // console.log('[C01][API] EstateObj/Modify success', res);
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询地产物件资料
          this.getEstateObjList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C01][API] EstateObj/Modify error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增地产物件资料
      this.api.apiPost('EstateObj/Create', data).then(
        (res) => {
          // console.log('[C01][API] EstateObj/Create success', res);
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询地产物件资料
          this.getEstateObjList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C01][API] EstateObj/Create error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoEstateSchedule(item) {
    // console.log('[C01] gotoEstateSchedule item', item);
    // 开启工程进度管理功能
    this.router.navigate(['/C03']);
    //加入暂存
    this.jslib.SetStorage('tempEstate', item);
  }

  gotoEstateRoom(item) {
    // console.log('[C01] gotoEstateRoom item', item);
    // 开启房型管理功能
    this.router.navigate(['/C02']);
    //加入暂存
    this.jslib.SetStorage('tempEstate', item);
  }

  editImageAlbum() {
    this.pop.setAlbum({
      data: this.imageAlbum,
      event: (list) => {
        // 储存产品形象相簿
        this.imageAlbum = list;
      }
    });
  }

  editSurroundings(type) {
    if (type == 'album') {
      this.pop.setAlbum({
        data: this.surroundings.album,
        event: (list) => {
          // 储存周围设施图片
          this.surroundings.album = list;
        }
      });
    } else if (type == 'item') {
      this.pop.setMultiEdit({
        data: this.surroundings.item,
        event: (list) => {
          // 储存周围设施资讯
          this.surroundings.item = list;
        }
      });
    }
  }
  editSurroundingsEn(type) {
    if (type == 'album') {
      this.pop.setAlbum({
        data: this.surroundingsEn.album,
        event: (list) => {
          // 储存周围设施图片
          this.surroundingsEn.album = list;
        }
      });
    } else if (type == 'item') {
      this.pop.setMultiEdit({
        data: this.surroundingsEn.item,
        event: (list) => {
          // 储存周围设施资讯
          this.surroundingsEn.item = list;
        }
      });
    }
  }

  editFacilities(type) {
    if (type == 'album') {
      this.pop.setAlbum({
        data: this.facilities.album,
        event: (list) => {
          // 储存服务设施图片
          this.facilities.album = list;
        }
      });
    } else if (type == 'item') {
      this.pop.setMultiEdit({
        data: this.facilities.item,
        event: (list) => {
          // 储存服务设施资讯
          this.facilities.item = list;
        }
      });
    }
  }
  editFacilitiesEn(type) {
    if (type == 'album') {
      this.pop.setAlbum({
        data: this.facilitiesEn.album,
        event: (list) => {
          // 储存服务设施图片
          this.facilitiesEn.album = list;
        }
      });
    } else if (type == 'item') {
      this.pop.setMultiEdit({
        data: this.facilitiesEn.item,
        event: (list) => {
          // 储存服务设施资讯
          this.facilitiesEn.item = list;
        }
      });
    }
  }

  editServices() {
    this.pop.setMultiEdit({
      data: this.services.item,
      event: (list) => {
        // 储存服务资讯
        this.services.item = list;
      }
    });
  }
  editServicesEn() {
    this.pop.setMultiEdit({
      data: this.servicesEn.item,
      event: (list) => {
        // 储存服务资讯
        this.servicesEn.item = list;
      }
    });
  }
}
