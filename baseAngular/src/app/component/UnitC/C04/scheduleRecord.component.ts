import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'scheduleRecord.component.html'
})
export class ScheduleRecordComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    EstateObjID: null,
    EstateSID: null,
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'ImgTitle', name: '图片标题' },
    { key: 'CreateDate', name: '建立时间' },
    { key: 'OrderNo', name: '排序' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 施工纪录资料阵列
  public list = [];
  // 施工纪录资料编辑物件
  public data;
  // 地产主档物件
  public tempEstate;
  // 工程进度物件
  public tempEstateSchedule;

  constructor(
    private route: ActivatedRoute
  ) {
    super();
    // 取得暂存
    let tempDataE = this.jslib.GetStorage('tempEstate');
    let tempDataES = this.jslib.GetStorage('tempEstateSchedule');
    console.log('[C03] tempEstate', tempDataE);
    console.log('[C03] tempEstateSchedule', tempDataES);
    if (tempDataE && tempDataES) {
      this.query.EstateObjID = tempDataE['ID'];
      this.tempEstate = tempDataE;
      this.query.EstateSID = tempDataES['ID'];
      this.tempEstateSchedule = tempDataES;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/C03']);
    }
  }

  ngOnInit() {
    // console.log('[C04] ngOnInit');
    // 初始化查询施工纪录资料
    this.getScheduleRecordList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getScheduleRecordList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    // 执行查询作业
    this.getScheduleRecordList();
  }

  private getScheduleRecordList() {
    // console.log('[C04] getScheduleRecordList');
    // 查询施工纪录资料
    this.api.apiPost('ScheduleRecord/GetList', this.query).then(
      (res) => {
        // console.log('[C04][API] ScheduleRecord/GetList success', res);
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        // console.log('[C04][API] ScheduleRecord/GetList error', err);
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // console.log('[C04] initializeForm');
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateObjID: new FormControl(this.query['EstateObjID']),
      EstateSID: new FormControl(this.query['EstateSID']),
      Img: new FormControl('', [Validators.required]),
      ImgTitle: new FormControl('', [Validators.required]),
      ImgDesc: new FormControl('', [Validators.required]),
      ImgTitleEn: new FormControl('', [Validators.required]),
      ImgDescEn: new FormControl('', [Validators.required]),
      CreateDate: new FormControl({ value: null, disabled: true }),
      OrderNo: new FormControl(0, [Validators.required]),
    });
  }

  Create() {
    // console.log('[C04] Create');
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateObjID: this.query['EstateObjID'],
      EstateSID: this.query['EstateSID'],
      ImgTitle: '',
      Img: '',
      ImgDesc: '',
      CreateDate: null,
      OrderNo: 0,
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    // console.log('[C04] Edit item', item);
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateObjID: this.query['EstateObjID'],
      EstateSID: this.query['EstateSID'],
      Img: this.data['Img'],
      ImgTitle: this.data['ImgTitle'],
      ImgDesc: this.data['ImgDesc'],
      ImgTitleEn: this.data['ImgTitleEn'],
      ImgDescEn: this.data['ImgDescEn'],
      CreateDate: this.data['CreateDate'],
      OrderNo: this.data['OrderNo'],
    });
    
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // console.log('[C04] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔施工纪录资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定通知纪录
        this.api.apiPost('ScheduleRecord/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[C04][API] ScheduleRecord/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询施工纪录资料
            this.getScheduleRecordList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E01][API] ScheduleRecord/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // console.log('[C04] SubmitForm');
    // 取得API参数物件
    let data = this.mainForm.value;
    // console.log('[C04] api-data', data);

    if (data['ID']) {
      // 修改施工纪录资料
      this.api.apiPost('ScheduleRecord/Modify', data).then(
        (res) => {
          // console.log('[C04][API] ScheduleRecord/Modify success', res);
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询施工纪录资料
          this.getScheduleRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C04][API] ScheduleRecord/Modify error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增施工纪录资料
      this.api.apiPost('ScheduleRecord/Create', data).then(
        (res) => {
          // console.log('[C04][API] ScheduleRecord/Create success', res);
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询施工纪录资料
          this.getScheduleRecordList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C04][API] ScheduleRecord/Create error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  ReturnC03() {
    this.router.navigate(['/C03']);
    this.jslib.RemoveStorage('tempEstateSchedule');
  }
}
