import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'fundsReport.component.html'
})
export class FundsReportComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    startDate: '',
    endDate: '',
    FundsID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'ID', name: 'ID' },
    { key: 'dReportDate', name: '日期' },
    { key: 'CreateDate', name: '建立时间' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 財務報表资料阵列
  public list = [];
  // 財務報表资料编辑物件
  public data;
  // 基金主档物件
  public tempFunds;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempFunds');
    if (tempData) {
      this.query.FundsID = tempData['ID'];
      this.tempFunds = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/C05']);
    }
  }

  ngOnInit() {
    // 初始化查询財務報表资料
    this.getFundsReportList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsReportList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    // 执行查询作业
    this.getFundsReportList();
  }

  private getFundsReportList() {
    // 查询財務報表资料
    this.api.apiPost('FundsReport/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      FundsID: new FormControl(this.query['FundsID']),
      Year: new FormControl('', [Validators.required]),
      Month: new FormControl('', [Validators.required]),
      FileUrl: new FormControl('', [Validators.required]),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      FundsID: this.query['FundsID'],
      Year: '',
      Month: '',
      FileUrl: ''
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      FundsID: this.query['FundsID'],
      Year: this.data['Year'],
      Month: this.data['Month'],
      FileUrl: this.data['FileUrl'],
      CreateDate: this.data['CreateDate']
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔財務報表资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定財務報表资料
        this.api.apiPost('FundsReport/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询財務報表资料
            this.getFundsReportList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改財務報表资料
      this.api.apiPost('FundsReport/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询財務報表度资料
          this.getFundsReportList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增財務報表资料
      this.api.apiPost('FundsReport/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询財務報表资料
          this.getFundsReportList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
    
  }

  ReturnC05(){
    this.router.navigate(['/C05']);
    this.jslib.RemoveStorage('tempFunds');
  }
}
