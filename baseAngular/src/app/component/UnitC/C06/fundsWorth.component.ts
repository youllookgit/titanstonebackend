import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'fundsWorth.component.html'
})
export class FundsWorthComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    min: null,
    max: null,
    startDate: '',
    endDate: '',
    FundsID: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'ID', name: 'ID' },
    { key: 'WorthDate', name: '日期' },
    { key: 'WorthValue', name: '每單位淨值' },
    { key: 'CreateDate', name: '建立时间' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 單位淨值资料阵列
  public list = [];
  // 單位淨值资料编辑物件
  public data;
  // 基金主档物件
  public tempFunds;

  constructor() {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempFunds');
    if (tempData) {
      this.query.FundsID = tempData['ID'];
      this.tempFunds = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/C05']);
    }
  }

  ngOnInit() {
    // 初始化查询單位淨值资料
    this.getFundsWorthList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsWorthList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['min'] = null;
    this.query['max'] = null;
    this.query['startDate'] = "";
    this.query['endDate'] = "";
    // 执行查询作业
    this.getFundsWorthList();
  }

  private getFundsWorthList() {
    // 查询單位淨值资料
    this.api.apiPost('FundsWorth/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      FundsID: new FormControl(this.query['FundsID']),
      WorthDate: new FormControl('', [Validators.required]),
      WorthValue: new FormControl('', [Validators.required]),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      FundsID: this.query['FundsID'],
      WorthDate: '',
      WorthValue: ''
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      FundsID: this.query['FundsID'],
      WorthDate: this.data['WorthDate'],
      WorthValue: this.data['WorthValue'],
      CreateDate: this.data['CreateDate']
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔單位淨值资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定單位淨值资料
        this.api.apiPost('FundsWorth/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询單位淨值资料
            this.getFundsWorthList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改單位淨值资料
      this.api.apiPost('FundsWorth/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询單位淨值度资料
          this.getFundsWorthList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增單位淨值资料
      this.api.apiPost('FundsWorth/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询單位淨值资料
          this.getFundsWorthList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
    
  }

  ReturnC05(){
    this.router.navigate(['/C05']);
    this.jslib.RemoveStorage('tempFunds');
  }
}
