import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../share/component/base.component';
import { FormsModule, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: 'funds.component.html'
})
export class FundsComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    fundType: '',
    buyType: '',
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'ID', name: 'ID' },
    { key: 'Name', name: '基金名稱' },
    { key: 'dFundType', name: '基金類型' },
    { key: 'dBuyType', name: '申購方式' },
    { 
      key: '', 
      name: '單位淨值',
      icon: 'fa-bar-chart',
      style: { 'text-align': 'center' },
      event: (item) => {
        this.gotoC06(item);
      }
    },
    { 
      key: '', 
      name: '財務報表',
      icon: 'fa-bar-chart',
      style: { 'text-align': 'center' },
      event: (item) => {
        this.gotoC07(item);
      }
    },
    { key: 'CreateDate', name: '建立時間' },
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 基金商品资料阵列
  public list = [];
  // 基金商品资料编辑物件
  public data;
  // 申購方式選項
  public buyTypeList;
 
  constructor() {
    super();
    // 取得申購方式選項
    this.api.apiPost('Option/GetByCode', { code:'FundBuyType' }).then(
      (res) => {
        if (Array.isArray(res)) {
          this.buyTypeList = res;
        }
      }
    );
  }

  ngOnInit() {
    // 初始化查询基金商品资料
    this.getFundsList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    this.query['fundType'] = "";
    this.query['buyType'] = "";
    this.query['status'] = "";
    // 执行查询作业
    this.getFundsList();
  }

  private getFundsList() {
    // 查询基金商品资料
    this.api.apiPost('Funds/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res)) {
          this.list = res;
        } 
      }, 
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(),
      Name: new FormControl(null, [Validators.required]),
      FundType: new FormControl(null, [Validators.required]),
      BuyType: new FormControl(null, [Validators.required]),
      CreateDate: new FormControl({ value: null, disabled: true }),
      Status: new FormControl(true, [Validators.required])
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      Name: '',
      FundType: '',
      BuyType: '',
      Status: true
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      Name: this.data['Name'],
      FundType: this.data['FundType'],
      BuyType: this.data['BuyType'],
      CreateDate: this.data['CreateDate'],
      Status: this.data['Status']
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔基金商品资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定基金商品资料
        this.api.apiPost('Funds/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询基金商品资料
            this.getFundsList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改基金商品资料
      this.api.apiPost('Funds/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询基金商品资料
          this.getFundsList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增基金商品资料
      this.api.apiPost('Funds/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询基金商品资料
          this.getFundsList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoC06(item) {
    // 开启單位淨值管理功能
    this.router.navigate(['/C06']);
    // 加入暂存
    this.jslib.SetStorage('tempFunds', item);
  }

  gotoC07(item) {
    // 开启財務報表管理功能
    this.router.navigate(['/C07']);
    // 加入暂存
    this.jslib.SetStorage('tempFunds', item);
  }
}
