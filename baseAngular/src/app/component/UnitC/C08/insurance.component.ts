import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'insurance.component.html'
})
export class InsuranceComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    status: ''
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'ID', name: 'ID' },
    { key: 'Name', name: '保险名称' },
    { key: 'TypeName', name: '人身保险' },
    { key: 'CreateDate', name: '建立时间' },
    { key: 'Status', name: '状态' },
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 资料阵列
  public list = [];
  // 资料编辑物件
  public data;
  // 基金主档物件
  public tempFundsSale;

  constructor() {
    super();
  }

  ngOnInit() {
    // 初始化查询资料
    this.getFundsDetailList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getFundsDetailList();
  }

  Reset() {
    // 初始化查询条件值
    this.query = {
      keyword: '',
      status: ''
    };
    // 执行查询作业
    this.getFundsDetailList();
  }

  private getFundsDetailList() {
    // 查询资料
    this.api.apiPost('Insurance/GetList', this.query).then(
      (res) => {
        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      Name: new FormControl('', [Validators.required]),
      NameEn: new FormControl('', [Validators.required]),
      TypeName: new FormControl('', [Validators.required]),
      TypeNameEn: new FormControl('', [Validators.required]),
      Status: new FormControl(true),
      Note: new FormControl(),
      CreateDate: new FormControl({ value: null, disabled: true })
    });
  }

  Create() {
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      Name: '',
      NameEn: '',
      TypeName: '',
      TypeNameEn: '',
      Note: '',
      Status: true,
      CreateDate: ''
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      Name: this.data['Name'],
      NameEn: this.data['NameEn'],
      TypeName: this.data['TypeName'],
      TypeNameEn: this.data['TypeNameEn'],
      Note: this.data['Note'],
      Status: this.data['Status'],
      CreateDate: this.data['CreateDate']
    });
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔资料?',
      cancelTxt: '取消',
      event: () => {
        // 删除指定资料
        this.api.apiPost('Insurance/Delete', { Id: this.mainForm.get("ID").value }).then(
          (res) => {
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询资料
            this.getFundsDetailList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // 取得API参数物件
    let data = this.mainForm.value;

    if (data['ID']) {
      // 修改资料
      this.api.apiPost('Insurance/Modify', data).then(
        (res) => {
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询度资料
          this.getFundsDetailList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增资料
      this.api.apiPost('Insurance/Create', data).then(
        (res) => {
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询资料
          this.getFundsDetailList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }

  }

  Export(){
    window.open( this.baseUrl + 'Export/InsuranceCSV', '_blank');
  }
}
