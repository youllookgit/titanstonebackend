import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BaseComponent } from "../../../share/component/base.component";

@Component({
  templateUrl: 'estateSchedule.component.html'
})
export class EstateScheduleComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;

  public isEditor = false;
  // 查询条件物件
  public query = {
    keyword: '',
    EstateObjID: null
  };
  // 资料列表栏位设定
  public tableSet = [
    { key: 'SchNo', name: '期数' },
    { key: 'Name', name: '阶段名称' },
    { key: 'TypeName', name: '工程状态' },
    { key: 'CompleteDate', name: '预估完工日' },
    { key: 'RealComplete', name: '实际完工日' },
    { 
      key: '', 
      name: '施工纪录',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.gotoScheduleRecord(item);
      }
    },
    { key: 'OrderNo', name: '排序'},
    {
      key: 'Edit',
      name: '',
      icon:'fa-pencil-square-o',
      event: (item) => {
        this.Edit(item);
      }
    }
  ];
  // 工程进度资料阵列
  public list = [];
  // 工程进度资料编辑物件
  public data;
  // 地产主档物件
  public tempEstate;

  constructor(
    private route: ActivatedRoute
  ) {
    super();
    // 取得暂存
    var tempData = this.jslib.GetStorage('tempEstate');
    // console.log('[C03] tempEstate', tempData);
    if (tempData) {
      this.query.EstateObjID = tempData['ID'];
      this.tempEstate = tempData;
    } else {
      // 取不到导回物件选择页
      this.router.navigate(['/C01']);
    }
  }

  ngOnInit() {
    // console.log('[C03] ngOnInit');
    // 初始化查询工程进度资料
    this.getEstateScheduleList();
    // 初始化表单物件
    this.initializeForm();
  }

  Submit() {
    // 执行查询作业
    this.getEstateScheduleList();
  }

  Reset() {
    // 初始化查询条件值
    this.query['keyword'] = "";
    // 执行查询作业
    this.getEstateScheduleList();
  }

  private getEstateScheduleList() {
    // 查询工程进度资料
    this.api.apiPost('EstateSchedule/GetList', this.query).then(
      (res:any) => {
        res.forEach(es => {
          es['TypeName'] = '';
          if(es['ScheduleType'] == '1')
              es['TypeName'] = '尚未动工';
          else if(es['ScheduleType'] == '2')
              es['TypeName'] = '施工中';
          else if(es['ScheduleType'] == '3')
              es['TypeName'] = '已完工';
        });

        if (Array.isArray(res))
          this.list = res;
      },
      (err) => {
        this.pop.setConfirm({ content: err['msg'] });
      }
    );
  }

  private initializeForm() {
    // console.log('[C03] initializeForm');
    // 初始化表单物件
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      EstateObjID: new FormControl(this.query['EstateObjID']),
      SchNo: new FormControl('', [Validators.required]),
      Name: new FormControl('', [Validators.required]),
      Brief: new FormControl(''),

      SchNoEn: new FormControl('', [Validators.required]),
      NameEn: new FormControl('', [Validators.required]),
      BriefEn: new FormControl(''),

      ScheduleType: new FormControl(null, [Validators.required]),
      CompleteDate: new FormControl(null),
      RealComplete: new FormControl(null),
      CreateDate: new FormControl({ value: null, disabled: true }),
      OrderNo: new FormControl(0, [Validators.required]),
    });
    // 设定表单栏位特殊连动性验证
    this.mainForm.get("ScheduleType").valueChanges.subscribe((value) => {
      // console.log('[C03] ScheduleType changed value =', value);
      if (value == "3")
        this.mainForm.get("RealComplete").setValidators([Validators.required]);
      else
        this.mainForm.get("RealComplete").setValidators(null);
      this.mainForm.get("RealComplete").updateValueAndValidity();
    });
  }

  Create() {
    // console.log('[C03] Create');
    // 初始化表单栏位内容
    this.mainForm.reset({
      ID: null,
      EstateObjID: this.query['EstateObjID'],
      SchNo: '',
      Name: '',
      Brief: '',

      SchNoEn: '',
      NameEn: '',
      BriefEn: '',

      ScheduleType: null,
      CompleteDate: null,
      RealComplete: null,
      CreateDate: null,
      OrderNo: 0,
    });

    // 切换至编辑页
    this.isEditor = true;
  }

  Edit(item) {
    // console.log('[C03] Edit item', item);
    this.data = item;
    // 设定表单栏位内容
    this.mainForm.reset({
      ID: this.data['ID'],
      EstateObjID: this.query['EstateObjID'],
      SchNo: this.data['SchNo'],
      Name: this.data['Name'],
      Brief: this.data['Brief'],

      SchNoEn: this.data['SchNoEn'],
      NameEn: this.data['NameEn'],
      BriefEn: this.data['BriefEn'],

      ScheduleType: this.data['ScheduleType'],
      CompleteDate: this.data['CompleteDate'],
      RealComplete: this.data['RealComplete'],
      CreateDate: this.data['CreateDate'],
      OrderNo: this.data['OrderNo'],
    });
    
    // 切换至编辑页
    this.isEditor = true;
  }

  Delete() {
    // console.log('[C03] Delete id =', this.data['ID']);
    // 提示确认删除
    this.pop.setConfirm({
      content:'确定删除此笔工程进度资料?',
      cancelTxt: '取消', 
      event: () => {
        // 删除指定通知纪录
        this.api.apiPost('EstateSchedule/Delete', { Id: this.data['ID'] }).then(
          (res) => {
            // console.log('[C03][API] EstateSchedule/Delete success', res);
            this.pop.setConfirm({ content: '删除成功' });
            // 重新查询工程进度资料
            this.getEstateScheduleList();
            // 返回列表页
            this.isEditor = false;
          },
          (err) => {
            // console.log('[E01][API] EstateSchedule/Delete error', err);
            this.pop.setConfirm({ content: err['msg'] });
          }
        );
      }
    });
  }

  Cancel() {
    // 返回列表页
    this.isEditor = false;
  }

  SubmitForm() {
    // console.log('[C03] SubmitForm');
    // 取得API参数物件
    let data = this.mainForm.value;
    console.log('[C03] api-data', data);

    if (data['ID']) {
      // 修改工程进度资料
      this.api.apiPost('EstateSchedule/Modify', data).then(
        (res) => {
          // console.log('[C03][API] EstateSchedule/Modify success', res);
          this.pop.setConfirm({ content: '修改成功' });
          // 重新查询工程进度资料
          this.getEstateScheduleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C03][API] EstateSchedule/Modify error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    } else {
      // 新增工程进度资料
      this.api.apiPost('EstateSchedule/Create', data).then(
        (res) => {
          // console.log('[C03][API] EstateSchedule/Create success', res);
          this.pop.setConfirm({ content: '新增成功' });
          // 重新查询工程进度资料
          this.getEstateScheduleList();
          // 返回列表页
          this.isEditor = false;
        },
        (err) => {
          // console.log('[C03][API] EstateSchedule/Create error', err);
          this.pop.setConfirm({ content: err['msg'] });
        }
      );
    }
  }

  gotoScheduleRecord(item) {
    // console.log('[C03] gotoScheduleRecord item', item);
    //加入暂存
    this.jslib.SetStorage('tempEstateSchedule', item);
    // 开启施工纪录管理功能
    this.router.navigate(['/C04']);
  }

  ReturnC01(){
    this.router.navigate(['/C01']);
    this.jslib.RemoveStorage('tempEstate');
  }
}
