import { Component, Input, OnInit } from '@angular/core';
import { navItems } from './../../_nav';
import { Router ,NavigationEnd} from '@angular/router';

import { Cinfig } from '../../../assets/config';
import { PopupService } from '../../share/component/popup/popup.service';
import { ApiService } from '../../share/service/api.service';
import { LitmitService } from '../../share/service/litmitservice';
import { filter } from 'rxjs/operators';
@Component({
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit{
  //public routesubscribe;
  public AllnavItems = navItems;
  public _nav = [];
  public userName = '';
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor(
    private litmit : LitmitService,
    private router : Router,
    private pop : PopupService,
    private api : ApiService
  ) {
      //濾掉沒權限的單元
      // this.routesubscribe = router.events.pipe(
      //   filter(event => event instanceof NavigationEnd)  
      // ).subscribe((event: NavigationEnd) => {
      //   var url = event.url.replace('/','');
      //   if(!litmit.Check(url + ',view')){
      //     this.router.navigate(['/dologin']);
      //   }
      // });
      this.SetMenuItem();
      this.changes = new MutationObserver((mutations) => {
        this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
      });
      this.changes.observe(<Element>this.element, {
        attributes: true
      });
  }

  ngOnInit(){
     
  }
  SetMenuItem(){
    var user = localStorage.getItem('user');
    if(user != null){
      user = JSON.parse(user);
      this.userName = user['Act'];
      this._nav = [];//this.AllnavItems;
      var permission = user['Permission'];
      this.AllnavItems.forEach((item,_index) => {
        var child = item['children'];
        if(child){
          var newchild = [];
          child.forEach(c => {
            var code = c.url.replace('/','');
            var pObj = permission.find(p => {return p.FuncCode == code});
            if(pObj && pObj.IsView){
              newchild.push(c);
            }
          });
          if(newchild.length > 0){
            var menuItem = Object.assign({}, this.AllnavItems[_index]);
            menuItem.children = newchild;
            this._nav.push(menuItem);
          }
        }
      });
    }
  }
  // ReSetPwd(){
  //   var user = localStorage.getItem('user');
  //   user = JSON.parse(user);

  //   this.pop.setResetPwd({event:(value)=>{
  //     console.log('reset',value);
  //     this.api.apiPost('Admin/Reset',{Id:user['Id'],newPwd:value}).then(
  //       (res)=>{
  //         this.pop.setConfirm({content:'储存成功',event:()=>{
  //           this.router.navigate(['/dologin']);
  //         }});
  //       },(error)=>{
  //         this.pop.setConfirm({content:error['msg']})
  //       }
  //     )
  //   }});
  // }
  LogOut(){
    this.router.navigate(['/dologin']);
  }
  
  //点选左上角logo不要有反应
  ngAfterViewInit(){
    var a_prev: any = document.getElementsByClassName('navbar-brand');
    a_prev[0].addEventListener("click", function(event){
      event.preventDefault();
    });
  }
  ngOnDestroy(){
   // this.routesubscribe.unsubscribe();
  }
}
