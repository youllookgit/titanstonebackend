import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BaseComponent} from '../../share/component/base.component';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';

@Component({
  templateUrl: 'baseform.component.html'
})


export class BaseformComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;
  public Query = {
    keyword : '',
    status : ''
  };
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit(){
   this.Submit();
  }

  public TableSet = [
    {key:'Act',name:'帐号'},
    {key:'Name',name:'名称'},
    {key:'Status',name:'状态'},
    {
      key:'Rule',
      name:'设定权限',
      style:{'text-align':'center'},
      icon:'fa-gear',
      event:(item)=>{
        console.log('设定权限',item);
      }
    },
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];

  Submit(){
    this.api.apiPost('Admin/GetAdmin',this.Query).then(
      (res)=>{
          this.Data = res;
      },(error)=>{
           console.log('[Fail]',error);
      });
  }
  
  Reset(){
    this.Query = {
      keyword : '',
      status : ''
    };
    this.Submit();
  }

  public IsEditor = false;
  Create(){
    
    this.IsEditor = true;
    //create form
    this.mainForm = new FormGroup({
      ID: new FormControl(null),
      Act:new FormControl(''),
      Name: new FormControl('',[Validators.required, Validators.maxLength(10)]),
      Pwd:new FormControl('',[Validators.required, Validators.minLength(6)]),
      Status:new FormControl(true)
    });
    console.log('Create',this.mainForm);
  }
  Cancel(){this.IsEditor = false; this.mainForm.reset();}
  Editor(item){
    this.IsEditor = true;
    console.log(item);
    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      Act: new FormControl(item['Act'],Validators.required),
      Name: new FormControl(item['Name'],[Validators.required, Validators.maxLength(10)]),
      Pwd:new FormControl(item['Pwd'],[Validators.required, Validators.minLength(6)]),
      Status:new FormControl(item['Status'])
    });
    console.log('Editor',this.mainForm);
  }
  FormSubmit(){
     var data = this.mainForm.value;
     
     var SendObj = {
      ID: data.ID,
      Act: data.Act,
      Name: data.Name,
      Pwd:data.Pwd,
      Status:data.Status,
     };
     this.api.apiPost('Admin/Create',SendObj).then((res)=>{
       this.pop.setConfirm({content:'储存成功',event:()=>{
        this.IsEditor = false;
        this.Submit();
       }});
     },(err)=>{
       this.pop.setConfirm({content:err['msg']});
     });


  }

  Delete(){
     var user = this.mainForm.value;
     this.pop.setConfirm({content:'确定删除 帐号' + user['Name'] + '?',cancelTxt: '取消', event:()=>{
       this.api.apiPost('Admin/Remove',{Id:user['ID']}).then(
         (res)=>{
          this.pop.setConfirm('删除成功!');
          this.IsEditor = false;
          this.Submit();
         },(err)=>{
           this.pop.setConfirm(err['msg']);
         }
       );
     }});
  };
}
