import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../../../share/component/base.component';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';

@Component({
  templateUrl: 'channel.component.html'
})


export class ChannelComponent extends BaseComponent implements OnInit {
  public channelType;
  public mainForm: FormGroup;
  public Query = {
    keyword : '',
    status : '',
    location: ''
  };
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
    // 取得選項
    this.api.apiPost('Option/GetByCode', { code:'channel' }).then(
      (res) => {
          this.channelType = res;   
      }
    );
  }

  ngOnInit(){
   this.Submit();
  }
  public TableSet = [
    {key:'Act',name:'帐号'},
    {key:'Name',name:'业务/渠道姓名'},
    {key:'dLocation',name:'所在地'},
    {key:'Unit',name:'单位'},
    {key:'Email',name:'Email'},
    {key:'Status',name:'状态'},
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];
  Submit(){
    this.api.apiPost('Channel/GetChannel',this.Query).then(
      (res)=>{
          this.Data = res;
      },(error)=>{
           console.log('[Fail]',error);
      });
  }
  
  Reset(){
    this.Query = {
      keyword : '',
      status : '',
      location: ''
    };
    this.Submit();
  }

  public IsEditor = false;
  Create(){
    
    this.api.apiPost('Channel/GetPermission',{ID:0}).then((res:any)=>{

      this.IsEditor = true;

      var plist = [];
      res.forEach(_p => {
        var pitem = {
          Name:new FormControl(_p['Name']),
          FuncCode:new FormControl(_p['FuncCode']),
          IsAdd : new FormControl(_p['IsAdd']),
          IsEdit : new FormControl(_p['IsEdit']),
          IsDelete : new FormControl(_p['IsDelete']),
          IsView : new FormControl(_p['IsView'])
        };
        plist.push(pitem);
      });
      
      this.mainForm = new FormGroup({
        ID: new FormControl({ value: null, disabled: true }),
        Act: new FormControl('', [
          Validators.required, 
          Validators.minLength(4),
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]), 
        Pwd: new FormControl('', [
          Validators.required, 
          Validators.minLength(6),
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]),
        Name: new FormControl('',[Validators.required, Validators.maxLength(50)]),
        Lacation:new FormControl('', Validators.required), 
        Unit: new FormControl(''),
        Phone: new FormControl(''),
        Email: new FormControl(''),
        Status:new FormControl(true),
        CreateDate: new FormControl({ value: '', disabled: true }),
        Permission:new FormControl(plist)
      });
    
    },(err)=>{console.log(err);});
  }

  Cancel(){this.IsEditor = false; this.mainForm.reset();}

  Editor(item){
   
    this.api.apiPost('Channel/GetPermission',{ID:item['ID']}).then((res:any)=>{
    
    this.IsEditor = true;
    //权限处理
    var plist = [];
    res.forEach(_p => {
      var pitem = {
        Name:new FormControl(_p['Name']),
        FuncCode:new FormControl(_p['FuncCode']),
        IsAdd : new FormControl(_p['IsAdd']),
        IsEdit : new FormControl(_p['IsEdit']),
        IsDelete : new FormControl(_p['IsDelete']),
        IsView : new FormControl(_p['IsView'])
      };
      plist.push(pitem);
    });

    this.mainForm = new FormGroup({
      ID: new FormControl(item['ID']),
      Act: new FormControl(item['Act'], [
        Validators.required, 
        Validators.minLength(4),
        Validators.maxLength(16),
        Validators.pattern('^[a-zA-Z0-9]*$')
      ]),
      Name: new FormControl(item['Name'],[Validators.required, Validators.maxLength(50)]),
      Pwd: new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(16),
        Validators.pattern('^[a-zA-Z0-9]*$')
      ]),
      Lacation:new FormControl(item['Lacation'], Validators.required), 
      Unit: new FormControl(item['Unit']),
      Phone: new FormControl(item['Phone']),
      Email: new FormControl(item['Email']),
      Status:new FormControl(item['Status']),
      CreateDate: new FormControl({ value: item['CreateDate'], disabled: true }),
      Permission:new FormControl(plist)
    });

    },(err)=>{console.log(err);});
  }
 
   FormSubmit(){
     var data = this.mainForm.value;
     //权限整理後送出
     var _p = [];
     var plist = data['Permission'];
     plist.forEach(pitem => {
       var _pobj = {
        Name:pitem['Name'].value,
        FuncCode:pitem['FuncCode'].value,
        IsAdd : pitem['IsAdd'].value,
        IsEdit : pitem['IsEdit'].value,
        IsDelete : pitem['IsDelete'].value,
        IsView : pitem['IsView'].value
       }
       _p.push(_pobj);
     });

     var SendObj = {
      ID: data.ID,
      Act: data.Act,
      Name: data.Name,
      Lacation:data.Lacation,
      Unit:data.Unit,
      Phone:data.Phone,
      Email:data.Email,
      Pwd:data.Pwd,
      Status:data.Status,
      Permission:_p
     };
    //console.log('Send Obj',SendObj);
     this.api.apiPost('Channel/Create',SendObj).then((res)=>{
       this.pop.setConfirm(
         {
         content:'储存成功',
         event:()=>{
          this.IsEditor = false;
          this.Submit();
        }
      });
     },(err)=>{
       this.pop.setConfirm({content:err['msg']});
     });


   }

   Delete(){
    
     var user = this.mainForm.value;
     this.pop.setConfirm({content:'确定删除 渠道帐号' + user['Name'] + '?',cancelTxt: '取消', event:()=>{
       this.api.apiPost('Channel/Remove',{Id:user['ID']}).then(
         (res)=>{
          this.pop.setConfirm('删除成功!');
          this.IsEditor = false;
          this.Submit();
         },(err)=>{
           this.pop.setConfirm(err['msg']);
         }
       );
     }});
   }
     //Permission
    public checkall = false;
    CheckAll(){
      this.checkall = !this.checkall;
      var p = this.mainForm.get('Permission').value;
      p.forEach(pitem => {
        pitem['IsAdd'].setValue(this.checkall);
        pitem['IsEdit'].setValue(this.checkall);
        pitem['IsDelete'].setValue(this.checkall);
        pitem['IsView'].setValue(this.checkall);
      });
    }
    public checkrow = false;
    CheckRow(pitem){
      this.checkrow = !this.checkrow;
      pitem['IsAdd'].setValue(this.checkrow);
      pitem['IsEdit'].setValue(this.checkrow);
      pitem['IsDelete'].setValue(this.checkrow);
      pitem['IsView'].setValue(this.checkrow);
    }

    Export(){
    
      window.open( this.baseUrl + 'Export/ChannelCSV', '_blank');
    }
}
