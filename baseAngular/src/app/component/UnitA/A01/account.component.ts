import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BaseComponent} from '../../../share/component/base.component';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';
@Component({
  templateUrl: 'account.component.html'
})


export class AccountComponent extends BaseComponent implements OnInit {

  public mainForm: FormGroup;
  public Query = {
    keyword : '',
    status : ''
  };
  public Data;
  constructor(
  ) {
    super();
    this.Data = [];
  }

  ngOnInit(){
   this.Submit();
  }

  public TableSet = [
    {key:'Act',name:'帐号'},
    {key:'Name',name:'名称'},
    {key:'Status',name:'状态'},
    {
      key:'Edit',
      name:'',
      icon:'fa-pencil-square-o',
      event:(item)=>{
        this.Editor(item);
      }
    }
  ];
  Submit(){
    this.api.apiPost('Admin/GetAdmin',this.Query).then(
      (res)=>{
          this.Data = res;
      },(error)=>{
           console.log('[Fail]',error);
      });
  }
  Reset(){
    this.Query = {
      keyword : '',
      status : ''
    };
    this.Submit();
  }
  public IsEditor = false;
  Create(){
    this.api.apiPost('Admin/GetPermission',{ID:0}).then((res:any)=>{

      this.IsEditor = true;

      var plist = [];
      res.forEach(_p => {
        var pitem = {
          Name:new FormControl(_p['Name']),
          FuncCode:new FormControl(_p['FuncCode']),
          IsAdd : new FormControl(_p['IsAdd']),
          IsEdit : new FormControl(_p['IsEdit']),
          IsDelete : new FormControl(_p['IsDelete']),
          IsView : new FormControl(_p['IsView'])
        };
        plist.push(pitem);
      });

      this.mainForm = new FormGroup({
        ID: new FormControl({ value: null, disabled: true }),
        Act: new FormControl('', [
          Validators.required, 
          Validators.minLength(4),
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]),
        Name: new FormControl('',[Validators.required, Validators.maxLength(50)]),
        Pwd: new FormControl('', [
          Validators.required,
          Validators.minLength(6), 
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]),
        Status:new FormControl(true),
        CreateDate: new FormControl({ value: '', disabled: true }),
        Permission:new FormControl(plist)
      });

    },(err)=>{console.log(err);});

    
  }
  Cancel(){this.IsEditor = false; this.mainForm.reset();}
  Editor(item){
    
    //权限处理
    this.api.apiPost('Admin/GetPermission',{ID:item['ID']}).then((res:any)=>{
  
      this.IsEditor = true;

      var plist = [];
      res.forEach(_p => {
        var pitem = {
          Name:new FormControl(_p['Name']),
          FuncCode:new FormControl(_p['FuncCode']),
          IsAdd : new FormControl(_p['IsAdd']),
          IsEdit : new FormControl(_p['IsEdit']),
          IsDelete : new FormControl(_p['IsDelete']),
          IsView : new FormControl(_p['IsView'])
        };
        plist.push(pitem);
      });
  
      this.mainForm = new FormGroup({
        ID: new FormControl({ value: item['ID'], disabled: true }),
        Act: new FormControl(item['Act'], [
          Validators.required, 
          Validators.minLength(4),
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]),
        Name: new FormControl(item['Name'],[Validators.required, Validators.maxLength(50)]),
        Pwd: new FormControl('', [
          Validators.minLength(6), 
          Validators.maxLength(16),
          Validators.pattern('^[a-zA-Z0-9]*$')
        ]),
        Status:new FormControl(item['Status']),
        CreateDate: new FormControl({ value: item['CreateDate'], disabled: true }),
        Permission:new FormControl(plist)
      });

    },(err)=>{console.log(err);});

  }
  FormSubmit(){
     var data = this.mainForm.getRawValue();
     
     var _p = [];
     var plist = data['Permission'];
     plist.forEach(pitem => {
       var _pobj = {
        Name:pitem['Name'].value,
        FuncCode:pitem['FuncCode'].value,
        IsAdd : pitem['IsAdd'].value,
        IsEdit : pitem['IsEdit'].value,
        IsDelete : pitem['IsDelete'].value,
        IsView : pitem['IsView'].value
       }
       _p.push(_pobj);
     });

     var SendObj = {
      ID: data.ID,
      Act: data.Act,
      Name: data.Name,
      Pwd:data.Pwd,
      Status:data.Status,
      Permission:_p
     };
     console.log('submit value',SendObj);
     this.api.apiPost('Admin/Create',SendObj).then((res)=>{
       this.pop.setConfirm({content:'储存成功',event:()=>{
        this.IsEditor = false;
        this.Submit();
       }});
     },(err)=>{
       this.pop.setConfirm({content:err['msg']});
     });
  }
  Delete(){
     var user = this.mainForm.getRawValue();
     this.pop.setConfirm({content:'确定删除 帐号' + user['Name'] + '?',cancelTxt: '取消', event:()=>{
       this.api.apiPost('Admin/Remove',{Id:user['ID']}).then(
         (res)=>{
          this.pop.setConfirm('删除成功!');
          this.IsEditor = false;
          this.Submit();
         },(err)=>{
           this.pop.setConfirm(err['msg']);
         }
       );
     }});
  };
  //Permission
  public checkall = false;
  CheckAll(){
    this.checkall = !this.checkall;
    var p = this.mainForm.get('Permission').value;
    p.forEach(pitem => {
      pitem['IsAdd'].setValue(this.checkall);
      pitem['IsEdit'].setValue(this.checkall);
      pitem['IsDelete'].setValue(this.checkall);
      pitem['IsView'].setValue(this.checkall);
    });
  }

  CheckRow(pitem){
    let isCheckedAll = (pitem['IsAdd'].value && pitem['IsEdit'].value && pitem['IsDelete'].value && pitem['IsView'].value);
    isCheckedAll = !isCheckedAll;
    pitem['IsAdd'].setValue(isCheckedAll);
    pitem['IsEdit'].setValue(isCheckedAll);
    pitem['IsDelete'].setValue(isCheckedAll);
    pitem['IsView'].setValue(isCheckedAll);
  }

  Export(){
    
    window.open( this.baseUrl + 'Export/AdminCSV', '_blank');
  }
}
