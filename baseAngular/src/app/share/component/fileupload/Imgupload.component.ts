
import { Component , OnChanges ,Input,Output,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'img-upload',
  templateUrl: './Imgupload.component.html'
})
export class ImgUploadComponent implements OnChanges  {
  @Input() Url : FormControl;
  constructor(
    public api : ApiService,
    private pop : PopupService
  ) {
   
  }
 
  acceptFile = 'image/png, image/gif, image/jpeg, image/bmp';
  fileUrl;
  ngOnChanges(){
    this.fileUrl = this.Url;
  }

  UploadHandler() {
    console.log('UploadHandler');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept',this.acceptFile);
  
    Fileinput.addEventListener('change', () =>  {
        if (Fileinput.files != null && Fileinput.files[0] != null) {
          //验大小
          console.log('Imageinput.files',Fileinput.files[0]);
          this.api.apiPostFile('HtmlEdit/ImageUpload',Fileinput.files[0]).then((url)=>{
            console.log('Upload res',url);
            this.fileUrl = url;
            //this.fileUrl = this.fileUrl.replace(/^.*[\\\/]/, '');
            // this.Source.setValue(url);
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }

  // pushImageToEditor(url,editor:any) {
  //   console.log('pushImageToEditor');
  //   const range = editor.getSelection(true);
  //   const index = range.index + range.length;
  //   editor.insertEmbed(range.index, 'image', url);
  // }
 
  // getEditorInstance(editorInstance:any) {
  //   console.log('editorInstance',editorInstance)
  //   let toolbar = editorInstance.getModule('toolbar');
  //   toolbar.addHandler('image', ()=>{
  //     this.imageHandler(editorInstance); 
  //   });
  // }
  // EditChange($event){
  //   this.Source.setValue($event);
  // }
}




