
import { Component , OnChanges ,Input,Output,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'file-upload',
  templateUrl: './fileupload.component.html'
})
export class FileUploadComponent implements OnChanges  {
  @Input() Source : FormControl;
  @Input() Type;
  constructor(
    public api : ApiService,
    private pop : PopupService
  ) {
   
  }
  fileUrl:any;
  acceptFile = '';
  fileName = '';
  ngOnChanges(){
    this.fileUrl = '';
    var _url = this.Source.value;
    if(this.Type == 'pdf'){
      this.fileUrl = this.Source.value;
      this.acceptFile = '.pdf';
      if(_url){
        this.fileName = _url.replace(/^.*[\\\/]/, '');
      }else{
        this.fileName = '';
      }
      

    }else{
      if(_url && _url != ''){
        this.fileUrl = this.Source.value;
        this.acceptFile = 'image/png, image/gif, image/jpeg, image/bmp';
        if(_url){
          this.fileName = _url.replace(/^.*[\\\/]/, '');
        }else{
          this.fileName = '';
        }
      }
    }
  }

  UploadHandler() {
    console.log('UploadHandler');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept',this.acceptFile);
  
    Fileinput.addEventListener('change', () =>  {
        if (Fileinput.files != null && Fileinput.files[0] != null) {
          //验大小
          console.log('Imageinput.files',Fileinput.files[0]);
          var apiMethod = (this.Type == 'pdf') ? 'PDFUpload' : 'ImageUpload';
          this.api.apiPostFile('HtmlEdit/' + apiMethod,Fileinput.files[0]).then((url)=>{
            console.log('Upload res',url);
            this.fileUrl = url;
            this.fileName = this.fileUrl.replace(/^.*[\\\/]/, '');
            this.Source.setValue(url);
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }

  // pushImageToEditor(url,editor:any) {
  //   console.log('pushImageToEditor');
  //   const range = editor.getSelection(true);
  //   const index = range.index + range.length;
  //   editor.insertEmbed(range.index, 'image', url);
  // }
 
  // getEditorInstance(editorInstance:any) {
  //   console.log('editorInstance',editorInstance)
  //   let toolbar = editorInstance.getModule('toolbar');
  //   toolbar.addHandler('image', ()=>{
  //     this.imageHandler(editorInstance); 
  //   });
  // }
  // EditChange($event){
  //   this.Source.setValue($event);
  // }
}




