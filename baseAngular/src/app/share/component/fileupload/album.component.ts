
import { Component , OnInit ,Input,Output,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-album',
  templateUrl: './album.component.html'
})
export class AlbumComponent implements OnInit  {

  open = false;
  _subscribe;
  fileUrl:any;
  acceptFile = 'image/png, image/gif, image/jpeg, image/bmp';
  callback;
  constructor(
    public api : ApiService,
    public pop : PopupService
  ) {
    this._subscribe = this.pop.AlbumSetting.subscribe(
      (setting: object) => {
        this.open = true;
        if(setting['data']){
          if(typeof setting['data'] == 'string' && setting['data'] != ''){
            this.fileUrl = JSON.parse(setting['data']);
          }else if(typeof setting['data'] == 'object'){
            this.fileUrl = setting['data'];
          }else{
            this.fileUrl = [];
          }
        }else{
          this.fileUrl = [];
        }
        this.callback = setting['event'];
      }
    );
  }

  ngOnInit() {

  }
  delete(index){
   this.pop.setConfirm({content:'确定删除?',cancelTxt: '取消', event:()=>{
     this.fileUrl.splice(index, 1);
   }});
  }
  Save(){
    this.open = false;
    this.callback(this.fileUrl);
  }
  Cancel(){
    this.open = false;
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
  // ngOnChanges(){
  //   this.fileUrl = '';
  //   var _url = this.Source.value;
  //   if(this.Type == 'pdf'){
  //     this.fileUrl = this.Source.value;
  //     this.acceptFile = '.pdf';
  //     this.fileName = _url.replace(/^.*[\\\/]/, '');

  //   }else{
  //     if(_url && _url != ''){
  //       this.fileUrl = this.Source.value;
  //       this.acceptFile = 'image/png, image/gif, image/jpeg, image/bmp';
  //       this.fileName = _url.replace(/^.*[\\\/]/, '');
  //     }
  //   }
  // }

  UploadHandler() {
    console.log('UploadHandler');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('multiple', 'multiple');
    Fileinput.setAttribute('accept',this.acceptFile);
  
    Fileinput.addEventListener('change', () => {
        if (Fileinput.files != null) {
          this.api.apiPostFileList('HtmlEdit/AlbumUpload',Fileinput.files).then((url)=>{
            console.log('Upload res',url);
            this.fileUrl = this.fileUrl .concat(url);
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }
}




