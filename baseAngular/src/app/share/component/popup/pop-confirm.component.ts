
import { Component } from '@angular/core';
import { PopupService } from './popup.service'

@Component({
  selector: 'app-pop-confirm',
  templateUrl: './pop-confirm.component.html'
})
export class PopConfirmComponent {
  public _subscribe;
  
  public open = false;
  public title = '';
  public content = '';
  public cancelTxt = '';
  public event: any;

  constructor(
    public pop: PopupService
  ) {
    // console.log('PopConfirmComponent constructor');
    this._subscribe = this.pop.confirnSetting.subscribe(
      (setting: object) => {
        this.open = true;
        this.title = setting['title'] || '';
        this.content = setting['content'] || '';
        this.cancelTxt = setting['cancelTxt'] || '';
        this.event = setting['event'];
      }
    );
  }

  Cancel() {
    this.open = false;
  }

  Submit() {
    if (typeof this.event == 'function') {
      this.event(true);
    }
    this.open = false;
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
}




