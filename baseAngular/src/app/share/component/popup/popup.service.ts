/**
 * 首页上选单
 */
import {Injectable, NgZone,EventEmitter} from '@angular/core';
@Injectable()
export class PopupService {
 public loadingStatus:EventEmitter<boolean> = new EventEmitter();
 public confirnSetting:EventEmitter<object> = new EventEmitter();
 public resetSetting:EventEmitter<object> = new EventEmitter();
 public transquerySetting:EventEmitter<object> = new EventEmitter();
 public AlbumSetting:EventEmitter<object> = new EventEmitter();
 public MultiEditSetting:EventEmitter<object> = new EventEmitter();
 constructor() {
  
 }
  setLoading(Isopen:boolean = true) {
   this.loadingStatus.emit(Isopen);
  }
  setConfirm(Setting:any) {
    if(typeof Setting == 'object'){
      this.confirnSetting.emit(Setting);
    }else if(typeof Setting == 'string'){
      this.confirnSetting.emit({content:Setting});
    }else{
      this.confirnSetting.emit(Setting);
    }
  }

  setResetPwd(Setting:object) {
    this.resetSetting.emit(Setting);
  }

  //TransQuery
  setTransQuery(Setting:object){
    this.transquerySetting.emit(Setting);
  }
  setAlbum(Setting:object){
    this.AlbumSetting.emit(Setting);
  }
  setMultiEdit(Setting:object){
    this.MultiEditSetting.emit(Setting);
  }
}
