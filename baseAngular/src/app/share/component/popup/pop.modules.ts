import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {PopupService} from './popup.service';

import {PopLoadingComponent} from './pop-loading.component';
import {PopConfirmComponent} from './pop-confirm.component';
import {PopResetComponent} from './pop-reset.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [ 
    PopLoadingComponent,
    PopConfirmComponent,
    PopResetComponent
  ],
  providers: [
    PopupService
  ],
  exports: [
    PopLoadingComponent,
    PopConfirmComponent,
    PopResetComponent
  ]
})

export class PopupComponentModule {
  constructor(){
    //console.log('PopupComponentModule Start >>');
  }
 }
