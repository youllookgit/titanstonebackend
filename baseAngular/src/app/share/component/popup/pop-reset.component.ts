
import { Component } from '@angular/core';
import { PopupService } from './popup.service'

@Component({
  selector: 'app-pop-reset',
  templateUrl: './pop-reset.component.html'
})
export class PopResetComponent {
  public _subscribe;
  public resetpwd = '';
  public open = false;
  public title = '';
  public content = '';
  public cancelTxt = '';
  public event: any;

  constructor(
    public pop: PopupService
  ) {
 
    // console.log('PopResetComponent constructor');
    this._subscribe = this.pop.resetSetting.subscribe(
      (setting: object) => {
        console.log('PopResetComponent Set',setting);
        this.open = true;
        this.event = setting['event'];
      }
    );
  }

  Cancel() {
    this.resetpwd ='';
    this.open = false;
  }

  Submit() {
    if(this.resetpwd.length >= 6){
      if (typeof this.event == 'function') {
        this.event(this.resetpwd);
        this.resetpwd ='';
      }
      this.open = false;
    }
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
}




