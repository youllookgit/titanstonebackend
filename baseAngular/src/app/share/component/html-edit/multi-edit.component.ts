
import { Component , OnInit} from '@angular/core';
import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'multi-edit',
  templateUrl: './multi-edit.component.html'
})
export class MultiEditComponent implements OnInit  {

  _type = 'object';
  open = false;
  data;
  _subscribe;
  callback;
  header;
  public IconShow = true;
  constructor(
    public api : ApiService,
    public pop : PopupService
  ) {
    this._subscribe = this.pop.MultiEditSetting.subscribe(
      (setting: object) => {
        console.log('setting',setting);
        this._type = 'object';
        if(setting['header']){
          this.header = setting['header'];
        }else{
          this.header = null;
        }
        if(setting['data']){
          if(typeof setting['data'] == 'string'){
            this._type = 'string';
            this.data = JSON.parse(setting['data']);
          }else if(typeof setting['data'] == 'object'){
            this.data = setting['data'];
          }else{
            this.data = [];
          }
          console.log('after data Handler',this.data);
        }else{
          this.data = [];
        }

        this.IconShow = true;
        if(typeof setting['icon'] == 'boolean'){
          this.IconShow = setting['icon'];
        }
        this.callback = setting['event'];
        this.open = true;
      }
    );
  }
  ngOnInit() {

  }
  Add(){
    this.data.push(
      {img:'',title:'',desc:''}
    );
  }
  Save(){
    //jq method
    var result = [];
    var length = $('#multiEdit tr').length;
    for(var i = 0;i < length-1;i++){
      var obj = {img:'',title:'',desc:'',order:''};
      obj.img = $('#multiEdit tr img').eq(i).attr('src');
      obj.title = $('#multiEdit tr [name="title"]').eq(i).val();
      obj.desc = $('#multiEdit tr [name="desc"]').eq(i).val();
      obj.order = $('#multiEdit tr [name="order"]').eq(i).val();
      obj.img = (obj.img) ? obj.img : '';
      obj.title = (obj.title) ? obj.title : '';
      obj.desc = (obj.desc) ? obj.desc : '';
      obj.order = (obj.order) ? obj.order : '0';
      if(obj.img != '' || obj.title != '' || obj.desc != ''){
        result.push(obj);
      }
      
    }
    if(typeof this.callback == 'function'){
      console.log('result',result);
      if(result.length == 0)
      {
        this.callback(undefined);
      }else{
        if(this._type == 'object'){
          this.callback(result);
        }else{
          this.callback(JSON.stringify(result));
        }
      }
    }
    this.open = false;
  }
  Delete(_index){
    //var tempD = this.data[_index];
    var obj = {img:'',title:'',desc:''};
    obj.img = $('#multiEdit tr img').eq(_index).attr('src');
    obj.title = $('#multiEdit tr [name="title"]').eq(_index).val();
    obj.desc = $('#multiEdit tr [name="desc"]').eq(_index).val();
    obj.img = (obj.img) ? obj.img : '';
    obj.title = (obj.title) ? obj.title : '';
    obj.desc = (obj.desc) ? obj.desc : '';

    if(obj.img == '' && obj.title == '' && obj.desc == ''){
      this.data.splice(_index, 1);
    }else{
      this.pop.setConfirm({content:'确定删除?',cancelTxt: '取消', event:()=>{
        this.data.splice(_index, 1);
      }});
    }
  }
  Cancel(){
    this.open = false;
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
}




