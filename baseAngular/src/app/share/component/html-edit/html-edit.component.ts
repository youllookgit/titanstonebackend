
import { Component , OnChanges ,Input,Output,EventEmitter,NgZone,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'html-edit',
  templateUrl: './html-edit.component.html'
})
export class HtmlEditFormContrlComponent implements OnChanges  {
  @Input() Source : FormControl;
  constructor(
    public api : ApiService,
    private pop : PopupService
  ) {
   
  }
  htmlContent = '';
  ngOnChanges(){
    //console.log('HtmlEditFormContrl ngOnChanges',this.Source);
    this.htmlContent = this.Source.value;
  }

  imageHandler(edit:any) {
    console.log('edit',edit);
    const Imageinput = document.createElement('input');
    Imageinput.setAttribute('type', 'file');
    Imageinput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp');
    Imageinput.classList.add('ql-image');
  
      Imageinput.addEventListener('change', () =>  {
        if (Imageinput.files != null && Imageinput.files[0] != null) {
          //验大小
          console.log('Imageinput.files',Imageinput.files[0]);
          this.api.apiPostFile('HtmlEdit/ImageUpload',Imageinput.files[0]).then((url)=>{
            console.log('ImageUpload res',url);
            this.pushImageToEditor(url,edit);
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Imageinput.click();
  }

  pushImageToEditor(url,editor:any) {
    console.log('pushImageToEditor');
    const range = editor.getSelection(true);
    const index = range.index + range.length;
    editor.insertEmbed(range.index, 'image', url);
  }
  // 具体富文本的配置信息，不需要用到的删除或注释掉即可
  //http://www.ngui.cc/news/show-5817.html
  public quill_config = 
  {
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons['bold', 'italic', 'underline', 'strike']
      //['blockquote', 'code-block'],
      //[{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      //[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      //[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
      //[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'size': [false, 'large'] }],
      //[{ 'header': [1] }],
      [{ 'color': [
        '#1DB6B4','#6EA551','#5B95C5','#F4BA34','#A6A6A6','#E17A3F',
        '#446EB6','#435466','#E8E6E7','#010101',
        '#CAF9F1','#97FFFF','#42DCD2','#1EC8C8','#157897',
        '#E5F3DA','#C0DBB2','#A6C78E','#557E3A','#38542B',
        '#DDEAF3','#BAD3E9','#9ABFDC','#3371AA','#204D76',
        '#FCEDCC','#F7DE9B','#F7D372','#BA8B23','#7B5E26',
        '#EDEDED','#DBDBDB','#C9C9C9','#7B7B7B','#525252',
        '#F7E4D5','#F1C8AC','#EBAD86','#BC5922','#7D3D21',
        '#D8DFF1','#B4C3E2','#8DA5D1','#2E528E','#1E3760',
        '#D4DBE5','#ABB7C7','#8494AD','#333E50','#282834',
        '#D0CECF','#ABABAB','#797977','#363638','#131315',
        '#7E7E7E','#5B5B5B','#39393B','#17171F','#08080A',
        '#AA1E27','#E62128','#F4BA34','#F4F075','#91C863',
        '#1EAA51','#186CB4','#1A2950','#673691'
      ] }],          // dropdown with defaults from theme [{ 'color': [] }, { 'background': [] }], 
      // [{ 'font': [] }],
      //[{ 'align': [] }],
      ['clean'],                                         // remove formatting button
      ['image']                       // link and image, video  ['link', 'image', 'video']
    ],
    //table:true
  };
  getEditorInstance(editorInstance:any) {
    //console.log('editorInstance',editorInstance)
    let toolbar = editorInstance.getModule('toolbar');
    toolbar.addHandler('image', ()=>{
      this.imageHandler(editorInstance); 
    });
  }
  EditChange($event){
    this.Source.setValue($event)
  }
}




