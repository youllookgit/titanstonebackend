
import { Component , OnChanges ,Input,Output,EventEmitter,NgZone,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { PopupService } from '../../component/popup/popup.service';
import { FormControl } from '@angular/forms';
declare var $;
@Component({
  selector: 'html-table',
  templateUrl: './html-table.component.html'
})
export class HtmlTableFormContrlComponent implements OnChanges  {
  @Input() Source : FormControl;
  constructor(
    public api : ApiService,
    private pop : PopupService
  ) {
   
  }
  htmlContent = '';
  ngOnChanges(){
    //console.log('HtmlEditFormContrl ngOnChanges',this.Source);
    this.htmlContent = this.Source.value;
  }

  imageHandler(edit:any) {
    console.log('edit',edit);
    const Imageinput = document.createElement('input');
    Imageinput.setAttribute('type', 'file');
    Imageinput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp');
    Imageinput.classList.add('ql-image');
  
      Imageinput.addEventListener('change', () =>  {
        if (Imageinput.files != null && Imageinput.files[0] != null) {
          //验大小
          console.log('Imageinput.files',Imageinput.files[0]);
          this.api.apiPostFile('HtmlEdit/ImageUpload',Imageinput.files[0]).then((url)=>{
            console.log('ImageUpload res',url);
            this.pushImageToEditor(url,edit);
          },(err)=>{
            this.pop.setConfirm({content:err['msg']});
          });
        }
    });
    Imageinput.click();
  }

  pushImageToEditor(url,editor:any) {
    console.log('pushImageToEditor');
    const range = editor.getSelection(true);
    const index = range.index + range.length;
    editor.insertEmbed(range.index, 'image', url);
  }
  // 具体富文本的配置信息，不需要用到的删除或注释掉即可
  //http://www.ngui.cc/news/show-5817.html
  public quill_config = 
  {
    toolbar: [
      ['bold', 'italic', 'underline'],        // toggled buttons['bold', 'italic', 'underline', 'strike']
      //['blockquote', 'code-block'],
      //[{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      //[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      //[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction
      //[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'size': [false, 'large'] }],
      [{ 'header': [1] }],
      [{ 'color': ['#232222','#57575A','#77777A','#6B5A95'] }],          // dropdown with defaults from theme [{ 'color': [] }, { 'background': [] }], 
      // [{ 'font': [] }],
      [{ 'align': [] }],
      ['clean'],                                         // remove formatting button
      ['image']                       // link and image, video  ['link', 'image', 'video']
    ],
    //table:true
  };
  getEditorInstance(editorInstance:any) {
    //console.log('editorInstance',editorInstance)
    let toolbar = editorInstance.getModule('toolbar');
    toolbar.addHandler('image', ()=>{
      this.imageHandler(editorInstance); 
    });
  }
  EditChange($event){
    this.Source.setValue($event)
  }
}




