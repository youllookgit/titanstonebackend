
import { Component ,OnChanges ,Input,Output,EventEmitter,NgZone,ViewChild,ElementRef} from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgControl } from "@angular/forms";
declare var $;
@Component({
  selector: 'tag-input',
  templateUrl: './tag.component.html'
})
export class TagInputComponent implements OnChanges  {
  @Input() DataList;
  @Input() Selected : FormControl;
  @Input() Create;
  
  @Output() DataChange = new EventEmitter();
  constructor(
    //public ngzone : NgZone
  ) {
   
  }
  @ViewChild('SelectTag') dom:ElementRef;
  public tempType = 'object';
  public selectizeObj;
  public selectedObj = [];
  ngOnChanges(){
    console.log('Tag Input',this.DataList);
    //参数调整 每个被选项目应该会在Source找的到 若无 新增选项
    this.tempType = 'object';
    var _s = this.Selected.value;
    if(_s == null || (typeof _s != 'object' && _s.length == 0)){
      _s = [];
    }//如果传 string , 区隔
    else if (typeof _s != 'object' && _s.length > 0)
    {
      this.tempType = 'string';
      _s = _s.split(',');
    }

    this.selectedObj = _s;

    if(this.selectedObj){
      this.selectedObj.forEach(_item => {
        var isExist = this.DataList.findIndex(f => {
          return f.value == _item;
        });
        if(isExist == -1){
          this.DataList.push({name:_item,value:_item});
        }
      });
    }
    this.selectizeObj = null;
    // this.ngzone.runOutsideAngular(()=>{
    // });
    setTimeout(()=>{
      console.log('onInitialize');
      //是否可动态新增
      var _create = (this.Create == false) ? false : true;
      this.selectizeObj = $(this.dom.nativeElement).selectize({
        create          : _create,
        plugins: ['remove_button'],
        onChange        : this.eventHandler('onChange'),
        //onItemAdd       : this.eventHandler('onItemAdd'),
        //onItemRemove    : this.eventHandler('onItemRemove'),
        //onOptionAdd     : this.eventHandler('onOptionAdd'),
        //onOptionRemove  : this.eventHandler('onOptionRemove'),
        //onDropdownOpen  : this.eventHandler('onDropdownOpen'),
        //onDropdownClose : this.eventHandler('onDropdownClose'),
        //onFocus         : this.eventHandler('onFocus'),
        //onBlur          : this.eventHandler('onBlur'),
        onInitialize    : function(name){return function() {console.log(name, arguments);};},
      });
      if(this.selectedObj && this.selectedObj.length > 0){
        this.selectizeObj[0].selectize.setValue(this.selectedObj);
      }
    },100)
  }
  
  eventHandler = function(name) {
    return (value)=>{
      // console.log(name, value);
      //this.DataChange.emit(value);
      if(value && this.tempType == 'string'){
        var _v = value.join(',');
        console.log('setValue',_v);
        this.Selected.setValue(_v);
      }else{
        console.log('setValue',value);
        this.Selected.setValue(value);
      }
      
    };
  };

}




