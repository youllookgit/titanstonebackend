
import { Component , OnChanges ,Input,Output,EventEmitter,NgZone,ViewChild,ElementRef} from '@angular/core';
import { FormControl,FormArray } from '@angular/forms';
declare var $;
@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
})
export class CheckboxComponent {
  @Input() DataList:any;
  @Input() Selected : FormControl;
  constructor(
  ) {
 
  }
  public Options;
  public formArray : FormArray
 
  ngOnChanges(){
    // console.log('Selected',this.Selected.value);
    // console.log('HtmlEditFormContrl ngOnChanges',this.DataList);
     
     if(this.DataList){
       this.Options = this.DataList;
       var opts = [];
       var ans = (this.Selected.value) ? this.Selected.value.split(',') : [];

       this.Options.forEach((item)=>{
         if(ans.includes(item.OptionValue)){
          opts.push(new FormControl(true));
         }else{
          opts.push(new FormControl(false));
         }
       });
       this.formArray = new FormArray(opts);
     }
  }
  get getformArr() {
    return this.formArray as FormArray;
  }
  Check(){
     var values = [];
     var result = this.formArray.value;
     result.forEach((ans,index) => {
       if(ans){
        values.push(this.Options[index].OptionValue)
       }
     });
     var resultStr = values.join(',')
     this.Selected.setValue(resultStr);
     this.Selected.updateValueAndValidity();
  }
}




