
import { Component , OnChanges ,Input,Output,EventEmitter,NgZone,ViewChild,ElementRef} from '@angular/core';

import { ApiService } from '../../service/api.service';
import { FormControl } from '@angular/forms';
declare var $;
declare var jslib;
@Component({
  selector: 'datatable',
  templateUrl: './datatable.component.html'
})
export class DataTableContrlComponent implements OnChanges  {
  @Input() Source:any;
  @Input() Setting:any;

  public Data;
  constructor(
    public api : ApiService
  ) {
   this.Data = [];
  }


  ngOnChanges(){
    //console.log('HtmlEditFormContrl ngOnChanges',this.Source);
    if(this.Setting){
      this.HeaderHandle(this.Setting);
    }
     if(this.Source){
       this.Data = this.Source;
       this.setPage();
     }
  }
  public headerSet;
  HeaderHandle(set){
    if(set){
      set.forEach(item=>{

        if(typeof item.style == 'undefined'){
          item.style = {};
        }

        if(typeof item.event == 'function'){
          item['type'] = 'func';
        };

        if(typeof item.type == 'undefined'){
          item['type'] = '';
        };
    
        if(item.key == 'Status' || item.key == 'Enabled'){
          item['type'] = 'status';
          //console.log('debug : ', item.isdefalut);
          if(item.isdefalut != 'undefined' && item.HandleType == true){
            item['type'] = 'status_v2';
          };

        }
      })
      this.headerSet = set;
    }
  }


  //page Used
  public PageData = [];
  public totalItems = 0;
  public currentPage = 1;
  public itemsPerPage = 10;
  public start = 0;
  public end = 0;
  pageChanged(event: any): void {
    var _start = 0;
    jslib.SetStorage('tempPage',event.page);
    if(event.page != 1){
      _start = Number(this.itemsPerPage) * (event.page - 1);
    } 
    var _end = Number(this.itemsPerPage) * event.page;
    this.PageData = this.Data.slice(_start,_end);
    this.start = _start;
    this.end = _end;
  }
  setPage(){
    this.totalItems = this.Data.length;
    this.currentPage = 1;
    var tempIndex = jslib.GetStorage('tempPage');
    if(tempIndex){
      this.currentPage = tempIndex;
      console.log('this.currentPage',this.currentPage)
      jslib.SetStorage('tempPage',1);
    }else{
      jslib.SetStorage('tempPage',1);
    }
    if(this.currentPage != 1){
      var _start = Number(this.itemsPerPage) * (this.currentPage - 1);
      var _end = Number(this.itemsPerPage) * this.currentPage;
      this.PageData = this.Data.slice(_start,_end);
      this.start = _start;
      this.end = _end;
    }else{
      this.PageData = this.Data.slice(0,Number(this.itemsPerPage));
      this.start = 0;
      this.end = this.itemsPerPage;
    }
    
    
    
  }
  PerPageSet(){
    this.setPage();
  }
}




