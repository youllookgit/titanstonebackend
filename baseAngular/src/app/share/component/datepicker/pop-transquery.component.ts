
import { Component } from '@angular/core';
import { PopupService } from '../../component/popup/popup.service'

@Component({
  selector: 'app-pop-transquery',
  templateUrl: './pop-transquery.component.html',
  styleUrls: ['./pop-transquery.component.css']
})
export class PopTransQueryComponent {
  private _subscribe;
  public Isopen = false;
  private event;
  public isTime = true;
  public timeTemp = '';
  constructor(
     private pop: PopupService
  ) {
    this._subscribe = this.pop.transquerySetting.subscribe(
      (setting: object) => {
        console.log('DatePicker setting',setting);
        this.Isopen = true;
        if(setting.hasOwnProperty('default')){
          var _d = setting['default'].split(' ');
          this.isTime = (_d.length == 2);
          if(this.isTime){
            this.timeTemp = _d[1];
          }else{
            this.timeTemp  = '';
          }
          this.Render(_d[0]);
        }else{
          var today = new Date();
          this.Render(today);
        }
        if(setting.hasOwnProperty('event')){
          this.event = setting['event'];
        }
        if(setting.hasOwnProperty('time')){
          this.isTime = setting['time'];
        }
      }
    );
    var today = new Date();
    this.Render(today);
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
  
  public Years = [];
  public Months = [1,2,3,4,5,6,7,8,9,10,11,12];
  public days = [];

  public default_y = 0;
  public default_m = 0;
  public default_d = 0;

  public select_Date = '';

  
  public hour = '';
  public mini = '';
  public sec = '';

  Render(default_date){
    //set time
    var t = this.timeTemp.split(':');
    if(t.length == 3){
       this.hour = t[0];
       this.mini = t[1];
       this.sec = t[2];
    }else{
      this.hour = '';
       this.mini = '';
       this.sec = '';
    }
    //date info
    var now = new Date();
    if(typeof default_date != 'undefined' && default_date != ''){
      now = new Date(default_date);
    }
    //get base info
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();

    //Setting Data
    this.default_y = year;
    this.default_m = month;
    this.default_d = day;
    this.select_Date = this.default_y + '/' + this.default_m + '/' + this.default_d;
    //Year Render
    this.Years = [];
    for(var i = year - 100 ; i <= year + 50 ; i++){
      this.Years.push(i);
    }
    //Day Render
    this.days = [];
    //count week
    var first_y = (this.default_y > 9) ? this.default_y + '' : '0' + this.default_y;
    var first_m = (this.default_m > 9) ? this.default_m + '' : '0' + this.default_m;
    var getfirst = new Date(first_y + '-' + first_m + '-' + '01');
    var default_week = getfirst.getDay();
    //上个月
    
    var lastmonth_days = this.daysInMonth(month - 1, year);
    var add_d = (lastmonth_days - default_week);
    for(var i =  add_d; i < lastmonth_days; i++){
      this.days.push(
        {
          text : i,
          enable:false
        });
    }
   
    for(var i = 1 ; i <= this.daysInMonth(month,year) ; i++){
      this.days.push(
        {
          text : i,
          enable:true,
          selected : ( i == this.default_d)
        });
    }
    //下个月
    var diff = 7 - (this.days.length % 7);
    for(var j = 1 ; j <= diff ; j++){
      this.days.push(
        {
          text : j,
          enable:false,
        });
    }
  //補足高度
  if(this.days.length == 35){
    for(var fix = 0 ; fix < 7 ; fix++){
      this.days.push(
        {
          text : '',
          class:''
        });
    }
  }
  }

  OnDataChange(param){
    if(param == '-'){
      this.default_y = (this.default_m - 1 < 1) ? this.default_y - 1 : this.default_y;
      this.default_m = (this.default_m - 1 < 1) ? 12 : this.default_m - 1;
    }
    if(param == '+'){
      this.default_y = (this.default_m + 1 > 12) ? this.default_y + 1 : this.default_y;
      this.default_m = (this.default_m + 1 > 12) ? 1 : this.default_m + 1;
    }
    var newDateStr = this.default_y + '/' + this.default_m + '/' + this.default_d;
    this.Render(newDateStr);
  }

  SelectDate(item){
    if(item['enable']){
      var _index =  this.days.findIndex(d => {return d['selected']});
      this.days[_index]['selected'] = false;
      item['selected'] = true;
      this.select_Date = this.default_y + '/' + this.default_m + '/' + item['text'];
    }
  }

  Cancel() {
    this.Isopen = false;
  }
  Clear() {
    this.event('');
    this.Isopen = false;
  }
  Submit() {
    var result = this.select_Date;
    var time = (this.hour == '') ? '00' : this.hour;
    time = time + ':' + ((this.mini == '') ? '00' : this.mini);
    time = time + ':' + ((this.sec == '') ? '00' : this.sec);
    if(this.isTime){
      result = result + ' ' + time;
    }
    this.event(result);
    this.Isopen = false;
  }

  daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }

  ChechH(){
    var n = Number(this.hour);
    if(isNaN(n)){
      this.hour = '';
    }else{
      if(n < 10)
        this.hour = '0' + n;
      else if(n > 24)
        this.hour = '23';
      else
        this.hour = '' + n;
    }
  }
  ChechM(){
    var n = Number(this.mini);
    if(isNaN(n)){
      this.mini = '';
    }else{
      if(n < 10)
        this.mini = '0' + n;
      else if(n > 59)
        this.mini = '59';
      else
        this.mini = '' + n;
    }
  }
  ChechS(){
    var n = Number(this.sec);
    if(isNaN(n)){
      this.sec = '';
    }else{
      if(n < 10)
        this.sec = '0' + n;
      else if(n > 59)
        this.sec = '59';
      else
        this.sec = '' + n;
    }
  }
}




