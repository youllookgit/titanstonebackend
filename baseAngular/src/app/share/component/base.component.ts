import { Component, OnInit , NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../share/service/api.service'
import { PopupService } from '../../share/component/popup/popup.service';
import {AppModule} from '../../app.module'
import {Cinfig} from '../../../assets/config'
import { LitmitService } from '../../share/service/litmitservice';
declare var jslib:any; //呼叫第三方js


@Component({
  template: ''
})
export class BaseComponent{
  public baseUrl = (Cinfig.DebugMode) ? Cinfig.localUrl : Cinfig.releaseUrl;
  public api : ApiService;
  public pop : PopupService;
  private litmit : LitmitService;
  public router : Router;
  public jslib = jslib;
  public ngzone : NgZone;
  constructor(  
 
  ) {
    this.api = AppModule.injector.get(ApiService);
    this.pop = AppModule.injector.get(PopupService);
    this.litmit = AppModule.injector.get(LitmitService);
    this.router = AppModule.injector.get(Router);
    this.ngzone = AppModule.injector.get(NgZone);
  }
  RouteRedirect(url){
    if(this.RouteLitmit(url)){
      this.router.navigate([url]);
    }
  }
  RouteLitmit(redirect){
    var url = redirect.replace('/','');
    var valid = this.litmit.Check(url + ',view');
    if(!valid){
      this.pop.setConfirm({content:'无权限检视此单元'});
    }
    return valid;
  }
}
