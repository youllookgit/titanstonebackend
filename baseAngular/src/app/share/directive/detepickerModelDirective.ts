import { Directive, HostListener, Input,Output,EventEmitter, Inject } from '@angular/core';
import { NgModel } from "@angular/forms";
import { PopupService } from '../component/popup/popup.service';

@Directive(
    {
         selector: '[datePickerNgModel]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class DatePickerNgModelDirective {
    public istime = false;
    @Input() ngModel;
    @Input() set datePickerNgModel(param){
        console.log('datePickerNgModel Input',param);
        this.istime = (param == 'full');
      }
    @Output() ngModelChange = new EventEmitter();
    constructor(
        public pop : PopupService
    ) {
    }
    preventDefault(event) {
        console.log('DatePickerNgModelDirective',this.ngModel);
        this.pop.setTransQuery({
            default: this.ngModel,
            time:this.istime,
            event : (date) => {
                console.log('event',date);
                this.ngModelChange.emit(date);
            }
        });
    }
}
