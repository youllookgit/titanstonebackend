import { Directive, ElementRef, HostListener, Input, Inject} from '@angular/core';
import { Router} from '@angular/router';
declare var jslib;
@Directive(
    {
         selector: '[litmit]'
    }
)
export class LitmitDirective {
    constructor(
        public el: ElementRef
    ) {
    }
    @Input() set litmit(param){
        if(!this.Check(param)){
            this.el.nativeElement.remove();
        }
    }

    Check(param){
        var p = param.split(',');
        var code = p[0]; //ex:A01
        var func = p[1]; //ex:view,add,edit,delete
        if(func == 'view'){
            func = 'IsView';
        }else if(func == 'add'){
            func = 'IsAdd';
        }else if(func == 'edit'){
            func = 'IsEdit';
        }else if(func == 'delete'){
            func = 'IsDelete';
        }

        var user = localStorage.getItem('user');
        user = JSON.parse(user);
        var permission = user['Permission'];
        var pObj = permission.find(item => {return item.FuncCode == code})
        if(pObj && pObj[func]){
            return true;
        }else{
            return false;
        }
    }
}
