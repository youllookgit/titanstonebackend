import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import { NgControl } from "@angular/forms";
import { PopupService } from '../component/popup/popup.service';

@Directive(
    {
         selector: '[datePicker]',
         host : {
            '(click)' : 'preventDefault($event)',
            '(input)': 'onEvent($event)',
          }
    }
)
export class DatePickerDirective {
    // @Input() formCtrl:NgControl;
    public istime = false;
    constructor(
        public el: ElementRef,
        public formCtrl : NgControl,
        public pop : PopupService
    ) {
    }
    @Input() set datePicker(param){
      this.istime = (param == 'full');
    }
    preventDefault(event) {
        console.log('preventDefault',event);
        let _value = this.el.nativeElement.value;
        this.pop.setTransQuery({
            default: _value,
            time:this.istime,
            event : (date) => {
              this.el.nativeElement.value = date;
              this.onEvent(true);
            }
        });
    }
   @HostListener('input',['$event']) onEvent($event){
    let valueToTransform = this.el.nativeElement.value;
    // do something with the valueToTransform
    this.formCtrl.control.setValue(valueToTransform);
  }
}
