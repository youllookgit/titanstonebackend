import { Injectable } from '@angular/core';
import { Http, Response,Headers } from '@angular/http';
import { PopupService } from '../component/popup/popup.service';
import { Cinfig } from '../../../assets/config';
import { Router} from '@angular/router';
import { LitmitService } from '../../share/service/litmitservice';
@Injectable()

export class ApiService {

  public Token = '';
  public baseUrl = (Cinfig.DebugMode) ? Cinfig.localUrl : Cinfig.releaseUrl;

  constructor(
    private http: Http,
    private pop : PopupService,
    private router : Router,
    private litmit : LitmitService,
  ) { }

  apiPost(url:string,param:any){
    
    

    this.pop.setLoading();
    var body = (param) ? param : {};
    return new Promise((resolve,reject)=>{
      //驗權限
      if(!this.RouteLitmit()){
        reject(
          {Success:false,
           msg:'无权限检视此单元'
          });
      }
      this.http.post(this.baseUrl + url,body,this.getOption()).toPromise().then(
        (res : Response) => {
          this.pop.setLoading(false);
          res = res.json();
          console.log(this.baseUrl +url,body,res);
          if(res['Success']){
            resolve(res['data']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.pop.setLoading(false);
          reject(error.json());
        }
      );
    });
    
  }
  //using form Data Send
  apiPostFile(url:string,file:File){

    this.pop.setLoading();

    let _formData = new FormData();
    _formData.append("upload", file);
    let body = _formData;

    console.log('[api Resquest',this.baseUrl +url,body);

    return new Promise((resolve,reject)=>{
      this.http.post(this.baseUrl + url,body).toPromise().then(
        (res : Response) => {
          this.pop.setLoading(false);
          res = res.json();
          console.log('[Repsonese]',res);
          if(res['success']){
            resolve(res['filepath']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.pop.setLoading(false);
          reject(error.json());
        }
      );
    });
    
  }

  //using form Data Send
  apiPostCSV(url:string,file:File){

    this.pop.setLoading();

    let _formData = new FormData();
    _formData.append("upload", file);
    let body = _formData;
    console.log('_formData',file);
    console.log('[api Resquest',this.baseUrl +url,body);

    return new Promise((resolve,reject)=>{
      // var user = localStorage.getItem('user');
      // user = JSON.parse(user);
      // let headers = new Headers();
      //   headers.append('Content-Type', 'multipart/form-data');
      //   headers.append('Accept', 'application/json');
      //   headers.append('Token', user['Token']);
      //   headers.append('LoginType', user['LoginType']);

      this.http.post(this.baseUrl + url,body).toPromise().then(
        (res : Response) => {
          this.pop.setLoading(false);
          res = res.json();
          console.log('[Repsonese]',res);
          if(res['success']){
            resolve(res['filepath']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.pop.setLoading(false);
          reject(error.json());
        }
      );
    });
    
  }

  //using form Data Send
  apiPostImport(url:string,file:File,param:any){

    this.pop.setLoading();
    var _param = (param) ? {} : param;

    let _formData = new FormData();
    _formData.append("upload", file);

    var keys = Object.keys(_param);
    if(keys.length > 0){
      for(var i = 0;i < keys.length;i++){
        _formData.append(keys[i], param[keys[i]]);
      }
    }

    let body = _formData;
    console.log('_formData',file);
    console.log('[api Resquest',this.baseUrl +url,body);

    return new Promise((resolve,reject)=>{
      // var user = localStorage.getItem('user');
      // user = JSON.parse(user);
      // let headers = new Headers();
      //   headers.append('Content-Type', 'multipart/form-data');
      //   headers.append('Accept', 'application/json');
      //   headers.append('Token', user['Token']);
      //   headers.append('LoginType', user['LoginType']);

      this.http.post(this.baseUrl + url,body).toPromise().then(
        (res : Response) => {
          this.pop.setLoading(false);
          res = res.json();
          console.log('[Repsonese]',res);
          if(res['success']){
            resolve(res['filepath']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.pop.setLoading(false);
          reject(error.json());
        }
      );
    });
    
  }
  //using form Data Send
  apiPostFileList(url:string,files:FileList){

    this.pop.setLoading();

    let _formData = new FormData();
    for(var i = 0; i < files.length; i++){
      _formData.append(files[i].name, files[i]);
    }
    // _formData.append("upload", file);
    let body = _formData;

    console.log('[api Resquest',this.baseUrl +url,body);

    return new Promise((resolve,reject)=>{
      this.http.post(this.baseUrl + url,body).toPromise().then(
        (res : Response) => {
          this.pop.setLoading(false);
          res = res.json();
          console.log('[Repsonese]',res);
          if(res['success']){
            resolve(res['filepath']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.pop.setLoading(false);
          reject(error.json());
        }
      );
    });
    
  }

  getOption(){
    var optionObj = {
      'LoginType':'',
      'Token':'',
      'Content-Type':'application/json'
    };
    var user = localStorage.getItem('user');
    if(user && user != ''){
      user = JSON.parse(user);
      optionObj.LoginType = user['LoginType'];
      optionObj.Token = user['Token'];
    }
   
    var headers = new Headers(optionObj);
    let options = {
      headers:headers
    };
    return options;
  }

  RouteLitmit(){
    var url = this.router.url.replace('/','');
    if(url == 'dologin'){
      return true;
    }
    var valid = this.litmit.Check(url + ',view');
    console.log('RouteLitmit url',url,valid);
    return valid;
  }
}
