import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

@Injectable()
export class LoadingService {
  public eventUpdated:EventEmitter<boolean> = new EventEmitter();
  public open = false;
  
  constructor() { }
  
  setLoading(Isopen:boolean) {
    this.open = Isopen;
    this.eventUpdated.emit(this.open);
  }

  getLoading(){
    return this.open;
  }

}
