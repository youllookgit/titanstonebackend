import { Injectable } from '@angular/core';

@Injectable()

export class LitmitService {

  constructor(
  ) { }

  Check(param){
    console.log('Check param',param);
    if(param.indexOf('doc') > -1){
        return true;
    }  
    var p = param.split(',');
    var code = p[0]; //ex:A01
    var func = p[1]; //ex:view,add,edit,delete
    if(func == 'view'){
        func = 'IsView';
    }else if(func == 'add'){
        func = 'IsAdd';
    }else if(func == 'edit'){
        func = 'IsEdit';
    }else if(func == 'delete'){
        func = 'IsDelete';
    }

    var user = localStorage.getItem('user');
    if(user!=null){
        user = JSON.parse(user);
        var permission = user['Permission'];
        var pObj = permission.find(item => {return item.FuncCode == code})
        if(pObj && pObj[func]){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
    
  }
}
