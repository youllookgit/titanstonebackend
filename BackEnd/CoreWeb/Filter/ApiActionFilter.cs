﻿using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Models;
using System;

namespace CoreWeb.Filter
{
    public class ApiActionFilter
    {
        public class ApiPermission : ActionFilterAttribute
        {
            public apiUserService us = new apiUserService();
            #region override 
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (filterContext.ActionDescriptor.IsDefined(typeof(IgnorePermissionAttribute), true))
                    return;

                var req = filterContext.HttpContext.Request;
                var Token = req.Headers["Token"];
                var Lang = req.Headers["lang"];

                if (string.IsNullOrEmpty(Token))
                {
                    //驗證User是否存在
                    BaseModel response = new BaseModel()
                    {
                        msg = "Token Error!"
                    };
                    ContentResult content = new ContentResult();
                    content.ContentType = "application/json";
                    content.Content = JsonConvert.SerializeObject(response);
                    filterContext.Result = content;
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    // 驗證Token是否有符合之會員資料
                    var user = us.CheckUser(Token);
                    if (user == null)
                    {
                        // 無符合之會員資料
                        BaseModel response = new BaseModel()
                        {
                            msg = "Token Error!"
                        };
                        ContentResult content = new ContentResult();
                        content.ContentType = "application/json";
                        content.Content = JsonConvert.SerializeObject(response);
                        filterContext.Result = content;
                        base.OnActionExecuting(filterContext);
                    }
                    else
                    {
                        // Session儲存當前登入之會員資料
                        apiUserService.SetUser(user);
                    }
                    apiUserService.SetLang(Lang);
                }
            }
            #endregion
        }
        
        public class IgnorePermissionAttribute : ActionFilterAttribute
        {
            public apiUserService us = new apiUserService();
            #region override 
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var req = filterContext.HttpContext.Request;
                var Token = req.Headers["Token"];
                var Lang = req.Headers["lang"];
                if (!string.IsNullOrEmpty(Token))
                {
                    // 驗證Token是否有符合之會員資料
                    var user = us.CheckUser(Token);
                    if (user != null)
                    {
                        // Session儲存當前登入之會員資料
                        apiUserService.SetUser(user);
                    }
                }
                else
                {
                    apiUserService.SetUser(null);
                }
                apiUserService.SetLang(Lang);

                if (filterContext.ActionDescriptor.IsDefined(typeof(IgnorePermissionAttribute), true))
                    return;
                
            }
            #endregion
        }
    }
}