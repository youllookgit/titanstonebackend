﻿using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Models;
using System;

namespace CoreWeb.Filter
{
    public class BEActionFilter
    {
        public class ApiPermission : ActionFilterAttribute
        {
            public AdminService us = new AdminService();
            #region override 
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (filterContext.ActionDescriptor.IsDefined(typeof(IgnorePermissionAttribute), true))
                    return;

                var req = filterContext.HttpContext.Request;
                var Token = req.Headers["Token"];
                var LoginType = req.Headers["LoginType"];

                if (string.IsNullOrEmpty(Token))
                {
                    //驗證User是否存在
                    BaseModel response = new BaseModel()
                    {
                        msg = "凭证异常，请重新登入"
                    };
                    ContentResult content = new ContentResult();
                    content.ContentType = "application/json";
                    content.Content = JsonConvert.SerializeObject(response);
                    filterContext.Result = content;
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    // 驗證Token是否有符合之會員資料
                    var user = us.CheckManager(Token, LoginType);
                    if (user == null)
                    {
                        // 無符合之會員資料
                        BaseModel response = new BaseModel()
                        {
                            msg = "凭证异常，请重新登入"
                        };
                        ContentResult content = new ContentResult();
                        content.ContentType = "application/json";
                        content.Content = JsonConvert.SerializeObject(response);
                        filterContext.Result = content;
                        base.OnActionExecuting(filterContext);
                    }
                    else
                    {
                        // Session儲存當前登入之會員資料
                        AdminService.SetAdmin(user,LoginType);
                    }
                }
            }
            #endregion
        }
        
        public class IgnorePermissionAttribute : ActionFilterAttribute
        {
            public apiUserService us = new apiUserService();
            #region override 
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (filterContext.ActionDescriptor.IsDefined(typeof(IgnorePermissionAttribute), true))
                    return;
            }
            #endregion
        }
    }
}