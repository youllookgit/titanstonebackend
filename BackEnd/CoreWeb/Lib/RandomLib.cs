﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoreWeb.Lib
{
    public class RandomLib
    {
        //產生隨機數字
        public static int IntRandom(int min,int max)
        {
            Random innerRnd = new Random();
            return innerRnd.Next(min, max);
        }
    }
}