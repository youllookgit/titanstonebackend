﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
namespace CoreWeb.Lib
{
    public class AppSetting
    {
        public static bool DebugMode()
        {
            var set = ConfigurationManager.AppSettings["DebugMode"];
            if (set != null)
            {
                if (set == "true")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string PushAuthorization()
        {
            var set = ConfigurationManager.AppSettings["PushAuthorization"];
            if (set != null)
            {
               return set;
            }
            else
            {
                return "";
            }
        }
    }
}