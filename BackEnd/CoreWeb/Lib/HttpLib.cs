﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json.Linq;
namespace CoreWeb.Lib
{
    public class HttpLib
    {
        private static bool CheckValidationResult(object sender,X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;// Always accept
        }
        public static Models.FacebookModel GetFaceBookRequest(string asses_token)
        {
            //訪問https需加上這句話  
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);
            //訪問http（不需要加上面那句話）  
            WebClient wc = new WebClient();
            wc.Credentials = CredentialCache.DefaultCredentials;
            wc.Encoding = Encoding.UTF8;
            string returnText = wc.DownloadString("https://graph.facebook.com/me?access_token=" + asses_token);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CoreWeb.Models.FacebookModel>(returnText);
        }

        public static Models.GooglePlusModel GetGoogleRequest(string asses_token)
        {
            //訪問https需加上這句話  
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(CheckValidationResult);
            //訪問http（不需要加上面那句話）  
            WebClient wc = new WebClient();
            wc.Credentials = CredentialCache.DefaultCredentials;
            wc.Encoding = Encoding.UTF8;
            string returnText = wc.DownloadString("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + asses_token);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CoreWeb.Models.GooglePlusModel>(returnText);
        }
    }
}