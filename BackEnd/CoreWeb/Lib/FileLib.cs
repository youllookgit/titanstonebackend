﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Web;
namespace CoreWeb.Lib
{
    public class FileLib
    {
        public static string rootUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;
        public static string rootSite = AppDomain.CurrentDomain.BaseDirectory;
        //本地開發用 本地開發記得切換過來 正式站為此架構
        //public static string rootPath = AppDomain.CurrentDomain.BaseDirectory;
        //遠端發佈測試站用 PS : TitanStone&UploadFile 為同級目錄
        public static string rootPath = (AppSetting.DebugMode()) ? AppDomain.CurrentDomain.BaseDirectory.Replace(@"TitanStone\", "") + @"TitanStone\" : AppDomain.CurrentDomain.BaseDirectory;

        public static string ImageUserFolder = "/UploadFile/user/";
        public static string ImageSaveFolder = "/UploadFile/edit/";
        public static string PDFSaveFolder = "/UploadFile/pdf/";
        //檔案是否存在
        public static bool IsFileExist(string FilePath)
        {
            if (File.Exists(FilePath))
                return true;
            else
                return false;
        }
        //建立檔案
        public static void CreateFile(string FilePath)
        {
            string FilaName = Path.GetFileName(FilePath);
            string Folder = FilePath.Replace(FilaName, "");
            if (IsFileExist(Folder))
            {
                File.CreateText(FilePath).Close();
            }
            else
            {
                Directory.CreateDirectory(Folder);
                File.CreateText(FilePath).Close();
            }

        }
        //寫入檔案 (含編碼)
        public static void WriteInFile(string FilePath, List<string> ContentList, System.Text.Encoding Encode)
        {
            //如果沒有就建立一個
            if (!IsFileExist(FilePath))
                CreateFile(FilePath);

            List<string> Content = new List<string>();
            //讀取
            StreamReader sr = new StreamReader(FilePath, Encode);
            while (!sr.EndOfStream)
            {
                Content.Add(sr.ReadLine());
            }
            sr.Close();
            //加入
            Content.AddRange(ContentList);

            WriteOverFile(FilePath, Content, Encode);
        }
        //重寫檔案
        public static void WriteOverFile(string FilePath, string Content)
        {
            //如果沒有就建立一個
            if (!IsFileExist(FilePath))
                CreateFile(FilePath);

            StreamWriter file = new System.IO.StreamWriter(FilePath);
            file.WriteLine(Content);

            file.Close();
        }
        //重寫檔案 (含編碼)
        public static void WriteOverFile(string FilePath, List<string> ContentList, System.Text.Encoding Encode)
        {
            //如果沒有就建立一個
            if (!IsFileExist(FilePath))
                CreateFile(FilePath);

            StreamWriter file = new System.IO.StreamWriter(FilePath, false, Encode);
            foreach (var row in ContentList)
            {
                file.WriteLine(row);
            }

            file.Close();
        }
        //讀取檔案
        public static List<string> ReadFileToList(string FilePath)
        {
            List<string> list = new List<string>();
            //記得指定編碼不然會亂碼
            using (StreamReader reader = new StreamReader(FilePath, System.Text.Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line); // Add to list.
                }
                reader.Close();
                reader.Dispose();
            }

            return list;
        }

        public static string ReadFile(string FilePath)
        {
            //記得指定編碼不然會亂碼
            string txtcontent = "";
            using (StreamReader reader = new StreamReader(FilePath, System.Text.Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    txtcontent += line; // Add to list.
                }
                reader.Close();
                reader.Dispose();
            }

            return txtcontent;
        }
        //讀取檔案 (含編碼)
        public static List<string> ReadFile(string FilePath, System.Text.Encoding Encode)
        {
            List<string> list = new List<string>();
            //記得指定編碼不然會亂碼
            using (StreamReader reader = new StreamReader(FilePath, Encode))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line); // Add to list.
                }
                reader.Close();
                reader.Dispose();
            }

            return list;
        }
        //刪除檔案
        public static void DeleteFile(string FilePath)
        {
            File.Delete(FilePath);
        }
        // 儲存圖片
        public static void WriteImage(string ImagePath, string base64Str)
        {
            try
            {
                int index = base64Str.IndexOf("base64,") != -1 ? base64Str.IndexOf("base64,") + 7 : 0;
                base64Str = base64Str.Substring(index).Trim();
                byte[] data = Convert.FromBase64String(base64Str);
                using (MemoryStream stream = new MemoryStream(data))
                {
                    Image image = Image.FromStream(stream);
                    image.Save(ImagePath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("圖片資料錯誤");
            }

        }
        //轉換圖片路徑至正式站
        public static string ImgPathToUrl(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                path = path.Replace("http://localhost:12005", rootUrl);
                path = path.Replace("$root", rootUrl);
                return path;
            }
            else
            {
                return "";
            }
        }
        //圖片路徑轉至儲存
        public static string ImgPathToSQL(string path)
        {
            if (path == null)
            {
                return "";
            }
            else
            {
                path = path.Replace("http://localhost:12005", "$root");
                path = path.Replace(FileLib.rootUrl, "$root");
                return path;
            }
        }
    }
}