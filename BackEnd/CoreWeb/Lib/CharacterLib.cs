﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace CoreWeb.Lib
{
    public class CharacterLib
    {
        //判斷傳入的字符是否是英文字母或數字
        public static bool IsNatural_Number(string str)
        {
           System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9]+$");
           return reg1.IsMatch(str);
        }
        // 判斷傳入的字符是否是數字
        public static bool Is_Number(string str)
        {
           System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
           return reg1.IsMatch(str);
        }

    }
}