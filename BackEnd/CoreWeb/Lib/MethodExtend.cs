﻿using System.Web;

namespace CoreWeb.Lib
{
    public static class StringExtension
    {
        public static string rootUrl = FileLib.rootUrl;
        //圖片路徑轉至SQL儲存路徑
        public static string ToSQL(this string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }
            else
            {
                path = path.Replace("http://localhost:12008", "$root");
                return path;
            }
        }
        //轉換圖片路徑至正式站
        public static string ToSiteUrl(this string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }
            else
            {
                path = path.Replace("http://localhost:12008", rootUrl);
                path = path.Replace("$root", rootUrl);
                return path;
            }
        }
        //轉換圖片路徑至實體路徑
        public static string ToRealUrl(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            else
            {
                string _path = str.Replace(rootUrl, "");
                return HttpContext.Current.Request.MapPath(_path);
            }
        }
    }
}