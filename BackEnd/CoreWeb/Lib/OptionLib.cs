﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

namespace CoreWeb.Lib
{
    public class OptionLib
    {
        //系統管理員權限
        public static Dictionary<string, string> PermissionDic = new Dictionary<string, string>()
        {
            { "A01", "管理员管理" },
            { "A02", "业务及渠道管理" },
            { "B01", "会员资料管理" },
            { "B02", "会员验证信管理" },
            { "B03", "投资意向表管理" },
            { "B04", "地产销售管理" },
            { "B05", "委託代管紀錄管理" },
            { "B06", "缴款纪录管理" },
            { "B07", "每月收益管理" },
            { "B08", "租赁纪录管理" },
            { "B09", "基金销售管理" },
            { "B10", "基金交易明细管理" },

            { "B11", "保险销售管理" },

            { "C01", "地产上稿管理" },
            { "C02", "房型管理" },
            { "C03", "工程进度管理" },
            { "C04", "施工纪录管理" },
            { "C05", "基金商品管理" },
            { "C06", "單位淨值管理" },
            { "C07", "財務報表管理" },

            { "C08", "保险商品管理" },

            { "D01", "理财趋势管理" },
            { "D02", "活动地区管理" },
            { "D03", "精选活动管理" },
            { "D04", "官方消息管理" },
            { "D05", "常见问题分类管理" },
            { "D06", "常见问题内容管理" },
            { "E01", "通知设定管理" },
            { "E02", "通知纪录管理" },
            { "E03", "联络我们管理" },
            { "E04", "活动报名管理" },
            { "E05", "收信人管理" }
        };
        //渠道管理員權限
        public static Dictionary<string, string> ChannelPermissionDic = new Dictionary<string, string>()
        {
            { "B01", "会员资料管理" },
            { "B03", "投资意向表管理" },
            { "B04", "地产销售管理" },
            { "B05", "委託代管紀錄管理" },
            { "B06", "缴款纪录管理" },
            { "B07", "每月收益管理" },
            { "B08", "租赁纪录管理" },
            { "B09", "基金销售管理" },
            { "B10", "基金交易明细管理" },
            { "B11", "保险销售管理" },
        };


        /// <summary>
        /// 活動類型管理
        /// </summary>
        public static Dictionary<string, string> ActTypeDic = new Dictionary<string, string>()
        {
            {"01", "活动"},
            {"02", "说明会"},
            {"03", "讲座"},
            {"04", "考察"}
        };


        /// <summary>
        /// 活動類型管理
        /// </summary>
        public static Dictionary<string, string> ActTypeEnDic = new Dictionary<string, string>()
        {
            {"01", "Activity"},
            {"02", "Seminar"},
            {"03", "Lecture"},
            {"04", "Inspection"}
        };
        /// <summary>
        /// 投資意向問題1
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion1 = new Dictionary<string, string>()
        {
            {"1", "基金" },
            {"2", "房地产" },
            {"3", "保险" },
            {"4", "其他" }
        };

        /// <summary>
        /// 投資意向問題2
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion2 = new Dictionary<string, string>()
        {
            {"1", "是" },
            {"0", "否" }
        };

        /// <summary>
        /// 投資意向問題3
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion3 = new Dictionary<string, string>()
        {
            {"1", "美洲市场" },
            {"2", "欧洲市场" },
            {"3", "亚洲市场" },
            {"4", "全球新兴市场" }
        };

        /// <summary>
        /// 投資意向問題4
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion4 = new Dictionary<string, string>()
        {
            {"1", "柬埔寨" },
            {"2", "越南" },
            {"3", "马来西亚" },
            {"4", "泰国" },
            {"5", "菲律宾" },
            {"6", "其它" }
        };

        /// <summary>
        /// 投資意向問題5
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion5 = new Dictionary<string, string>()
        {
            {"1", "能否安全退场" },
            {"2", "项目地点" },
            {"3", "开发商实力" },
            {"4", "首付款项" }
        };

        /// <summary>
        /// 投資意向問題6
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion6 = new Dictionary<string, string>()
        {
            {"1", "税务规划" },
            {"2", "储蓄报酬" },
            {"3", "资产传承" },
            {"4", "意外理赔" }
        };

        /// <summary>
        /// 投資意向問題7
        /// </summary>
        public static Dictionary<string, string> PreferenceQuestion7 = new Dictionary<string, string>()
        {
            {"1", "是" },
            {"0", "否" }
        };
        /// <summary>
        /// 地產銷售管理 車位類型
        /// </summary>
        public static Dictionary<string, string> EstateSale_ParkType = new Dictionary<string, string>()
        {
            {"''","" },
            {"1", "无车位" },
            {"2", "有车位" },
            {"3", "有车位，含於总价" }
        };

        /// <summary>
        /// 地產銷售管理 租貸狀態
        /// </summary>
        public static Dictionary<string, string> EstateSale_RentStatus = new Dictionary<string, string>()
        {
            {"''","" },
            {"1", "出租中" },
            {"2", "待租中" }
        };

        public static Dictionary<string, string> ContactUs_Status = new Dictionary<string, string>()
        {
            {"1", "待处理" },
            {"2", "待处理" },
            {"3", "已处理"  }
        };
    }
}