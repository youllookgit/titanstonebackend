﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Lib
{
    public class ApiErrorCode
    {
        public static Dictionary<int, string> ErrorCode = new Dictionary<int, string>()
        {
            { 1, "此帳號尚未註冊或格式錯誤" },
            { 2, "密碼有誤!" },
            { 3, "此帳號尚未驗證" },
            { 4, "請輸入正確的Email格式" },
            { 5, "此帳號已被註冊" },
            { 6, "密碼需有英文或數字，至少六個字元" },
            { 7, "密碼不能為空值" },
            { 8, "此帳號已認證" },
            { 9, "洗菜紀錄資料內容錯誤" },
            { 10, "缺少驗證Token" },
            { 11, "驗證Token已失效" },
            { 12, "註冊失敗" },
            { 99, "系統錯誤" }
        };
    }
}