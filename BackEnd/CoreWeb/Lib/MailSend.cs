﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace CoreWeb.Lib
{
    public class MailLib
    {
        public static void SendMail(string msg, string mysubject, string MailTo, string Mailcc = "")
        {
            var debug = AppSetting.DebugMode();
            if (debug)
            {
                MailMessage message = new MailMessage("youllook.manager@gmail.com", MailTo);//MailMessage(寄信者, 收信者)
                if (!string.IsNullOrEmpty(Mailcc))
                    message.Bcc.Add(Mailcc);//密件副本

                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;//E-mail編碼
                message.SubjectEncoding = Encoding.UTF8;//E-mail編碼
                message.Priority = MailPriority.Normal;//設定優先權
                message.Subject = mysubject;//E-mail主旨
                message.Body = msg;//E-mail內容

                SmtpClient MySmtp = new SmtpClient("smtp.gmail.com", 587);//設定gmail的smtp
                MySmtp.Credentials = new System.Net.NetworkCredential("youllook.manager@gmail.com", "B0982393454");//gmail的帳號密碼System.Net.NetworkCredential(帳號,密碼)
                MySmtp.EnableSsl = true;//開啟ssl
                MySmtp.Send(message);

                MySmtp = null;
                message.Dispose();
            }
            else
            {
                //巨石
                string smtp = "mail.titanstonegroup.com";
                int smtpPort = 25;
                string mailFrom = "system@titanstonegroup.com";
                MailMessage message = new MailMessage(mailFrom, MailTo);
                if (!string.IsNullOrEmpty(Mailcc))
                    message.Bcc.Add(Mailcc);//密件副本

                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;//E-mail編碼
                message.SubjectEncoding = Encoding.UTF8;//E-mail編碼
                message.Priority = MailPriority.Normal;//設定優先權
                message.Subject = mysubject;//E-mail主旨
                message.Body = msg;//E-mail內容

                SmtpClient MySmtp = new SmtpClient(smtp, smtpPort);
                // MySmtp.Credentials = new System.Net.NetworkCredential(mailAccount, mailPwd);
                // MySmtp.EnableSsl = true;//開啟ssl
                MySmtp.Send(message);

                MySmtp = null;
                message.Dispose();
            }
        }
    }
}