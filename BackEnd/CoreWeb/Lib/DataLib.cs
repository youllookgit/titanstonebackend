﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoreWeb.Lib
{
    public class DataLib
    {
        public static int[] StrToIntArray(string content)
        {
            string[] sa = content.Split(',');
            int[] ia = new int[sa.Length];
            for (int i = 0; i < ia.Length; ++i)
            {
                int j;
                string s = sa[i];
                if (int.TryParse(s, out j))
                {
                    ia[i] = j;
                }
            }
            return ia;
        }

        public static bool isPwdForamt(string pwd)
        {
            if (string.IsNullOrEmpty(pwd))
            {
                return false;
            }

            Regex regex = new Regex(@"^[a-zA-Z0-9]{6,12}$");
            
            return regex.Match(pwd).Success;
        }

        
    }
}