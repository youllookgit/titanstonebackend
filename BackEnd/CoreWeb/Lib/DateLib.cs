﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

namespace CoreWeb.Lib
{
    public class DateLib
    {
        static string[] week = new string[] { "日", "一", "二", "三", "四", "五", "六" };
        static string[] weekEn = new string[] { "Sun", "Mon", "Thu", "Wed", "Thir", "Fri", "Sat" };
      
        public static string DateToWeekName(DateTime date,string lang = "")
        {
            string result = "";
            if (lang == LangLib.en)
            {
                result = weekEn[(int)date.DayOfWeek];
            }
            else
            {
                result = week[(int)date.DayOfWeek];
            }
            return result;
        }
        public static string DateToWeekName(DateTime? date, string lang = "")
        {
            string result = "";
            if (date != null)
            {
                if (lang == LangLib.en)
                {
                    result = weekEn[(int)date.Value.DayOfWeek];
                }
                else
                {
                    result = week[(int)date.Value.DayOfWeek];
                }
                
            }
            return result;
        }
        public static int[] StrToIntArray(string content)
        {
            string[] sa = content.Split(',');
            int[] ia = new int[sa.Length];
            for (int i = 0; i < ia.Length; ++i)
            {
                int j;
                string s = sa[i];
                if (int.TryParse(s, out j))
                {
                    ia[i] = j;
                }
            }
            return ia;
        }
    }
}