﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CoreWeb.Lib
{
    public class Security
    {
        private static string KEY = "RELATION";
        public struct DESKeyPack
        {
            public byte[] Key, IV;

            public DESKeyPack(byte[] data)
            {
                Key = new byte[8];
                Buffer.BlockCopy(data, 0, Key, 0, 8);
                IV = new byte[8];
                Buffer.BlockCopy(data, 8, IV, 0, 8);
            }
        }
        private static DESKeyPack genKeyPack(string keyString)
        {
            //可以在不同的版本設不同的SALT值 //則不同版本的程式不能用來解密 
            const string salt = "SALT";
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] data = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(keyString + salt));
            md5.Clear();
            DESKeyPack dkp = new DESKeyPack(data);
            return dkp;
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="rawString"></param>
        /// <param name="keyString"></param>
        /// <returns></returns>
        public static string Encrypt(string rawString)
        {
            rawString = (!string.IsNullOrEmpty(rawString)) ? rawString : "";
            string keyString = KEY;
            DESKeyPack dkp = genKeyPack(keyString);
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            ICryptoTransform trans = des.CreateEncryptor(dkp.Key, dkp.IV);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, trans, CryptoStreamMode.Write);
            byte[] rawData = UTF8Encoding.UTF8.GetBytes(rawString);
            cs.Write(rawData, 0, rawData.Length);
            cs.Close();
            return Base64Encode(Convert.ToBase64String(ms.ToArray()));
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="encString"></param>
        /// <param name="keyString"></param>
        /// <returns></returns>
        public static string Decrypt(string encString)
        {
            encString = (!string.IsNullOrEmpty(encString)) ? encString : "";
            string keyString = KEY;
            encString = Base64Decode(encString);
            DESKeyPack dkp = genKeyPack(keyString);
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            ICryptoTransform trans = des.CreateDecryptor(dkp.Key, dkp.IV);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, trans, CryptoStreamMode.Write);
            byte[] rawData = Convert.FromBase64String(encString);
            cs.Write(rawData, 0, rawData.Length);
            cs.Close();
            return UTF8Encoding.UTF8.GetString(ms.ToArray());
        }

        public static string Base64Encode(string encString)
        {
            try
            {
                byte[] toEncodeAsBytes = Encoding.UTF8.GetBytes(encString);
                return Convert.ToBase64String(toEncodeAsBytes);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string Base64Decode(string toDecrypt)
        {
            try
            {
                byte[] encodedDataAsBytes = Convert.FromBase64String(toDecrypt.Replace(" ", "+"));
                return Encoding.UTF8.GetString(encodedDataAsBytes);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}