﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class InsuranceController : Controller
    {
        public BaseModel res = new BaseModel();
        public InsuranceService Srv = new InsuranceService();

        // 新增保險商品
        [HttpPost]
        public ActionResult Create(DbContent.Insurance req)
        {
            try
            {
                Srv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 查詢保險商品
        [HttpPost]
        public ActionResult GetList(string keyword,bool? status)
        {
            try
            {
                var data = Srv.GetList(keyword,status);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    NameEn = x.NameEn,
                    TypeName = x.TypeName,
                    TypeNameEn = x.TypeNameEn,
                    Note = x.Note,
                    Status = x.Status,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 修改保險商品
        [HttpPost]
        public ActionResult Modify(DbContent.Insurance req)
        {
            try
            {
                Srv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 刪除保險商品
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                Srv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}