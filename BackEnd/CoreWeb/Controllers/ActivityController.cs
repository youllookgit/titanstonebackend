﻿using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class ActivityController : Controller
    {
        public BaseModel res = new BaseModel();
        ActivityService activitySrv = new ActivityService();
        
        [HttpPost]
        //精選活動管理
        public ActionResult GetActivity(string keyword, bool? status, string acttypeid, string actareaid, string estateObjID)
        {
            try
            {
                var dataSource = activitySrv.GetActivity(keyword, status, acttypeid, actareaid, estateObjID);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    ActAddr = x.ActAddr,
                    ActAddrEn = x.ActAddrEn,
                    ActArea = x.ActArea,
                    ActAreaName = activitySrv.GetActAreaName(x.ActArea),
                    ActDate = x.ActDate.HasValue ? x.ActDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    ActPosition = x.ActPosition,
                    ActPositionEn = x.ActPositionEn,
                    ActType = x.ActType,
                    BeginDate = x.BeginDate.HasValue ? x.BeginDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Brief = x.Brief,
                    BriefEn = x.BriefEn,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    Detail = FileLib.ImgPathToUrl(x.Detail),
                    DetailEn = FileLib.ImgPathToUrl(x.DetailEn),
                    EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    EstateObjIDs = x.EstateObjIDs,
                    Img = FileLib.ImgPathToUrl(x.Img),
                    Name = x.Name,
                    NameEn = x.NameEn,
                    OrderNo = x.OrderNo,
                    Status = x.Status,
                    Process = (x.Process == null) ? "" : x.Process,
                    ProcessEn = (x.ProcessEn == null) ? "" : x.Process,
                    Traffic = FileLib.ImgPathToUrl(x.Traffic),
                    TrafficEn = FileLib.ImgPathToUrl(x.TrafficEn),
                    ActTypeName = activitySrv.GetActTypeName(x.ActType)

                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult GetActTypeData()
        {
            ActOption _data = new ActOption();
            try
            {
                var dataSource = OptionLib.ActTypeDic.ToList();
                _data.ActType = dataSource;

                var dataActArea = activitySrv.GetActArea();
                var collection = dataActArea.Select(x => new {
                    value = x.OptionValue,
                    name = x.Name
                });

                _data.ActArea = collection;
                res.data = _data;
                res.Success = true;
            }
            catch(Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(DbContent.Activity data)
        {
            try
            {
                activitySrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                activitySrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}