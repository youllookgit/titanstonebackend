﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class BenefitRecordController : Controller
    {
        public BaseModel res = new BaseModel();
        public BenefitRecordService benefitRecordSrv = new BenefitRecordService();
        public OptionService optSrv = new OptionService();
        /// <summary>
        /// 新增每月收益
        /// </summary>
        /// <param name="req">每月收益資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.BenefitRecord req)
        {
            try
            {
                benefitRecordSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢每月收益紀錄
        /// </summary>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="payStartDate">撥款日期查詢起日</param>
        /// <param name="payEndDate">撥款日期查詢迄日</param>
        /// <param name="EstateObjID">地產物件ID</param>
        /// <param name="EstateSaleID">地產銷售紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string startDate, string endDate, string payStartDate, string payEndDate, int? EstateObjID, int? EstateSaleID)
        {
            try
            {
                var payStatuses = optSrv.GetListByCode("AllotStatus");
                var data = benefitRecordSrv.GetList(startDate, endDate, payStartDate, payEndDate, EstateObjID, EstateSaleID);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateObjID = x.EstateObjID,
                    EstateSaleID = x.EstateSaleID,
                    RentDataID = x.RentDataID,
                    CharterType = x.CharterType,
                    RenterName = x.RenterName,
                    RenterGender = x.RenterGender,
                    RenterIdNo = x.RenterIdNo,
                    RenterBirth = x.RenterBirth.HasValue ? x.RenterBirth.Value.ToString("yyyy/MM/dd") : "",
                    Income = x.Income,
                    Expense = x.Expense,
                    PayDate = x.PayDate.HasValue ? x.PayDate.Value.ToString("yyyy/MM/dd") : "",
                    IncomeDate = x.IncomeDate.HasValue ? x.IncomeDate.Value.ToString("yyyy/MM/dd") : "",
                    PdfFile = x.PdfFile,
                    Note = x.Note,
                    PayStatus = x.PayStatus,
                    dPayStatus = (payStatuses.Where(p => p.OptionValue == x.PayStatus).FirstOrDefault() != null) ? (payStatuses.Where(p => p.OptionValue == x.PayStatus).First().Name) : x.PayStatus,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定每月收益紀錄
        /// </summary>
        /// <param name="req">每月收益資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.BenefitRecord req)
        {
            try
            {
                benefitRecordSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定每月收益紀錄
        /// </summary>
        /// <param name="ID">每月收益紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                benefitRecordSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(int EstateObjID, int EstateSaleID, List<ImportModel.BenefitRecordData> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                benefitRecordSrv.Import(EstateObjID, EstateSaleID, data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}