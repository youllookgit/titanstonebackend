﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Filter;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System.Collections.Generic;
using static CoreWeb.Models.ApiModel;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.IgnorePermissionAttribute]
    public class HtmlEditController : Controller
    {
        //圖片上傳
        [HttpPost]
        public ActionResult ImageUpload(HttpPostedFileBase upload, string filename)
        {
            if (upload != null)
            {
                string fileSavePath = "";
                try
                {
                    string fileRealName = Guid.NewGuid().ToString("N");
                    var nameExtend = upload.FileName.Split('.');
                    if (string.IsNullOrEmpty(filename))
                    {
                        filename = upload.FileName.Replace("." + nameExtend[nameExtend.Length - 1], "");
                    }
                    var newFileName = fileRealName + "." + nameExtend[nameExtend.Length - 1];
                    newFileName = newFileName.Trim(' ');
                    fileSavePath = Lib.FileLib.rootPath + Lib.FileLib.ImageSaveFolder + newFileName;
                   fileSavePath = fileSavePath.Trim(' ');

                   //Save Image
                   System.Drawing.Image sourceimage =
                          System.Drawing.Image.FromStream(upload.InputStream);
                    sourceimage.Save(fileSavePath);
                    sourceimage.Dispose();
                    return Json(new { success = true, filepath = FileLib.rootUrl + FileLib.ImageSaveFolder + newFileName });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, msg = ex.Message + "path:" + fileSavePath });
                }

            }
            else
            {
                return Json(new { Success = false, msg = "File is Null!" });
            }

        }

        //圖片上傳
        [HttpPost]
        public ActionResult PDFUpload(HttpPostedFileBase upload, string filename)
        {
            if (upload != null)
            {
                try
                {
                    string fileRealName = Guid.NewGuid().ToString("N");
                    var nameExtend = upload.FileName.Split('.');
                    if (string.IsNullOrEmpty(filename))
                    {
                        filename = upload.FileName.Replace("." + nameExtend[nameExtend.Length - 1], "");
                    }
                    var newFileName = fileRealName + "." + nameExtend[nameExtend.Length - 1];
                    newFileName = newFileName.Trim(' ');
                    var fileSavePath = Lib.FileLib.rootPath + Lib.FileLib.PDFSaveFolder + newFileName;
                    fileSavePath = fileSavePath.Trim(' ');
                    //Save PDF
                    upload.SaveAs(fileSavePath);
                    return Json(new { success = true, filepath = FileLib.rootUrl + FileLib.PDFSaveFolder + newFileName });

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, msg = ex.Message });
                }

            }
            else
            {
                return Json(new { Success = false, msg = "File is Null!" });
            }

        }

        //圖片上傳
        [HttpPost]
        public ActionResult AlbumUpload()
        {
            var files = Request.Files;
            if (files != null)
            {
                try
                {
                    string result = "";
                    string[] allKeys = files.AllKeys;
                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase upload = Request.Files[file] as HttpPostedFileBase;
                        string filename = upload.FileName;
                        string fileRealName = Guid.NewGuid().ToString("N");
                        var nameExtend = upload.FileName.Split('.');
                        if (string.IsNullOrEmpty(filename))
                        {
                            filename = upload.FileName.Replace("." + nameExtend[nameExtend.Length - 1], "");
                        }
                        var newFileName = fileRealName + "." + nameExtend[nameExtend.Length - 1];
                        newFileName = newFileName.Trim(' ');
                        var fileSavePath = Lib.FileLib.rootPath + Lib.FileLib.ImageSaveFolder + newFileName;
                        fileSavePath = fileSavePath.Trim(' ');

                        //Save Image
                        System.Drawing.Image sourceimage =
                               System.Drawing.Image.FromStream(upload.InputStream);
                        sourceimage.Save(fileSavePath);
                        result += FileLib.rootUrl + FileLib.ImageSaveFolder + newFileName + ",";
                        //return Json(new { success = true, filepath = FileLib.rootUrl + FileLib.ImageSaveFolder + newFileName });
                    }
                    var resultArray = result.TrimEnd(',').Split(',');
                    return Json(new { success = true, filepath = resultArray });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, msg = ex.Message });
                }

            }
            else
            {
                return Json(new { Success = false, msg = "File is Null!" });
            }

        }
    }
}