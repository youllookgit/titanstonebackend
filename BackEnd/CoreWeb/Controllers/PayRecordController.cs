﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class PayRecordController : Controller
    {
        public BaseModel res = new BaseModel();
        public PayRecordService payRecordSrv = new PayRecordService();
        public OptionService optSrv = new OptionService();
        /// <summary>
        /// 新增繳款
        /// </summary>
        /// <param name="req">繳款資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.PayRecord req)
        {
            try
            {
                payRecordSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢繳款紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="type">繳款狀態</param>
        /// <param name="EstateObjID">地產物件ID</param>
        /// <param name="EstateSaleID">地產銷售紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, string type, int? EstateObjID, int? EstateSaleID)
        {
            try
            {
                var payStatuses = optSrv.GetListByCode("PayStatus");
                var data = payRecordSrv.GetList(keyword, startDate, endDate, type, EstateObjID, EstateSaleID);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateObjID = x.EstateObjID,
                    EstateSaleID = x.EstateSaleID,
                    EstateScheduleID = x.EstateScheduleID,
                    Amount = x.Amount,
                    Note = x.Note,
                    PayStatus = x.PayStatus,
                    dPayStatus = (payStatuses.Where(p => p.OptionValue == x.PayStatus).FirstOrDefault() != null) ? (payStatuses.Where(p => p.OptionValue == x.PayStatus).First().Name) : x.PayStatus,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定繳款紀錄
        /// </summary>
        /// <param name="req">繳款資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.PayRecord req)
        {
            try
            {
                payRecordSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定繳款紀錄
        /// </summary>
        /// <param name="ID">繳款紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                payRecordSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(int EstateObjID, int EstateSaleID, List<ImportModel.PayRecordData> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                payRecordSrv.Import(EstateObjID, EstateSaleID, data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}