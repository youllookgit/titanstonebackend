﻿using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FaqItemController : Controller
    {
        public BaseModel res = new BaseModel();
        FaqItemService faqitemSrv = new FaqItemService();

        [HttpPost]
        //常見問題內容管理
        public ActionResult GetFaqItem(string keyword,int? faqsortid, bool? status, DateTime? startdate, DateTime? enddate)
        {
            try
            {
                var dataSource = faqitemSrv.GetFaqItem(keyword, faqsortid, status, startdate, enddate);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Status = x.Status,
                    Title = x.Title,
                    TitleEn = x.TitleEn,
                    Detail = FileLib.ImgPathToUrl(x.Detail),
                    DetailEn = FileLib.ImgPathToUrl(x.DetailEn),
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo,
                    FaqSortName = x.FaqSortName,
                    FaqSortID = x.FaqSortID

                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(DbContent.FaqItem data)
        {
            try
            {
                faqitemSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                faqitemSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}