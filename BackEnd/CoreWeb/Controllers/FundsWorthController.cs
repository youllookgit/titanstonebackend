﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FundsWorthController : Controller
    {
        public BaseModel res = new BaseModel();
        public FundsWorthService fundsWorthSrv = new FundsWorthService();

        /// <summary>
        /// 新增單位淨值
        /// </summary>
        /// <param name="req">單位淨值資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.FundsWorth req)
        {
            try
            {
                fundsWorthSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢單位淨值紀錄
        /// </summary>
        /// <param name="min">查詢淨值最小值</param>
        /// <param name="max">查詢淨值最大值</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="FundsID">基金商品ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(double? min, double? max, string startDate, string endDate, int? FundsID)
        {
            try
            {
                var data = fundsWorthSrv.GetList(min, max, startDate, endDate, FundsID);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    FundsID = x.FundsID,
                    WorthDate = x.WorthDate.HasValue ? x.WorthDate.Value.ToString("yyyy/MM/dd") : "",
                    WorthValue = x.WorthValue,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定單位淨值紀錄
        /// </summary>
        /// <param name="req">單位淨值資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.FundsWorth req)
        {
            try
            {
                fundsWorthSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定單位淨值紀錄
        /// </summary>
        /// <param name="ID">單位淨值紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                fundsWorthSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}