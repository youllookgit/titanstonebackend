﻿using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class EmailLogController : Controller
    {
        public BaseModel res = new BaseModel();
        public EmailLogService emaillogSrv = new EmailLogService();
        public OptionService optSrv = new OptionService();
        //會員驗證信管理
        [HttpPost]
        public ActionResult GetEmailLog(string keyword, string startDate, string endDate, bool? status, string type)
        {
            try
            {
                var emailType = optSrv.GetListByCode("EmailType");
                var dataSource = emaillogSrv.GetEmailLog(keyword, startDate, endDate, status, type);
                
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Status = x.Status,
                    TypeCode = OptionService.GetOptName(emailType, x.TypeCode),
                    Email = x.Email,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    CheckDate = x.CheckDate.HasValue ? x.CheckDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty         
                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定會員驗證信紀錄
        /// </summary>
        /// <param name="ID">會員驗證信紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                emaillogSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}