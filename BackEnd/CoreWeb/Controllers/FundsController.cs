﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FundsController : Controller
    {
        public BaseModel res = new BaseModel();
        public FundsService fundsSrv = new FundsService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增基金商品
        /// </summary>
        /// <param name="req">基金商品資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.Funds req)
        {
            try
            {
                fundsSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢基金商品紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="status">啟用狀態</param>
        /// <param name="fundType">基金類型</param>
        /// <param name="buyType">申購方式</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, bool? status, string fundType, string buyType)
        {
            try
            {
                var fundTypeList = optSrv.GetListByCode("FundType");
                var buyTypeList = optSrv.GetListByCode("FundBuyType");
                string name = null;
                string value = null;
                foreach (var item in buyTypeList)
                {
                    name = name == null ? item.Name : (name + "," + item.Name);
                    value = value == null ? item.OptionValue : (value + "," + item.OptionValue);
                }
                buyTypeList.Add(new DbContent.Option() { Name = name, OptionValue = value });

                var data = fundsSrv.GetList(keyword, status, fundType, buyType);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    FundType = x.FundType,
                    dFundType = fundTypeList.Where(p => p.OptionValue == x.FundType).FirstOrDefault().Name,
                    BuyType = x.BuyType,
                    dBuyType = buyTypeList.Where(p => p.OptionValue == x.BuyType).FirstOrDefault().Name,
                    Status = x.Status,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定基金商品紀錄
        /// </summary>
        /// <param name="req">基金商品資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.Funds req)
        {
            try
            {
                fundsSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定基金商品紀錄
        /// </summary>
        /// <param name="ID">基金商品紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                fundsSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}