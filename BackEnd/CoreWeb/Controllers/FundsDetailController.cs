﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FundsDetailController : Controller
    {
        public BaseModel res = new BaseModel();
        public FundsDetailService fundsSaleSrv = new FundsDetailService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增基金交易明細
        /// </summary>
        /// <param name="req">基金交易明細資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.FundsDetail req)
        {
            try
            {
                fundsSaleSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢基金交易明細紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="redemptionMin">贖回淨值最小值</param>
        /// <param name="redemptionMax">贖回淨值最大值</param>
        /// <param name="purchaseMin">申購淨值最小值</param>
        /// <param name="purchaseMax">申購淨值最大值</param>
        /// <param name="txnType">交易類型</param>
        /// <param name="txnStartDate">交易日期查詢起日</param>
        /// <param name="txnEndDate">交易日期查詢迄日</param>
        /// <param name="FundsSaleID">基金銷售ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate,
            double? redemptionMin, double? redemptionMax, double? purchaseMin, double? purchaseMax,
            string txnType, string txnStartDate, string txnEndDate, int? FundsSaleID)
        {
            try
            {
                var txnTypeList = optSrv.GetListByCode("FundTxnType");

                var data = fundsSaleSrv.GetList(keyword, startDate, endDate, redemptionMin, redemptionMax, purchaseMin, purchaseMax, txnType, txnStartDate, txnEndDate, FundsSaleID);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    FundsSaleID = x.FundsSaleID,
                    TxnType = x.TxnType,
                    dTxnType = txnTypeList.Where(p => p.OptionValue == x.TxnType).FirstOrDefault().Name,
                    TxnNo = x.TxnNo,
                    TxnDate = x.TxnDate.HasValue ? x.TxnDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    Unit = x.Unit,
                    RedemptionVal = x.RedemptionVal,
                    RedemptionAmt = x.RedemptionAmt,
                    PrefFee = x.PrefFee,
                    PurchaseVal = x.PurchaseVal,
                    PurchaseAmt = x.PurchaseAmt,
                    HandlingFee = x.HandlingFee,
                    DividendAmt = x.DividendAmt,
                    PayStatus = x.PayStatus,
                    Note = x.Note,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定基金交易明細紀錄
        /// </summary>
        /// <param name="req">基金交易明細資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.FundsDetail req)
        {
            try
            {
                fundsSaleSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定基金交易明細紀錄
        /// </summary>
        /// <param name="ID">基金交易明細紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                fundsSaleSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(List<ImportModel.FundSaleDetail> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                fundsSaleSrv.Import(data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}