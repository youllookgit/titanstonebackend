﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class EstateObjController : Controller
    {
        public BaseModel res = new BaseModel();
        public EstateObjService objectSrv = new EstateObjService();

        /// <summary>
        /// 新增地產物件
        /// </summary>
        /// <param name="req">地產物件資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.EstateObj req)
        {
            try
            {
                objectSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢地產物件紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="status">啟用狀態</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="type">銷售類型</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, bool? status, string startDate, string endDate, string type)
        {
            try
            {
                var data = objectSrv.GetList(keyword, status, startDate, endDate, type);

                string[] emptyArray = new string[] { };
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    SaleType = x.SaleType,
                    MainImg = x.MainImg,
                    Brief = x.Brief,
                    Price = x.Price,
                    SpecTag = (string.IsNullOrEmpty(x.SpecTag)) ? emptyArray : x.SpecTag.Split(','),
                    VideoUrl = x.VideoUrl,
                    VideoImg = x.VideoImg,
                    Country = x.Country,
                    EstateType = x.EstateType,
                    Address = x.Address,
                    FloorDesc = x.FloorDesc,
                    FloorImg = x.FloorImg,
                    CompleteDate = x.CompleteDate.HasValue ? x.CompleteDate.Value.ToString("yyyy/MM/dd") : "",
                    CaseSpec = FileLib.ImgPathToUrl(x.CaseSpec),
                    FileUrl = x.FileUrl,
                    OnShelfDate = x.OnShelfDate.HasValue ? x.OnShelfDate.Value.ToString("yyyy/MM/dd hh:mm:ss") : "",
                    OffShelfDate = x.OffShelfDate.HasValue ? x.OffShelfDate.Value.ToString("yyyy/MM/dd hh:mm:ss") : "",
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo,
                    Status = x.Status,
                    Around = x.Around,
                    Device = x.Device,
                    Service = x.Service,
                    Album = x.Album,
                    //en add
                    NameEn = x.NameEn,
                    BriefEn = x.BriefEn,
                    SpecTagEn = (string.IsNullOrEmpty(x.SpecTagEn)) ? emptyArray : x.SpecTagEn.Split(','),
                    CountryEn = x.CountryEn,
                    EstateTypeEn = x.EstateTypeEn,
                    CaseSpecEn = FileLib.ImgPathToUrl(x.CaseSpecEn),
                    AroundEn = x.AroundEn,
                    DeviceEn = x.DeviceEn,
                    ServiceEn = x.ServiceEn,
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定地產物件紀錄
        /// </summary>
        /// <param name="req">地產物件資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.EstateObj req)
        {
            try
            {
                objectSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定地產物件紀錄
        /// </summary>
        /// <param name="ID">地產物件紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                objectSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //聯絡我們取得名稱用
        [HttpPost]
        public ActionResult GetNameByID(int? id)
        {
            try
            {
                string name = objectSrv.GetNameByID(id);
                res.Success = true;
                res.data = new { Name=name };
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}