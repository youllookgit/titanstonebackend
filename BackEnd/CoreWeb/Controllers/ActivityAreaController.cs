﻿using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class ActivityAreaController : Controller
    {
        public BaseModel res = new BaseModel();
        ActivityAreaService actareaSrv = new ActivityAreaService();
        [HttpPost]
        //常見問題內容管理
        public ActionResult GetActivityArea(string keyword, bool? status)
        {
            try
            {
                var dataSource = actareaSrv.GetActivityArea(keyword, status);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    NameEn = x.NameEn,
                    Enabled = x.Enabled,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo,

                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(DbContent.Option data)
        {
            try
            {
                actareaSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                actareaSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}