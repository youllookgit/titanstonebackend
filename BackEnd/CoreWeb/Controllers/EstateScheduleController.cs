﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class EstateScheduleController : Controller
    {
        public BaseModel res = new BaseModel();
        public EstateScheduleService scheduleSrv = new EstateScheduleService();

        /// <summary>
        /// 新增工程進度
        /// </summary>
        /// <param name="req">工程進度資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.EstateSchedule req)
        {
            try
            {
                scheduleSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢工程進度紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="EstateObjID">地產物件ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, int? EstateObjID)
        {
            try
            {
                var data = scheduleSrv.GetList(keyword, EstateObjID);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateObjID = x.EstateObjID,
                    SchNo = x.SchNo,
                    Name = x.Name,
                    Brief = x.Brief,
                    SchNoEn = x.SchNoEn,
                    NameEn = x.NameEn,
                    BriefEn = x.BriefEn,
                    ScheduleType = x.ScheduleType,
                    CompleteDate = x.CompleteDate.HasValue ? x.CompleteDate.Value.ToString("yyyy/MM/dd") : "",
                    RealComplete = x.RealComplete.HasValue ? x.RealComplete.Value.ToString("yyyy/MM/dd") : "",
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定工程進度紀錄
        /// </summary>
        /// <param name="req">工程進度資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.EstateSchedule req)
        {
            try
            {
                scheduleSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定工程進度紀錄
        /// </summary>
        /// <param name="ID">工程進度紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                scheduleSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}