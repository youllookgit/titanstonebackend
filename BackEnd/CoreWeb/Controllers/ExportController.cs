﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Filter;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System.Reflection;
using System.IO;
using System.Text;
using static CoreWeb.Models.ApiModel;
using ExcelDataReader;

namespace CoreWeb.Controllers
{
    [ApiActionFilter.IgnorePermission]
    public class ExportController : Controller
    {
        public ExportService exService = new ExportService();
        [HttpGet]
        public ActionResult AdminCSV()
        {
            var list = (List<AdminModel>)ExportService.GetExport();
            if (list != null)
            {
                SysPrmisnService syspsrv = new SysPrmisnService();
                //設定標題
                string title = @"ID,
                            帳號,
                            人員名稱,
                            {permissiontitle},
                            建立時間,
                            狀態";

                //add permission title
                string permissiontitlt = "";
                foreach (var item in OptionLib.PermissionDic)
                {
                    permissiontitlt += "權限管理(" + item.Key + "_view),";
                    permissiontitlt += "權限管理(" + item.Key + "_add),";
                    permissiontitlt += "權限管理(" + item.Key + "_edit),";
                    permissiontitlt += "權限管理(" + item.Key + "_delete),";
                }
                title = title.Replace("{permissiontitle}", permissiontitlt.TrimEnd(','));
                //整理資料
                List<ExportModel.AdminData> export = new List<ExportModel.AdminData>();
                foreach (var item in list)
                {
                    ExportModel.AdminData data = new ExportModel.AdminData()
                    {
                        ID = item.ID.ToString(),
                        Act = item.Act,
                        Name = item.Name,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        Status = item.Status.ToString()
                    };
                    var _p = syspsrv.GetPermission("Admin", item.ID.Value);
                    _p = _p.OrderBy(p => p.FuncCode).ToList();
                    //add permission
                    string pstr = "";
                    foreach (var pitem in _p)
                    {
                        pstr += pitem.IsView.ToString() + ",";
                        pstr += pitem.IsAdd.ToString() + ",";
                        pstr += pitem.IsEdit.ToString() + ",";
                        pstr += pitem.IsDelete.ToString() + ",";
                    }
                    pstr = pstr.TrimEnd(',');
                    data.Permission = pstr;
                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult ChannelCSV()
        {
            var list = (List<ChannelEditModel>)ExportService.GetExport();
            if (list != null)
            {
                SysPrmisnService syspsrv = new SysPrmisnService();
                //設定標題
                string title = @"帐号,
                            渠道姓名,
                            所在地,
                            Email,
                            联络电话,
                            单位,
                            {permissiontitle},
                            建立時間,
                            狀態";//整理資料

                //add permission title
                string permissiontitlt = "";
                foreach (var item in OptionLib.ChannelPermissionDic)
                {
                    permissiontitlt += "權限管理(" + item.Key + "_view),";
                    permissiontitlt += "權限管理(" + item.Key + "_add),";
                    permissiontitlt += "權限管理(" + item.Key + "_edit),";
                    permissiontitlt += "權限管理(" + item.Key + "_delete),";
                }
                title = title.Replace("{permissiontitle}", permissiontitlt.TrimEnd(','));

                List<ExportModel.ChannelData> export = new List<ExportModel.ChannelData>();
                foreach (var item in list)
                {
                    var _p = syspsrv.GetPermission("Channel", item.ID.Value);
                    _p = _p.OrderBy(p => p.FuncCode).ToList();
                    //add permission
                    string pstr = "";
                    foreach (var pitem in _p)
                    {
                        pstr += pitem.IsView.ToString() + ",";
                        pstr += pitem.IsAdd.ToString() + ",";
                        pstr += pitem.IsEdit.ToString() + ",";
                        pstr += pitem.IsDelete.ToString() + ",";
                    }
                    pstr = pstr.TrimEnd(',');

                    ExportModel.ChannelData data = new ExportModel.ChannelData()
                    {
                        Act = item.Act,
                        Name = item.Name,
                        dLocation = GetOption("channel", item.Lacation),
                        Email = item.Email,
                        Phone = "'" + item.Phone + "'",
                        Unit = item.Unit,
                        Permission = pstr,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        Status = item.Status.ToString()
                    };

                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult MemberCSV()
        {
            var list = (List<DbContent.User>)ExportService.GetExport();
            if (list != null)
            {
                ChannelService channelSrv = new ChannelService();
                //設定標題
                string title = @"帐号,
                            Email验证,
                            姓名,
                            ID/身分证号码,
                            生日,
                            国籍,
                            联络电话,
                            所属渠道名称,
                            建立時間,
                            狀態";

                //var channel_data = channelSrv.GetChannel("", "", null);
                //channel_data = channel_data.OrderBy(p => p.ID).ToList();

                ////add permission title
                //string channellt = "";
                //foreach (var item in channel_data)
                //{
                //    channellt += "所屬渠道(" + item.Name + "),";
                //}
                //title = title.Replace("{channel}", channellt);

                List<ExportModel.Member> export = new List<ExportModel.Member>();
                foreach (var item in list)
                {
                    ExportModel.Member data = new ExportModel.Member()
                    {
                        Act = item.Act,
                        Valid = item.Valid.ToString(),
                        Name = item.Name,
                        IdentityNo = item.IdentityNo,
                        Birth = item.Birth.HasValue ? item.Birth.Value.ToString("yyyy/MM/dd") : "",
                        Country = GetOption("Country", item.Country),
                        Phone = "'" + item.PhonePrefix + " " + item.Phone + "'",
                        Channel = exService.GetChannelName(item.Channel),
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        Status = item.Status.ToString()
                    };
                    //item.Channel = (item.Channel == null) ? "" : item.Channel;
                    //var channel_ary = item.Channel.Split(',');
                    //var _p = channelSrv.GetChannel("", "", null);
                    //_p = _p.OrderBy(p => p.ID).ToList();

                    //add permission
                    //string pstr = "";

                    //foreach (var _c in _p)
                    //{
                    //    if (channel_ary.Contains(_c.ID.ToString()))
                    //    {
                    //        pstr += "True,";
                    //    }
                    //    else
                    //    {
                    //        pstr += "False,";
                    //    }
                    //}

                    //data.Channel = pstr;
                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult PreferenceCSV()
        {
            var list = (List<PreferenceModel>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"Email,
                            姓名,
                            ID/身分证号码	,
                            更新時間,
                            您曾经投资过哪些产品？,
                            请问您是否有海外投资经验及规划？,
                            请问您目前看好的投资目标市场？,
                            请问您有曾经投资过东南亚哪些国家？,
                            请问您购买海外地产时，最重视哪些因素？,
                            请问您作保险规划时，优先考虑的因素？,
                            请问您是否会主动与理财专员做资产检视及咨询？";

                List<ExportModel.Preference> export = new List<ExportModel.Preference>();
                foreach (var item in list)
                {
                    ExportModel.Preference data = new ExportModel.Preference()
                    {
                        Email = item.Email,
                        Name = item.Name,
                        IdentityNo = item.IdentityNo,
                        UpdateDate = item.UpdateDate.HasValue ? item.UpdateDate.Value.ToString("yyyy/MM/dd") : ""
                    };
                    //string pstr = string.Empty;
                    foreach (PreferenceItem oitem in item.ItemData)
                    {
                        //取q1答案內容
                        if (oitem.PrefernceName.Equals("q1", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q1 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                string sAnsName = string.Empty;
                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion1.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q1 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q1 = data.q1.TrimEnd('&');
                        }

                        //取q2答案內容
                        if (oitem.PrefernceName.Equals("q2", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q2 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    string sAnsName = string.Empty;
                                    if (OptionLib.PreferenceQuestion2.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q2 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q2 = data.q2.TrimEnd('&');
                        }
                        //取q3答案內容
                        if (oitem.PrefernceName.Equals("q3", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q3 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {

                                string sAnsName = string.Empty;
                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion3.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q3 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q3 = data.q3.TrimEnd('&');
                        }

                        //取q5答案內容
                        if (oitem.PrefernceName.Equals("q4", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q4 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                string sAnsName = string.Empty;

                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion4.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q4 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q4 = data.q4.TrimEnd('&');
                        }

                        //取q5答案內容
                        if (oitem.PrefernceName.Equals("q5", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q5 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                string sAnsName = string.Empty;

                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion5.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q5 += sAnsName;
                                    }
                                }
                            }
                            data.q5 = data.q5.TrimEnd('&');
                        }

                        //取q6答案內容
                        if (oitem.PrefernceName.Equals("q6", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q6 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                string sAnsName = string.Empty;
                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion6.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q6 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q6 = data.q6.TrimEnd('&');
                        }

                        //取q7答案內容
                        if (oitem.PrefernceName.Equals("q7", StringComparison.OrdinalIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(oitem.Ans))
                            {
                                data.q7 = "";
                                continue;
                            }
                            string[] ans = oitem.Ans.Split(',');
                            for (int i = 0; i < ans.Length; i++)
                            {
                                string sAnsName = string.Empty;

                                if (string.IsNullOrEmpty(ans[i]) == false)
                                {
                                    if (OptionLib.PreferenceQuestion7.TryGetValue(ans[i], out sAnsName))
                                    {
                                        data.q7 += sAnsName + "&";
                                    }
                                }
                            }
                            data.q7 = data.q7.TrimEnd('&');
                        }
                    }

                    //data.PreferenceItem = pstr;
                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult EstatesaleCSV()
        {
            var list = (List<EstateSaleModel>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"物件ID,
                            购买人ID,
                            所属渠道名称,
                            购买人姓名,
                            物件名称,
                            房型,
                            地址,
                            楼层,
                            单位面积,
                            套内面积,
                            公设面积,
                            购买总价,
                            签约日期,
                            系统通知缴款,
                            是否有车位,
                            车位价格,
                            租赁状态,
                            建立时间,
                            状态";

                List<ExportModel.EstateSale> export = new List<ExportModel.EstateSale>();
                foreach (var item in list)
                {
                    ExportModel.EstateSale data = new ExportModel.EstateSale()
                    {
                        EstateNo = item.EstateNo,
                        UserIdentityNo = item.UserIdentityNo.Replace(',', '，'),
                        Channel = exService.GetChannelName(item.Channel),
                        BuyerName = item.BuyerName,
                        EstateObjName = item.EstateObjName,
                        EstateRoomName = item.EstateRoomName,
                        Addr = item.Addr.Replace(",", ""),
                        Floor = item.Floor,
                        UnitArea = item.UnitArea,
                        RoomArea = item.RoomArea,
                        PublicArea = item.PublicArea,
                        PriceSum = item.PriceSum.HasValue ? item.PriceSum.Value.ToString() : "",
                        SignDate = item.SignDate.HasValue ? item.SignDate.Value.ToString() : "",
                        SystemStatus = "否", //TODO DB沒欄位

                        ParkingPrice = item.ParkingPrice.HasValue ? item.ParkingPrice.Value.ToString() : "",

                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        Status = item.Status.ToString()
                    };

                    string value = string.Empty;
                    OptionLib.EstateSale_ParkType.TryGetValue(item.ParkingType, out value);
                    data.ParkingType = value;
                    value = string.Empty;
                    OptionLib.EstateSale_RentStatus.TryGetValue(item.RentStatus, out value);
                    data.RentStatus = value;

                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult ContactUsCSV()
        {
            var list = (List<DbContent.ContactUs>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"联络人姓名,
                            Email,
                            国籍,
                            联络电话,
                            Line/WeChatID,
                            联络主题,
                            选择项目,
                            联络时段,
                            内容,
                            回覆内容,
                            处理状态,
                            处理人员,
                            回复时间,
                            备注";
                List<ExportModel.ContactUs> export = new List<ExportModel.ContactUs>();

                foreach (var item in list)
                {
                    ExportModel.ContactUs data = new ExportModel.ContactUs();
                    data.Name = item.Name;
                    data.Email = item.Email;
                    data.Country = GetOption("Country", item.Country);
                    data.Phone = "'"+ item.Phone + "'";
                    data.SocialID = (string.IsNullOrEmpty(item.SocialID)) ? "" : item.SocialID;
                    data.Subject = GetOption("Contact", item.Subject);
                    data.SeletedItem = item.SeletedItem;
                    data.ContactTime = GetOption("Time", item.ContactTime);
                    data.Detail = (item.Detail != null) ? item.Detail.Replace(',', '，') : "";
                    data.Reply = (item.Reply != null) ? item.Reply.Replace(',', '，') : "";
                    //Status = item.Status.ToString(),
                    data.Handler = item.Handler;
                    data.ReplyTime = item.ReplyTime.HasValue ? item.ReplyTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";
                    data.Note = item.Note;

                    string statustype = string.Empty;

                    OptionLib.ContactUs_Status.TryGetValue(item.Status.ToString(), out statustype);
                    data.Status = statustype;
                    EstateObjService objectSrv = new EstateObjService();
                    int iSelectItem = 0;
                    int.TryParse(item.SeletedItem, out iSelectItem);
                    data.SeletedItem = objectSrv.GetNameByID(iSelectItem);


                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult SeminarCSV()
        {
            var list = (List<DbContent.Seminar>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"活动标题,
                            活动时间,
                            地址,
                            联络人姓名,
                            国籍,
                            联络电话,
                            Email,
                            参加人数,
                            内容,
                            前台回传的时间,
                            备注";

                List<ExportModel.Seminar> export = new List<ExportModel.Seminar>();

                foreach (var item in list)
                {
                    ExportModel.Seminar data = new ExportModel.Seminar()
                    {
                        Title = item.Title,
                        StartTime = item.StartTime.HasValue ? item.StartTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                        Addr = item.Addr,
                        ContactName = item.ContactName,
                        Country = GetOption("Country", item.Country),
                        Phone = item.Phone,
                        Email = item.Email,
                        Count = item.Count.HasValue ? item.Count.Value.ToString() : "0",
                        Detail = item.Detail,
                        SendDate = item.SendDate.HasValue ? item.SendDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                        Note = item.Note
                    };
                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }

        public string GetOption(string code, string OptionValue)
        {
            string optionName = string.Empty;
            OptionService optSrv = new OptionService();
            var channel = optSrv.GetListByCode(code);


            var optdata = channel.Where(p => p.OptionValue == OptionValue).FirstOrDefault();
            if (optdata != null)
            {
                optionName = optdata.Name;
            }
            return optionName;
        }

        private void ExportCSV<T>(string filename, string title, IEnumerable<T> result)
        {
            string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            Response.AppendHeader("content-disposition", "attachment;filename=" + strFileName);
            Response.ContentType = "application/unkown";
            Response.ContentEncoding = Encoding.UTF8;
            GenerateCSV(title, result);
        }

        private void GenerateCSV<T>(string title, IEnumerable<T> list)
        {
            Type t = typeof(T);
            //將資料寫到前端
            StreamWriter sw = new StreamWriter(Response.OutputStream, Encoding.UTF8);
            object o = Activator.CreateInstance(t);
            PropertyInfo[] props = o.GetType().GetProperties();

            title = title.Replace("\r\n", "");
            title = title.TrimEnd(',');
            title = title.Replace(" ", "");
            sw.Write(title);
            sw.WriteLine();

            //將資料表的資料寫出
            foreach (T item in list)
            {
                foreach (PropertyInfo pi in props)
                {
                    //特殊處理
                    //if (pi.Name == "Polies")
                    //{
                    //    string whatToWrite = Convert.ToString(item.GetType().GetProperty(pi.Name).GetValue(item, null)).Replace("\r", "").Replace("\n", "") + ',';
                    //    sw.Write(whatToWrite);
                    //}
                    string whatToWrite = Convert.ToString(item.GetType().GetProperty(pi.Name).GetValue(item, null)).Replace(",", "，").Replace("\r", "").Replace("\n", "") + ',';
                    sw.Write(whatToWrite);

                }
                sw.WriteLine();
            }

            sw.Close();
        }


        [HttpGet]
        public ActionResult FundsSaleCSV()
        {
            var list = (List<FundsSaleModel>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"基金单号,
                            购买人ID,
                            所属渠道名称,
                            购买人姓名,
                            基金类型,
                            基金名称,
                            申购方式,
                            累积申购金额,                            
                            备注,
                            建立时间";
                //基金交易明细,

                List<ExportFundsSaleModel> export = new List<ExportFundsSaleModel>();

                OptionService optSrv = new OptionService();
                var fundTypeList = optSrv.GetListByCode("FundType");
                var buyTypeList = optSrv.GetListByCode("FundBuyType");

                foreach (var item in list)
                {
                    ExportFundsSaleModel data = new ExportFundsSaleModel()
                    {
                        FundsNo = item.FundsNo,
                        UserIdentityNo = item.UserIdentityNo,
                        Channel = exService.GetChannelName(item.Channel),
                        BuyerName = item.BuyerName,
                        //FundsID = item.FundsID,
                        FundType = fundTypeList.Where(p => p.OptionValue == item.FundType).FirstOrDefault().Name,
                        FundsName = item.FundsName,
                        //dFundType = fundTypeList.Where(p => p.OptionValue == x.FundType).FirstOrDefault().Name,
                        BuyType = buyTypeList.Where(p => p.OptionValue == item.BuyType).FirstOrDefault().Name,
                        //dBuyType = buyTypeList.Where(p => p.OptionValue == x.BuyType).FirstOrDefault().Name,
                        PurchaseTotal = item.PurchaseTotal,
                        Note = item.Note,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                    };

                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult FundsDetailCSV()
        {
            var list = (List<DbContent.FundsDetail>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"交易类型,
                            交易单号,
                            交易日期,
                            单位数,
                            赎回淨值,
                            赎回总金额(不含手续费),
                            绩效费,
                            申购淨值,
                            申购金额(不含手续费),  
                            手续费,
                            分配金額,
                            缴费状态,                           
                            备注,
                            建立时间";
                //基金交易明细,

                List<FundsDetailModel> export = new List<FundsDetailModel>();

                OptionService optSrv = new OptionService();
                var txnTypeList = optSrv.GetListByCode("FundTxnType");

                foreach (var item in list)
                {
                    FundsDetailModel data = new FundsDetailModel()
                    {

                        TxnType = txnTypeList.Where(p => p.OptionValue == item.TxnType).FirstOrDefault().Name,
                        TxnNo = item.TxnNo,
                        TxnDate = item.TxnDate.HasValue ? item.TxnDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                        Unit = item.Unit.HasValue ? item.Unit.Value.ToString() : "",
                        RedemptionVal = item.RedemptionVal.HasValue ? item.RedemptionVal.Value.ToString() : "",
                        RedemptionAmt = item.RedemptionAmt.HasValue ? item.RedemptionAmt.Value.ToString() : "",
                        PrefFee = item.PrefFee.HasValue ? item.PrefFee.Value.ToString() : "",
                        PurchaseVal = item.PurchaseVal.HasValue ? item.PurchaseVal.Value.ToString() : "",
                        PurchaseAmt = item.PurchaseAmt.HasValue ? item.PurchaseAmt.Value.ToString() : "",
                        HandlingFee = item.HandlingFee.HasValue ? item.HandlingFee.Value.ToString() : "",
                        DividendAmt = item.DividendAmt.HasValue ? item.DividendAmt.Value.ToString() : "",
                        PayStatus = (item.PayStatus == "1") ? "本期未缴" : (item.PayStatus == "2") ? "本期已缴" : "",
                        Note = item.Note,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")

                    };



                    export.Add(data);
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }

            return new EmptyResult();
        }


        [HttpGet]
        public ActionResult charterCSV()
        {
            var list = (List<DbContent.CharterData>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"代管类型,
                                合约期间,                                
                                包租金额/月,
                                租赁期间,
                                租客名称,
                                租客性别,
                                租客ID/护照,
                                租客生日,                                
                                租赁金额/月,
                                建立时间";


                List<CharterModel> export = new List<CharterModel>();

                OptionService optSrv = new OptionService();
                var typeList = optSrv.GetListByCode("CharterType");
                RentDataService rentSrv = new RentDataService();

                foreach (var item in list)
                {
                    var rantdata = rentSrv.GetList("", "", item.EstateID, item.ID);

                    foreach (var subitem in rantdata)
                    {
                        CharterModel data = new CharterModel()
                        {
                            ManagerType = typeList.Where(p => p.OptionValue == item.ManagerType).FirstOrDefault().Name,
                            EndDate = string.Format("{0}~{1}", item.StartDate.HasValue ? item.StartDate.Value.ToString("yyyy/MM/dd") : "", item.EndDate.HasValue ? item.EndDate.Value.ToString("yyyy/MM/dd") : ""),
                            CharterAmount = item.CharterAmount.HasValue ? item.CharterAmount.ToString() : "",

                            RentEnd = string.Format("{0}~{1}", subitem.RentStart.HasValue ? subitem.RentStart.Value.ToString("yyyy/MM/dd") : "", subitem.RentEnd.HasValue ? subitem.RentEnd.Value.ToString("yyyy/MM/dd") : ""),
                            RenterName = subitem.RenterName,
                            RenterGender = subitem.RenterGender,
                            RenterIdNo = subitem.RenterIdNo,
                            RenterBirth = subitem.RenterBirth.HasValue ? subitem.RenterBirth.Value.ToString("yyyy/MM/dd") : "",
                            RentAmount = subitem.RentAmount.HasValue ? subitem.RentAmount.ToString() : "",

                            CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        };

                        export.Add(data);

                    }
                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult payRecordCSV()
        {
            var list = (List<DbContent.PayRecord>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"物件ID,
                                物件名称,                                
                                房型,
                                购买人ID,
                                期数,
                                本期应付金额,
                                缴款状态,
                                备注,                                                                
                                建立时间";

                List<PayRecordModel> export = new List<PayRecordModel>();

                OptionService optSrv = new OptionService();
                var typeList = optSrv.GetListByCode("PayStatus");
                EstateScheduleService scheduleSrv = new EstateScheduleService();
                EstateObjService objectSrv = new EstateObjService();
                EstateSaleService esSrv = new EstateSaleService();
                EstateRoomService roomSrv = new EstateRoomService();

                foreach (var item in list)
                {
                    //取得工期資訊
                    var scheduldata = scheduleSrv.GetList("", item.EstateObjID).Where(p => p.ID == item.EstateScheduleID).FirstOrDefault();
                    //取得銷售資料
                    var esdata = esSrv.GetList("", "", "", "", "", "", null).Where(p => p.ID == item.EstateSaleID).FirstOrDefault();
                    //取得房型資料
                    DbContent.EstateRoom roomdata = roomSrv.GetList("", "", "", item.EstateObjID).Where(p => p.ID == esdata.EstateRoomID).FirstOrDefault();
                    PayRecordModel data = new PayRecordModel()
                    {
                        ID = esdata.EstateNo,
                        EstateObjName = objectSrv.GetNameByID(item.EstateObjID),
                        EstateRoomName = (roomdata != null) ? roomdata.Name : "",
                        UserIdentityNo = esdata.UserIdentityNo,                        
                        EstateScheduleID = scheduldata.SchNo,
                        Amount = item.Amount.HasValue ? item.Amount.Value.ToString() : "",                                                
                        PayStatus = (typeList.Where(p => p.OptionValue == item.PayStatus).FirstOrDefault() != null) ? (typeList.Where(p => p.OptionValue == item.PayStatus).First().Name) : item.PayStatus,
                        Note = item.Note,                        
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    };

                    export.Add(data);


                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }
        [HttpGet]
        public ActionResult benefitRecordCSV()
        {
            var list = (List<DbContent.BenefitRecord>)ExportService.GetExport();
                        
            if (list != null)
            {
                //設定標題
                string title = @"物件ID,
                                物件名称,                                
                                房型,
                                购买人ID,
                                类型,
                                租客姓名,
                                租客性别,
                                租客ID/护照,     
                                租客生日,
                                月份,
                                收入金额,
                                支出金额,
                                拨款状态,
                                明细,
                                备注,
                                建立时间";

                List<BenefitRecordModel> export = new List<BenefitRecordModel>();

                OptionService optSrv = new OptionService();
                var payStatuses = optSrv.GetListByCode("AllotStatus");
                
                EstateObjService objectSrv = new EstateObjService();
                EstateSaleService esSrv = new EstateSaleService();
                EstateRoomService roomSrv = new EstateRoomService();

                foreach (var item in list)
                {                    
                    //取得銷售資料
                    var esdata = esSrv.GetList("", "", "", "", "", "", null).Where(p => p.ID == item.EstateSaleID).FirstOrDefault();
                    //取得房型資料
                    var roomdata = roomSrv.GetList("", "", "", item.EstateObjID).Where(p => p.ID == esdata.EstateRoomID).FirstOrDefault();
                    BenefitRecordModel data = new BenefitRecordModel()
                    {
                        ID = esdata.EstateNo,
                        EstateObjName = objectSrv.GetNameByID(item.EstateObjID),
                        EstateRoomName = roomdata.Name,
                        UserIdentityNo = esdata.UserIdentityNo,
                        CharterType = item.CharterType,
                        RenterName = item.RenterName,
                        RenterGender = item.RenterGender,
                        RenterIdNo = item.RenterIdNo,
                        RenterBirth = item.RenterBirth.HasValue ? item.RenterBirth.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                        IncomeDate = item.IncomeDate.HasValue ? item.IncomeDate.Value.ToString("MM") : "",
                        Income = item.Income.HasValue ? item.Income.Value.ToString() : "",
                        Expense = item.Expense.HasValue ? item.Expense.Value.ToString() : "",

                        PayStatus = (payStatuses.Where(p => p.OptionValue == item.PayStatus).FirstOrDefault() != null) ? (payStatuses.Where(p => p.OptionValue == item.PayStatus).First().Name) : item.PayStatus,
                        PdfFile = item.PdfFile.ToSiteUrl(),
                        Note = item.Note,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    };

                    export.Add(data);


                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult insuranceSaleCSV()
        {
            var list = (List<InsuranceSaleModel>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"保单号码,
                                要保人ID,                                
                                所属渠道,
                                要保人,
                                被保险人,
                                负责业务姓名,
                                负责业务联络电话,
                                保险名称,     
                                人身保险,
                                保单状态,
                                保单生效日,
                                保障期间(起),
                                保障期间(訖),
                                缴费年期,
                                缴别,
                                保单币别,
                                保单币别(英文),
                                保费金额,
                                保障内容,
                                保障内容(英文),
                                备注事项,
                                备注事项(英文),
                                上传商品建议书,
                                上传要保书,                                
                                备注,
                                建立时间";

                List<ExportInsModel> export = new List<ExportInsModel>();

                InsuranceService insureSrv = new InsuranceService();
                var insdata = insureSrv.GetList("", null);
                OptionService optSrv = new OptionService();
                var intStatus = optSrv.GetListByCode("InsuranceValid", null);
                var payType = optSrv.GetListByCode("InsurancePay", null);
                foreach (var item in list)
                {
                    ExportInsModel data = new ExportInsModel()
                    {
                        InsNo = item.InsNo,
                        UserIdentityNo = item.UserIdentityNo,
                        Channel = exService.GetChannelName(item.Channel),
                        BuyerName = item.BuyerName,
                        BenefitName = item.BenefitName,
                        SaleName = item.SaleName,
                        SalePhone = "'" + item.SalePhone + "'",
                        InsID = insdata.Where(p=>p.ID == item.InsID).FirstOrDefault().Name,
                        InsSortName = item.InsSortName,
                        InsStatus = intStatus.Where(p => p.OptionValue.Contains(item.InsStatus)).FirstOrDefault().Name,
                        ValidDate = item.ValidDate,
                        ValidStart = item.ValidStart,
                        ValidEnd = item.ValidEnd,
                        PayPeriod = item.PayPeriod,
                        PayType = payType.Where(p=>p.OptionValue.Contains(item.PayType)).FirstOrDefault().Name,
                        Currency = item.Currency,
                        CurrencyEn = item.CurrencyEn,
                        Amount = item.Amount.ToString(),
                        InsContent = item.InsContent,
                        InsContentEn = item.InsContentEn,
                        InsNote = item.InsNote,
                        InsNoteEn = item.InsNoteEn,
                        Recommend = item.Recommend.ToSiteUrl(),
                        DealFile = item.DealFile.ToSiteUrl(),
                        Note = item.Note,
                        CreateDate = item.CreateDate,
                    };

                    export.Add(data);


                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult InsuranceCSV()
        {
            var list = (List<DbContent.Insurance>)ExportService.GetExport();

            if (list != null)
            {
                //設定標題
                string title = @"ID,
                                保险名称,  
                                保险名称(英文),  
                                人身保险,
                                人身保险(英文),
                                备注,
                                建立时间,
                                狀態";

                List<InsuranceModel> export = new List<InsuranceModel>();
                
                foreach (var item in list)
                {
                    InsuranceModel data = new InsuranceModel()
                    {
                        ID = item.ID,
                        Name = item.Name,
                        NameEn = item.NameEn,
                        TypeName = item.TypeName,
                        TypeNameEn = item.TypeNameEn,
                        Note = item.Note,
                        CreateDate = item.CreateDate.ToString("yyyy/MM/dd"),
                        Status = item.Status == true ? "启用中" : "停用中"
                    };

                    export.Add(data);


                }
                string strFileName = "Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                ExportCSV(strFileName, title, export);
            }
            return new EmptyResult();
        }
        
    }

}
