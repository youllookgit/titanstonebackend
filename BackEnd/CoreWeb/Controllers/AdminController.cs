﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class AdminController : Controller
    {
        //SERVICE
        public BaseModel res = new BaseModel();
        public AdminService adminSrv = new AdminService();
        public SysPrmisnService syserv = new SysPrmisnService();

        [Filter.BEActionFilter.IgnorePermission]
        public ActionResult LogIn(Login data)
        {
            try
            {
                var p = adminSrv.LogIn(data);
                res.Success = true;
                res.data = p;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        [HttpPost]
        public ActionResult GetAdmin(string keyword,bool? status)
        {
            try
            {
                var dataSource = adminSrv.GetAdmin(keyword, status);
                //save to Export
                ExportService.SaveExport(dataSource);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Act = x.Act,
                    Pwd = x.Pwd,
                    Name = x.Name,
                    //Permission = x.Permission,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    Status = x.Status
                });
                res.data = collection;
                
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
        [HttpPost]
        public ActionResult Create(AdminModel data)
        {
            try
            {
                adminSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                adminSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
        [HttpPost]
        public ActionResult GetPermission(int ID)
        {
            try
            {
                var p = syserv.GetPermission("Admin",ID);
                res.Success = true;
                res.data = p;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        
    }
}