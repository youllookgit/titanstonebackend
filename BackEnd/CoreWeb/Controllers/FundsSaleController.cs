﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FundsSaleController : Controller
    {
        public BaseModel res = new BaseModel();
        public FundsSaleService fundsSaleSrv = new FundsSaleService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增基金銷售
        /// </summary>
        /// <param name="req">基金銷售資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.FundsSale req)
        {
            try
            {
                fundsSaleSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢基金銷售紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="fundType">基金類型</param>
        /// <param name="buyType">申購方式</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string fundType, string buyType, string startDate, string endDate, double? sum, double? sumlitmit)
        {
            try
            {
                var fundTypeList = optSrv.GetListByCode("FundType");
                var buyTypeList = optSrv.GetListByCode("FundBuyType");

                var data = fundsSaleSrv.GetList(keyword, fundType, buyType, startDate, endDate, sum, sumlitmit);
                ExportService.SaveExport(data);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    FundsNo = x.FundsNo,
                    UserIdentityNo = x.UserIdentityNo,
                    Channel = x.Channel,
                    ChannelName = x.ChannelName,
                    BuyerName = x.BuyerName,
                    FundsID = x.FundsID,
                    FundsName = x.FundsName,
                    FundType = x.FundType,
                    dFundType = fundTypeList.Where(p => p.OptionValue == x.FundType).FirstOrDefault().Name,
                    BuyType = x.BuyType,
                    dBuyType = buyTypeList.Where(p => p.OptionValue == x.BuyType).FirstOrDefault().Name,
                    Note = x.Note,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    PurchaseTotal = x.PurchaseTotal
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定基金銷售紀錄
        /// </summary>
        /// <param name="req">基金銷售資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.FundsSale req)
        {
            try
            {
                fundsSaleSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定基金銷售紀錄
        /// </summary>
        /// <param name="ID">基金銷售紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                fundsSaleSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(List<ImportModel.FundSale> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                fundsSaleSrv.Import(data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}