﻿
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System.IO;
using ExcelDataReader;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class UserController : Controller
    {
        public BaseModel res = new BaseModel();
        public UserService usSrv = new UserService();
        public OptionService opSrv = new OptionService();
        //會員管理
        [HttpPost]
        public ActionResult GetUsers(string keyword, string startDate, string endDate, string country, string channel, bool? valid, bool? status)
        {
            try
            {
                var countries = opSrv.GetListByCode("Country");
                var dataSource = usSrv.GetUser(keyword, startDate, endDate, country, channel, valid, status);
                //save to Export
                ExportService.SaveExport(dataSource);

                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    DeviceID = x.PushID,
                    Act = x.Act,
                    Pwd = Security.Decrypt(x.Pwd),
                    Valid = x.Valid,
                    Name = x.Name,
                    IdentityNo = x.IdentityNo,
                    Birth = x.Birth.HasValue ? x.Birth.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Country = x.Country,
                    CName = (countries.Where(p => p.OptionValue == x.Country).FirstOrDefault() != null) ? (countries.Where(p => p.OptionValue == x.Country).First().Name) : "",
                    PhonePrefix = x.PhonePrefix,
                    Phone = x.Phone,
                    ImageUrl = x.ImageUrl,
                    Channel = x.Channel,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    Status = x.Status
                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        [HttpPost]
        public ActionResult Create(DbContent.User data)
        {
            try
            {
                usSrv.Create(data);
                res.Success = true;
            }          
            catch (Exception ex)
            {
                //System.Data.Entity.Validation.DbEntityValidationException DbEntityValidationException = (System.Data.Entity.Validation.DbEntityValidationException)ex;
                //throw DbEntityValidationException;
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                usSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult ChangeStatus(int ID)
        {
            try
            {
                usSrv.ChangeStatus(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

        [HttpPost]
        public ActionResult ChangeValid(int ID)
        {
            try
            {
                usSrv.ChangeValid(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

        //匯入檔案上傳
        [Filter.BEActionFilter.IgnorePermissionAttribute]
        [HttpPost]
        public ActionResult MemberImport(HttpPostedFileBase upload, string filename)
        {
            if (upload != null)
            {
                try
                {
                    //先存檔
                    string fileRealName = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var nameExtend = upload.FileName.Split('.');
                    if (string.IsNullOrEmpty(filename))
                    {
                        filename = upload.FileName.Replace("." + nameExtend[nameExtend.Length - 1], "");
                    }
                    var newFileName = fileRealName + "." + nameExtend[nameExtend.Length - 1];
                    newFileName = newFileName.Trim(' ');
                    var fileSavePath = Lib.FileLib.rootPath + Lib.FileLib.ImageUserFolder + newFileName;
                    fileSavePath = fileSavePath.Trim(' ');
                    //Save File
                    upload.SaveAs(fileSavePath);

                    usSrv.UserImport(fileSavePath);

                    //匯入
                    return Json(new { success = true });

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, msg = ex.Message });
                }

            }
            else
            {
                return Json(new { Success = false, msg = "File is Null!" });
            }

        }


    }
}