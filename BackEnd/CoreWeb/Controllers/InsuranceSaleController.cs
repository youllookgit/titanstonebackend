﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class InsuranceSaleController : Controller
    {
        public BaseModel res = new BaseModel();
        public InsuranceSaleService Srv = new InsuranceSaleService();

        // 新增保險商品
        [HttpPost]
        public ActionResult Create(DbContent.InsuranceSale req)
        {
            try
            {
                Srv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 查詢保險商品
        [HttpPost]
        public ActionResult GetList(
            string keyword, string Status,
            DateTime? validStart, DateTime? validEnd,
            DateTime? startDate, DateTime? endDate)
        {
            try
            {
                var _data = Srv.GetList(keyword, Status, validStart, validEnd, startDate, endDate);
                ExportService.SaveExport(_data);
                res.data = _data;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 修改保險商品
        [HttpPost]
        public ActionResult Modify(DbContent.InsuranceSale req)
        {
            try
            {
                Srv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
        // 刪除保險商品
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                Srv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(List<CoreWeb.Models.ImportModel.InsSale> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                Srv.Import(data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}