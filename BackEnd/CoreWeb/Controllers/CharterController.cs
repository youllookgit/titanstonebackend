﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class CharterController : Controller
    {
        public BaseModel res = new BaseModel();
        public CharterDataService charterSrv = new CharterDataService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增代管
        /// </summary>
        /// <param name="req">代管資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.CharterData req)
        {
            try
            {
                charterSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢代管紀錄
        /// </summary>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="type">代管類型</param>
        /// <param name="EstateID">地產銷售ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string startDate, string endDate, string type, int? EstateID)
        {
            try
            {
                var typeList = optSrv.GetListByCode("CharterType");
                var data = charterSrv.GetList(startDate, endDate, type, EstateID);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateID = x.EstateID,
                    ManagerType = x.ManagerType,
                    dManagerType = typeList.Where(p => p.OptionValue == x.ManagerType).FirstOrDefault().Name,
                    StartDate = x.StartDate.HasValue ? x.StartDate.Value.ToString("yyyy/MM/dd") : "",
                    EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy/MM/dd") : "",
                    dCharterDate = (x.StartDate.HasValue ? x.StartDate.Value.ToString("yyyy/MM/dd") : "") 
                        + "~" + (x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy/MM/dd") : ""),
                    ManageDeal = x.ManageDeal,
                    CharterAmount = x.CharterAmount.HasValue ? x.CharterAmount.ToString() : "",
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定代管紀錄
        /// </summary>
        /// <param name="req">代管資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.CharterData req)
        {
            try
            {
                charterSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定代管紀錄
        /// </summary>
        /// <param name="ID">代管紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                charterSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(int EstateSaleID, List<ImportModel.CharterData> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                charterSrv.Import(EstateSaleID, data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}