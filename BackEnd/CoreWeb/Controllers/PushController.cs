﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class PushController : Controller
    {
        public BaseModel res = new BaseModel();
        public PushService pushSrv = new PushService();
        public OptionService ops = new OptionService();

        /// <summary>
        /// 新增推播通知
        /// </summary>
        /// <param name="req">推播通知資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(PushModel req)
        {
            try
            {
                pushSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢通知紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">發送時間起日</param>
        /// <param name="endDate">發送時間迄日</param>
        /// <param name="type">類型</param>
        /// <param name="TargetType">通知對象</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, string type, string targettype)
        {
            try
            {
                var nameList = ops.GetListByCode("Noitce");
                var data = pushSrv.GetLogs(keyword, startDate, endDate, type, targettype);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    PType = x.PType,
                    Name = nameList.Where(p => p.OptionValue == x.PType).FirstOrDefault().Name,
                    TargetType = x.TargetType,
                    Detail = x.Detail,
                    Lang = x.Lang,
                    DeviceIDs = x.PushIDs,                    
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢指定通知發送對象
        /// </summary>
        /// <param name="ID">通知紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTargets(int ID)
        {
            try
            {
                var data = pushSrv.GetItems(ID);
                res.data = data;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定通知紀錄
        /// </summary>
        /// <param name="ID">通知紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                pushSrv.DeleteLog(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}