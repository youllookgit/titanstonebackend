﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FundsReportController : Controller
    {
        public BaseModel res = new BaseModel();
        public FundsReportService fundsWorthSrv = new FundsReportService();

        /// <summary>
        /// 新增財務報表
        /// </summary>
        /// <param name="req">財務報表資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.FundsReport req)
        {
            try
            {
                fundsWorthSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢財務報表紀錄
        /// </summary>
        /// <param name="keyword">報表日期</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="FundsID">基金商品ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, int? FundsID)
        {
            try
            {
                var data = fundsWorthSrv.GetList(keyword, startDate, endDate, FundsID);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    FundsID = x.FundsID,
                    Year = x.Year,
                    Month = x.Month,
                    dReportDate = x.Year + "/" + x.Month,
                    FileUrl = x.FileUrl.ToSiteUrl(),
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定財務報表紀錄
        /// </summary>
        /// <param name="req">財務報表資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.FundsReport req)
        {
            try
            {
                fundsWorthSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定財務報表紀錄
        /// </summary>
        /// <param name="ID">財務報表紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                fundsWorthSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}