﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Lib;
using CoreWeb.Service;
using System.Net;

namespace CoreWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var a = Security.Decrypt("Q042dEN2Y0RxRFk9");
            return View();
        }
        public ActionResult TestLink()
        {
            return View();
        }
        public ActionResult Share(string type,string id,string lang)
        {
            ViewBag.Type = type;
            ViewBag.ID = id;
            string Content = "檢視分享的內容";
            if (lang == LangLib.en)
            {
                Content = "View shared content";
            }
            ViewBag.Content = Content;
            return View();
        }
        //public ActionResult Android()
        //{
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(FileLib.rootSite + "/Content/app/StoneDemo.apk");
        //    string fileName = "StoneDemo.apk";
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        //}
        //public ActionResult Apple()
        //{
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(FileLib.rootSite + "/Content/app/StoneDemo.ipa");
        //    string fileName = "StoneDemo.ipa";
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        //}

        public ActionResult BackEnd()
        {
            return Redirect(Url.Content("~/Application/index.html"));
        }

        public ActionResult AccountVerify(string Param)
        {
            try
            {
                int UserID = int.Parse(Security.Decrypt(Param));
                UserService us = new UserService();
                string msg = us.IsVerify(UserID);
                if (msg == "")
                {
                    return View("AccountVerify");
                }
                else
                {
                    return RedirectToAction("TimeOut", new { msg = msg });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("TimeOut", new { msg = ex.Message });
            }
            
        }
        public ActionResult ResetPwd(string Param,string lang)
        {
            string _lang = LangLib.cn;
            if (lang == LangLib.en)
            {
                _lang = LangLib.en;
            }
            try
            {
                TempData["Param"] = Param;
                TempData["Lang"] = _lang;
                int UserID = int.Parse(Security.Decrypt(Param));
                UserService us = new UserService();
                var user = us.GetByID(UserID);
                if (user.UpdateDate.AddDays(1) > DateTime.Now)
                {
                    //標記MailLog以驗證
                    CoreWeb.Areas.App.Service.apiUserService apiusr = new Areas.App.Service.apiUserService();
                    apiusr.CheckMailLog("2", user.Act);
                    TempData["Name"] = user.Name;
                    if (lang == LangLib.cn)
                    {
                        return View("ResetPwd");
                    }
                    else
                    {
                        return View("ResetPwdEn");
                    }
                }
                else
                {
                    return RedirectToAction("TimeOut");
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("TimeOut", new { msg = ex.Message });
            }
        }
        public ActionResult PwdSave(string Param, string pwd1, string Lang)
        {
            try
            {
                int UserID = int.Parse(Security.Decrypt(Param));
                pwd1 = Security.Encrypt(pwd1);
                UserService us = new UserService();
                string msg = us.ResetPwd(UserID, pwd1);
                if (msg == "")
                {
                    if (Lang == LangLib.en)
                    {
                        TempData["msg"] = "Reset password done! Please sign in again!";
                    }
                    else
                    {
                        TempData["msg"] = "密码变更成功!请再次重新登入!";
                    }
                    
                    return View("AccountVerify");
                }
                else
                {
                    return RedirectToAction("TimeOut", new { msg = msg });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("TimeOut", new { msg = ex.Message });
            }
        }
        public ActionResult TimeOut(string msg = "")
        {
            TempData["msg"] = msg;
            return View("TimeOut");
        }

        [HttpGet]
        public ActionResult TestMail(string mail = "")
        {
            if (string.IsNullOrEmpty(mail))
            {
                mail = "youllook@gmail.com";
            }
            try
            {
                MailLib.SendMail("發信測試內容","系統發信測試", mail);
                return Json(new { Success=true}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(ex.Message + "/" + ex.InnerException, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Terms()
        {
            return View();
        }

    }
}