﻿using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class TradeNewsController : Controller
    {
        public BaseModel res = new BaseModel();
        TradeNewsService tradenewsSrv = new TradeNewsService();

        [HttpPost]
        //理財趨勢管理
        public ActionResult GetTradeNews(string keyword, bool? status, DateTime? displaystartdate, DateTime? displayenddate, DateTime? beginstartdate, DateTime? beginenddate, DateTime? startdate, DateTime? enddate, string tradetypeid)
        {
            try
            {
                var dataSource = tradenewsSrv.GetTradeNews(keyword, status, displaystartdate, displayenddate, beginstartdate, beginenddate, startdate, enddate, tradetypeid);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    NameEn = x.NameEn,
                    DisplayDate = x.DisplayDate.HasValue ? x.DisplayDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    TradeType = x.TradeType,
                    Author = x.Author,
                    AuthorEn = x.AuthorEn,
                    Brief = x.Brief,
                    BriefEn = x.BriefEn,
                    Img = FileLib.ImgPathToUrl(x.Img),
                    Detail = FileLib.ImgPathToUrl(x.Detail),
                    DetailEn = FileLib.ImgPathToUrl(x.DetailEn),
                    Recommend = x.Recommend,
                    BeginDate = x.BeginDate.HasValue ? x.BeginDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Status = x.Status,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo,
                    TradeTypeName = tradenewsSrv.TradeTypeName(x.TradeType)
                });

                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult GetActivity()
        {
            try
            {
                var dataSource = tradenewsSrv.GetActivity();
                var collection = dataSource.Select(x => new
                {
                    name = x.Name,
                    value = x.ID
                });
                res.data = collection;
                res.Success = true;
            }
            catch(Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }


        [HttpPost]
        public ActionResult Create(DbContent.TradeNews data)
        {
            try
            {
                tradenewsSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                tradenewsSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}