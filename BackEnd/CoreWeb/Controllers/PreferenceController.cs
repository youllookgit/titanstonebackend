﻿using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class PreferenceController : Controller
    {
        public BaseModel res = new BaseModel();
        public PreferenceService prfSrv = new PreferenceService();

        [HttpPost]
        public ActionResult GetPreference(string keyword, string startDate, string endDate, int? UserId)
        {
            try
            {
                var dataSource = prfSrv.GetPreference(keyword, startDate, endDate, UserId);
                //save to Export
                ExportService.SaveExport(dataSource);
                var collection = dataSource.Select(x => new
                {
                    Email = x.Email,
                    Name = x.Name,
                    IdentityNo = x.IdentityNo,
                    UpdateDate = x.UpdateDate.HasValue ? x.UpdateDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    ItemData = x.ItemData
                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}