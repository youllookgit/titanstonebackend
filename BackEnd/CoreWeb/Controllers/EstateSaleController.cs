﻿using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class EstateSaleController : Controller
    {
        public BaseModel res = new BaseModel();
        public EstateSaleService esSrv = new EstateSaleService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增地產銷售
        /// </summary>
        /// <param name="req">地產銷售資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.EstateSale req)
        {
            try
            {
                esSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢地產銷售紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="type">租賃狀態</param>
        /// <param name="floor">楼层</param>
        /// <param name="room">房型</param>
        /// <param name="status">啟用狀態</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, string type, string floor, string room, bool? status)
        {
            try
            {
                var rentStatusList = optSrv.GetListByCode("RentStatus");
                var data = esSrv.GetList(keyword, startDate, endDate, type, floor, room, status);

                ExportService.SaveExport(data);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateNo = x.EstateNo,
                    UserIdentityNo = x.UserIdentityNo,
                    IsFinish = x.IsFinish,
                    Channel = x.Channel,
                    BuyerName = x.BuyerName,
                    EstateObjID = x.EstateObjID,
                    EstateObjName = x.EstateObjName,
                    EstateRoomID = x.EstateRoomID,
                    EstateRoomName = x.EstateRoomName,
                    Addr = x.Addr,
                    Floor = x.Floor,
                    UnitArea = x.UnitArea,
                    RoomArea = x.RoomArea,
                    PublicArea = x.PublicArea,
                    PriceSum = x.PriceSum,
                    DealFile = x.DealFile,
                    SignDate = x.SignDate.HasValue ? x.SignDate.Value.ToString("yyyy/MM/dd") : "",
                    ParkingType = x.ParkingType,
                    ParkingTypeName = x.ParkingTypeName,
                    ParkingPrice = x.ParkingPrice,
                    ManagerType = x.ManagerType,
                    ManagerDataID = x.ManagerDataID,
                    IsPush = (x.IsPush == null) ? false : x.IsPush.Value,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    UpdDate = x.UpdDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    Status = x.Status,
                    RentStatus = rentStatusList.Where(p => p.OptionValue == x.RentStatus).FirstOrDefault().Name,
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定地產銷售紀錄
        /// </summary>
        /// <param name="req">地產銷售資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.EstateSale req)
        {
            try
            {
                esSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定地產銷售紀錄
        /// </summary>
        /// <param name="ID">地產銷售紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                esSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        //匯入檔案上傳
        [HttpPost]
        public ActionResult Import(List<ImportModel.EstateData> data)
        {
            try
            {
                data.Remove(data[0]);//移除第一列 描述
                esSrv.EstateSaleImport(data);
                //匯入
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}