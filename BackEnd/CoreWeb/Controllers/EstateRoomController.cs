﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class EstateRoomController : Controller
    {
        public BaseModel res = new BaseModel();
        public EstateRoomService roomSrv = new EstateRoomService();

        /// <summary>
        /// 新增房型
        /// </summary>
        /// <param name="req">房型資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.EstateRoom req)
        {
            try
            {
                roomSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢房型紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="EstateObjID">地產物件ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, int? EstateObjID)
        {
            try
            {

                var data = roomSrv.GetList(keyword, startDate, endDate, EstateObjID.Value);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateObjID = x.EstateObjID,
                    Name = x.Name,
                    Brief = x.Brief,
                    FloorDesc = x.FloorDesc,
                    Area = x.Area,
                    Img = x.Img,
                    CreateDate = x.CreateDate.HasValue ? x.CreateDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    OrderNo = x.OrderNo
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定房型紀錄
        /// </summary>
        /// <param name="req">房型資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.EstateRoom req)
        {
            try
            {
                roomSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定房型紀錄
        /// </summary>
        /// <param name="ID">房型紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                roomSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}