﻿using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class NewsController : Controller
    {
        public BaseModel res = new BaseModel();
        NewsService newsSrv = new NewsService();

        [HttpPost]
        //官方消息管理
        public ActionResult GetNews(string keyword, bool? status, DateTime? publicstartdate, DateTime? publicenddate, DateTime? beginstartdate, DateTime? beginenddate, DateTime? startdate, DateTime? enddate)
        {
            try
            {
                var dataSource = newsSrv.GetNews(keyword, status, publicstartdate, publicenddate, beginstartdate, beginenddate, startdate, enddate);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Title = x.Title,
                    TitleEn = x.TitleEn,
                    PublicDate = x.PublicDate.HasValue ? x.PublicDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Brief = x.Brief,
                    BriefEn = x.BriefEn,
                    ImageUrl = FileLib.ImgPathToUrl(x.ImageUrl),
                    Detail = FileLib.ImgPathToUrl(x.Detail),
                    DetailEn = FileLib.ImgPathToUrl(x.DetailEn),
                    BeginDate = x.BeginDate.HasValue ? x.BeginDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    EndDate = x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty,
                    Status = x.Status,                    
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo,                    

                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(DbContent.News data)
        {
            try
            {
                newsSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                newsSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}