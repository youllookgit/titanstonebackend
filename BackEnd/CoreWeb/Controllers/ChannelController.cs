﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class ChannelController : Controller
    {

        public BaseModel res = new BaseModel();
        public ChannelService channelSrv = new ChannelService();
        public SysPrmisnService syserv = new SysPrmisnService();
        public OptionService optSrv = new OptionService();
        [HttpPost]
        public ActionResult GetChannel(string keyword, string location, bool? status)
        {
            try
            {
                var channel = optSrv.GetListByCode("channel");
                var dataSource = channelSrv.GetChannel(keyword, location, status);
                //save to Export
                ExportService.SaveExport(dataSource);

                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Act = x.Act,
                    Pwd = x.Pwd,
                    Name = x.Name,
                    Lacation = x.Lacation,
                    dLocation = (channel.Where(p => p.OptionValue == x.Lacation).FirstOrDefault() != null) ? (channel.Where(p => p.OptionValue == x.Lacation).First().Name) : "",
                    Unit = x.Unit,
                    Phone = x.Phone,
                    Email = x.Email,
                    Permission = x.Permission,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    Status = x.Status
                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(ChannelEditModel data)
        {
            try
            {
                channelSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                channelSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult GetPermission(int ID)
        {
            try
            {
                var p = syserv.GetPermission("Channel", ID);
                res.Success = true;
                res.data = p;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}