﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class SeminarController : Controller
    {
        public BaseModel res = new BaseModel();
        public SeminarService seminarSrv = new SeminarService();
        public OptionService ops = new OptionService();
        /// <summary>
        /// 新增活動報名
        /// </summary>
        /// <param name="req">活動報名資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.Seminar req)
        {
            try
            {
                // TODO
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢活動報名紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startPDate">發送時間起日</param>
        /// <param name="endPDate">發送時間迄日</param>
        /// <param name="startSDate">活動時間起日</param>
        /// <param name="endSDate">活動時間迄日</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startPDate, string endPDate, string startSDate, string endSDate)
        {
            try
            {
                var countyList = ops.GetListByCode("Country");
                var data = seminarSrv.GetList(keyword, startPDate, endPDate, startSDate, endSDate);

                ExportService.SaveExport(data);

                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Title = x.Title,
                    StartTime = x.StartTime.HasValue ? x.StartTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    Addr = x.Addr,
                    ContactName = x.ContactName,
                    Country = x.Country,// countyList.Where(p => p.OptionValue == x.Country).FirstOrDefault().Name,
                    Phone = x.Phone,
                    Email = x.Email,
                    Count = x.Count,
                    Detail = x.Detail,
                    Handler = x.Handler,
                    SendDate = x.SendDate.HasValue ? x.SendDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    Note = x.Note,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                });

                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定活動報名紀錄
        /// </summary>
        /// <param name="req">活動報名資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.Seminar req)
        {
            try
            {
                seminarSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定活動報名紀錄
        /// </summary>
        /// <param name="ID">活動報名紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                seminarSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}