﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class ScheduleRecordController : Controller
    {
        public BaseModel res = new BaseModel();
        public ScheduleRecordService scheduleRecordSrv = new ScheduleRecordService();

        /// <summary>
        /// 新增施工紀錄
        /// </summary>
        /// <param name="req">施工紀錄資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.ScheduleRecord req)
        {
            try
            {
                scheduleRecordSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢施工紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="EstateObjID">地產物件ID</param>
        /// <param name="EstateSID">工程進度ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, int? EstateObjID, int? EstateSID)
        {
            try
            {
                var data = scheduleRecordSrv.GetList(keyword, EstateObjID, EstateSID);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Img = x.Img,
                    ImgTitle = x.ImgTitle,
                    ImgDesc = x.ImgDesc,
                    ImgTitleEn = x.ImgTitleEn,
                    ImgDescEn = x.ImgDescEn,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定施工紀錄
        /// </summary>
        /// <param name="req">施工紀錄資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.ScheduleRecord req)
        {
            try
            {
                scheduleRecordSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定施工紀錄
        /// </summary>
        /// <param name="ID">施工紀錄紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                scheduleRecordSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}