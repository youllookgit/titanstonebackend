﻿using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class FaqSortController : Controller
    {
        public BaseModel res = new BaseModel();
        public FaqSortService faqSortSrv = new FaqSortService();
        //常見問題分類管理
        [HttpPost]
        public ActionResult GetFaqSort(string keyword, bool? status/*, DateTime? startdate, DateTime? enddate*/)
        {
            try
            {
                var dataSource = faqSortSrv.GetFaqSort(keyword, status/*, startdate, enddate*/);
                var collection = dataSource.Select(x => new
                {
                    ID = x.ID,
                    Status = x.Status,
                    Name = x.Name,
                    NameEn = x.NameEn,
                    ImageUrl = FileLib.ImgPathToUrl(x.ImageUrl),
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    OrderNo = x.OrderNo

                });
                res.data = collection;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Create(DbContent.FaqSort data)
        {
            try
            {
                faqSortSrv.Create(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }

        [HttpPost]
        public ActionResult Remove(int Id)
        {
            try
            {
                faqSortSrv.Remove(Id);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}