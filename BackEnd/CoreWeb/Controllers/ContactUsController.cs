﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class ContactUsController : Controller
    {
        public BaseModel res = new BaseModel();
        public ContactUsService contactUsSrv = new ContactUsService();

        /// <summary>
        /// 新增聯絡我們
        /// </summary>
        /// <param name="req">聯絡我們資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.ContactUs req)
        {
            try
            {
                // TODO
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢聯絡我們紀錄
        /// </summary>
        /// <param name="keyword">搜尋關鍵字</param>
        /// <param name="startDate">發送時間起日</param>
        /// <param name="endDate">發送時間迄日</param>
        /// <param name="country">國籍</param>
        /// <param name="subject">主題</param>
        /// <param name="status">處理狀態</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string keyword, string startDate, string endDate, string country, string subject, string status)
        {
            try
            {
                var data = contactUsSrv.GetList(keyword, startDate, endDate, country, subject, status);
                ExportService.SaveExport(data);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Email = x.Email,
                    Country = x.Country,
                    Phone = x.Phone,
                    Subject = x.Subject,
                    SeletedItem = x.SeletedItem,
                    ContactTime = x.ContactTime,
                    Detail = x.Detail,
                    Reply = x.Reply,
                    Status = x.Status,
                    Handler = x.Handler,
                    SocialID = x.SocialID,
                    CreateDate = x.CreateDate.HasValue ? x.CreateDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    ReplyTime = x.ReplyTime.HasValue ? x.ReplyTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : "",
                    Note = x.Note
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定聯絡我們紀錄
        /// </summary>
        /// <param name="req">聯絡我們資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.ContactUs req)
        {
            try
            {
                contactUsSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定聯絡我們紀錄
        /// </summary>
        /// <param name="ID">聯絡我們紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                contactUsSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}