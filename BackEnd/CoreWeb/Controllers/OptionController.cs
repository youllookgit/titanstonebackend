﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class OptionController : Controller
    {
        public BaseModel res = new BaseModel();
        public OptionService ops = new OptionService();
        /// <summary>
        /// 取得選項 泛用method
        /// </summary>
        [HttpPost]
        public ActionResult GetByCode(string code)
        {
            try
            {
                var list = ops.GetListByCode(code);
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得國家列表
        [HttpPost]
        public ActionResult GetCountry()
        {
            try
            {
                var list = ops.GetListByCode("Country");
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得所在地列表
        [HttpPost]
        public ActionResult GetLocation()
        {
            try
            {
                var list = ops.GetListByCode("Location");
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

        /// <summary>
        /// 取得 理財趨勢管理 => 文章分類
        /// </summary>
        [HttpPost]
        public ActionResult GetTrade()
        {
            try
            {
                var list = ops.GetListByCode("TradeType");
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }


    }
}