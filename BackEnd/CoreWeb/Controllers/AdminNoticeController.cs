﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class AdminNoticeController : Controller
    {
        public BaseModel res = new BaseModel();
        public AdminNoticeService adminNoticeSrv = new AdminNoticeService();

        /// <summary>
        /// 新增收信人
        /// </summary>
        /// <param name="req">收信人資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.AdminNotice req)
        {
            try
            {
                adminNoticeSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢收信人資料
        /// </summary>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(bool? status)
        {
            try
            {
                var data = adminNoticeSrv.GetList(status);
                res.data = data;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定收信人資料
        /// </summary>
        /// <param name="req">收信人資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.AdminNotice req)
        {
            try
            {
                adminNoticeSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定收信人資料
        /// </summary>
        /// <param name="ID">收信人紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                adminNoticeSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}