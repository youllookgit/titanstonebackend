﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Models;
using CoreWeb.Service;
using CoreWeb.Lib;

namespace CoreWeb.Controllers
{
    [Filter.BEActionFilter.ApiPermission]
    public class RentController : Controller
    {
        public BaseModel res = new BaseModel();
        public RentDataService rentSrv = new RentDataService();
        public OptionService optSrv = new OptionService();

        /// <summary>
        /// 新增租賃
        /// </summary>
        /// <param name="req">租賃資料</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DbContent.RentData req)
        {
            try
            {
                rentSrv.Create(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢租賃紀錄
        /// </summary>
        /// <param name="startDate">查詢起日</param>
        /// <param name="endDate">查詢迄日</param>
        /// <param name="EstateSaleID">地產銷售ID</param>
        /// <param name="CharterDataID">代管紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList(string startDate, string endDate, int? EstateSaleID, int? CharterDataID)
        {
            try
            {
                var statusList = optSrv.GetListByCode("RentStatus");
                var data = rentSrv.GetList(startDate, endDate, EstateSaleID, CharterDataID);
                var list = data.Select(x => new
                {
                    ID = x.ID,
                    EstateSaleID = x.EstateSaleID,
                    CharterDataID = x.CharterDataID,
                    RentStatus = x.RentStatus,
                    dRentStatus = statusList.Where(p => p.OptionValue == x.RentStatus).FirstOrDefault().Name,
                    RentStart = x.RentStart.HasValue ? x.RentStart.Value.ToString("yyyy/MM/dd") : "",
                    RentEnd = x.RentEnd.HasValue ? x.RentEnd.Value.ToString("yyyy/MM/dd") : "",
                    dRentDate = (x.RentStart.HasValue ? x.RentStart.Value.ToString("yyyy/MM/dd") : "") 
                        + "~" + (x.RentEnd.HasValue ? x.RentEnd.Value.ToString("yyyy/MM/dd") : ""),
                    RenterName = x.RenterName,
                    RenterGender = x.RenterGender,
                    RenterIdNo = x.RenterIdNo,
                    RenterBirth = x.RenterBirth.HasValue ? x.RenterBirth.Value.ToString("yyyy/MM/dd") : "",
                    RentDeal = x.RentDeal,
                    RentAmount = x.RentAmount,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"),
                });
                res.data = list;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 修改指定租賃紀錄
        /// </summary>
        /// <param name="req">租賃資料</param>
        /// <returns></returns>
        public ActionResult Modify(DbContent.RentData req)
        {
            try
            {
                rentSrv.Modify(req);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 刪除指定租賃紀錄
        /// </summary>
        /// <param name="ID">租賃紀錄ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                rentSrv.Delete(ID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }

        /// <summary>
        /// 查詢最新一筆有效租客資料
        /// </summary>
        /// <param name="EstateSaleID">地產銷售ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetLatestRenter(int EstateSaleID)
        {
            try
            {
                var data = rentSrv.GetLatestRenter(EstateSaleID);
                if (data != null) 
                {
                    res.data = new
                    {
                        RentDataID = data.ID,
                        RenterName = data.RenterName,
                        RenterGender = data.RenterGender,
                        RenterIdNo = data.RenterIdNo,
                        RenterBirth = data.RenterBirth.HasValue ? data.RenterBirth.Value.ToString("yyyy/MM/dd") : "",
                    };
                }
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.msg = ex.Message;
            }

            return Json(res);
        }
    }
}