﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class BaseModel
    {
       public bool Success { get; set; }
       public string msg { get; set; }
       public object data { get; set; }
       public BaseModel()
       {
            Success = false;
            msg = "";
            data = new object { };
       }
    }
}