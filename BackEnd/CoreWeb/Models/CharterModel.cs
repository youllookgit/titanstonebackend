﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class CharterModel
    {
        /// <summary>
        /// 代管類型
        /// </summary>
        public string ManagerType { get; set; }
        /// <summary>
        /// 合約期間
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 包租金額/月
        /// </summary>
        public string CharterAmount { get; set; }
        /// <summary>
        /// 租貸期間
        /// </summary>
        public string RentEnd { get; set; }
        /// <summary>
        /// 租客名稱
        /// </summary>
        public string RenterName { get; set; }
        /// <summary>
        /// 租客性別
        /// </summary>
        public string RenterGender { get; set; }
        /// <summary>
        /// 租客ID/護照
        /// </summary>
        public string RenterIdNo { get; set; }
        /// <summary>
        /// 租客生日
        /// </summary>
        public string RenterBirth { get; set; }
        /// <summary>
        /// 租賃金額/月
        /// </summary>
        public string RentAmount { get; set; }       
        /// <summary>
        /// 建立時間
        /// </summary>
        public string CreateDate { get; set; }

    }

}