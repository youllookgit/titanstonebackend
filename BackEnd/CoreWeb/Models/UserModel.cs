﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class UserModel
    {
       public string CreateDate { get; set; }
       public string LoginType { get; set; }
       public string Email { get; set; }
       public bool IsValid { get; set; }
    }
    public class FacebookModel
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class GooglePlusModel
    {
        public string id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
    }
}