﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class FundsDetailModel
    {
        /// <summary>
        /// 交易類型
        /// </summary>
        public string TxnType { get; set; }
        /// <summary>
        /// 交易單號
        /// </summary>
        public string TxnNo { get; set; }
        /// <summary>
        /// 交易時間
        /// </summary>
        public string TxnDate { get; set; }
        /// <summary>
        /// 單位數
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 贖回淨值
        /// </summary>
        public string RedemptionVal { get; set; }
        /// <summary>
        /// 赎回总金额(不含手续费)
        /// </summary>
        public string RedemptionAmt { get; set; }
        /// <summary>
        /// 绩效费
        /// </summary>
        public string PrefFee { get; set; }
        /// <summary>
        /// 申购淨值
        /// </summary>
        public string PurchaseVal { get; set; }
        /// <summary>
        /// 申购金额(不含手续费)
        /// </summary>
        public string PurchaseAmt { get; set; }
        /// <summary>
        /// 手续费
        /// </summary>
        public string HandlingFee { get; set; }
        /// <summary>
        /// 分配金額
        /// </summary>
        public string DividendAmt { get; set; }
        /// <summary>
        /// 缴费状态
        /// </summary>
        public string PayStatus { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 建立时间
        /// </summary>
        public string CreateDate { get; set; }
        
    }

}