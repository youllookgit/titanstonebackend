﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class ChannelModel
    {
        public int? Id { get; set; }
        public string Account { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        public string Name { get; set; }
        public string Lacation { get; set; }
        public string Unit { get; set; }
        public string Phone { get; set; }
        public bool Enabled { get; set; }
        public List<bool> Permission { get; set; }
    }
}