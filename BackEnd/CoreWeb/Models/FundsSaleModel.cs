﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class FundsSaleModel
    {
        public int ID { get; set; }
        public string FundsNo { get; set; }
        public string UserIdentityNo { get; set; }
        public string Channel { get; set; }
        public string ChannelAct { get; set; }
        public string ChannelName { get; set; }
        public string BuyerName { get; set; }
        public int FundsID { get; set; }
        public string FundsName { get; set; }
        public string FundType { get; set; }
        public string BuyType { get; set; }
        public string Note { get; set; }
        public System.DateTime CreateDate { get; set; }
        /// <summary>
        /// 累積申購金額
        /// </summary>
        public double PurchaseTotal { get; set; }
    }

    public class ExportFundsSaleModel
    {        
        public string FundsNo { get; set; }
        public string UserIdentityNo { get; set; }
        public string Channel { get; set; }
        public string BuyerName { get; set; }
        //public int FundsID { get; set; }
        public string FundType { get; set; }
        public string FundsName { get; set; }
        public string BuyType { get; set; }
        public double PurchaseTotal { get; set; }
        public string Note { get; set; }
        public string CreateDate { get; set; }
        
    }
}