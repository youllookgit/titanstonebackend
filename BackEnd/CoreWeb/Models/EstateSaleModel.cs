﻿using System;
using System.Web;

namespace CoreWeb.Models
{
    public class EstateSaleModel
    {
        /// <summary>
        /// 系統編號
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 物件ID
        /// </summary>
        public string EstateNo { get; set; }
        /// <summary>
        /// 交屋狀態
        /// </summary>
        public string IsFinish { get; set; }
        /// <summary>
        /// 購買人ID
        /// </summary>
        public string UserIdentityNo { get; set; }
        /// <summary>
        /// 所屬渠道
        /// </summary>
        public string Channel { get; set; }
        /// <summary>
        /// 購買人姓名
        /// </summary>
        public string BuyerName { get; set; }
        /// <summary>
        /// 地產物件ID
        /// </summary>
        public int? EstateObjID { get; set; }
        /// <summary>
        /// 地產房型ID
        /// </summary>
        public int? EstateRoomID { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Addr { get; set; }
        /// <summary>
        /// 樓層
        /// </summary>
        public string Floor { get; set; }
        /// <summary>
        /// 單元面積
        /// </summary>
        public string UnitArea { get; set; }
        /// <summary>
        /// 套內面積
        /// </summary>
        public string RoomArea { get; set; }
        /// <summary>
        /// 公設面積
        /// </summary>
        public string PublicArea { get; set; }
        /// <summary>
        /// 購買總價
        /// </summary>
        public double? PriceSum { get; set; }
        /// <summary>
        /// 銷售合約
        /// </summary>
        public string DealFile { get; set; }
        /// <summary>
        /// 簽約日期
        /// </summary>
        public DateTime? SignDate { get; set; }
        /// <summary>
        /// 是否有車位
        /// </summary>
        public string ParkingType { get; set; }
        /// <summary>
        /// 是否有車位
        /// </summary>
        public string ParkingTypeName { get; set; }
        /// <summary>
        /// 車位價格
        /// </summary>
        public double? ParkingPrice { get; set; }
        /// <summary>
        /// 代管狀態
        /// </summary>
        public string ManagerType { get; set; }
        /// <summary>
        /// 代管狀態資料ID
        /// </summary>
        public int? ManagerDataID { get; set; }
        /// <summary>
        /// 系统通知
        /// </summary>
        public bool? IsPush { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime UpdDate { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 地產物件名稱
        /// </summary>
        public string EstateObjName { get; set; }
        /// <summary>
        /// 地產房型名稱
        /// </summary>
        public string EstateRoomName { get; set; }
        /// <summary>
        /// 租賃狀態
        /// </summary>
        public string RentStatus { get; set; }
    }
}
							