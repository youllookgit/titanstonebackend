﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    //匯出用model
    public class ImportModel
    {

        public class EstateData
        {
            public string EstateNo { get; set; } //必填
            public string IsFinish { get; set; } //必填
            public string UserIdentityNo { get; set; } //必填
            public string Channel { get; set; }
            public string BuyerName { get; set; } //必填
            public string EstateObjName { get; set; } //必填
            public string Room { get; set; } //必填
            public string Addr { get; set; } //必填
            public string Floor { get; set; } //必填
            public string Import { get; set; }
            public string UnitArea { get; set; }
            public string RoomArea { get; set; }
            public string PublicArea { get; set; }
            public string PriceSum { get; set; }
            public string SignDate { get; set; }
            public string IsPush { get; set; } //必填
            public string ParkingType { get; set; } //必填
            public string ParkingPrice { get; set; }
            public string Status { get; set; }
        }

        public class UserData
        {
            public string Act { get; set; } //必填
            public string Pwd { get; set; } //必填
            public string Name { get; set; } //必填
            public string IdentityNo { get; set; }
            public string Birth { get; set; }
            public string Country { get; set; } //必填
            public string PhonePrefix { get; set; }
            public string Phone { get; set; }
            public string Channel { get; set; }

        }

        public class PayRecordData
        {
            public string SchNo { get; set; } //必填
            public float Amount { get; set; } //必填
            public string PayStatus { get; set; } //必填
            public string Note { get; set; }
        }

        public class BenefitRecordData
        {
            public string CharterType { get; set; } 
            public string RenterName { get; set; }
            public string RenterGender { get; set; }
            public string RenterIdNo { get; set; }
            public DateTime? RenterBirth { get; set; }
            public float? Income { get; set; }
            public DateTime? IncomeDate { get; set; }
            public DateTime? PayDate { get; set; }
            public float? Expense { get; set; }
            public string PayStatus { get; set; }
            public string Note { get; set; }
        }

        public class CharterData
        {
            public string ManagerType { get; set; } //必填
            public DateTime StartDate { get; set; } //必填
            public DateTime EndDate { get; set; } //必填

            public DateTime? RentStart { get; set; } //必填
            public DateTime? RentEnd { get; set; } //必填
            public string RenterName { get; set; } //必填
            public string RenterGender { get; set; } 
            public string RenterIdNo { get; set; }
            public DateTime? RenterBirth { get; set; } //必填
            public float RentAmount { get; set; }
        }

        public class FundSale
        {
            public string FundsNo { get; set; }
            public string UserIdentityNo { get; set; }
            public string Channel { get; set; }
            public string BuyerName { get; set; }
            public string FundsName { get; set; }
            public string BuyType { get; set; }
            public string Note { get; set; }
        }

        public class FundSaleDetail
        {
            public int FundsSaleID { get; set; }
            public string TxnType { get; set; }
            public string TxnNo { get; set; }
            public DateTime? TxnDate { get; set; }
            public double? Unit { get; set; }
            public double? PurchaseVal { get; set; }
            public double? PurchaseAmt { get; set; }
            public double? RedemptionVal { get; set; }
            public double? RedemptionAmt { get; set; }
            public double? PrefFee { get; set; }
            public double? DividendAmt { get; set; }
            public double? HandlingFee { get; set; }
            public string PayStatus { get; set; }
            public string Note { get; set; }
        }

        public class InsSale : DbContent.InsuranceSale
        {
            public string InsName { get; set; }
        }
    }
}