﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class Login
    {
        public string Act { get; set; }
        public string Pwd { get; set; }
        public string Type { get; set; }
    }
    public class LoginData
    {
        public string Act { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string LoginType { get; set; }
        public List<SysPermissionItem> Permission { get; set; }
    }
    public class AdminApiModel
    {
        public int? ID { get; set; }
        public string Act { get; set; }
        public string Name { get; set; }
        public string Pwd { get; set; }
        public bool Status { get; set; }
        public string CreateDate { get; set; }
    }
    public class AdminModel
    {
       public int? ID { get; set; }
       public string Act { get; set; }
       public string Name { get; set; }
       public string Pwd { get; set; }
       public bool Status { get; set; }
       public List<SysPermissionItem> Permission { get; set; }
       public DateTime CreateDate { get; set; }
    }
    public class ChannelEditModel
    {
        public int? ID { get; set; }
        public string Act { get; set; }
        public string Name { get; set; }
        public string Pwd { get; set; }
        public string Lacation { get; set; }
        public string Unit { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public List<SysPermissionItem> Permission { get; set; }
        public DateTime CreateDate { get; set; }
    }
}