﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class PayRecordModel
    {
        /// <summary>
        /// 物件ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 物件名稱
        /// </summary>
        public string EstateObjName { get; set; }
        /// <summary>
        /// 房型
        /// </summary>
        public string EstateRoomName { get; set; }
        /// <summary>
        /// 購買人ID
        /// </summary>
        public string UserIdentityNo { get; set; }
        /// <summary>
        /// 期數
        /// </summary>
        public string EstateScheduleID { get; set; }
        /// <summary>
        /// 本期應付金額
        /// </summary>
        public string Amount { get; set; }
        /// <summary>
        /// 繳款狀態
        /// </summary>
        public string PayStatus { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 建立时间
        /// </summary>
        public string CreateDate { get; set; }
    }

}