﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    //匯出用model
    public class ExportModel
    {
        //系統管理
        public class AdminData
        {
            public string ID { get; set; }
            public string Act { get; set; }
            public string Name { get; set; }
            public string Permission { get; set; }
            public string CreateDate { get; set; }
            public string Status { get; set; }
        }
        //渠道管理
        public class ChannelData
        {
            public string Act { get; set; }
            public string Name { get; set; }
            public string dLocation { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Unit { get; set; }
            public string Permission { get; set; }
            public string CreateDate { get; set; }
            public string Status { get; set; }
        }
        /// <summary>
        /// 會員資料管理
        /// </summary>
        public class Member
        {
            public string Act { get; set; }
            public string Valid { get; set; }
            public string Name { get; set; }
            public string IdentityNo { get; set; }
            public string Birth { get; set; }
            public string Country { get; set; }
            public string Phone { get; set; }
            public string Channel { get; set; }
            public string CreateDate { get; set; }
            public string Status { get; set; }
        }

        public class Preference
        {
            public string Email { get; set; }
            public string Name { get; set; }
            public string IdentityNo { get; set; }
            public string UpdateDate { get; set; }
            public string q1 { get; set; } = "";
            public string q2 { get; set; } = "";
            public string q3 { get; set; } = "";
            public string q4 { get; set; } = "";
            public string q5 { get; set; } = "";
            public string q6 { get; set; } = "";
            public string q7 { get; set; } = "";
        }
        public class EstateSale
        {
            /// <summary>
            /// 物件ID
            /// </summary>
            public string EstateNo { get; set; }
            /// <summary>
            /// 購買人ID
            /// </summary>
            public string UserIdentityNo { get; set; }
            /// <summary>
            /// 所屬渠道
            /// </summary>
            public string Channel { get; set; }
            /// <summary>
            /// 購買人姓名
            /// </summary>
            public string BuyerName { get; set; }
            /// <summary>
            /// 地產物件名稱
            /// </summary>
            public string EstateObjName { get; set; }
            /// <summary>
            /// 地產房型名稱
            /// </summary>
            public string EstateRoomName { get; set; }
            /// 地址
            /// </summary>
            public string Addr { get; set; }
            /// <summary>
            /// 樓層
            /// </summary>
            public string Floor { get; set; }
            /// <summary>
            /// 單元面積
            /// </summary>
            public string UnitArea { get; set; }
            /// <summary>
            /// 套內面積
            /// </summary>
            public string RoomArea { get; set; }
            /// <summary>
            /// 公設面積
            /// </summary>
            public string PublicArea { get; set; }
            /// <summary>
            /// 購買總價
            /// </summary>
            public string PriceSum { get; set; }
            /// <summary>
            /// 簽約日期
            /// </summary>
            public string SignDate { get; set; }
            /// <summary>
            /// 系统通知缴款
            /// </summary>
            public string SystemStatus { get; set; }
            /// <summary>
            /// 是否有車位
            /// </summary>
            public string ParkingType { get; set; }
            /// <summary>
            /// 車位價格
            /// </summary>
            public string ParkingPrice { get; set; }
            /// <summary>
            /// 租賃狀態
            /// </summary>
            public string RentStatus { get; set; }
            /// <summary>
            /// 建立日期
            /// </summary>
            public string CreateDate { get; set; }
            /// <summary>
            /// 狀態
            /// </summary>
            public string Status { get; set; }

        }

        public class ContactUs
        {
            /// <summary>
            /// 聯絡人姓名
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 信箱
            /// </summary>
            public string Email { get; set; }
            /// <summary>
            /// 國籍
            /// </summary>
            public string Country { get; set; }
            /// <summary>
            /// 連絡電話
            /// </summary>
            public string Phone { get; set; }
            /// <summary>
            /// Line/WeChatID
            /// </summary>
            public string SocialID { get; set; }
            /// <summary>
            /// 聯絡主題
            /// </summary>
            public string Subject { get; set; }
            /// <summary>
            /// 選擇項目
            /// </summary>
            public string SeletedItem { get; set; }
            /// <summary>
            /// 聯絡時段
            /// </summary>
            public string ContactTime { get; set; }
            /// <summary>
            /// 问题详述
            /// </summary>
            public string Detail { get; set; }
            /// <summary>
            /// 回覆內容
            /// </summary>
            public string Reply { get; set; }
            /// <summary>
            /// 處理狀態
            /// </summary>
            public string Status { get; set; }
            /// <summary>
            /// 處理人員
            /// </summary>
            public string Handler { get; set; }
            /// <summary>
            /// 發送時間
            /// </summary>
            public string ReplyTime { get; set; }
            /// <summary>
            /// 備註
            /// </summary>
            public string Note { get; set; }
        }

        public class Seminar
        {         
            public string Title { get; set; }
            public string StartTime { get; set; }
            public string Addr { get; set; }
            public string ContactName { get; set; }
            public string Country { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string Count { get; set; }
            public string Detail { get; set; }            
            public string SendDate { get; set; }
            public string Note { get; set; }
            
        }
    }
}