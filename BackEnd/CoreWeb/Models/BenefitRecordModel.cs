﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class BenefitRecordModel
    {
        /// <summary>
        /// 物件ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 物件名稱
        /// </summary>
        public string EstateObjName { get; set; }
        /// <summary>
        /// 房型
        /// </summary>
        public string EstateRoomName { get; set; }
        /// <summary>
        /// 購買人ID
        /// </summary>
        public string UserIdentityNo { get; set; }
        /// <summary>
        /// 類型
        /// </summary>
        public string CharterType { get; set; }
        /// <summary>
        /// 租客姓名
        /// </summary>
        public string RenterName { get; set; }
        /// <summary>
        /// 租客性別
        /// </summary>
        public string RenterGender { get; set; }
        /// <summary>
        /// 租客ID/護照
        /// </summary>
        public string RenterIdNo { get; set; }
        /// <summary>
        /// 租客生日
        /// </summary>
        public string RenterBirth { get; set; }
        /// <summary>
        /// 月份
        /// </summary>
        public string IncomeDate { get; set; }
        /// <summary>
        /// 收入金額
        /// </summary>
        public string Income { get; set; }
        /// <summary>
        /// 支出金額
        /// </summary>
        public string Expense { get; set; }
        /// <summary>
        /// 撥款狀態
        /// </summary>
        public string PayStatus { get; set; }
        /// <summary>
        /// 明細
        /// </summary>
        public string PdfFile { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public string CreateDate { get; set; }
    }

}