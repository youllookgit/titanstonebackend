﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class ScanObj
    {
        public string BarCode { get; set; }
        public string ScanType { get; set; }
    }
    public class ScanRecord
    {
        public string id { get; set; }
        public string name { get; set; }
        public string brand { get; set; }
        public string note { get; set; }
        public string barcode { get; set; }
        public int count { get; set; }
    }

}