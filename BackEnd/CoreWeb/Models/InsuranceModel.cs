﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class InsuranceModel
    {
        public int ID { get; set; }
        /// <summary>
        /// 保單名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 保單名稱(英文)
        /// </summary>
        public string NameEn { get; set; }
        /// <summary>
        /// 人身保險
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 人身保险(英文)
        /// </summary>
        public string TypeNameEn { get; set; }
        public string Note { get; set; }
        public string CreateDate { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
    }

}