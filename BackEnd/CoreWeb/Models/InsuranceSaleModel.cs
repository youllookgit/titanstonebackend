﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class InsuranceSaleModel
    {
        public int ID { get; set; }
        public int InsID { get; set; }
        public string InsName { get; set; }
        public string InsSortName { get; set; }
        public string InsNo { get; set; }
        public string UserIdentityNo { get; set; }
        public string Channel { get; set; }
        public string ChannelName { get; set; }
        public string BuyerName { get; set; }
        public string BenefitName { get; set; }
        public string SaleName { get; set; }
        public string SalePhone { get; set; }
        public string InsStatus { get; set; }
        public string StatusName { get; set; }
        public string ValidDate { get; set; }
        public string ValidStart { get; set; }
        public string ValidEnd { get; set; }
        public string ValidRange { get; set; } //保障起訖日
        public string PayPeriod { get; set; }
        public string PayType { get; set; }
        public string PayTypeName { get; set; }
        public string Currency { get; set; }
        public string CurrencyEn { get; set; }
        public double Amount { get; set; }
        public double? InsAmount { get; set; }
        public string InsFee { get; set; }
        public string InsContent { get; set; }
        public string InsContentEn { get; set; }
        public string InsNote { get; set; }
        public string InsNoteEn { get; set; }
        public string Recommend { get; set; }
        public string DealFile { get; set; }
        public string Note { get; set; }
        public string CreateDate { get; set; }
    }

    public class ExportInsModel
    {
        public string InsNo { get; set; }
        public string UserIdentityNo { get; set; }
        public string Channel { get; set; }
        public string BuyerName { get; set; }
        public string BenefitName { get; set; }
        public string SaleName { get; set; }
        public string SalePhone { get; set; }
        public string InsID { get; set; }
        public string InsSortName { get; set; }
        public string InsStatus { get; set; }
        public string ValidDate { get; set; }
        public string ValidStart { get; set; }
        public string ValidEnd { get; set; }
        public string PayPeriod { get; set; }
        public string PayType { get; set; }
        public string Currency { get; set; }
        public string CurrencyEn { get; set; }
        public string Amount { get; set; }
        public string InsContent { get; set; }
        public string InsContentEn { get; set; }
        public string InsNote { get; set; }
        public string InsNoteEn { get; set; }
        public string Recommend { get; set; }
        public string DealFile { get; set; }
        public string Note { get; set; }
        public string CreateDate { get; set; }
    }
}