﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class PushModel
    {
        public string PType { get; set; }
        public string Lang { get; set; }
        public string Detail { get; set; }
        public string TargetType { get; set; }
        public string DeviceIDs { get; set; }
    }
}