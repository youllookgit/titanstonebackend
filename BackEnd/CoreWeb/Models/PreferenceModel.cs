﻿using System;
using System.Collections.Generic;
using System.Web;

namespace CoreWeb.Models
{
    public class PreferenceModel 
    {
        /// <summary>
        /// UserID
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ID/身分證號碼
        /// </summary>
        public string IdentityNo { get; set; }
        /// <summary>
        /// 更新時間
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// Item
        /// </summary>
        public List<PreferenceItem> ItemData { get; set; }
        /// <summary>
        /// 初始
        /// </summary>
        public PreferenceModel()
        {
            ItemData = new List<PreferenceItem>();
        }
    }

    public class PreferenceItem
    {
        public int? PrefernceItemID { get; set; }
        public string PrefernceName { get; set; }
        public string Ans { get; set; }
    }   
}
