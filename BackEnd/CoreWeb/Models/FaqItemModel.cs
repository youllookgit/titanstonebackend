﻿using System;
using System.Web;

namespace CoreWeb.Models
{
    public class FaqItemModel
    {
        // 系統編號
        public int ID { get; set; }
        // 狀態
        public bool Status { get; set; }
        // 標題
        public string Title { get; set; }
        // 標題En
        public string TitleEn { get; set; }
        // 內容
        public string Detail { get; set; }
        // 內容En
        public string DetailEn { get; set; }
        // 建立時間
        public DateTime CreateDate { get; set; }
        // 排序編號
        public int? OrderNo { get; set; }
        // 分類ID
        public int? FaqSortID { get; set; }
        // 分類名稱
        public string FaqSortName { get; set; }

    }
}
