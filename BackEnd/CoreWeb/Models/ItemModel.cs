﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Models
{
    public class ItemCateModel
    {
       public int ID { get; set; }
       public string Name { get; set; }
       public int Count { get; set; }
    }
    public class ItemDataModel
    {
        public int ID { get; set; }
        public int CateID { get; set; }
        public string CateName { get; set; }
        public string Name { get; set; }
        public bool top { get; set; }
    }

    public class ImportCateModel
    {
        public int? ItemCateID { get; set; }
        public string CateName { get; set; }
        public List<ImportDataModel> ItemData { get; set; }
        public ImportCateModel()
        {
            ItemData = new List<ImportDataModel>();
        }
    }
    public class ImportDataModel
    {
        public int? ItemDataID { get; set; }
        public string DataName { get; set; }
    }
}