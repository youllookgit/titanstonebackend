//------------------------------------------------------------------------------
// <auto-generated>
//    這個程式碼是由範本產生。
//
//    對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//    如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreWeb.DbContent
{
    using System;
    using System.Collections.Generic;
    
    public partial class Activity
    {
        public int ID { get; set; }
        public string ActType { get; set; }
        public string Name { get; set; }
        public string NameEn { get; set; }
        public Nullable<System.DateTime> ActDate { get; set; }
        public string ActArea { get; set; }
        public string ActPosition { get; set; }
        public string ActPositionEn { get; set; }
        public string ActAddr { get; set; }
        public string ActAddrEn { get; set; }
        public string Brief { get; set; }
        public string BriefEn { get; set; }
        public string Img { get; set; }
        public string Album { get; set; }
        public string EstateObjIDs { get; set; }
        public string Process { get; set; }
        public string ProcessEn { get; set; }
        public string Traffic { get; set; }
        public string TrafficEn { get; set; }
        public string Detail { get; set; }
        public string DetailEn { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public bool Status { get; set; }
    }
}
