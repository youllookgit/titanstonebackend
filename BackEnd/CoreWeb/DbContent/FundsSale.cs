//------------------------------------------------------------------------------
// <auto-generated>
//    這個程式碼是由範本產生。
//
//    對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//    如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreWeb.DbContent
{
    using System;
    using System.Collections.Generic;
    
    public partial class FundsSale
    {
        public int ID { get; set; }
        public string FundsNo { get; set; }
        public string UserIdentityNo { get; set; }
        public string Channel { get; set; }
        public string BuyerName { get; set; }
        public int FundsID { get; set; }
        public string FundsName { get; set; }
        public string FundType { get; set; }
        public string BuyType { get; set; }
        public string Note { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
