﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;
using System.IO;

namespace CoreWeb.Areas.App.Controllers
{
    public class UserController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiUserService usSrv = new apiUserService();

        //註冊
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Regist(RegistModel model)
        {
            try
            {
                usSrv.Create(model);
                res.Success = true;
                usSrv.SaveMailLog("1", model.Act);
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //登入
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult LogIn(UserLoginModel model)
        {
            try
            {
                var user =  usSrv.LogIn(model);
                user.IdentityNo = (string.IsNullOrEmpty(user.IdentityNo)) ? "" : user.IdentityNo;
                res.data = user;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //圖片上傳
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult ImgUpd(HttpPostedFileBase upload, string filename)
        {
            if (upload != null)
            {
                try
                {
                    string fileRealName = Guid.NewGuid().ToString("N");
                    var nameExtend = upload.FileName.Split('.');
                    if (string.IsNullOrEmpty(filename))
                    {
                        filename = upload.FileName.Replace("." + nameExtend[nameExtend.Length - 1], "");
                    }
                    var newFileName = fileRealName + "." + nameExtend[nameExtend.Length - 1];
                    newFileName = newFileName.Trim(' ');
                    var fileSavePath = Lib.FileLib.rootPath + Lib.FileLib.ImageUserFolder + newFileName;
                    fileSavePath = fileSavePath.Trim(' ');
                    //Save Image
                    System.Drawing.Image sourceimage =
                           System.Drawing.Image.FromStream(upload.InputStream);
                    sourceimage.Save(fileSavePath);
                    return Json(new { Success = true, filepath = FileLib.rootUrl + FileLib.ImageUserFolder + newFileName });
                }
                catch (Exception ex)
                {
                    return Json(new { Success = false, msg = ex.Message });
                }
            }
            else
            {
                return Json(new { Success = false, msg = "File is Null!" });
            }

        }
        //註冊通知設定
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult RegistNotice(DeviceModel model)
        {
            try
            {
                var notice = usSrv.GetNotice(model);
                res.data = notice;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //修改通知設定
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult NoticeUpd(DbContent.DeviceNotice model)
        {
            try
            {
                usSrv.UpdateNotice(model);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //忘記密碼
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Forget(string Mail)
        {
            try
            {
                var lang = apiUserService.GetLang();
                var user = usSrv.GetUserByEmail(Mail);
                // TODO: 發送密碼重置信
                string HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/reset_pwd.html");
                if(lang == LangLib.en)
                {
                    HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/reset_pwdEn.html");
                }
                HtmlContent = HtmlContent.Replace("{NAME}", user.Name);
                HtmlContent = HtmlContent.Replace("{URL}", FileLib.rootUrl + "/Home/ResetPwd?Param=" + Security.Encrypt(user.ID.ToString()) + "&lang=" + lang);
                MailLib.SendMail(HtmlContent, "Titan Stone Change Passwords", user.Act);
                res.Success = true;
                //紀錄 EmailLog
                usSrv.SaveMailLog("2",Mail);
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //==========需登入才能打通==========//
        //更新投資意向
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult UpdateUser(apiUserModel model)
        {
            var user = apiUserService.GetUser();
            try
            {
                //簡單防呆
                model.UserID = user.ID;
                usSrv.UpdateUser(model);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //更新投資意向
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult UpdatePrefer(List<PreferModel> model)
        {
            //GetUser
            var user = apiUserService.GetUser();
            try
            {
                usSrv.UpdatePrefer(user.ID, model);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //更新密碼
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult UpdatePwd(string NewPwd,string OldPwd)
        {
            //GetUser
            var user = apiUserService.GetUser();
            try
            {
                usSrv.UpdatePwd(user.ID, OldPwd, NewPwd);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //更新使用者圖片
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult SaveImg(string img)
        {
            //GetUser
            var user = apiUserService.GetUser();
            string lang = apiUserService.GetLang();
            try
            {
                usSrv.SaveImg(user.ID,img);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得UserChart
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult Chart()
        {
            //GetUser
            var user = apiUserService.GetUser();
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetChart(user, lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 房產資訊
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyEstate()
        {
            //GetUser
            var user = apiUserService.GetUser();
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyEstate(user, lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 房產細節資訊
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyEstateItem(int id)
        {
            //GetUser
            var user = apiUserService.GetUser();
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyEstateItem(id, user, lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 基金列表
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyFund()
        {
            //GetUser
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyFundList(lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 基金內容
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyFundData(int id)
        {
            //GetUser
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyFundDetail(id,lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 基金內容
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyFundReport(int id)
        {
            //GetUser
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.MyFundReport(id, lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        public ActionResult MyInsuranceList()
        {
            //GetUser
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyInsuranceList(lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得User 基金內容
        [ApiActionFilter.ApiPermission]
        [HttpPost]
        public ActionResult MyInsuranceData(int id)
        {
            //GetUser
            string lang = apiUserService.GetLang();
            try
            {
                var source = usSrv.GetMyInsuranceData(id, lang);
                res.Success = true;
                res.data = source;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}