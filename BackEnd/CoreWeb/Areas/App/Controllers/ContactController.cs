﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class ContactController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiContactService srv = new apiContactService();
        //註冊
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Post(apiContactUs data)
        {
            try
            {
                srv.Post(data);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}