﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class SeminarController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiSeminarService activitydSrv = new apiSeminarService();
        //取得精選活動List
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetSeminarData(string ActType, string ActArea)
        {
            try
            {
                string lang = apiUserService.GetLang();
                var data = activitydSrv.GetSeminarList(ActType, ActArea, lang);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得精選活動內容
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetSeminar(int id)
        {
            string lang = apiUserService.GetLang();
            try
            {
                var data = activitydSrv.GetSeminarDetail(id, lang);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //報名說明會
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult SignList(int id)
        {
            string lang = apiUserService.GetLang();
            try
            {
                var data = activitydSrv.GetSeminarOption(id, lang);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //報名說明會
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult SignPost(SignPost data)
        {
            string lang = apiUserService.GetLang();
            try
            {
                activitydSrv.SignSave(data, lang);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

    }
}
