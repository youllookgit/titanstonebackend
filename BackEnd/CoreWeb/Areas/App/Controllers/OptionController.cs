﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class OptionController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiOptionService ser = new apiOptionService();

        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetOptions(string code)
        {
            try
            {
                string lang = apiUserService.GetLang();
                List<apiOption> model = new List<apiOption>();
                var list = ser.GetList(code);
                foreach (var item in list)
                {
                    var _option = new apiOption()
                    {
                        id = item.ID,
                        name = (lang == LangLib.en) ? item.NameEn : item.Name,
                        value = item.OptionValue,
                        extend1 = item.Extend1,
                        extend2 = item.Extend2
                    };
                    model.Add(_option);
                }
                res.data = model;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}