﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class TrendController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiTrendService trendSrv = new apiTrendService();
        
        //取得理財趨勢列表
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetTrendList(string trendtype)
        {
            try
            {
                string lang = apiUserService.GetLang();
                res.data = trendSrv.GetTrendList(trendtype, lang);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

        //取得理財趨勢內容
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetTrendData(int id)
        {
            try
            {
                string lang = apiUserService.GetLang();
                res.data = trendSrv.GetTrendData(id,lang);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}
