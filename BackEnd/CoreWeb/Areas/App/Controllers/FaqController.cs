﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class FaqController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiFaqService srv = new apiFaqService();
        
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetList()
        {
            string lang = apiUserService.GetLang();
            try
            {
                var data = srv.GetList(lang);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetDetail(int id)
        {
            string lang = apiUserService.GetLang();
            try
            {
                var data = srv.GetDetail(id,lang);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}