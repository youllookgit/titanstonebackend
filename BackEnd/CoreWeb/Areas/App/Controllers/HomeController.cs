﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class HomeController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiHomeService srv = new apiHomeService();
        //註冊
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Main()
        {
            try
            {
                bool isLogin = apiUserService.GetUser() != null;
                string lang = apiUserService.GetLang();
                var data = srv.Get(lang, isLogin);
                res.Success = true;
                res.data = data;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}