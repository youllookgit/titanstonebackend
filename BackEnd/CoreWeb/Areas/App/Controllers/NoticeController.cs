﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;
using System.IO;

namespace CoreWeb.Areas.App.Controllers
{
    public class NoticeController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiNoticeService nSrv = new apiNoticeService();

        //取得通知收件夾
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetNoticeList(string pushid)
        {
            try
            {
                var _data = nSrv.GetNoticeList(pushid);
                res.data = _data;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得未讀數
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult UnReadSum(string pushID)
        {
            try
            {
                var count = nSrv.NoticeUnRead(pushID);
                res.data = count;
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得通知收件夾
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Read(string[] pushItemid, string pushID)
        {
            try
            {
                nSrv.NoticeRecordRead(pushItemid, pushID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
        //取得通知收件夾
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult Delete(string[] pushItemid, string pushID)
        {
            try
            {
                nSrv.NoticeRecordDelete(pushItemid, pushID);
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}