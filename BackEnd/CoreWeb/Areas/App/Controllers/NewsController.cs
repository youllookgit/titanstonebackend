﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
using CoreWeb.Areas.App.Service;
using CoreWeb.Filter;

namespace CoreWeb.Areas.App.Controllers
{
    public class NewsController : Controller
    {
        public BaseModel res = new BaseModel();
        public apiNewsService newsdSrv = new apiNewsService();
        //取得官方消息列表
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetNewsList()
        {
            try
            {
                string lang = apiUserService.GetLang();
                var newslist = newsdSrv.GetNewsList(lang);
                res.Success = true;
                res.data = newslist;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }

        //取得官方消息內容
        [ApiActionFilter.IgnorePermission]
        [HttpPost]
        public ActionResult GetNewsData(int id)
        {
            try
            {
                string lang = apiUserService.GetLang();
                var newdata = newsdSrv.GetNewsData(id,lang);
                res.Success = true;
                res.data = newdata;
            }
            catch (Exception ex)
            {
                res.msg = ex.Message;
            }
            return Json(res);
        }
    }
}
