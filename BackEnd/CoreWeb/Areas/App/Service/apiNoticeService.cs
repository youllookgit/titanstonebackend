﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Areas.App.Service
{
    public class apiNoticeService : CoreWeb.Service.BaseService
    {
        //取得通知收件夾
        public List<NoticeMsg> GetNoticeList(string pushid)
        {
            List<NoticeMsg> model = new List<NoticeMsg>();
            var list = basedb.PushLogItem.Where(p => p.PushID == pushid).OrderByDescending(p => p.CreateDate).ToList();
            foreach (var pData in list)
            {
                NoticeMsg m = new NoticeMsg()
                {
                    id = pData.ID,
                    content = pData.Detail,
                    date = pData.CreateDate.ToString("yyyy/MM/dd"),
                    read = pData.IsRead,
                    title = pData.Title
                };
                model.Add(m);
            }

            return model;
        }
        //更改收件夾事件 刪除
        public int NoticeUnRead(string pushID)
        {
            if (string.IsNullOrEmpty(pushID))
            {
                return 0;
            }
            else
            {
                var sum = basedb.PushLogItem.Where(p => p.PushID == pushID && p.IsRead == false).Count();
                return sum;
            }
        }
        //更改收件夾事件 已讀
        public void NoticeRecordRead(string[] pushItemid,string pushID)
        {
            string Ids = string.Join(",", pushItemid);
            string cmd = $"UPDATE PushLogItem SET IsRead = 1 WHERE PushID = '{pushID}' AND ID IN ({Ids})";
            int updCount = basedb.Database.ExecuteSqlCommand(cmd);
        }
        //更改收件夾事件 刪除
        public void NoticeRecordDelete(string[] pushItemid, string pushID)
        {
            string Ids = string.Join(",", pushItemid);
            string cmd = $"DELETE PushLogItem WHERE PushID = '{pushID}' AND ID IN ({Ids})";
            int updCount = basedb.Database.ExecuteSqlCommand(cmd);
        }
    }
}