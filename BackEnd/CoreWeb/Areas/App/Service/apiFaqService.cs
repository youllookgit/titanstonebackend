﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Lib;

namespace CoreWeb.Areas.App.Service
{
    public class apiFaqService : CoreWeb.Service.BaseService
    {
        public List<apiFaq> GetList(string lang)
        {
            List<apiFaq> model = new List<apiFaq>();
            var list = basedb.FaqSort.Where(p => p.Status == true).ToList();
            foreach (var item in list)
            {
                item.OrderNo = (item.OrderNo == 0) ? null : item.OrderNo;
            }
            list = list.OrderByDescending(p => p.OrderNo.HasValue).ThenBy(p => p.OrderNo).ThenByDescending(p => p.CreateDate).ToList();
            foreach (var item in list)
            {
                apiFaq d = new apiFaq()
                {
                    ID = item.ID,
                    Img = FileLib.ImgPathToUrl(item.ImageUrl),
                    Name = (lang == LangLib.cn) ? item.Name : item.NameEn
                };
                model.Add(d);
            }
            return model;
        }
        public List<apiFaqDetal> GetDetail(int sortID,string lang)
        {
            List<apiFaqDetal> model = new List<apiFaqDetal>();
            var list = basedb.FaqItem.Where(p => p.Status == true && p.FaqSortID == sortID).OrderBy(p => p.OrderNo).ToList();
            foreach (var item in list)
            {
                apiFaqDetal d = new apiFaqDetal()
                {
                    ID = item.ID,
                    Title = (lang == LangLib.cn) ? item.Title : item.TitleEn,
                    Detail = (lang == LangLib.cn) ?  FileLib.ImgPathToUrl(item.Detail) : FileLib.ImgPathToUrl(item.DetailEn)
                };
                model.Add(d);
            }
            return model;
        }


    }
}