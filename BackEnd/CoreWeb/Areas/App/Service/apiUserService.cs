﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Areas.App.Service
{
    public class apiUserService : CoreWeb.Service.BaseService
    {
        public apiOptionService optsr = new apiOptionService();
        public void Create(RegistModel model)
        {
            // 驗證EMAIL帳號
            try
            {
                var addr = new System.Net.Mail.MailAddress(model.Act);
                if (addr.Address != model.Act)
                    throw new Exception("Email wrong format"); //請輸入正確的Email格式
            }
            catch (Exception ex)
            {
                throw new Exception("Email wrong format"); //請輸入正確的Email格式
            }

            //驗證是否已被註冊
            var IsExist = basedb.User.Where(p => p.Act == model.Act).FirstOrDefault();
            if (IsExist != null)
                throw new Exception("This Mail Already registered");

            // 驗證密碼
            if (!string.IsNullOrEmpty(model.Pwd))
            {
                model.Pwd = Security.Encrypt(model.Pwd);
            }
            else
                throw new Exception("Password wrong format"); //密碼不能為空值

            // 圖片轉相對路徑儲存
            if (!string.IsNullOrEmpty(model.ImageUrl))
            {
                model.ImageUrl = model.ImageUrl.Replace(FileLib.rootUrl, "");
            }

            //SaveData
            DbContent.User data = new DbContent.User()
            {
                Act = model.Act,
                Pwd = model.Pwd,
                Name = model.Name,
                IdentityNo = "",
                Birth = model.Birth,
                Country = model.Country,
                PhonePrefix = model.PhonePrefix,
                Phone = model.Phone,
                ImageUrl = model.ImageUrl,
                Channel = "",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Valid = false,
                Status = true,
                DeviceID = model.DeviceID,
                Platform = model.Platform,
                Version = model.Version,
                PushID = model.PushID,
                Lang = model.Lang
            };
            basedb.User.Add(data);
            basedb.SaveChanges();

            //新增投資意向預設值
            DbContent.Preference pdata = new DbContent.Preference()
            {
                UserID = data.ID,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };
            basedb.Preference.Add(pdata);
            basedb.SaveChanges();
            foreach (var _pre in model.Prefer)
            {
                DbContent.PreferenceItem pitem = new DbContent.PreferenceItem()
                {
                    PreferenceID = pdata.ID,
                    PType = _pre.PType,
                    PAnswer = _pre.PAnswer
                };
                basedb.PreferenceItem.Add(pitem);
            }
            basedb.SaveChanges();

            //修改通知設定預設值 打開會員可見選項
            var notice = basedb.DeviceNotice.Where(p => p.PushID == model.PushID).FirstOrDefault();
            if (notice != null)
            {
                notice.Pay = true;
                notice.Rent = true;
                basedb.SaveChanges();
            }

            //儲存成功寄MAIL
            var HtmlContent = "";
            var mailTitle = "";
            if (data.Lang == LangLib.en)
            {
                HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/verifyEn.html");
                mailTitle = "TitanStone Account registered successfully";
            }
            else
            {
                HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/verify.html");
                mailTitle = "巨石app帐号注册成功";
            }
                

            HtmlContent = HtmlContent.Replace("{NAME}", model.Name);
            HtmlContent = HtmlContent.Replace("{URL}", FileLib.rootUrl + "/Home/AccountVerify?Param=" + Security.Encrypt(data.ID.ToString()));
            MailLib.SendMail(HtmlContent, mailTitle, model.Act);
           
        }
        public apiUserModel LogIn(UserLoginModel model)
        {
            var user = basedb.User.Where(p => p.Act == model.Act).FirstOrDefault();

            if (user == null)
            {
                string errmsg = (model.Lang == Lib.LangLib.en) ? "This account is not registered or form mistake" : "此帐号尚未注册或格式错误";
                throw new Exception(errmsg); //密碼有誤
            }

            //驗證密碼
            var _pwd = Security.Encrypt(model.Pwd);
            if (_pwd != user.Pwd)
            {
                string errmsg = (model.Lang == Lib.LangLib.en) ? "Your ID or password is incorrect, please confirm again" : "帐号或密码错误,请重新确认";
                throw new Exception(errmsg); //密碼有誤
            }
                

            if (user.Valid == false)
            {
                string errmsg = (model.Lang == Lib.LangLib.en) ? "Account is not activated. We've re-sent a verification link to your email, please click to activate your account." : "帐号尚未完成验证。验证信已重新发送至您的邮箱，请点击信中连结完成验证。";
                //未認證補寄信
                //儲存成功寄MAIL
                var HtmlContent = "";
                var mailTitle = "";
                if (user.Lang == LangLib.en)
                {
                    HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/verifyEn.html");
                    mailTitle = "TitanStone Account Verify";
                }
                else
                {
                    HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/verify.html");
                    mailTitle = "巨石app帐号验证";
                }
                HtmlContent = HtmlContent.Replace("{NAME}", user.Name);
                HtmlContent = HtmlContent.Replace("{URL}", FileLib.rootUrl + "/Home/AccountVerify?Param=" + Security.Encrypt(user.ID.ToString()));
                MailLib.SendMail(HtmlContent, mailTitle, model.Act);
                throw new Exception(errmsg); //此帳號尚未驗證
            }
                

            if (user.Status == false)
            {
                string errmsg = (model.Lang == Lib.LangLib.en) ? "This account has been disabled" : "此帐号已被停用";
                throw new Exception(errmsg);  //此帳號已被停用
            }
                

            user.DeviceID = model.DeviceID;
            user.Platform = model.Platform;
            user.Version = model.Version;
            user.PushID = model.PushID;
            user.Lang = model.Lang;
            //產生登入Token 並儲存
            if (string.IsNullOrEmpty(user.Token))
            {
                var EncodeStr = user.ID.ToString() + "," + model.Pwd;
                var token = Security.Encrypt(EncodeStr);
                user.Token = token;
                user.LastLogin = DateTime.Now;
            }
            basedb.SaveChanges();

            apiUserModel data = new apiUserModel()
            {
                UserID = user.ID,
                Name = user.Name,
                Token = user.Token,
                Act = user.Act,
                IdentityNo = (string.IsNullOrEmpty(user.IdentityNo)) ? "" : user.IdentityNo,
                Birth = (user.Birth == null) ? "" : user.Birth.Value.ToString("yyyy-MM-dd"),
                Country = user.Country,
                PhonePrefix = (string.IsNullOrEmpty(user.PhonePrefix)) ? "" : user.PhonePrefix,
                Phone = (user.Birth == null) ? "" : user.Phone,
                ImageUrl = ""
            };
            //圖片補回絕對路徑
            if (!string.IsNullOrEmpty(user.ImageUrl))
            {
                data.ImageUrl = FileLib.ImgPathToUrl(user.ImageUrl);
            }
            //更新語系
            var DeviceLang = basedb.DeviceNotice.Where(p => p.PushID == model.PushID).FirstOrDefault();
            if(DeviceLang != null)
            {
                DeviceLang.Lang = model.Lang;
                basedb.SaveChanges();
            }

            #region 取得投資意向
            data.Prefer = new List<PreferModel>();
            var preferdata = basedb.Preference.Where(p => p.UserID == user.ID).FirstOrDefault();
            if (preferdata != null)
            {
                var preItems = basedb.PreferenceItem.Where(p => p.PreferenceID == preferdata.ID).ToList();
                foreach (var item in preItems)
                {
                    PreferModel m = new PreferModel()
                    {
                        PType = item.PType,
                        PAnswer = item.PAnswer
                    };
                    data.Prefer.Add(m);
                };
            }
            #endregion

            return data;
        }
        //修改密碼
        public void UpdatePwd(int UserID,string oldpwd,string newpwd)
        {
            var test = Security.Decrypt("S3lJY1orWkhIZ3k4VklTZzArL1VPQT09");
            var user = basedb.User.Where(p => p.ID == UserID).FirstOrDefault();
            var checkpwd = Security.Encrypt(oldpwd);
            if (checkpwd != user.Pwd)
            {
                string errmsg = (user.Lang == Lib.LangLib.en) ? "Your ID or password is incorrect, please confirm again" : "帐号或密码错误,请重新确认";
                throw new Exception(errmsg); //密碼有誤
            }
            user.Pwd = Security.Encrypt(newpwd);

            var EncodeStr = user.ID.ToString() + "," + newpwd;
            var token = Security.Encrypt(EncodeStr);
            user.Token = token;

            basedb.SaveChanges();
        }
        //修改使用者資料
        public void UpdateUser(apiUserModel model)
        {
            var user = basedb.User.Where(p => p.ID == model.UserID).FirstOrDefault();
            user.Birth = DateTime.Parse(model.Birth);
            user.Country = model.Country;
            user.PhonePrefix = model.PhonePrefix;
            user.Phone = model.Phone;
            basedb.SaveChanges();
        }
        //修改投資意向
        public void UpdatePrefer(int UserID,List<PreferModel> list)
        {
            var prefer = basedb.Preference.Where(p => p.UserID == UserID).FirstOrDefault();
            if (prefer != null)
            {
                prefer.UpdateDate = DateTime.Now;
                basedb.SaveChanges();

                var preferItem = basedb.PreferenceItem.Where(p => p.PreferenceID == prefer.ID).ToList();
                foreach (var item in list)
                {
                    var dataItem = preferItem.Where(p => p.PType == item.PType).FirstOrDefault();
                    if (dataItem != null)
                    {
                        dataItem.PAnswer = item.PAnswer;
                    }
                    else
                    {
                        DbContent.PreferenceItem newItem = new DbContent.PreferenceItem()
                        {
                            PreferenceID = prefer.ID,
                            PType = item.PType,
                            PAnswer = item.PAnswer
                        };
                        basedb.PreferenceItem.Add(newItem);
                    }
                }
                basedb.SaveChanges();
            }
        }
        public static string SaveUserImage(string base64)
        {
            string imgName = Guid.NewGuid().ToString().Replace("-", "").ToLower() + ".png";
            string imgPath = FileLib.rootPath + FileLib.ImageSaveFolder + imgName;
            FileLib.WriteImage(imgPath, base64);
            return imgName;
        }
        //設定登入使用者
        public static void SetUser(DbContent.User user)
        {
            HttpContext.Current.Session["TokenUser"] = user;
        }
        //取得登入使用者
        public static DbContent.User GetUser()
        {
            DbContent.User user = null;;
            if (HttpContext.Current.Session["TokenUser"] != null)
            {
                user = (DbContent.User)HttpContext.Current.Session["TokenUser"];
            }
           return user;
        }
        //驗證該user
        public DbContent.User CheckUser(string token)
        {
            var user = basedb.User.Where(p => p.Token == token && p.Valid == true && p.Status == true).FirstOrDefault();
            return user;
        }
        //處理推播設定
        public DbContent.DeviceNotice GetNotice(DeviceModel param)
        {
            var data = basedb.DeviceNotice.Where(p => p.PushID == param.PushID).FirstOrDefault();
            if (data != null)
            {
                data.DeviceID = param.DeviceID;
                data.Platform = param.Platform;
                data.Version = param.Version;
                data.Lang = param.Lang;
                data.UpdDate = DateTime.Now;
                basedb.SaveChanges();
                //更新User語系
                var u = basedb.User.Where(p => p.PushID == param.PushID).FirstOrDefault();
                if (u != null)
                {
                    u.Lang = param.Lang;
                    basedb.SaveChanges();
                }
                return data;
            }
            else
            {
                DbContent.DeviceNotice newdata = new DbContent.DeviceNotice()
                {
                    Pay = true,
                    Rent = true,
                    Estate = true,
                    Activity = true,
                    Trend = true,
                    News = true,
                    System = true,
                    DeviceID = param.DeviceID,
                    Platform = param.Platform,
                    PushID = param.PushID,
                    Version = param.Version,
                    Lang = param.Lang,
                    CreateDate = DateTime.Now
                };
                basedb.DeviceNotice.Add(newdata);
                basedb.SaveChanges();
                //更新User語系
                var u = basedb.User.Where(p => p.PushID == param.PushID).FirstOrDefault();
                if (u != null)
                {
                    u.Lang = param.Lang;
                    basedb.SaveChanges();
                }
                return newdata;
            }
        }
        public void UpdateNotice(DbContent.DeviceNotice model)
        {
            var data = basedb.DeviceNotice.Where(p => p.PushID == model.PushID).FirstOrDefault();
            if (data != null)
            {
                data.Pay = model.Pay;
                data.Rent = model.Rent;
                data.Estate = model.Estate;
                data.Activity = model.Activity;
                data.Trend = model.Trend;
                data.News = model.News;
                data.System = model.System;
                // data.UpdDate = DateTime.Now;
                basedb.SaveChanges();
            }
        }
        //重設密碼
        public DbContent.User GetUserByEmail(string email)
        {
            string lang = apiUserService.GetLang();
            string mailerror = (lang == Lib.LangLib.en) ? "Please enter the correct email format" : "请输入正确的电子邮件格式";
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                if (addr.Address != email)
                    throw new Exception(mailerror);
            }
            catch (Exception ex)
            {
                throw new Exception(mailerror);
            }

            var user = basedb.User.Where(p => p.Act == email).FirstOrDefault();
            if (user == null)
            {
                string errmsg = (lang == Lib.LangLib.en) ? "This account is not registered or form mistake" : "此帐号尚未注册或格式错误";
                throw new Exception(errmsg);
            }
            else
            {
                string validmsg = (lang == Lib.LangLib.en) ? "This account has been disabled" : "此帐号被停用";
                if (user.Status == false)
                    throw new Exception(validmsg);

                user.UpdateDate = DateTime.Now;
                basedb.SaveChanges();
            }

            return user;
        }
        //設定登入使用者
        public static void SetLang(string lang)
        {
            lang = (!string.IsNullOrEmpty(lang)) ? lang : "zh_CN";
            HttpContext.Current.Session["UserLang"] = lang;
        }
        //取得登入使用者
        public static string GetLang()
        {
            if (HttpContext.Current.Session["UserLang"] != null)
            {
                return (string)HttpContext.Current.Session["UserLang"];
            }
            else
            {
                return "zh_CN";
            }
        }
        //儲存圖片
        public void SaveImg(int userid,string img)
        {
            var user = basedb.User.Where(p => p.ID == userid).FirstOrDefault();
            if (user != null)
            {
                user.ImageUrl = FileLib.ImgPathToSQL(img);
                basedb.SaveChanges();
            }
        }
        //取得UserChart
        public List<ChartModel> GetChart(DbContent.User user,string lang)
        {
            var opsr = new apiOptionService();
            List<ChartModel> data = new List<ChartModel>();
            var options = opsr.GetList("ChartType");
            foreach (var item in options)
            {
                ChartModel m = new ChartModel()
                {
                    Name = item.Name,
                    Sum = 0
                };
                if(lang == LangLib.en)
                {
                    m.Name = item.NameEn;
                }
                //加入取得各單元(基金01，地產02，保險03)資料邏輯
                if (item.OptionValue == "01")
                {
                    m.Sum = 0;
                    if (!string.IsNullOrEmpty(user.IdentityNo))
                    {
                        double _fsum = 0;
                        var flist = basedb.FundsSale.Where(p => p.UserIdentityNo == user.IdentityNo).ToList();
                        foreach (var f in flist)
                        {
                            _fsum += FundsCostSum(f.ID);
                        }
                        m.Sum = (_fsum >= 0) ? _fsum : 0;
                    }
                }
                if (item.OptionValue == "02")
                {
                    //取得所有地產加總
                    var elist = basedb.EstateSale.Where(p => p.Status == true &&  p.UserIdentityNo.Contains(user.IdentityNo)).ToList();
                    double _sum = 0;
                    foreach (var edata in elist)
                    {
                        if (edata.PriceSum != null)
                        {
                            _sum = _sum + edata.PriceSum.Value;
                        }
                    }
                    m.Sum = (_sum >= 0) ? _sum : 0;
                }
                if (item.OptionValue == "03")
                {
                    string sumsql = $"SELECT ISNULL((SELECT SUM(Amount) FROM InsuranceSale WHERE UserIdentityNo = '{user.IdentityNo}'),Cast(0 as float)) as Total";
                    var sqlcmd = basedb.Database.SqlQuery<double>(sumsql);
                    var result = sqlcmd.First();
                    
                    m.Sum = (result >= 0) ? result : 0;
                }

                data.Add(m);
            }

            return data;
        }
        //取得User地產資訊
        public MyEstateModel GetMyEstate(DbContent.User user, string lang)
        {
            if (user == null || string.IsNullOrEmpty(user.IdentityNo))
            {
                MyEstateModel empty = new MyEstateModel();
                empty.nowYear = DateTime.Now.Year;
                empty.nowMonth = DateTime.Now.Month;
                empty.Income = new List<double>();
                empty.MyEstate = new List<MyEstateItem>();
                return empty;
            }

            var opsr = new apiOptionService();
            //取得類別資訊
            var SaleType = opsr.GetList("FontSale");

            MyEstateModel model = new MyEstateModel();
            model.nowYear = DateTime.Now.Year;
            model.nowMonth = DateTime.Now.Month;

            #region 取得今年所有月份收入總計
            var _y = DateTime.Now.Year;
            var Ystart = new DateTime(_y, 1, 1);
            var Yend = new DateTime(_y+1, 1, 1);
            //[Step1] 取得 user 可檢視所有房產
            var allestate = basedb.EstateSale.Where(p => p.UserIdentityNo.Contains(user.IdentityNo)).Select(p => p.ID).ToList();
            //[Step2] 取得 所有房產的 整年度(已付款)資料
            var allincome = basedb.BenefitRecord.Where(p => 
            p.PayStatus == "2" && 
            allestate.Contains(p.EstateSaleID) &&
            p.PayDate >= Ystart &&
            p.PayDate <= Yend
            ).ToList();

            //[Step3] 整理並統計至1月至此月份
            List<double> incomeList = new List<double>();
            for (var _m = 0;_m < model.nowMonth; _m++)
            {
                var m_start = new DateTime(_y, (_m + 1), 1);
                var m_end = new DateTime((((_m + 2) > 12) ? _y + 1 : _y), (((_m + 2) > 12) ? ((_m + 2) - 12) : _m + 2), 1);
                //該月 已付款
                var thismImcome = allincome.Where(p => p.PayDate >= m_start && p.PayDate < m_end).ToList();
                var m_In = thismImcome.Sum(p => p.Income);
                m_In = (m_In == null) ? 0 : m_In;
                var m_Out = thismImcome.Sum(p => p.Expense);
                m_Out = (m_Out == null) ? 0 : m_Out;
                var m_sum = m_In - m_Out;
                incomeList.Add(m_sum.Value);
            }
            model.Income = incomeList;

            #endregion

            #region 取得所有屬於該User購買資料
            List<MyEstateItem> estatelist = new List<MyEstateItem>();
            var saledata = basedb.EstateSale.Where(p => p.Status == true && p.UserIdentityNo.Contains(user.IdentityNo)).ToList();
            foreach (var item in saledata)
            {
                MyEstateItem estate = new MyEstateItem();
                estate.ID = item.ID;
                //取得建案資料
                var eObj = basedb.EstateObj.Where(p => p.ID == item.EstateObjID).FirstOrDefault();
                if (eObj != null)
                {
                    estate.Img = FileLib.ImgPathToUrl(eObj.MainImg);
                    estate.EstateName = (lang == LangLib.en) ? eObj.NameEn : eObj.Name;
                    estate.EstateNo = item.EstateNo;
                    estate.ManageType = item.ManagerType;
                }
                //取得房型資料
                var rObj = basedb.EstateRoom.Where(p => p.ID == item.EstateRoomID).FirstOrDefault();
                if(rObj != null)
                {
                    estate.RoomName = rObj.Name;
                }

                //取得銷售狀態 1:預售 2.成屋(畫面不顯示) 3.代租中 4.包租中 5.已出租
                //20200107 若為未交屋 則顯示預售
                if (item.IsFinish == "1")
                {
                    estate.SaleType = "1";
                }
                else
                {
                    estate.SaleType = "2";
                    //檢查合約是否代租/包租 [CharterData] 1:包租 2:委託出租
                    var today = DateTime.Now;
                    var endday = DateTime.Now.AddDays(1);
                    var cdata = basedb.CharterData.Where(p => 
                    p.EstateID == item.ID &&
                    p.StartDate <= today && p.EndDate >= endday).OrderByDescending(p => p.CreateDate).FirstOrDefault();
                    if (cdata != null)
                    {
                        if (cdata.ManagerType == "1")
                        {
                            estate.SaleType = "4";
                        }
                        else
                        {
                            estate.SaleType = "3";
                            //檢查合約是否已出租 [RentData] 1:出租中 2: 待租中
                            var isrent = basedb.RentData.Where(p => 
                            p.EstateSaleID == item.ID &&
                            p.CharterDataID == cdata.ID &&
                            p.RentStart <= today && p.RentEnd >= endday).OrderByDescending(p => p.CreateDate).FirstOrDefault();
                            if (isrent != null)
                            {
                                //合約出租中才顯示已出租
                                if (isrent.RentStatus == "1")
                                {
                                    estate.SaleType = "5";
                                }
                            }
                        }

                    }
                    else
                    {
                        estate.SaleType = "2"; //成屋
                    }
                   
                }
                estate.SaleTypeName = apiOptionService.GetOptName(SaleType, estate.SaleType,lang);

                estatelist.Add(estate);
            }
            model.MyEstate = estatelist;
            #endregion

            return model;
        }
        //取得User地產資訊
        public MyEstateInfo GetMyEstateItem(int EstateSaleID,DbContent.User user, string lang)
        {
            if (user == null || string.IsNullOrEmpty(user.IdentityNo))
                return null;

            //銷售基本資料
            var sale = basedb.EstateSale.Where(p => p.ID == EstateSaleID).FirstOrDefault();
            if (sale == null)
                return null;

            int EstatesID = sale.EstateObjID.Value; //建案ID
            int RoomID = sale.EstateRoomID.Value;   //房型ID
            //建案基本資料
            var eObj = basedb.EstateObj.Where(p => p.ID == EstatesID).FirstOrDefault();
            //房型基本資料
            var RoomData = basedb.EstateRoom.Where(p => p.ID == sale.EstateRoomID).FirstOrDefault();

            //取得類別資訊
            var opsr = new apiOptionService();

            MyEstateInfo model = new MyEstateInfo();
            //總價
            model.Sum = (sale.PriceSum == null) ? 0 : sale.PriceSum.Value;

            #region 處理繳款資訊,繳款期限 及是否繳納 
            //old 建案狀態(SaleType) 1:預售 2.成屋
            //new 交屋狀態(IsFinish) 1:預售 2.成屋
            if (sale.IsFinish == "1")
            {
                PayItem _p = new PayItem()
                {
                   IsPay = true
                };
                //已繳交總金額
                var _paysum = basedb.PayRecord.Where(p =>
                p.EstateObjID == EstatesID &&
                p.EstateSaleID == EstateSaleID &&
                p.PayStatus == "2" &&
                p.Amount != null
                ).Sum(p => p.Amount);
                _p.PaySum = (_paysum == null) ? 0 : _paysum;

                //[STEP1] 先取得工程進度管理 狀態為 3.已完工 的實際完工日期
                var _sch = basedb.EstateSchedule.Where(p =>
                p.EstateObjID == EstatesID &&
                p.ScheduleType == "3"
                ).OrderByDescending(p => p.RealComplete).FirstOrDefault();
                
                //[STEP2] 繳款期限為該工期 實際完工日 + 30天
                if (_sch != null)
                {
                    _p.PayDate = (_sch.RealComplete == null) ? "" : _sch.RealComplete.Value.AddDays(30).ToString("yyyy/MM/dd");
                    //[STEP3] 尋找該工期之繳款紀錄
                    var _precord = basedb.PayRecord.Where(p =>
                    p.EstateObjID == EstatesID &&
                    p.EstateSaleID == EstateSaleID &&
                    p.EstateScheduleID == _sch.ID).FirstOrDefault();
                    if (_precord != null)
                    {
                        _p.PayAmount = _precord.Amount;
                        if (_precord.PayStatus == "1")
                        {
                            _p.IsPay = false;
                        }
                    }
                }
              model.Pay = _p;
            }
            #endregion

            #region 取得當前進度
            var SchTypeOpt = opsr.GetList("ScheduleType");
            model.Schedule = new List<ScheduleItem>();
            var SchData = basedb.EstateSchedule.Where(p => p.EstateObjID == EstatesID).OrderBy(p => p.OrderNo).ToList();

            //重新排序施工紀錄
            foreach (var _s in SchData)
            {
                _s.OrderNo = (_s.OrderNo == 0) ? null : _s.OrderNo;
            }
            SchData = SchData.OrderByDescending(p => p.OrderNo.HasValue).ThenBy(p => p.OrderNo).ThenByDescending(p => p.CreateDate).ToList();

            foreach (var item in SchData)
            {
                ScheduleItem sd = new ScheduleItem()
                {
                    ScheduleID = item.ID,
                    SchNo = (lang == LangLib.en) ? item.SchNoEn : item.SchNo,
                    Name = (lang == LangLib.en) ? item.NameEn : item.Name,
                    Brief = (lang == LangLib.en) ? item.BriefEn : item.Brief,
                    ScheduleType = item.ScheduleType,
                    ScheduleTypeName = ""
                };
                sd.EndDate = (item.CompleteDate == null) ? "" : item.CompleteDate.Value.ToString("yyyy/MM/dd");
                //若已完工 則顯示實際完工日期 1 尚未动工 2 施工中 3 已完工
                if (item.ScheduleType == "3")
                {
                    sd.EndDate = (item.RealComplete == null) ? "" : item.RealComplete.Value.ToString("yyyy/MM/dd");
                    sd.ScheduleTypeName = apiOptionService.GetOptName(SchTypeOpt, sd.ScheduleType, lang);
                }
                else
                {
                    //除已完工外　其餘均顯示　預計完工日
                    sd.ScheduleTypeName = (lang == LangLib.en) ? "Completion date" : "預計完工日";
                }
                //sd.ScheduleTypeName = apiOptionService.GetOptName(SchTypeOpt, sd.ScheduleType, lang);

                sd.work = new List<WorkItem>();
                #region 取得 施工相簿
                var worlData = basedb.ScheduleRecord.Where(p => 
                p.EstateObjID == EstatesID &&
                p.EstateSID == item.ID
                ).OrderBy(p => p.OrderNo == 0).ThenBy(x => x.OrderNo).ToList();
                
                foreach (var w in worlData)
                {
                    WorkItem wmodel = new WorkItem()
                    {
                        ID = w.ID,
                        Brief = w.ImgDesc,
                        Name = w.ImgTitle
                    };
                    if (lang == LangLib.en)
                    {
                        wmodel.Brief = w.ImgDescEn;
                        wmodel.Name = w.ImgTitleEn;
                    }
                    wmodel.Image = FileLib.ImgPathToUrl(w.Img);
                    sd.work.Add(wmodel);
                }
                #endregion
                model.Schedule.Add(sd);
            }
            #endregion
            
            #region 取得地產資訊
            model.Sale = new EstateSaleDetail()
            {
                ID = sale.ID,
                Addr = (lang == LangLib.cn) ? eObj.Address : eObj.AddressEn,
                UnitName =(RoomData != null) ? RoomData.Name : "",
                UnitArea = sale.UnitArea,
                RoomArea = sale.RoomArea,
                PublicArea = sale.PublicArea,
                PriceSum = sale.PriceSum,
                ParkingType = sale.ParkingType,
                ParkingPrice = sale.ParkingPrice
            };
            //判斷是否顯示車位價格
            if (sale.ParkingType != "2")
            {
                model.Sale.ParkingPrice = null;
            }

            model.Sale.UnitImage = (RoomData!=null) ? FileLib.ImgPathToUrl(RoomData.Img) : "";
            model.Sale.DealFile = (string.IsNullOrEmpty(sale.DealFile)) ? "" : FileLib.ImgPathToUrl(sale.DealFile);

            #endregion

            #region 計算收益紀錄
            var benefitList = basedb.BenefitRecord.Where(p =>
                p.EstateSaleID == EstateSaleID &&
                p.EstateObjID == EstatesID &&
                p.PayDate != null &&
                p.PayStatus == "2"
            ).OrderByDescending(p => p.PayDate).ToList();
            List<IncomeItem> incomeList = new List<IncomeItem>();
            foreach (var b_item in benefitList)
            {
                var thisY = b_item.PayDate.Value.Year;
                var thisM = b_item.PayDate.Value.Month;

                var isYear = incomeList.Where(p => p.Year == thisY).FirstOrDefault();
                if (isYear == null)
                {
                    List<IncomeDetail> mdata = new List<IncomeDetail>();
                    //加入12個月
                    for(var m = 1;m <= 12;m++)
                    {
                        IncomeDetail _m = new IncomeDetail()
                        {
                            Month = m,
                            PdfFile = ""
                        };
                        //加入該筆月份資料
                        if(m == thisM)
                        {
                            _m.Income = b_item.Income;
                            _m.Expense = b_item.Expense;
                            _m.PdfFile = (string.IsNullOrEmpty(b_item.PdfFile)) ? "" : FileLib.ImgPathToUrl(b_item.PdfFile);
                        }
                        mdata.Add(_m);
                    }

                    IncomeItem newIncome = new IncomeItem();
                    newIncome.Year = thisY;
                    newIncome.Data = mdata;
                    incomeList.Add(newIncome);
                }
                else
                {
                    //若已經有建立年度資料 直接累計至月份
                    var findMonth = isYear.Data.Where(p => p.Month == thisM).First();
                    if (findMonth.Income != null)
                    {
                        if (b_item.Income != null)
                        {
                            findMonth.Income = findMonth.Income + b_item.Income;
                        }
                    }
                    else
                    {
                        findMonth.Income = b_item.Income;
                    }

                    if (findMonth.Expense != null)
                    {
                        if (b_item.Expense != null)
                        {
                            findMonth.Expense = findMonth.Expense + b_item.Expense;
                        }
                    }
                    else
                    {
                        findMonth.Expense = b_item.Expense;
                    }
                    findMonth.PdfFile = (string.IsNullOrEmpty(b_item.PdfFile)) ? "" : FileLib.ImgPathToUrl(b_item.PdfFile);
                }
            }
            model.Income = incomeList;
            #endregion

            #region 彙整委託內容
            ManageData manage = new ManageData();
            #region 取得銷售狀態 1:預售 2.成屋(畫面不顯示) 3.代租中 4.包租中 5.已出租
            //old 
            //if (eObj.SaleType == "1")
            //new 交屋狀態(IsFinish) 1:預售 2.成屋
            if (sale.IsFinish == "1")
            {
                manage.SaleType = "1";
            }
            else
            {
                //檢查合約是否代租/包租 [CharterData] 1:包租 2:委託出租
                var today = DateTime.Now;
                var endday = DateTime.Now.AddDays(1);
                var cdata = basedb.CharterData.Where(p =>
                p.EstateID == EstateSaleID &&
                p.StartDate <= today && p.EndDate >= endday).OrderByDescending(p => p.CreateDate).FirstOrDefault();
                if (cdata != null)
                {
                    if (cdata.ManagerType == "1")
                    {
                        manage.SaleType = "4";
                    }
                    else
                    {
                        manage.SaleType = "3";
                        #region 彙整租賃狀況 檢查合約是否已出租 [RentData] 1:出租中 2: 待租中
                        var isrent = basedb.RentData.Where(p =>
                         p.EstateSaleID == EstateSaleID &&
                         p.CharterDataID == cdata.ID &&
                         p.RentStart <= today && p.RentEnd >= endday).OrderByDescending(p => p.CreateDate).FirstOrDefault();
                        if (isrent != null)
                        {
                            //合約出租中才顯示已出租
                            if (isrent.RentStatus == "1")
                            {
                                var genderOpt = opsr.GetList("Gender");
                                manage.SaleType = "5";
                                //加入出租資訊
                                RentData _rent = new RentData()
                                {
                                    Rent = isrent.RentAmount,
                                    IsPay = false,
                                    RendName = isrent.RenterName,
                                    RendIDNo = isrent.RenterIdNo,
                                    Birth = (isrent.RenterBirth != null) ? isrent.RenterBirth.Value.ToString("yyyy/MM/dd") : "",
                                    RendStart = (isrent.RentStart != null) ? isrent.RentStart.Value.ToString("yyyy/MM/dd") : "",
                                    RendEnd = (isrent.RentEnd != null) ? isrent.RentEnd.Value.ToString("yyyy/MM/dd") : "",
                                    RendFile = FileLib.ImgPathToUrl(isrent.RentDeal)
                                };
                                _rent.Gender = apiOptionService.GetOptName(genderOpt, isrent.RenterGender, lang);
                                //判斷本期是否繳交 收益紀錄最新一期檢視 該房客IdNo 付款狀態 1:尚未缴款 2:已缴款
                                var isPayData = basedb.BenefitRecord.Where(p =>
                                 p.EstateObjID == EstatesID &&
                                 p.EstateSaleID == EstateSaleID).OrderByDescending(p => p.PayDate).FirstOrDefault();
                                if (isPayData != null)
                                {
                                    if (isPayData.PayStatus == "2")
                                        _rent.IsPay = true;
                                }
                                model.Rent = _rent;
                            }

                        }
                        #endregion
                    }
                    manage.ManageStart = (cdata.StartDate!=null) ? cdata.StartDate.Value.ToString("yyyy/MM/dd") : "";
                    manage.ManageEnd = (cdata.EndDate != null) ? cdata.EndDate.Value.ToString("yyyy/MM/dd") : "";
                    manage.ManageFile = FileLib.ImgPathToUrl(cdata.ManageDeal);
                }
                else
                {
                    manage.SaleType = "2"; //成屋
                }
            }
            #endregion
            model.Manage = manage;


            #endregion

            return model;
        }

        //紀錄寄信Log 1:注册  2:忘记密码
        public void SaveMailLog(string type,string mail)
        {
            DbContent.EmailLog log = new DbContent.EmailLog()
            {
              Email = mail,
              Status = false,
              CreateDate = DateTime.Now,
              TypeCode = type
            };
            basedb.EmailLog.Add(log);
            basedb.SaveChanges();
        }
        //標記寄信Log 以驗證 1:注册  2:忘记密码
        public void CheckMailLog(string type, string mail)
        {
            var data = basedb.EmailLog.Where(p => p.Email == mail && p.TypeCode == type).OrderByDescending(p => p.CreateDate).FirstOrDefault();
            if (data != null)
            {
                data.Status = true;
                data.CheckDate = DateTime.Now;
                basedb.SaveChanges();
            }
        }

        public List<FundSale> GetMyFundList(string lang)
        {
            var user = apiUserService.GetUser();
           
            List<FundSale> model = new List<FundSale>();
            //取得類別名稱
            var FtypeOpts = optsr.GetList("FundType");
            var BtypeOpts = optsr.GetList("FundBuyType");
            var data = basedb.FundsSale.Where(p => p.UserIdentityNo == user.IdentityNo).ToList();
            foreach (var item in data)
            {
                // 加入購買基金項目 FundType 1:私募 2:避险
                FundSale m = new FundSale()
                {
                    ID = item.ID,
                    FundsNo = item.FundsNo,
                    FundType = item.FundType, // 基金類型(FundType) 1:私募 2:避险
                    FundsName = item.FundsName,
                    BuyType = null,
                    BuyTypeName = null,  //申購方式
                    Profit = 0, //含息损益
                    Cost = 0    //投资成本
                };

                //取得基金份額
                m.UnitCount = FundsUnitSum(item.ID);

                //取得基金類型名稱
                var ftName = apiOptionService.GetOptName(FtypeOpts, item.FundType, lang);
                m.FundTypeName = ftName;

                //取得購買類別 单笔/定期定额 (可複選)
                //取得基金類別
                //var fdata = basedb.Funds.Where(p => p.ID == item.FundsID).FirstOrDefault();
                //if (fdata != null)
                //{
                //    item.BuyType = fdata.BuyType;
                //}
                // TxnType 1:申購  2:赎回 3:分红
                //投資成本 > 每一次申購時的金額加總
                //m.Cost = FundsSaleSum(item.ID);
                //投資成本 20200506 調整 比照下方投資成本
                m.Cost = FundsCostSum(item.ID);
                
                //計算含息損益
                m.Profit = FundsProfit(item.ID);

                //add Buy Type
                if (!string.IsNullOrEmpty(item.BuyType))
                {
                    var blist = item.BuyType.Split(',');
                    var buyTypeName = new List<string>();
                    foreach (var b in blist)
                    {
                        string tname = apiOptionService.GetOptName(BtypeOpts,b, lang);
                        buyTypeName.Add(tname);
                    }
                    m.BuyType = blist.ToList();
                    m.BuyTypeName = buyTypeName;
                }
                model.Add(m);
            }
            return model;
        }

        public FundSaleDetail GetMyFundDetail(int SaleID,string lang)
        {
            var user = apiUserService.GetUser();

            FundSaleDetail model = new FundSaleDetail();
            //取得類別名稱
            var TxnOpts = optsr.GetList("FundTxnType"); //交易類別 申购 1 赎回 2 分红 3
            var PayOpts = optsr.GetList("FundPayStatus"); //本期未缴 1 本期已缴 2

            var data = basedb.FundsSale.Where(p => p.UserIdentityNo == user.IdentityNo && p.ID == SaleID).FirstOrDefault();
            if (data == null)
                throw new Exception("Data Not Found!");


            #region 處理資料主體邏輯
            //是否需顯示已付款 基金購買類別有 定期定額才需顯示
            var fdata = basedb.Funds.Where(p => p.ID == data.FundsID).FirstOrDefault();
            if (fdata.BuyType.Contains("2")) // BuyType 2 > 定期定額
            {
                //取得最近一期繳款金額 計算出當月繳款金額&是否繳款
                var detail = basedb.FundsDetail.Where(p => p.FundsSaleID == SaleID && p.TxnType == "1").OrderByDescending(p => p.TxnDate).FirstOrDefault();
                if (detail != null)
                {
                    model.MonthPayAmt = 0;
                    if (detail.PurchaseAmt != null) //加總申購淨值 20200424:Bryan 當月繳款金額應該串「申購金額+手續費」
                        model.MonthPayAmt += detail.PurchaseAmt.Value;
                    if (detail.HandlingFee != null) //加總手續費
                        model.MonthPayAmt += detail.HandlingFee.Value;

                    model.PayStatus = detail.PayStatus;
                    model.PayStatusName = apiOptionService.GetOptName(PayOpts,detail.PayStatus,lang);
                }
            }
            //对帐日份额净值 單位數*該日淨值
            var UnitNowData = FundsNowUnitData(fdata.ID);
            model.UnitValue = (UnitNowData != null && UnitNowData.WorthValue != null) ? UnitNowData.WorthValue.Value : 0; //該日淨值
            model.UnitCount = FundsUnitSum(SaleID);        //累計單位數(基金份额)
            model.Cost = FundsCostSum(SaleID);             //投资成本
            model.NowValue = model.UnitValue * model.UnitCount; //对帐日市值 = 該日淨值*單位數

            //不含息损益 > 市值 - 成本
            model.NoBonusProfit = (model.NowValue - model.Cost);
            //不含息损益率% > 不含息损益/成本
            if (model.Cost != 0)
                model.NoBonusProfitRage = (model.NoBonusProfit / Math.Abs(model.Cost)) * 100;
            else
                model.NoBonusProfitRage = 0;
                
            //累积配息
            model.BonusSum = FundsBonusSum(SaleID);

            //含息损益 > (市值 + 累积配息) - 成本
            model.BonusProfit = (model.NowValue + model.BonusSum) - model.Cost;
            //含息损益率 > 含息损益/成本
            if (model.Cost != 0)
                model.BonusProfitRage = (model.BonusProfit / Math.Abs(model.Cost)) * 100;
            else
                model.BonusProfitRage = 0;
            //对帐日期
            model.UnitValueDate = (UnitNowData != null && UnitNowData.WorthDate != null) ? UnitNowData.WorthDate.Value.ToString("yyyy/MM") : "";
            
            #endregion

            #region 選項&交易记录
            model.TransRecord = new List<FundSaleItem>();
            var records = basedb.FundsDetail.Where(p => p.FundsSaleID == SaleID).OrderByDescending(p => p.TxnDate).ToList();
            foreach (var item in records)
            {
                FundSaleItem r = new FundSaleItem()
                {
                    ID = item.ID,
                    TxnType = item.TxnType,
                    TxnName = apiOptionService.GetOptName(TxnOpts, item.TxnType, lang),
                    TxnNo = item.TxnNo, //交易單號
                    TxnDate = (item.TxnDate == null) ? "" : item.TxnDate.Value.ToString("yyyy/MM/dd"), //交易日期

                    Unit = 0,

                    PurchaseVal = 0,  //申购淨值
                    PurchaseAmt = 0,  //申購金額

                    RedemptionVal = 0,//赎回淨值
                    RedemptionAmt = 0,//赎回份额

                    PrefFee = 0,      //绩效费
                    HandlingFee = 0,  //手续费

                    PayStatus = apiOptionService.GetOptName(PayOpts, item.PayStatus, lang),
                    CreateDate = item.CreateDate.ToString("yyyy/MM/dd")
                };
                //申購項目處理
                if (item.TxnType == "1")
                {
                    r.Unit = (item.Unit == null) ? 0 : item.Unit.Value; //申购份额
                    r.PurchaseVal = item.PurchaseVal; //申购淨值
                    r.PurchaseAmt = item.PurchaseAmt; //申购金額
                    r.HandlingFee = item.HandlingFee; //手续费
                }
                //贖回項目處理
                if (item.TxnType == "2")
                {
                    r.Unit = (item.Unit == null) ? 0 : item.Unit.Value;//贖回份额
                    r.RedemptionVal = item.RedemptionVal; //贖回淨值
                    r.RedemptionAmt = item.RedemptionAmt; //贖回淨值
                    r.PrefFee = item.PrefFee; //绩效费
                    r.HandlingFee = item.HandlingFee; //手续费
                }
                //分紅項目處理
                if (item.TxnType == "3")
                {
                    r.DividendAmt = item.DividendAmt;
                }
                model.TransRecord.Add(r);
            }

            model.RecordOpt = new List<FundType>();
            foreach (var opt in TxnOpts)
            {
                FundType _opt = new FundType()
                {
                    Value = opt.OptionValue
                };
                _opt.Name = apiOptionService.GetOptName(TxnOpts, opt.OptionValue,lang);

                model.RecordOpt.Add(_opt);
            }
            #endregion

            #region 业绩走势
            // 由小到大排列 彙整後 將年分由大到小排
            model.FundWorth = new List<FundWorth>();

            List<int> Years = new List<int>();
            var trendData = basedb.FundsWorth.Where(p => p.FundsID == fdata.ID).OrderBy(p => p.WorthDate).ToList();
            foreach (var t in trendData)
            {
                if (t.WorthDate != null)
                {
                    var _y = t.WorthDate.Value.Year;
                    if (!Years.Contains(_y))
                    {
                        Years.Add(_y);
                    }
                }
            }
            foreach (var y in Years)
            {
                FundWorth f = new FundWorth()
                {
                    Year = y,
                    xAxes = xAxesList(y),
                    yAxes = new List<double?>(),
                    LastUpdate = ""
                };
                //取得Y軸資料
                var filter = trendData.Where(p => p.WorthDate.Value.Year == y).ToList();
                for(var i = 1; i <= 12;i++)
                {
                    var m = filter.Where(p => p.WorthDate.Value.Month == i).FirstOrDefault();
                    if (m != null && m.WorthValue != null)
                    {
                        f.yAxes.Add(m.WorthValue.Value);
                    }
                    else
                    {
                        f.yAxes.Add(null);
                    }
                };
                //取得最後更新日 
                var lastUpd = filter.OrderByDescending(p => p.WorthDate).FirstOrDefault();
                if (lastUpd != null && lastUpd.WorthDate != null)
                {
                    f.LastUpdate = lastUpd.WorthDate.Value.ToString("yyyy/MM/dd");
                }

                model.FundWorth.Add(f);
            }
            //將年分由大到小排
            if (model.FundWorth.Count() > 0)
            {
                model.FundWorth = model.FundWorth.OrderByDescending(p => p.Year).ToList();
            }
            #endregion

            return model;
        }
        // 依據FundsSaleID統計申購總金額
        private double FundsSaleSum(int FundsSaleID)
        {
            string sumsql = $"SELECT ISNULL((SELECT SUM(PurchaseAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID}),Cast(0 as float)) as Total";
            var sqlcmd = basedb.Database.SqlQuery<double>(sumsql);
            var result = sqlcmd.First();
            return result;
        }
        //計算含息損益
        private double FundsProfit(int FundsSaleID)
        {
            var SaleData = basedb.FundsSale.Where(p => p.ID == FundsSaleID).First();

            //取得單位淨值 最新一期
            double UnitNowValue = (double)0;
            var fdata = basedb.Funds.Where(p => p.ID == SaleData.FundsID).FirstOrDefault();
            if (fdata != null)
            {
                UnitNowValue = FundsNowUnitValue(fdata.ID);
            }
             
            //取得單位數加總(申購單位加總)
            var unit = FundsUnitSum(FundsSaleID);

            //取得配息加總
            var bonus = FundsBonusSum(FundsSaleID);

            //投資成本加總(申購金額加總)
            var cost = FundsCostSum(FundsSaleID);

            //計算含息損益  投資市值(最新市值*單位數) + 配息加總 - 投資成本加總(申購金額加總)
            var result = ((UnitNowValue * unit) + bonus) - cost;

            return result;
        }
        //取得單位數加總(申購單位 - 贖回單位 加總)
        private double FundsUnitSum(int FundsSaleID)
        {
            //取得單位數加總(申購單位加總)
            string unit_Sum_sql = $"SELECT ISNULL((SELECT SUM(Unit) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 1),Cast(0 as float)) as Total";
            var unitcmd = basedb.Database.SqlQuery<double>(unit_Sum_sql);
            var unit = unitcmd.First();

            string unit_Sale_sql = $"SELECT ISNULL((SELECT SUM(Unit) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 2),Cast(0 as float)) as Total";
            var unitSalecmd = basedb.Database.SqlQuery<double>(unit_Sale_sql);
            var Sale = unitSalecmd.First();
            return (unit - Sale);
        }
        //投資成本加總 (申購淨值 - 贖回淨值 加總)
        private double FundsCostSum(int FundsSaleID)
        {
            //投資成本加總(申購金額加總)
            string cost_sql = $"SELECT ISNULL((SELECT SUM(PurchaseAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 1),Cast(0 as float)) as Total";
            var cost_cmd = basedb.Database.SqlQuery<double>(cost_sql);
            var cost = cost_cmd.First();

            //(贖回淨值加總)
            string earn_sql = $"SELECT ISNULL((SELECT SUM(RedemptionAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 2),Cast(0 as float)) as Total";
            var earn_cmd = basedb.Database.SqlQuery<double>(earn_sql);
            var earn = earn_cmd.First();

            return cost - earn;
        }
        //取得配息加總
        private double FundsBonusSum(int FundsSaleID)
        {
            //取得配息加總
            string bonus_Sum_sql = $"SELECT ISNULL((SELECT SUM(DividendAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 3),Cast(0 as float)) as Total";
            var bonus_cmd = basedb.Database.SqlQuery<double>(bonus_Sum_sql);
            var bonus = bonus_cmd.First();
            return bonus;
        }
        //取得基金最新日值 
        private double FundsNowUnitValue(int FundsID)
        {
            var model = basedb.FundsWorth.Where(p => p.FundsID == FundsID).OrderByDescending(p => p.WorthDate).FirstOrDefault();
            if (model != null && model.WorthValue != null)
            {
                return model.WorthValue.Value;
            }
            else
            {
                return 0;
            }
            
        }
        //取得基金最新日值 資料
        private DbContent.FundsWorth FundsNowUnitData(int FundsID)
        {
            var model = basedb.FundsWorth.Where(p => p.FundsID == FundsID).OrderByDescending(p => p.WorthDate).FirstOrDefault();
            return model;
        }
        private List<string> xAxesList(int y)
        {
            List<string> x = new List<string>()
            {
                "01",
                "",
                "",
                "",
                "",
                "06",
                "",
                "",
                "",
                "",
                "",
                "12",
            };
            return x;
        }
        //取得財務報表
        public List<FundReport> MyFundReport(int SaleID, string lang)
        {
            List<FundReport> model = new List<FundReport>();
            var sale = basedb.FundsSale.Where(p => p.ID == SaleID).FirstOrDefault();
            if (sale != null)
            {
                var rlist = basedb.FundsReport.Where(p => p.FundsID == sale.FundsID).OrderByDescending(p => p.Year).ThenBy(p => p.Month).ToList();
                foreach (var r in rlist)
                {
                    FundReport data = new FundReport()
                    {
                        ID = r.ID,
                        Month = r.Month,
                        Year = r.Year,
                        FileUrl = r.FileUrl.ToSiteUrl()
                    };
                    model.Add(data);
                }
                return model;
            }
            else
            {
                return model;
            }
        }

        //取得保險列表
        public List<MyInsurance> GetMyInsuranceList(string lang)
        {
            var InsList = basedb.Insurance.ToList();//所有保險列表
            apiOptionService opsr = new apiOptionService();
            var StatusOpt = opsr.GetList("InsuranceValid");

            var userData = apiUserService.GetUser();
            List<MyInsurance> model = new List<MyInsurance>();
            string Idno = userData.IdentityNo;
            if (!string.IsNullOrEmpty(Idno))
            {
                var data = basedb.InsuranceSale.Where(p => p.UserIdentityNo == Idno).ToList();
                foreach (var item in data)
                {
                    var InsData = InsList.Where(p => p.ID == item.InsID).FirstOrDefault();
                    if (InsData != null)
                    {
                        
                        MyInsurance m = new MyInsurance()
                        {
                            ID = item.ID,
                            BenefitName = item.BenefitName,
                            StatusName = apiOptionService.GetOptName(StatusOpt, item.InsStatus, lang),
                            Currency = (lang == LangLib.en) ? item.CurrencyEn : item.Currency,
                            Amount = (item.InsAmount == null) ? 0 : item.InsAmount.Value,
                            InsName = (lang == LangLib.en) ? InsData.NameEn : InsData.Name,
                            TypeName = (lang == LangLib.en) ? InsData.TypeNameEn : InsData.TypeName,
                            InsNo = item.InsNo
                        };
                        model.Add(m);
                    }
                }
            }
            return model;
        }

        //取得保險內容
        public MyInsuranceItem GetMyInsuranceData(int InsSaleID,string lang)
        {
            var InsList = basedb.Insurance.ToList();//所有保險列表
            apiOptionService opsr = new apiOptionService();
            var StatusOpt = opsr.GetList("InsuranceValid");
            var PayTypeOpt = opsr.GetList("InsurancePay");
            var userData = apiUserService.GetUser();
            

            string Idno = userData.IdentityNo;
            var item = basedb.InsuranceSale.Where(p => p.UserIdentityNo == Idno && p.ID == InsSaleID).FirstOrDefault();
            if (!string.IsNullOrEmpty(Idno) && item != null)
            {
                var InsData = InsList.Where(p => p.ID == item.InsID).FirstOrDefault();
                MyInsuranceItem model = new MyInsuranceItem()
                {
                    ID = item.ID,
                    InsTypeName = apiOptionService.GetOptName(PayTypeOpt, item.PayType, lang),
                    StatusName = apiOptionService.GetOptName(StatusOpt, item.InsStatus, lang),
                    Currency = (lang == LangLib.en) ? item.CurrencyEn : item.Currency,
                    Amount = item.Amount,
                    InsAmount = (item.InsAmount == null) ? 0 : item.InsAmount.Value,
                    InsName = (lang == LangLib.en) ? InsData.NameEn : InsData.Name,
                    TypeName = (lang == LangLib.en) ? InsData.TypeNameEn : InsData.TypeName,
                    InsNo = item.InsNo,
                    ValidDate = (item.ValidDate != null) ? item.ValidDate.Value.ToString("yyyy/MM/dd") : null,
                    PayPeriod = item.PayPeriod,
                    ValidRange = null,
                    NextPayDate = "", //下期繳費日
                    BuyerName = item.BuyerName,
                    BenefitName = item.BenefitName,
                    SaleName = item.SaleName,
                    SalePhone = item.SalePhone,

                    //InsContent = (lang == LangLib.en) ? item.InsContentEn.Replace(@"\n", "<br/>") : item.InsContent.Replace(@"\n", "<br/>"),
                    InsNote = (lang == LangLib.en) ? item.InsNoteEn : item.InsNote,

                    Recommend = (!string.IsNullOrEmpty(item.Recommend)) ? item.Recommend.ToSiteUrl() : "",
                    DealFile = (!string.IsNullOrEmpty(item.DealFile)) ? item.DealFile.ToSiteUrl() : ""
                };
                if (item.ValidStart != null && item.ValidEnd != null)
                {
                    model.ValidRange = item.ValidStart.Value.ToString("yyyy/MM/dd") + " - " + item.ValidEnd.Value.ToString("yyyy/MM/dd");
                }
                if (lang == LangLib.en)
                {
                    model.InsContent = (string.IsNullOrEmpty(item.InsContentEn)) ? "" : item.InsContentEn.Replace(@"\n", "<br/>");
                    //model.InsNote = (string.IsNullOrEmpty(item.InsNoteEn)) ? "" : item.InsNoteEn.Replace(@"\n", "<br/>");
                }
                else
                {
                    model.InsContent = (string.IsNullOrEmpty(item.InsContent)) ? "" : item.InsContent.Replace(@"\n", "<br/>");
                   // model.InsNote = (string.IsNullOrEmpty(item.InsNote)) ? "" : item.InsNote.Replace(@"\n", "<br/>");
                }
                //項目排序
                model.InsContent = apiOptionService.OrderAppItem(model.InsContent);
                //model.InsNote = apiOptionService.OrderAppItem(model.InsNote);

                //計算下期繳費日 
                if (item.InsStatus == "1" && item.ValidDate != null)
                {
                    //繳別 1年繳 2 半年繳 3 季繳 4月繳 5趸缴
                    if (item.PayType == "1")
                    {
                        while (item.ValidDate.Value < DateTime.Now)
                        {
                            item.ValidDate = item.ValidDate.Value.AddYears(1);
                        }
                        model.NextPayDate = item.ValidDate.Value.ToString("yyyy/MM/dd");
                    }
                    if (item.PayType == "2")
                    {
                        while (item.ValidDate.Value < DateTime.Now)
                        {
                            item.ValidDate = item.ValidDate.Value.AddMonths(6);
                        }
                        model.NextPayDate = item.ValidDate.Value.ToString("yyyy/MM/dd");
                    }
                    if (item.PayType == "3")
                    {
                        while (item.ValidDate.Value < DateTime.Now)
                        {
                            item.ValidDate = item.ValidDate.Value.AddMonths(3);
                        }
                        model.NextPayDate = item.ValidDate.Value.ToString("yyyy/MM/dd");
                    }
                    if (item.PayType == "4")
                    {
                        while (item.ValidDate.Value < DateTime.Now)
                        {
                            item.ValidDate = item.ValidDate.Value.AddMonths(1);
                        }
                        model.NextPayDate = item.ValidDate.Value.ToString("yyyy/MM/dd");
                    }

                }
                return model;
            }
            
            return null;
        }
    }
}
