﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Areas.App.Service
{
    public class apiNewsService : CoreWeb.Service.BaseService
    {
        //取得理財趨勢列表
        public List<apiNewsData> GetNewsList(string lang)
        {
            List<apiNewsData> model = new List<apiNewsData>();
            var data = basedb.vwNews.ToList();
            foreach (var item in data)
            {
                apiNewsData oData = new apiNewsData()
                {
                    ID = item.ID,
                    Brief = (LangLib.cn == lang) ? item.Brief : item.BriefEn,
                    Detail = (LangLib.cn == lang) ? FileLib.ImgPathToUrl(item.Detail) : FileLib.ImgPathToUrl(item.DetailEn),
                    ImageUrl = FileLib.ImgPathToUrl(item.ImageUrl),
                    PublicDate = item.PublicDate.HasValue ? item.PublicDate.Value.ToString("yyyy/MM/dd") : "",
                    Week = DateLib.DateToWeekName(item.PublicDate,lang),
                    Title = (LangLib.cn == lang) ? item.Title : item.TitleEn,
                };
                model.Add(oData);
            }
            return model;
        }

        //取得理財趨勢
        public apiNewsDetail GetNewsData(int id,string lang)
        {
            //需驗證status
            var data = basedb.vwNews.Where(p => p.ID == id && p.Status == true).FirstOrDefault();
            if (data != null)
            {
                apiNewsDetail model = new apiNewsDetail()
                {
                    ID = data.ID,
                    Brief = (LangLib.cn == lang) ? data.Brief : data.BriefEn,
                    Detail = (LangLib.cn == lang) ? data.Detail : data.DetailEn,
                    ImageUrl = FileLib.ImgPathToUrl(data.ImageUrl),
                    PublicDate = data.PublicDate.HasValue ? data.PublicDate.Value.ToString("yyyy/MM/dd") : "",
                    Week = DateLib.DateToWeekName(data.PublicDate, lang),
                    Title = (LangLib.cn == lang) ? data.Title : data.TitleEn
                };

                #region 取得上一則 下一則
                var pre = basedb.vwNews.Where(p => p.RowIndex < (data.RowIndex)).OrderByDescending(p => p.RowIndex).FirstOrDefault();
                model.BeforeID = (pre == null) ? "" : pre.ID.ToString();

                var next = basedb.vwNews.Where(p => p.RowIndex > (data.RowIndex)).FirstOrDefault();
                model.AfterID = (next == null) ? "" : next.ID.ToString();
                #endregion

                return model;
            }
            else
              return null;
        }

        
    }
}