﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Areas.App.Service;
namespace CoreWeb.Areas.App.Service
{
    public class apiTrendService : CoreWeb.Service.BaseService
    {
        const string code = "TradeType";
        public apiOptionService optionSrv = new apiOptionService();

        //取得理財趨勢列表
        public apiTrendList GetTrendList(string trendtype,string lang)
        {
            var isLogin = apiUserService.GetUser() != null;
            apiTrendList model = new apiTrendList();
            model.IsLogin = isLogin;

            #region 取得選單資料
            var optionList = optionSrv.GetList(code);
            foreach (var item in optionList)
            {
                var _option = new apiTrendOption()
                {
                    id = item.ID,
                    name = item.Name,
                    value = item.OptionValue
                };

                if (lang == LangLib.en)
                {
                    _option.name = item.NameEn;
                }
                model.option.Add(_option);
            }
            #endregion

            #region 取得趨勢資料
            var trend = basedb.vwTradeNews.Where(p => p.ID > 0);
            var trendList = trend.ToList();
            if (string.IsNullOrEmpty(trendtype) == false)
            {
                trendList = trendList.Where(p => p.TradeType == trendtype).ToList();
            }
            //沒登入只能看三篇
            if (isLogin == false)
            {
                trendList = trendList.Take(3).ToList();
            }
            foreach (var item in trendList)
            {
                apiTrendDataList oData = new apiTrendDataList()
                {
                    ID = item.ID,
                    Name = item.Name,
                    DisplayDate = item.DisplayDate.HasValue ? item.DisplayDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Author = item.Author,
                    Detail = FileLib.ImgPathToUrl(item.Detail),
                    Brief = item.Brief,
                    Img = FileLib.ImgPathToUrl(item.Img)
                };
                //多語系處理
                if (lang == LangLib.en)
                {
                    oData.Name = item.NameEn;
                    oData.Brief = item.BriefEn;
                    oData.Author = item.AuthorEn;
                    oData.Detail = FileLib.ImgPathToUrl(item.DetailEn);
                }
                //取得類別名稱 從選單資料mapping
                var typeObj = optionList.Where(p => p.OptionValue == item.TradeType).FirstOrDefault();
                if (typeObj != null)
                {
                    oData.TradeType = (lang == LangLib.en) ? typeObj.NameEn : typeObj.Name;
                }
                else
                {
                    oData.TradeType = "";
                }
                model.list.Add(oData);
            }
            #endregion

            return model;
        }
        //取得理財趨勢內容
        public apiTrendData GetTrendData(int id,string lang)
        {
            //需判斷 status 是否為 true
            var trend = basedb.vwTradeNews.Where(p => p.ID == id && p.Status == true).FirstOrDefault();
            if (trend != null)
            {
                //取得類別資料
                var optionList = optionSrv.GetList(code);

                apiTrendData model = new apiTrendData()
                {
                    ID = trend.ID,
                    Name = (lang == LangLib.cn) ? trend.Name : trend.NameEn,
                    DisplayDate = trend.DisplayDate.HasValue ? trend.DisplayDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    Brief = (lang == LangLib.cn) ? trend.Brief : trend.BriefEn,
                    Author = (lang == LangLib.cn) ? trend.Author : trend.AuthorEn,
                    Detail = (lang == LangLib.cn) ? FileLib.ImgPathToUrl(trend.Detail) : FileLib.ImgPathToUrl(trend.DetailEn),
                    //Recommend = trend.Recommend,
                    BeginDate = trend.BeginDate.HasValue ? trend.BeginDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    EndDate = trend.EndDate.HasValue ? trend.EndDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    CreateDate = trend.CreateDate.ToString("yyyy/MM/dd"),
                    Status = trend.Status,
                    Img = FileLib.ImgPathToUrl(trend.Img)
                };
                //取得類別名稱 從選單資料mapping
                var typeObj = optionList.Where(p => p.OptionValue == trend.TradeType).FirstOrDefault();
                if (typeObj != null)
                {
                    model.TradeType = (lang == LangLib.en) ? typeObj.NameEn : typeObj.Name;
                }
                else
                {
                    model.TradeType = "";
                }

                #region 取得推薦
                if (!string.IsNullOrEmpty(trend.Recommend))
                {
                    int[] recmdID = Lib.DateLib.StrToIntArray(trend.Recommend);
                    var recmd = basedb.TradeNews.Where(p => recmdID.Contains(p.ID)).ToList();
                    foreach (var cmd in recmd)
                    {
                        Recommend oRecData = new Recommend()
                        {
                            ID = cmd.ID,
                            Img = FileLib.ImgPathToUrl(cmd.Img),
                            Name = (lang == LangLib.en) ? cmd.Name : cmd.NameEn
                        };
                    }
                }

                #endregion

                #region 取得上一則 下一則
                var pre = basedb.vwTradeNews.Where(p => p.RowIndex == (trend.RowIndex - 1)).FirstOrDefault();
                model.BeforeID = (pre == null) ? "" : pre.ID.ToString();

                var next = basedb.vwTradeNews.Where(p => p.RowIndex == (trend.RowIndex + 1)).FirstOrDefault();
                model.AfterID = (next == null) ? "" : next.ID.ToString();
                //是否允許看下一筆
                if (apiUserService.GetUser() == null)
                {
                    model.AfterValid = (trend.RowIndex < 3);
                }
                else
                {
                    model.AfterValid = true;
                }
                #endregion

                return model;
            }
            else
                return null;

        }
    }
}