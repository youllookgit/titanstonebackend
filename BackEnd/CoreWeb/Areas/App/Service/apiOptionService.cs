﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using Newtonsoft.Json;
namespace CoreWeb.Areas.App.Service
{
    public class apiOptionService : CoreWeb.Service.BaseService
    {
        public List<DbContent.Option> GetList(string code)
        {
            return basedb.Option.Where(p => p.Code == code && p.Enabled == true).OrderBy(p => p.OrderNo).ToList();
        }
        public static string GetOptName(List<DbContent.Option> data,string value,string lang)
        {
            var obj = data.Where(p => p.OptionValue == value).FirstOrDefault();
            if (obj == null)
            {
                return "";
            }
            else
            {
                if (lang != LangLib.en)
                {
                    return obj.Name;
                }
                else
                {
                    return obj.NameEn;
                }
            }
        }

        //針對資料欄位存Json格式的內容作排序
        public static string OrderAppItem(string content)
        {
            string result = "";
            if (string.IsNullOrEmpty(content))
            {
                return result;
            }

            //content = content.ToSiteUrl();
            List<JsonModel> model = new List<JsonModel>();
            model = JsonConvert.DeserializeObject<List<JsonModel>>(content);

            if (model.Count() == 0)
            {
                return result;
            }

            foreach (var item in model)
            {
                item.img = item.img.ToSiteUrl();
                item.order = (item.order == 0) ? null : item.order;
            }
            model = model.OrderByDescending(p => p.order.HasValue).ThenBy(p => p.order).ToList();

            result = JsonConvert.SerializeObject(model);
            return result;
        }

        public class JsonModel
        {
            public string img { get; set; }
            public string title { get; set; }
            public string desc { get; set; }
            public int? order { get; set; }
        }

        //針對資料欄位存Json格式的內容作排序
        public static string OrderMultiItem(string content)
        {
            string result = "";
            if (string.IsNullOrEmpty(content))
            {
                return result;
            }

            content = FileLib.ImgPathToUrl(content);

            MultiItemModel model = new MultiItemModel();
            model = JsonConvert.DeserializeObject<MultiItemModel>(content);

            if (model.item != null && model.item.Count() > 0)
            {
                foreach (var item in model.item)
                {
                    item.order = (item.order == 0) ? null : item.order;
                }
                model.item = model.item.OrderByDescending(p => p.order.HasValue).ThenBy(p => p.order).ToList();
            }
            result = JsonConvert.SerializeObject(model);
            return result;
        }

        //針對資料欄位存Json格式的內容作排序
        public static string OrderOneItem(string content)
        {
            string result = "";
            if (string.IsNullOrEmpty(content))
            {
                return result;
            }

            content = FileLib.ImgPathToUrl(content);

            OneItemModel model = new OneItemModel();
            model = JsonConvert.DeserializeObject<OneItemModel>(content);

            if (model.item != null && model.item.Count() > 0)
            {
                foreach (var item in model.item)
                {
                    item.order = (item.order == 0) ? null : item.order;
                }
                model.item = model.item.OrderByDescending(p => p.order.HasValue).ThenBy(p => p.order).ToList();
            }
            return JsonConvert.SerializeObject(model);
        }

        public class MultiItemModel
        {
            public List<string> album { get; set; }
            public List<JsonModel> item { get; set; }
        }
        public class OneItemModel
        {
            public List<JsonModel> item { get; set; }
        }
    }
}