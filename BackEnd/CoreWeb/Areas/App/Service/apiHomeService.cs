﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Lib;
namespace CoreWeb.Areas.App.Service
{
    public class apiHomeService : CoreWeb.Service.BaseService
    {
        public apiHome Get(string lang, bool login = false)
        {
            apiHome model = new apiHome()
            {
                Estate = new List<apiHomeEstate>(),
                Trend = new List<apiHomeTrend>(),
                Act = new List<apiHomeAct>(),
                News = new List<apiHomeNews>()
            };
            //取得活動選項
            var options = basedb.Option.Where(p => p.Code == "ActType" && p.Enabled == true).OrderBy(p => p.OrderNo).ToList();

            #region 取得地產 top5
            var estate = basedb.vwEstateObj.Take(5).ToList();
            foreach (var item in estate)
            {
                apiHomeEstate e = new apiHomeEstate()
                {
                    ID = item.ID,
                    Country = (lang == LangLib.en) ? item.CountryEn : item.Country,
                    Img = FileLib.ImgPathToUrl(item.MainImg),
                    Name = (lang == LangLib.en) ? item.NameEn : item.Name
                };
                
                model.Estate.Add(e);
            }
            #endregion

            #region 取得趨勢 top5 是否登入綁定顯示數量
            int count = (login) ? 5 : 3;
            var trend = basedb.vwTradeNews.Take(count).ToList();
            foreach (var item in trend)
            {
                apiHomeTrend t = new apiHomeTrend()
                {
                    ID = item.ID,
                    Title = (lang == LangLib.cn) ? item.Name : item.NameEn,
                    Img = FileLib.ImgPathToUrl(item.Img)
                };
                model.Trend.Add(t);
            }
            #endregion

            #region 取得活動 top5
            var Act = basedb.vwActivity.Take(5).ToList();
            foreach (var item in Act)
            {
                apiHomeAct a = new apiHomeAct()
                {
                    ID = item.ID,
                    Img = FileLib.ImgPathToUrl(item.Img),
                    Title = (lang == LangLib.cn) ? item.Name : item.NameEn,
                    Date = (item.ActDate == null) ? "" : item.ActDate.Value.ToString("yyyy/MM/dd"),
                    Tag = item.ActType,
                    Week = Lib.DateLib.DateToWeekName(item.BeginDate, lang)
                };
                var tagName = options.Where(p => p.OptionValue == a.Tag).FirstOrDefault();
                if (tagName != null)
                {
                    a.Tag = (lang == LangLib.cn) ? tagName.Name : tagName.NameEn;
                }
                model.Act.Add(a);
            }
            #endregion

            #region 取得官方消息 top3
            var News = basedb.vwNews.Take(3).ToList();
            foreach (var item in News)
            {
                apiHomeNews n = new apiHomeNews()
                {
                    ID = item.ID,
                    Img = FileLib.ImgPathToUrl(item.ImageUrl),
                    Title = (lang == LangLib.cn) ? item.Title : item.TitleEn,
                    Date = (item.PublicDate == null) ? "" : item.PublicDate.Value.ToString("yyyy/MM/dd"),
                    Week = DateLib.DateToWeekName(item.PublicDate,lang)
                };
                model.News.Add(n);
            }
            #endregion

            return model;
        }


    }
}