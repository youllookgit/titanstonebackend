﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Areas.App.Service
{
    public class apiSeminarService : CoreWeb.Service.BaseService
    {
        public apiOptionService opsr = new apiOptionService();
        const string typeCode = "ActType";
        const string areaCode = "ActArea";
        //取得精選活動列表
        public apiSeminarList GetSeminarList(string Type, string Area,string lang)
        {
            apiSeminarList model = new apiSeminarList();

            #region 取得選項列表
             var optionType = opsr.GetList(typeCode);
             var optionArea = opsr.GetList(areaCode);
            //塞資料
            foreach (var opt in optionType)
            {
                apiOption optitem = new apiOption(){value = opt.OptionValue};
                optitem.name = (lang == LangLib.en) ? opt.NameEn : opt.Name;
                model.typeOpt.Add(optitem);
            }
            foreach (var opt in optionArea)
            {
                apiOption optitem = new apiOption() { value = opt.OptionValue };
                optitem.name = (lang == LangLib.en) ? opt.NameEn : opt.Name;
                model.areaOpt.Add(optitem);
            }
            #endregion

            #region 取得活動列表
            var act = basedb.vwActivity.Where(p => p.ID > 0);
            if (string.IsNullOrEmpty(Type) == false)
            {
                act = act.Where(p => p.ActType == Type);
            }
            if (string.IsNullOrEmpty(Area) == false)
            {
                act = act.Where(p => p.ActArea == Area);
            }
            foreach (var item in act.ToList())
            {
                ActData data = new ActData()
                {
                    id = item.ID,
                    Img = FileLib.ImgPathToUrl(item.Img),
                    ActType = item.ActType,
                    TypeName = "",
                    Name = item.Name,
                    ActDate = item.ActDate.HasValue ? item.ActDate.Value.ToString("yyyy/MM/dd") : "",
                    ActTime = item.ActDate.HasValue ? item.ActDate.Value.ToString("HH:mm") : "",
                    Week = DateLib.DateToWeekName(item.ActDate,lang),
                    ActArea = item.ActArea,
                    ActAddr = item.ActAddr,
                    ActPosition = item.ActPosition,
                    Brief = item.Brief,
                    Detail = FileLib.ImgPathToUrl(item.Detail)
                };
                //英文
                if (lang == LangLib.en)
                {
                    data.Name = item.NameEn;
                    data.ActAddr = item.ActAddrEn;
                    data.ActPosition = item.ActPositionEn;
                    data.Brief = item.BriefEn;
                    data.Detail = FileLib.ImgPathToUrl(item.DetailEn);
                }
                //選項值
                var _type = optionType.Where(p => p.OptionValue == item.ActType).FirstOrDefault();
                if (_type != null)
                {
                    data.TypeName = (lang == LangLib.en) ? _type.NameEn : _type.Name;
                }
                var _area = optionArea.Where(p => p.OptionValue == item.ActArea).FirstOrDefault();
                if (_area != null)
                {
                    data.AreaName = (lang == LangLib.en) ? _area.NameEn : _area.Name;
                }
                model.list.Add(data);
            }
            #endregion

            return model;
        }
        //取得精選活動內容
        public ActDetail GetSeminarDetail(int id,string lang)
        {
            #region 取得選項列表
             var optionType = opsr.GetList(typeCode);
             var optionArea = opsr.GetList(areaCode);
            #endregion
            #region 取得活動內容
            var item = basedb.Activity.Where(p => p.ID == id).FirstOrDefault();

            ActDetail model = new ActDetail()
            {
                id = item.ID,
                ActType = item.ActType,
                TypeName = "",
                Name = item.Name,
                ActDate = item.ActDate.HasValue ? item.ActDate.Value.ToString("yyyy/MM/dd") : "",
                ActTime = item.ActDate.HasValue ? item.ActDate.Value.ToString("HH:mm") : "",
                Week = DateLib.DateToWeekName(item.ActDate, lang),
                ActArea = item.ActArea,
                ActAddr = item.ActAddr,
                ActPosition = item.ActPosition,
                Brief = item.Brief,
                Detail = FileLib.ImgPathToUrl(item.Detail),
               
            };
            if (lang == LangLib.en)
            {
                model.Process = apiOptionService.OrderAppItem(item.ProcessEn);
                model.Traffic = apiOptionService.OrderAppItem(item.TrafficEn);
            }
            else
            {
                model.Process = apiOptionService.OrderAppItem(item.Process);
                model.Traffic = apiOptionService.OrderAppItem(item.Traffic);
            }

            //把主圖陣列塞到相簿
            if (!string.IsNullOrEmpty(item.Img))
            {
                model.ImgList.Add(FileLib.ImgPathToUrl(item.Img));
            }
            if (!string.IsNullOrEmpty(item.Album))
            {
                var album = FileLib.ImgPathToUrl(item.Album).Split(',');
                model.ImgList.AddRange(album);
            }

            //英文
            if (lang == LangLib.en)
            {
                model.Name = item.NameEn;
                model.ActAddr = item.ActAddrEn;
                model.ActPosition = item.ActPositionEn;
                model.Brief = item.BriefEn;
                model.Detail = FileLib.ImgPathToUrl(item.DetailEn);
            }

            //選項值
            var _type = optionType.Where(p => p.OptionValue == item.ActType).FirstOrDefault();
            if (_type != null)
            {
                model.TypeName = (lang == LangLib.en) ? _type.NameEn : _type.Name;
            }
            var _area = optionArea.Where(p => p.OptionValue == item.ActArea).FirstOrDefault();
            if (_area != null)
            {
                model.AreaName = (lang == LangLib.en) ? _area.NameEn : _area.Name;
            }
            #endregion

            #region 取得建案
            if (!string.IsNullOrEmpty(item.EstateObjIDs))
            {
                var _estate = DataLib.StrToIntArray(item.EstateObjIDs);
                foreach (var e_id in _estate)
                {
                    var esData = basedb.EstateObj.Where(p => p.ID == e_id).FirstOrDefault();
                    if (esData != null)
                    {
                        SeminarEstateObj se = new SeminarEstateObj()
                        {
                            id = esData.ID,
                            Name = (lang == LangLib.en) ? esData.NameEn : esData.Name
                        };
                        model.EstateObj.Add(se);
                    }
                }
            }
            #endregion

            #region 取得相關活動列表 條件 相同建案
            var relate_act = basedb.vwActivity
                .Where(p => p.EstateObjIDs != null 
                && p.EstateObjIDs == item.EstateObjIDs
                && p.ID != model.id
                && (p.BeginDate <= DateTime.Now || p.BeginDate == null)
                && (p.EndDate >= DateTime.Now || p.EndDate == null)
                ).Take(5).ToList();
            foreach (var relate in relate_act)
            {
                SeminarRelated r = new SeminarRelated()
                {
                    id = relate.ID,
                    Img = FileLib.ImgPathToUrl(relate.Img),
                    Name = (lang == LangLib.en) ? relate.NameEn : relate.Name,
                    ActDate = relate.ActDate.HasValue ? relate.ActDate.Value.ToString("yyyy/MM/dd") : "",
                    ActTime = relate.ActDate.HasValue ? relate.ActDate.Value.ToString("HH:mm") : "",
                    Week = DateLib.DateToWeekName(relate.ActDate, lang),
                    ActArea = relate.ActArea,
                    AreaName = ""

                };
                var related_area = optionArea.Where(p => p.OptionValue == relate.ActArea).FirstOrDefault();
                if (related_area != null)
                {
                    r.AreaName = (lang == LangLib.en) ? related_area.NameEn : related_area.Name;
                }
                model.related.Add(r);
            }
            #endregion

            return model; ;
        }
        //取得同類別活動報名選項
        public List<SignOption> GetSeminarOption(int id, string lang)
        {
            List<SignOption> model = new List<SignOption>();

            #region 取得選項列表
            var optionArea = opsr.GetList(areaCode);
            #endregion

            #region 取得與活動同類別內容
            var baseData = basedb.vwActivity.Where(p => p.ID == id).FirstOrDefault();
            if (baseData == null)
                throw new Exception("Data Not Found!");


            var list = basedb.vwActivity.Where(p => p.ActType == baseData.ActType).ToList();
            foreach (var item in list)
            {
                var areaData = optionArea.Where(p => p.OptionValue == item.ActArea).FirstOrDefault();

                SignOption data = new SignOption()
                {
                    id = item.ID,
                    Name = (lang == LangLib.en) ? item.NameEn : item.Name,
                    ActDate = item.ActDate.HasValue ? item.ActDate.Value.ToString("yyyy/MM/dd") : "",
                    ActTime = item.ActDate.HasValue ? item.ActDate.Value.ToString("HH:mm") : "",
                    Week = DateLib.DateToWeekName(item.ActDate, lang),
                    ActArea = item.ActArea,
                    ActAddr = (lang == LangLib.en) ? item.ActAddrEn : item.ActAddr,
                    ActPosition = (lang == LangLib.en) ? item.ActPositionEn : item.ActPosition,
                };
                //名稱
                if (lang == LangLib.en)
                {
                    data.AreaName = ((areaData != null) ? areaData.NameEn : "");
                }
                else
                {
                    data.AreaName = ((areaData != null) ? areaData.Name : "");
                }
                
                model.Add(data);
            }
            #endregion

            return model; ;
        }
        //活動報名存檔
        public void SignSave(SignPost model, string lang)
        {
            DbContent.Seminar data = new DbContent.Seminar()
            {
               Sid = model.id,
               Title = model.ActName,
               StartTime = model.ActDate,
               Addr = model.ActAddr,
               ContactName = model.Name,
               Country = model.Country,
               Email = model.Email,
               Phone = model.Prefix + model.Phone,
               Count = model.Count,
               Detail = model.Note,
               CreateDate = DateTime.Now
            };
            basedb.Seminar.Add(data);
            basedb.SaveChanges();

            //寄信
            var HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/seminar.html");
            HtmlContent = HtmlContent.Replace("{NAME}", model.Name);
            HtmlContent = HtmlContent.Replace("{ActName}", model.ActName);
            string time =
                model.ActDate.ToString("yyyy/MM/dd") + 
                " (" + DateLib.DateToWeekName(model.ActDate, lang) + ") " +
                model.ActDate.ToString("HH:mm");
            HtmlContent = HtmlContent.Replace("{Time}", time);
            HtmlContent = HtmlContent.Replace("{Phone}", model.Prefix + " " +  model.Phone);
            HtmlContent = HtmlContent.Replace("{Addr}", model.ActAddr);
            HtmlContent = HtmlContent.Replace("{Count}", model.Count.ToString());
            if (!string.IsNullOrEmpty(model.Note))
            {
                HtmlContent = HtmlContent.Replace("{Note}", model.Note.Replace("\n", "<br/>"));
            }
            else
            {
                HtmlContent = HtmlContent.Replace("{Note}","");
            }
            
            MailLib.SendMail(HtmlContent, "Titan Stone 說明會報名", model.Email);

            //管理員寄信
            CoreWeb.Service.AdminNoticeService sr = new CoreWeb.Service.AdminNoticeService();
            sr.SendActSign(model,data.CreateDate);


        }
    }

}