﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;
using CoreWeb.Service;
namespace CoreWeb.Areas.App.Service
{
    public class apiContactService : CoreWeb.Service.BaseService
    {
        public void Post(apiContactUs data)
        {
            DbContent.ContactUs model = new DbContent.ContactUs()
            {
                Name = data.Name,
                Email = data.Email,
                ContactTime = data.ContactTime,
                Subject = data.ContactType,
                Detail = data.Note,
                SeletedItem = data.EstateId,
                CreateDate = DateTime.Now,
                Country = data.Country,
                Status = "1",
                Phone = data.Prefix + data.Phone,
                SocialID = data.SocialID
            };
            basedb.ContactUs.Add(model);
            basedb.SaveChanges();
            //寄送管理通知
            AdminNoticeService sr = new AdminNoticeService();
            sr.SendContactUs(model);
        }
    }
}