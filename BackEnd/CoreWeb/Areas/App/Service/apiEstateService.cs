﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Lib;
using CoreWeb.Models;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Areas.App.Service
{
    public class apiEstateService : CoreWeb.Service.BaseService
    {
        public apiOptionService opsr = new apiOptionService();
        public List<apiEstate> GetList(string lang)
        {
            List<apiEstate> model = new List<apiEstate>();
            var option = opsr.GetList("SaleType");
            var list = basedb.vwEstateObj.ToList();
            foreach (var item in list)
            {
                apiEstate data = new apiEstate()
                {
                    ID = item.ID,
                    Name = (lang == LangLib.cn) ? item.Name : item.NameEn,
                    SaleType = item.SaleType,
                    SaleName = "",
                    MainImg = FileLib.ImgPathToUrl(item.MainImg),
                    Brief = (lang == LangLib.cn) ? item.Brief : item.BriefEn,
                    Price = item.Price,
                    SpecTag = new string[]{ }
                };
                if (lang == LangLib.cn)
                {
                    if (!string.IsNullOrEmpty(item.SpecTag))
                        data.SpecTag = item.SpecTag.Split(',');
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.SpecTagEn))
                        data.SpecTag = item.SpecTagEn.Split(',');
                }
                var typeObj = option.Where(p => p.OptionValue == item.SaleType).FirstOrDefault();
                if (typeObj != null)
                {
                    data.SaleName = (lang == LangLib.cn) ? typeObj.Name : typeObj.NameEn;
                }
                model.Add(data);
            }
            return model;
        }

        public apiEstateDetail GetDetail(int id,string lang)
        {
            var option = opsr.GetList("SaleType");
            var item = basedb.vwEstateObj.Where(p => p.ID == id).FirstOrDefault();

            #region 取得基本資料
            apiEstateDetail data = new apiEstateDetail()
            {
                ID = item.ID,
                Name = (lang == LangLib.cn) ? item.Name : item.NameEn,
                SaleType = item.SaleType,
                SaleName = "",
                Country = (lang == LangLib.cn) ? item.Country : item.CountryEn,
                MainImg = FileLib.ImgPathToUrl(item.MainImg),
                Brief = (lang == LangLib.cn) ? item.Brief : item.BriefEn,
                EstateType = (lang == LangLib.cn) ? item.EstateType : item.EstateTypeEn,
                Address = (lang == LangLib.cn) ? item.Address : item.Address,
                Price = item.Price,
                SpecTag = new string[] { },
                Detail = (lang == LangLib.cn) ? FileLib.ImgPathToUrl(item.CaseSpec) : FileLib.ImgPathToUrl(item.CaseSpecEn),
                FloorImg = FileLib.ImgPathToUrl(item.FloorImg),
                FloorDesc = item.FloorDesc,
                FileUrl = FileLib.ImgPathToUrl(item.FileUrl),
                Around = apiOptionService.OrderMultiItem(item.Around),
                Device = apiOptionService.OrderMultiItem(item.Device),
                Service = apiOptionService.OrderOneItem(item.Service),
                Album = FileLib.ImgPathToUrl(item.Album),
                //VideoUrl = item.VideoUrl,
                //VideoImg = FileLib.ImgPathToUrl(item.VideoImg)
                CompleteDate = (item.CompleteDate != null) ? item.CompleteDate.Value.ToString("yyyy/MM/dd") : ""
            };
            if (lang == LangLib.cn)
            {
                if (!string.IsNullOrEmpty(item.SpecTag))
                    data.SpecTag = item.SpecTag.Split(',');
            }
            else
            {
                if (!string.IsNullOrEmpty(item.SpecTagEn))
                    data.SpecTag = item.SpecTagEn.Split(',');
             
                    data.Around  = apiOptionService.OrderMultiItem(item.AroundEn);
                    data.Device = apiOptionService.OrderMultiItem(item.DeviceEn);
                    data.Service = apiOptionService.OrderMultiItem(item.ServiceEn);
            }

            if (!string.IsNullOrEmpty(item.VideoUrl) && !string.IsNullOrEmpty(item.VideoImg))
            {
                data.VideoUrl = item.VideoUrl;
                data.VideoImg = FileLib.ImgPathToUrl(item.VideoImg);
            }
            else
            {
                data.VideoUrl = "";
                data.VideoImg = "";
            }
            var typeObj = option.Where(p => p.OptionValue == item.SaleType).FirstOrDefault();
            if (typeObj != null)
            {
                data.SaleName = (lang == LangLib.en) ? typeObj.NameEn : typeObj.Name;
            }
            #endregion

            #region 取得Room
            var roomlist = basedb.EstateRoom.Where(p => p.EstateObjID == item.ID).OrderBy(p => p.OrderNo).ToList();
            foreach (var room in roomlist)
            {
                apiEstateFloor f = new apiEstateFloor()
                {
                    ID = room.ID,
                    Img = FileLib.ImgPathToUrl(room.Img),
                    Name = room.Name,
                    Brief = room.Brief,
                    Area = room.Area,
                    FloorDesc = room.FloorDesc
                };
                data.floor.Add(f);
            }
            #endregion

            #region 取得說明會
            var idStr = item.ID.ToString();
             var actlist = basedb.vwActivity.Where(p => p.EstateObjIDs.Contains(idStr)).ToList();
             var optionType = opsr.GetList("ActType");
             var optionArea = opsr.GetList("ActArea");
            foreach (var act in actlist)
            {
                ActData actitem = new ActData()
                {
                    id = act.ID,
                    Img = FileLib.ImgPathToUrl(act.Img),
                    ActType = act.ActType,
                    TypeName = "",
                    Name = act.Name,
                    ActDate = act.ActDate.HasValue ? act.ActDate.Value.ToString("yyyy/MM/dd") : "",
                    ActTime = act.ActDate.HasValue ? act.ActDate.Value.ToString("HH:mm") : "",
                    Week = DateLib.DateToWeekName(act.ActDate, lang),
                    ActArea = act.ActArea,
                    ActAddr = act.ActAddr,
                    ActPosition = act.ActPosition,
                    Brief = act.Brief,
                    Detail = FileLib.ImgPathToUrl(act.Detail)
                };
                //英文
                if (lang == LangLib.en)
                {
                    actitem.Name = act.NameEn;
                    actitem.ActAddr = act.ActAddrEn;
                    actitem.ActPosition = act.ActPositionEn;
                    actitem.Brief = act.BriefEn;
                    actitem.Detail = FileLib.ImgPathToUrl(act.DetailEn);
                }
                //選項值
                var _type = optionType.Where(p => p.OptionValue == act.ActType).FirstOrDefault();
                if (_type != null)
                {
                    actitem.TypeName = (lang == LangLib.en) ? _type.NameEn : _type.Name;
                }
                var _area = optionArea.Where(p => p.OptionValue == act.ActArea).FirstOrDefault();
                if (_area != null)
                {
                    actitem.AreaName = (lang == LangLib.en) ? _area.NameEn : _area.Name;
                }
                data.actlist.Add(actitem);
            }
            #endregion

            return data;
        }
    }
}