﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    public class apiUserModel
    {
        public int UserID { get; set; }
        public string Token { get; set; }
        public string Act { get; set; }
        public string Name { get; set; }
        public string IdentityNo { get; set; }
        public string Birth { get; set; }
        public string Country { get; set; }
        public string PhonePrefix { get; set; }
        public string Phone { get; set; }
        public string ImageUrl { get; set; }
        public List<PreferModel> Prefer { get; set; }
    }
    public class UserLoginModel
    {
        public string Act { get; set; }
        public string Pwd { get; set; }
        //hidden value
        public string DeviceID { get; set; } //手機唯一碼
        public string Platform { get; set; } //平台名稱
        public string Version { get; set; } //平台版本
        public string PushID { get; set; }   //推播序號
        public string Lang { get; set; }   //語系
    }
    public class RegistModel
    {
        public string Act { get; set; }
        public string Pwd { get; set; }
        public string Name { get; set; }
        public DateTime Birth { get; set; }
        public string Country { get; set; }
        public string PhonePrefix { get; set; } //電話前置碼
        public string Phone { get; set; }
        public string ImageUrl { get; set; }
        public List<PreferModel> Prefer { get; set; }
        //hidden value
        public string DeviceID { get; set; } //手機唯一碼
        public string Platform { get; set; } //平台名稱
        public string Version { get; set; } //平台版本
        public string PushID { get; set; }   //推播序號
        public string Lang { get; set; }
    }
    public class PreferModel
    {
        public string PType { get; set; }
        public string PAnswer { get; set; }
    }
    public class NoticeModel
    {
        public bool Pay { get; set; }
        public bool Rent { get; set; }
        public bool Estate { get; set; }
        public bool Activity { get; set; }
        public bool Trend { get; set; }
        public bool News { get; set; }
        public bool System { get; set; }
    }
    public class NoticeMsg
    {
        public int id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public bool read { get; set; }
        public string date { get; set; }
    }
    public class DeviceModel
    {
        public string DeviceID { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }
        public string PushID { get; set; }
        public string Lang { get; set; }
    }
    public class ChartModel
    {
        public string Name { get; set; }
        public double Sum { get; set; }
    }
    public class MyEstateModel
    {
        public int nowYear { get; set; }
        public int nowMonth { get; set; }
        public List<double> Income { get; set; }
        public List<MyEstateItem> MyEstate { get; set; }
    }
    public class MyEstateItem
    {
        public int ID { get; set; }
        public string Img { get; set; }
        public string SaleType { get; set; }
        public string SaleTypeName { get; set; }
        public string ManageType { get; set; }
        public string ManageTypeName { get; set; }
        public string EstateNo { get; set; }
        public string EstateName { get; set; }
        public string RoomName { get; set; }
    }
    public class MyEstateInfo
    {
        public double Sum { get; set; } //本建案成交金額 (算報酬率用)
        public PayItem Pay { get; set; } //應付款項資訊
        public List<ScheduleItem> Schedule { get; set; } //当前进度
        public EstateSaleDetail Sale { get; set; } //地產資訊
        public List<IncomeItem> Income { get; set; }
        public ManageData Manage { get; set; }
        public RentData Rent { get; set; }
    }
    //付款期程
    public class PayItem
    {
        public double? PaySum { get; set; } //缴交金额
        public double? PayAmount { get; set; } //本期应付金额
        public bool IsPay { get; set; } //是否繳款
        public string PayDate { get; set; } //已繳款日期
    }
    //当前进度
    public class ScheduleItem
    {
        public int ScheduleID { get; set; }
        public string SchNo { get; set; }
        public string Name { get; set; }
        public string Brief { get; set; }
        public string ScheduleType { get; set; }
        public string ScheduleTypeName { get; set; }
        public string EndDate { get; set; }
        public List<WorkItem> work { get; set; } //施工項目相簿
    }
    //施工紀錄
    public class WorkItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Brief { get; set; }
        public string Image { get; set; }
    }
    //地產資訊
    public class EstateSaleDetail
    {
        public int ID { get; set; }
        public string Addr { get; set; }
        public string UnitImage { get; set; }
        public string UnitName { get; set; }
        public string UnitArea { get; set; }
        public string RoomArea { get; set; }
        public string PublicArea { get; set; }
        public double? PriceSum { get; set; }
        public string ParkingType { get; set; }
        public double? ParkingPrice { get; set; }
        public string DealFile { get; set; }
    }
    //收益計算
    public class IncomeItem
    {
        public int Year { get; set; }
        public List<IncomeDetail> Data { get; set; }
    }
    //收益計算 月資訊
    public class IncomeDetail
    {
        public int Month { get; set; }
        public double? Income { get; set; }
        public double? Expense { get; set; }
        public string PdfFile { get; set; }
    }
    //租賃狀況
    public class RentData
    {
        public double? Rent { get; set; }
        public bool IsPay { get; set; } //本期是否缴交
        public string RendName { get; set; }
        public string RendIDNo { get; set; }
        public string Gender { get; set; }
        public string Birth { get; set; }
        public string RendStart { get; set; }
        public string RendEnd { get; set; }
        public string RendFile { get; set; }
    }
    //委託內容
    public class ManageData
    {
        public string SaleType { get; set; }
        public string ManageStart { get; set; }
        public string ManageEnd { get; set; }
        public string ManageFile { get; set; }
    }

    //基金列表
    public class FundSale
    {
        public int ID { get; set; }
        public string FundsNo { get; set; }
        public string FundsName { get; set; }
        public string FundType { get; set; }
        public string FundTypeName { get; set; }
        public List<string> BuyType { get; set; }
        public List<string> BuyTypeName { get; set; }
        public double UnitCount { get; set; } //基金份額
        public double Profit { get; set; } //含息损益
        public double Cost { get; set; }   //投资成本
        public FundSale()
        {
            BuyType = new List<string>();
        }
    }
    //基金內容
    public class FundSaleDetail
    {
        public double? MonthPayAmt { get; set; } //当月缴款金额
        public string PayStatus { get; set; }     //繳款狀態
        public string PayStatusName { get; set; }     //繳款狀態名稱

        public double UnitValue { get; set; }  //对帐日份额净值
        public double UnitCount { get; set; }  //基金份额
        public double Cost { get; set; }       //投资成本
        public double NowValue { get; set; }   //对帐日市值

        public double NoBonusProfit { get; set; }    //不含息损益
        public double NoBonusProfitRage { get; set; }//不含息损益率
        public double BonusSum { get; set; }         //累积配息
        public double BonusProfit { get; set; }      //含息损益
        public double BonusProfitRage { get; set; }  //含息损益率

        public string UnitValueDate { get; set; }  //对帐日期

        public List<FundType> RecordOpt { get; set; }   //選項
        public List<FundSaleItem> TransRecord { get; set; } //交易记录

        public List<FundWorth> FundWorth { get; set; }      //业绩走势
        //public string Statement { get; set; }        //检视对帐单
        //public List<FundReport> FundReport { get; set; }    //业绩走势
    }
    public class FundType
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class FundSaleItem
    {
        public int ID { get; set; }
        public string TxnType { get; set; }
        public string TxnName { get; set; }
        public string TxnNo { get; set; }
        public string TxnDate { get; set; }
        public double Unit { get; set; } //份额
        public double? PurchaseVal { get; set; } //申购淨值
        public double? PurchaseAmt { get; set; } //申購金額

        public double? RedemptionVal { get; set; } //赎回淨值
        public double? RedemptionAmt { get; set; } //赎回金額
        public double? PrefFee { get; set; } //绩效费

        public double? HandlingFee { get; set; } //手续费

        public double? DividendAmt { get; set; } //分红金额

        public string PayStatus { get; set; }
        public string CreateDate { get; set; }
    }
    public class FundWorth
    {
        public int Year { get; set; }
        public List<string> xAxes { get; set; }
        public List<double?> yAxes { get; set; }
        public string LastUpdate { get; set; }
    }
    public class FundReport
    {
        public int ID { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string FileUrl { get; set; }
    }

    //保險列表
    public class MyInsurance
    {
        public int ID { get; set; }
        public string InsNo { get; set; }
        public string InsName { get; set; }
        public string TypeName { get; set; }
        public string BenefitName { get; set; }
        public string StatusName { get; set; }
        public string Currency { get; set; }
        public double Amount { get; set; }
    }
    //保險項目
    public class MyInsuranceItem
    {
        public int ID { get; set; }
        public string InsNo { get; set; }
        public string InsName { get; set; }
        public string TypeName { get; set; }
        public string BuyerName { get; set; }
        public string StatusName { get; set; }
        public string Currency { get; set; }
        public double Amount { get; set; }
        public double InsAmount { get; set; }
        public string InsTypeName { get; set; }
        public string ValidDate { get; set; }
        public string ValidRange { get; set; }
        public string PayPeriod { get; set; }
        public string NextPayDate { get; set; }
        public string BenefitName { get; set; }
        public string SaleName { get; set; }
        public string SalePhone { get; set; }

        public string InsContent { get; set; }
        public string InsNote { get; set; }

        public string Recommend { get; set; }
        public string DealFile { get; set; }
    }

}