﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    //首頁用model
    public class apiContactUs
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Prefix { get; set; }
        public string Phone { get; set; }
        public string SocialID { get; set; }
        public string ContactType { get; set; }
        public string ContactTime { get; set; }
        public string EstateId { get; set; }
        public string Note { get; set; }
    }
}