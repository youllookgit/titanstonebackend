﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    //首頁用model
    public class apiHome
    {
        public List<apiHomeEstate> Estate { get; set; }
        public List<apiHomeTrend> Trend { get; set; }
        public List<apiHomeAct> Act { get; set; }
        public List<apiHomeNews> News { get; set; }
    }
    public class apiHomeEstate
    {
        public int ID { get; set; }
        public string Img { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
    public class apiHomeTrend
    {
        public int ID { get; set; }
        public string Img { get; set; }
        public string Title { get; set; }
        public string TitleEn { get; set; }
    }
    public class apiHomeAct
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Date { get; set; }
        public string Week { get; set; }
        public string Tag { get; set; }
    }
    public class apiHomeNews
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Date { get; set; }
        public string Week { get; set; }
    }
}