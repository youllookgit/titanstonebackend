﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    //首頁用model
    public class apiFaq
    {
        public int ID { get; set; }
        public string Img { get; set; }
        public string Name { get; set; }
    }
    public class apiFaqDetal
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
    }
}