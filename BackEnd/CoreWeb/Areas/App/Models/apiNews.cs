﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    public class apiNewsData
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string PublicDate { get; set; }
        public string Week { get; set; }
        public string Brief { get; set; }
        public string ImageUrl { get; set; }
        public string Detail { get; set; }
        //public string BeginDate { get; set; }
        //public string EndDate { get; set; }
        //public string CreateDate { get; set; }
        //public int? OrderNo { get; set; }
    }

    public class apiNewsDetail
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string PublicDate { get; set; }
        public string Week { get; set; }
        public string Brief { get; set; }
        public string ImageUrl { get; set; }
        public string Detail { get; set; }
        //public string BeginDate { get; set; }
        //public string EndDate { get; set; }
        //public string CreateDate { get; set; }
        //public int? OrderNo { get; set; }
        public string BeforeID { get; set; }
        public string AfterID { get; set; }
    }
}