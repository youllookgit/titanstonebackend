﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    public class apiTrendList
    {
        public List<apiTrendOption> option { get; set; }
        public List<apiTrendDataList> list { get; set; }
        public bool IsLogin { get; set; }
        public apiTrendList()
        {
            option = new List<apiTrendOption>();
            list = new List<apiTrendDataList>();
        }
    }

    public class apiTrendOption
    {
        public int id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }
    public class apiTrendDataList
    {
        public int ID { get; set; }
        public string TradeType { get; set; }
        public string Name { get; set; }
        public string DisplayDate { get; set; }
        public string Brief { get; set; }
        public string Img { get; set; }
        public string Author { get; set; }
        public string Detail { get; set; }
    }

    public class apiTrendData
    {
        public int ID { get; set; }
        public string TradeType { get; set; }
        public string Name { get; set; }
        public string DisplayDate { get; set; }
        public string Brief { get; set; }
        public string Img { get; set; }
        public string Author { get; set; }
        public string Detail { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string CreateDate { get; set; }
        public bool Status { get; set; }
        public List<Recommend> RecommandList { get; set; }
        public string BeforeID { get; set; }
        public string AfterID { get; set; }
        public bool AfterValid { get; set; } //是否允許看下一筆
        public apiTrendData()
        {
            RecommandList = new List<Recommend>();
        }
    }

    public class Recommend
    {
        public int ID { get; set; }
        public string Img { get; set; }
        public string Name { get; set; }
    }
}