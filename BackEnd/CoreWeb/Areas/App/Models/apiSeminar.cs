﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    //列表資料
    public class apiSeminarList
    {
        public List<apiOption> typeOpt { get; set; }  /*類型選單*/
        public List<apiOption> areaOpt { get; set; }  /*場次選單*/
        public List<ActData> list { get; set; }
        public apiSeminarList()
        {
            typeOpt = new List<apiOption>();
            areaOpt = new List<apiOption>();
            list = new List<ActData>();
        }
    }
    //列表用model
    public class ActData
    {
        public int id { get; set; }
        public string Img { get; set; }
        public string ActType { get; set; }
        public string TypeName { get; set; }
        public string Name { get; set; }
        public string ActDate { get; set; }
        public string ActTime { get; set; }
        public string Week { get; set; }
        public string ActArea { get; set; }
        public string AreaName { get; set; }
        public string ActPosition { get; set; }
        public string ActAddr { get; set; }
        public string Brief { get; set; }
        public string Detail { get; set; }
    }
    //內容model
    public class ActDetail
    {
        public int id { get; set; }
        public string ActType { get; set; }
        public string TypeName { get; set; }
        public string Name { get; set; }
        public string ActDate { get; set; }
        public string ActTime { get; set; }
        public string Week { get; set; }
        public string ActArea { get; set; }
        public string AreaName { get; set; }
        public string ActPosition { get; set; }
        public string ActAddr { get; set; }
        public List<string> ImgList = new List<string>();
        public string Brief { get; set; }
        public string Process { get; set; } //流程
        public string Traffic { get; set; } //交通
        public string Detail { get; set; }
        public List<SeminarEstateObj> EstateObj { get; set; } //建案列表
        public List<SeminarRelated> related { get; set; } //建案列表
        public ActDetail()
        {
            ImgList = new List<string>();
            EstateObj = new List<SeminarEstateObj>();
            related = new List<SeminarRelated>();
        }
    }
    //建案列表
    public class SeminarEstateObj
    {
        public int id { get; set; }
        public string Name { get; set; }
    }
    //相關場次
    public class SeminarRelated
    {
        public int id { get; set; }
        public string Img { get; set; }
        public string Name { get; set; }
        public string ActDate { get; set; }
        public string ActTime { get; set; }
        public string Week { get; set; }
        public string ActArea { get; set; }
        public string AreaName { get; set; }
    }
   
    //說明會選單
    public class SignOption
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string ActDate { get; set; }
        public string ActTime { get; set; }
        public string Week { get; set; }
        public string ActArea { get; set; }
        public string AreaName { get; set; }
        public string ActPosition { get; set; }
        public string ActAddr { get; set; }
    }
    //說明會送出
    public class SignPost
    {
        public int id { get; set; }
        public string ActName { get; set; }
        public DateTime ActDate { get; set; }
        public string ActAddr { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Prefix { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Count { get; set; }
        public string Note { get; set; }
    }
}