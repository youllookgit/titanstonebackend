﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    public class apiOption
    {
        public int id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string extend1 { get; set; }
        public string extend2 { get; set; }

    }
}