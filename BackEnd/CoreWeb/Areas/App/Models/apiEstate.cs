﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Areas.App.Models
{
    //列表model
    public class apiEstate
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string SaleType { get; set; }
        public string SaleName { get; set; }
        public string MainImg { get; set; }
        public string Brief { get; set; }
        public string Price { get; set; }
        public string[] SpecTag { get; set; }
    }
    public class apiEstateDetail
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
        public string SaleType { get; set; }
        public string SaleName { get; set; }
        public string MainImg { get; set; }
        public string Brief { get; set; }
        public string Price { get; set; }
        public string EstateType { get; set; }
        public string Address { get; set; }
        public string[] SpecTag { get; set; }
        public string Detail { get; set; }
        public string FloorImg { get; set; }
        public string FloorDesc { get; set; }
        public List<apiEstateFloor> floor { get; set; }
        public string FileUrl { get; set; }
        public string Around { get; set; }
        public string Device { get; set; }
        public string Service { get; set; }
        public string Album { get; set; }
        public string VideoUrl { get; set; }
        public string VideoImg { get; set; }
        public List<ActData> actlist { get; set; }
        public string CompleteDate { get; set; }


        public apiEstateDetail()
        {
            floor = new List<apiEstateFloor>();
            actlist = new List<ActData>();
        }
    }
    public class apiEstateFloor
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Brief { get; set; }
        public string Area { get; set; }
        public string Img { get; set; }
        public string FloorDesc { get; set; }
    }
}