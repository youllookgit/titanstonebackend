﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using CoreWeb.Lib;
namespace CoreWeb.Service
{
    public class PushService: Service.BaseService
    {
        //自動推播類別 : 發送邏輯
        //工程缴款 1 房租收款 2 物业产品 3
        //理财趋势 4 精选活动 5 官方消息 6
        //系統 7

        //自動推播類別 工程缴款 1 [會員推播]
        public void SchedulePush(int EsSchID)
        {
            string pushType = "1";
            //取得工程進度資料 並驗證是否需推播 驗證條件 : 已完工 ScheduleType:3
            var sData = basedb.EstateSchedule.Where(p => 
            p.ID == EsSchID && p.ScheduleType == "3"
            ).FirstOrDefault();
          
            if (sData != null)
            {
                List<string> IDNos = new List<string>();

                #region 取得有購買該物件(已啟用) 所有不重複IDNo 並開啟系統通知繳款
                var sales = basedb.EstateSale.Where(p => p.EstateObjID == sData.EstateObjID && p.Status == true && p.IsPush == true).ToList();
                foreach (var s in sales)
                {
                    if (!string.IsNullOrEmpty(s.UserIdentityNo))
                    {
                        var ids = s.UserIdentityNo.Split(',');
                        foreach (var id in ids)
                        {
                            if (IDNos.Contains(id) == false)
                            {
                                IDNos.Add(id);
                            }
                        }
                    }

                }
                #endregion

                List<string> PushIDs = new List<string>();
                #region 取得所有IDNo (會員) 的 PushID
                var users = basedb.User.Where(p => 
                    p.Status == true && 
                    p.IdentityNo != null &&
                    p.PushID != null &&
                    IDNos.Contains(p.IdentityNo)
                ).ToList();

                foreach (var u in users)
                {
                    if (!string.IsNullOrEmpty(u.PushID))
                    {
                        if (PushIDs.Contains(u.PushID) == false)
                        {
                            PushIDs.Add(u.PushID);
                        }
                    }
                }
                #endregion
                //物件名称 第X期工程进度完成，请于Y前完成缴款。
                string esName = "";
                string esNameEn = "";
                string endData = "";
                if (sData != null)
                {
                    var esData = basedb.EstateObj.Where(p => p.ID == sData.EstateObjID).FirstOrDefault();
                    esName = (esData == null) ? "" : esData.Name + " ";
                    esData.NameEn = (esData == null) ? "" : esData.NameEn + " ";
                    endData = (sData.RealComplete == null) ? "" : sData.RealComplete.Value.AddDays(30).ToString("yyyy/MM/dd");
                }

                string pushMsg = esName + sData.SchNo + "期工程进度完成，请于" + endData + "前完成缴款。";//"工程缴款通知";
                string pushMsgEn = "Progress Payment: Phase " + sData.SchNoEn + " construction of " + esNameEn
                   + " has be completed, and please make a payment by " + endData;//"工程缴款通知";

                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = string.Join(",", PushIDs) //過濾推播功能(非會員會自動取)
                };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = string.Join(",", PushIDs) //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //自動推播類別 房租收款 2 [會員推播]
        public void RentPush(int BenefitRecordID)
        {
            string pushType = "2"; //房租收款 2
            
            //取得收益記錄管理 並驗證是否需推播 驗證條件 : 已撥款 PayStatus:2
            var sData = basedb.BenefitRecord.Where(p => 
              p.ID == BenefitRecordID && p.PayStatus == "2"
            ).FirstOrDefault();

            string esName = "";
            string datestr = "";

            string pushMsg = "";
            string pushMsgEn = "";


            if (sData != null)
            {
                var esData = basedb.EstateObj.Where(p => p.ID == sData.EstateObjID).FirstOrDefault();
                esName = (esData == null) ? "" : esData.Name + " ";
                if (sData.PayDate != null)
                {
                    datestr = sData.IncomeDate.Value.ToString("MM") + "月 ";
                    pushMsg = esName + datestr + "租金已收款";// "房租收款通知";
                    pushMsgEn = "Rent Income: rent of " + esData.NameEn + " is received on " + sData.PayDate.Value.ToString("yyyy.MM.dd");//"房租收款通知";
                }
            }
            
            

            if (sData != null)
            {
                List<string> IDNos = new List<string>();

                #region 取得有購買該物件(已啟用) 所有不重複IDNo
                var sales = basedb.EstateSale.Where(p => p.ID == sData.EstateSaleID && p.Status == true).ToList();
                foreach (var s in sales)
                {
                    if (!string.IsNullOrEmpty(s.UserIdentityNo))
                    {
                        var ids = s.UserIdentityNo.Split(',');
                        foreach (var id in ids)
                        {
                            if (IDNos.Contains(id) == false)
                            {
                                IDNos.Add(id);
                            }
                        }
                    }

                }
                #endregion

                List<string> PushIDs = new List<string>();
                #region 取得所有IDNo (會員) 的 PushID
                var users = basedb.User.Where(p => 
                    p.Status == true && 
                    p.IdentityNo != null &&
                    p.PushID != null &&
                    IDNos.Contains(p.IdentityNo)
                ).ToList();

                foreach (var u in users)
                {
                    if (!string.IsNullOrEmpty(u.PushID))
                    {
                        if (PushIDs.Contains(u.PushID) == false)
                        {
                            PushIDs.Add(u.PushID);
                        }
                    }
                }
                #endregion

                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = string.Join(",", PushIDs) //過濾推播功能(非會員會自動取)
            };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = string.Join(",", PushIDs) //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //自動推播類別 物业产品 3 [非會員推播]
        public void EstatePush(int EstateID)
        {
            string pushType = "3"; //物业产品 3
            // 驗證條件 : 該建案啟用 Status:true
            var sData = basedb.EstateObj.Where(p =>
             p.ID == EstateID && p.Status == true
           ).FirstOrDefault();

            string pushMsg = sData.Name;// "物业产品通知";
            string pushMsgEn = sData.NameEn;//"物业产品通知";

            if (sData != null)
            {
                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //自動推播類別 理财趋势 4 [非會員推播]
        public void TrendPush(int TrendID)
        {
            string pushType = "4"; //理财趋势 4
            // 驗證條件 : 啟用 Status:true
            var sData = basedb.TradeNews.Where(p =>
             p.ID == TrendID && p.Status == true
           ).FirstOrDefault();

            string pushMsg = sData.Name;// "理财趋势通知";
            string pushMsgEn = sData.NameEn;//"理财趋势通知";

            if (sData != null)
            {
                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //自動推播類別 精选活动 5 [非會員推播]
        public void SeminarPush(int ActivityID)
        {
            string pushType = "5"; //精选活动 5
            // 驗證條件 : 啟用 Status:true
            var sData = basedb.Activity.Where(p =>
             p.ID == ActivityID && p.Status == true
           ).FirstOrDefault();

            string pushMsg = PushTitleLitmit(sData.Name);// "精选活动通知";
            string pushMsgEn = PushTitleLitmit(sData.NameEn);//"精选活动通知";

            if (sData != null)
            {
                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //自動推播類別 官方消息 6 [非會員推播]
        public void NewsPush(int NewsID)
        {
            string pushType = "6"; //官方消息 6
            
            // 驗證條件 : 啟用 Status:true
            var sData = basedb.News.Where(p =>
             p.ID == NewsID && p.Status == true
           ).FirstOrDefault();

            string pushMsg = PushTitleLitmit(sData.Title);// "官方消息通知";
            string pushMsgEn = PushTitleLitmit(sData.TitleEn);//"官方消息通知";

            if (sData != null)
            {
                //建立推播Cn
                PushModel pushReqCn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.cn,
                    Detail = pushMsg,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqCn, true);
                //建立推播En
                PushModel pushReqEn = new PushModel()
                {
                    PType = pushType,
                    TargetType = "3", //自訂
                    Lang = LangLib.en,
                    Detail = pushMsgEn,
                    DeviceIDs = "" //過濾推播功能(非會員會自動取)
                };
                Create(pushReqEn, true);
            }
        }
        //新增Push / ingore > 忽略無推播對象的錯誤訊息
        public void Create(PushModel req,bool ingore = false)
        {
            // [PType] 通知類型
            //  1: 工程繳款
            //  2: 房租收款
            //  3: 物業產品
            //  4: 理財趨勢
            //  5: 精選活動
            //  6: 官方消息
            //  7: 系統
            // [TargetType] 通知對象類型
            //  1: 所有用戶
            //  2: 會員用戶
            //  3: 自訂

            // 新增通知紀錄 沒PushIDs 會存不會發
            DbContent.PushLog model = new DbContent.PushLog();
            model.PType = req.PType;
            model.Lang = req.Lang;
            model.TargetType = req.TargetType;
            model.Detail = req.Detail;
            req.DeviceIDs = (string.IsNullOrEmpty(req.DeviceIDs)) ? "" : req.DeviceIDs;
            model.CreateDate = DateTime.Now;
            //過濾推播權限
            if (model.TargetType == "3")
            {
                model.PushIDs = FilterPush(req.PType, req.DeviceIDs, req.DeviceIDs, model.Lang);
            }
            else
            {
                if (model.TargetType == "2")
                {
                    var _DeviceIDs = basedb.User.Where(p => p.Lang == model.Lang).Select(p => p.PushID).ToList();
                    req.DeviceIDs = String.Join(",", _DeviceIDs);
                }
                model.PushIDs = FilterPush(req.PType, req.DeviceIDs,null,model.Lang);
            }

            var noticeType = basedb.Option.Where(p => p.Code == "Noitce" && p.OptionValue == req.PType).FirstOrDefault();

            string noticeName = "";
            if (noticeType != null)
            {
                noticeName = (model.Lang == LangLib.en) ? noticeType.NameEn : noticeType.Name;
            }
            if (string.IsNullOrEmpty(model.PushIDs)　&& ingore == false)
            {
                throw new Exception("无任何可推播对象");
            }

            basedb.PushLog.Add(model);
            basedb.SaveChanges();

            if (string.IsNullOrEmpty(model.PushIDs) && ingore == true)
            {
                return;
            }

            //儲存至收件夾
            if (!string.IsNullOrEmpty(model.PushIDs))
            {
                var _ids = model.PushIDs.Split(',');
                foreach (var id in _ids)
                {
                    DbContent.PushLogItem _mitem = new DbContent.PushLogItem()
                    {
                        IsRead = false,
                        PushLogID = model.ID,
                        PushID = id,
                        Title = noticeName,
                        Detail = model.Detail,
                        CreateDate = DateTime.Now
                    };
                    basedb.PushLogItem.Add(_mitem);
                }
                basedb.SaveChanges();
            }

            //推播字串刪減
            model.Detail = PushTitleLitmit(model.Detail);
            string url = "https://api.jpush.cn/v3/push";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";

            #region 加入 Authorization
            string encodeStr = AppSetting.PushAuthorization();
            string basicStr = "Basic " + Lib.Security.Base64Encode(encodeStr);

            request.Headers.Add("Authorization", basicStr);
            #endregion

            string result = "";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string param = "";
                if (model.TargetType == "1")
                {
                    #region [TargetType] 通知對象類型 1: 所有用戶
                    object body = new
                    {
                        platform = "all",
                        audience = "all",
                        notification = new
                        {
                            alert = model.Detail,
                            android = new
                            {
                                title = noticeName,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            },
                            ios = new
                            {
                                badge = 0,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            }
                        },
                        //開發環境
                        options = new { apns_production = true }
                    };
                    param = JsonConvert.SerializeObject(body);
                    #endregion
                }
                else if (model.TargetType == "2")
                {
                    #region [TargetType] 通知對象類型 2: 會員用戶
                    string sqlcmd = "SELECT DISTINCT PushID FROM [dbo].[User] WHERE PushID IS NOT NULL AND PushID != 'no-id'";
                    List<string> ids = basedb.Database.SqlQuery<string>(sqlcmd).ToList();
                    object body = new
                    {
                        platform = "all",
                        audience = new { registration_id = ids.ToArray() },
                        notification = new
                        {
                            alert = model.Detail,
                            android = new
                            {
                                title = noticeName,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            },
                            ios = new
                            {
                                badge = 0,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            }
                        },
                        //開發環境
                        options = new { apns_production = true }
                    };
                    param = JsonConvert.SerializeObject(body);
                    #endregion
                }
                else if (model.TargetType == "3")
                {
                    #region [TargetType] 通知對象類型 3: 自訂
                    object body = new
                    {
                        platform = "all",
                        audience = new { registration_id = model.PushIDs.Split(',') },
                        notification = new
                        {
                            alert = model.Detail,
                            android = new
                            {
                                title = noticeName,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            },
                            ios = new
                            {
                                badge = 0,
                                extras = new { typeid = req.PType, name = noticeName, dateStr = DateTime.Now.ToString("yyyy-MM-dd") }
                            }
                        },
                        //開發環境
                        options = new { apns_production = true }
                    };
                    param = JsonConvert.SerializeObject(body);
                    #endregion
                }
                streamWriter.Write(param);
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            var pushRes = JsonConvert.DeserializeObject<pushRespone>(result);

            AdminNoticeService adsr = new AdminNoticeService();
            if (pushRes.error == null)
            {
                model.sendno = pushRes.sendno;
                model.msg_id = pushRes.msg_id;
                basedb.SaveChanges();
                //寄送推播發送結果
                adsr.SendPushNotice(model.ID,true);
            }
            else
            {
                model.msg_id = pushRes.msg_id;
                model.errorcode = pushRes.error.code.ToString();
                model.errorcode = pushRes.error.message;
                basedb.SaveChanges();
                //寄送推播發送結果
                adsr.SendPushNotice(model.ID, false);
                throw new Exception(pushRes.error.message);
            }
        }
        // 查詢PushLog
        public List<DbContent.PushLog> GetLogs(string keyword, string startDate, string endDate, string type, string targettype)
        {
            var data = basedb.PushLog.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    // TODO: 其他搜尋欄位
                    p => p.Detail.Contains(keyword) ||
                    p.PushIDs.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (!string.IsNullOrEmpty(type))
            {
                data = data.Where(
                    p => p.PType == type
               );
            }
            //檢查通知對象
            if (!string.IsNullOrEmpty(targettype))
            {
                data = data.Where(
                    p => p.TargetType == targettype
                );
            }

            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }
        // 新增PushLog
        public void CreateLog(DbContent.PushLog model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.PushLog.Add(model);
            basedb.SaveChanges();
        }
        // 刪除PushLog
        public void DeleteLog(int id)
        {
            // 取得指定資料
            var data = basedb.PushLog.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.PushLog.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
        // 過濾推播類別 關閉對象的ID
        public string FilterPush(string PushType,string PushIDStr,string TargetID = null,string Lang = "zh_CN")
        {
            string[] PushIDs = (string.IsNullOrEmpty(PushIDStr)) ? new string[] { } : PushIDStr.Split(',');

            List<string> filter = new List<string>();
            //工程缴款 1
            if (PushType == "1")
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.Pay == true &&
                p.PushID != "no-id" &&
                p.Lang == Lang &&
                PushIDs.Contains(p.PushID)).ToList();
                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //房租收款 2
            if (PushType == "2")
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.Rent == true &&
                p.PushID != "no-id" &&
                p.Lang == Lang &&
                PushIDs.Contains(p.PushID)).ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //物业产品 3 
            if (PushType == "3")  
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.Estate == true &&
                p.Lang == Lang &&
                p.PushID != "no-id").ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //理财趋势 4
            if (PushType == "4")
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.Trend == true &&
                p.Lang == Lang &&
                p.PushID != "no-id").ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //精选活动 5
            if (PushType == "5") 
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.Activity == true &&
                p.Lang == Lang &&
                p.PushID != "no-id").ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //官方消息 6
            if (PushType == "6") 
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.News == true &&
                p.Lang == Lang &&
                p.PushID != "no-id").ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }
            //系統 7
            if (PushType == "7")
            {
                var litmit = basedb.DeviceNotice.Where(p =>
                p.System == true &&
                p.Lang == Lang &&
                p.PushID != "no-id").ToList();

                foreach (var _l in litmit)
                {
                    if (!string.IsNullOrEmpty(_l.PushID))
                    {
                        if (filter.Contains(_l.PushID) == false)
                        {
                            filter.Add(_l.PushID);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(TargetID))
            {
                var target = TargetID.Split(',');
                var targetFilter = new List<string>();
                foreach (string t in target)
                {
                    var isexist = filter.Where(p => p == t).FirstOrDefault();
                    if (!string.IsNullOrEmpty(isexist))
                    {
                        //是否語系正確
                        var langCurrect = basedb.DeviceNotice.Where(p => p.PushID == isexist && p.Lang == Lang).FirstOrDefault();
                        if (langCurrect != null)
                        {
                            targetFilter.Add(t);
                        }
                    }
                }
                return String.Join(",", targetFilter);
            }
            else
            {
                return String.Join(",", filter);
            }
        }
        // 查詢PushLogItem
        public string[] GetItems(int LogId)
        {
            var data = basedb.PushLog.Where(p => p.ID == LogId).FirstOrDefault();
            if (data != null)
            {
                return data.PushIDs.Split(',');
            }
            else
            {
                return null;
            }
        }

        private class pushRespone
        {
            public string sendno { get; set; }
            public string msg_id { get; set; }
            public pushError error { get; set; }
        }
        private class pushError
        {
            public int code { get; set; }
            public string message { get; set; }
        }

        private string PushTitleLitmit(string title)
        {
            string result = title;
            if (result.Length > 30)
            {
                result = title.Substring(0, 27) + "...";
            }
            return result;
        }
    }
}