﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FundsWorthService: Service.BaseService
    {
        // 查詢FundsWorth
        public List<DbContent.FundsWorth> GetList(double? min, double? max, string startDate, string endDate, int? FundsID)
        {
            var data = basedb.FundsWorth.Where(p => p.ID > 0);
            if (FundsID != null)
            {
                data = data.Where(p => p.FundsID == FundsID);
            }
            if (min != null)
            {
                data = data.Where(p => p.WorthValue >= min);
            }
            if (max != null)
            {
                data = data.Where(p => p.WorthValue <= max);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.WorthDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.WorthDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            return data.ToList();
        }

        // 新增FundsWorth
        public void Create(DbContent.FundsWorth model)
        {
            // 檢查資料設定日期
            var data = basedb.FundsWorth.Where(p => p.WorthDate == model.WorthDate).FirstOrDefault();
            if (data == null)
            {
                // 處理數據
                model.CreateDate = DateTime.Now;
                // 新增資料
                basedb.FundsWorth.Add(model);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Assigning Date Duplicated");
            }
        }

        // 修改FundsWorth
        public void Modify(DbContent.FundsWorth model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.FundsWorth.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.WorthDate = model.WorthDate;
                    data.WorthValue = model.WorthValue;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除FundsWorth
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.FundsWorth.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.FundsWorth.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}