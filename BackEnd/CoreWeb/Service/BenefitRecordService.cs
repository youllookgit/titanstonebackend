﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class BenefitRecordService: Service.BaseService
    {
        // 查詢BenefitRecord
        public List<DbContent.BenefitRecord> GetList(string startDate, string endDate, string payStartDate, string payEndDate, int? EstateObjID, int? EstateSaleID)
        {
            var data = basedb.BenefitRecord.Where(p => p.ID > 0);
            if (EstateObjID != null)
            {
                data = data.Where(p => p.EstateObjID == EstateObjID);
            }
            if (EstateSaleID != null)
            {
                data = data.Where(p => p.EstateSaleID == EstateSaleID);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (!string.IsNullOrEmpty(payStartDate))
            {
                DateTime psDate = DateTime.Parse(payStartDate);
                data = data.Where(p => p.PayDate >= psDate);
            }
            if (!string.IsNullOrEmpty(payEndDate))
            {
                DateTime peDate = DateTime.Parse(payEndDate).AddDays(1);
                data = data.Where(p => p.PayDate < peDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.PdfFile = item.PdfFile != null ? FileLib.ImgPathToUrl(item.PdfFile) : item.PdfFile;
            });
            return list;
        }

        // 新增BenefitRecord
        public void Create(DbContent.BenefitRecord model)
        {
            // 處理數據
            model.PdfFile = model.PdfFile != null ? FileLib.ImgPathToSQL(model.PdfFile) : model.PdfFile;
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.BenefitRecord.Add(model);
            basedb.SaveChanges();

            if (model.PayStatus == "2")
            {
                //發房租收款推播
                PushService psr = new PushService();
                psr.RentPush(model.ID);
            }
        }

        // 修改BenefitRecord
        public void Modify(DbContent.BenefitRecord model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.BenefitRecord.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.CharterType = model.CharterType;
                    data.RenterName = model.RenterName;
                    data.RenterGender = model.RenterGender;
                    data.RenterIdNo = model.RenterIdNo;
                    data.RenterBirth = model.RenterBirth;
                    data.Income = model.Income;
                    
                    data.Expense = model.Expense;
                    data.PayDate = model.PayDate;
                    data.IncomeDate = model.IncomeDate;

                    data.PdfFile = model.PdfFile != null ? FileLib.ImgPathToSQL(model.PdfFile) : model.PdfFile;
                    data.Note = model.Note;
                    data.PayStatus = model.PayStatus;
                    basedb.SaveChanges();

                    if (data.PayStatus == "2")
                    {
                        //發房租收款推播
                        PushService psr = new PushService();
                        psr.RentPush(data.ID);
                    }
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除BenefitRecord
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.BenefitRecord.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.BenefitRecord.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void Import(int EstateObjID, int EstateSaleID, List<CoreWeb.Models.ImportModel.BenefitRecordData> data)
        {
            if (EstateObjID <= 0 || EstateSaleID <= 0 || data == null || data.Count() == 0)
            {
                throw new Exception("汇入格式不正确");
            }
            //查詢該案件租賃合約
            var RentDataList = basedb.RentData.Where(
                p => p.ID > 0 &&
                p.EstateSaleID == EstateSaleID &&
                p.RentStatus == "1"
            ).OrderByDescending(p => p.RentEnd).ToList();

            foreach (var item in data)
            {
                //需檢查租約租約
                var RentData = RentDataList.FirstOrDefault();
                if (RentData == null)
                   throw new Exception("查无有效租赁合约");

                if (item.CharterType != "1" && item.CharterType != "2")
                    throw new Exception("类型不正确");

                if (item.RenterGender != "male" && item.RenterGender != "female")
                    throw new Exception("租客性别不正确");

                if (item.PayStatus != "1" && item.PayStatus != "2")
                    throw new Exception(" 拨款状态不正确");

                DbContent.BenefitRecord model = new DbContent.BenefitRecord()
                {
                    EstateObjID = EstateObjID,
                    EstateSaleID = EstateSaleID,
                    PayStatus = item.PayStatus,
                    CharterType = item.CharterType,
                    CreateDate = DateTime.Now,
                    Expense = item.Expense,
                    Note = item.Note,
                    PayDate = item.PayDate,
                    Income = item.Income,
                    IncomeDate = item.IncomeDate,
                    RenterBirth = item.RenterBirth,
                    RenterGender = item.RenterGender,
                    RenterIdNo = item.RenterIdNo,
                    RenterName = item.RenterName,
                    RentDataID = RentData.ID
                };
                basedb.BenefitRecord.Add(model);
            }
            
            basedb.SaveChanges();
        }
    }
}