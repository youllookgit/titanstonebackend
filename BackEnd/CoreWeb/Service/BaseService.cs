﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class BaseService
    {
        public CoreWeb.DbContent.Entities basedb;

        public BaseService(CoreWeb.DbContent.Entities basedb = null)
        {
            this.basedb = basedb ?? new CoreWeb.DbContent.Entities();
        }
    }
}