﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class SeminarService: Service.BaseService
    {
        // 查詢Seminar
        public List<DbContent.Seminar> GetList(string keyword, string startPDate, string endPDate, string startSDate, string endSDate)
        {
            var data = basedb.Seminar.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Title.Contains(keyword) || 
                    p.Addr.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(startPDate))
            {
                DateTime sDate = DateTime.Parse(startPDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endPDate))
            {
                DateTime eDate = DateTime.Parse(endPDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (!string.IsNullOrEmpty(startSDate))
            {
                DateTime sDate = DateTime.Parse(startSDate);
                data = data.Where(p => p.StartTime >= sDate);
            }
            if (!string.IsNullOrEmpty(endSDate))
            {
                DateTime eDate = DateTime.Parse(endSDate).AddDays(1);
                data = data.Where(p => p.StartTime < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        // 新增Seminar
        public void Create(DbContent.Seminar model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.Seminar.Add(model);
            basedb.SaveChanges();
        }

        // 修改Seminar
        public void Modify(DbContent.Seminar model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.Seminar.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Handler = model.Handler;
                    data.SendDate = DateTime.Now;
                    data.Note = model.Note;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除Seminar
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.Seminar.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.Seminar.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}