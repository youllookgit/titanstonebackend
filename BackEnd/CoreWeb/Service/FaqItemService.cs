﻿using CoreWeb.Lib;
using CoreWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FaqItemService : Service.BaseService
    {
        //取得列表
        public List<FaqItemModel> GetFaqItem(string keyword, int? faqsortID, bool? status, DateTime? startdate, DateTime? enddate)
        {

            List<FaqItemModel> oFaq_Data = new List<FaqItemModel>();
            var data = basedb.FaqItem.Where(p => p.ID > 0);
            
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Title.Contains(keyword)
               );
            }

            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            if (faqsortID != null)
            {
                data = data.Where(p => p.FaqSortID == faqsortID);
            }

            if (startdate != null)
            {
                data = data.Where(p => p.CreateDate >= startdate);
            }

            if (enddate != null)
            {
                data = data.Where(p => p.CreateDate <= enddate);
            }
					
            data = data.OrderByDescending(p => p.CreateDate);

            foreach(var item in data)
            {
                FaqItemModel oModel = new FaqItemModel();
                oModel.ID = item.ID;
                oModel.Status = item.Status;
                oModel.Title = item.Title;
                oModel.TitleEn = item.TitleEn;
                oModel.Detail = item.Detail;
                oModel.DetailEn = item.DetailEn;
                oModel.OrderNo = item.OrderNo;
                oModel.CreateDate = item.CreateDate;
                oModel.FaqSortID = item.FaqSortID;
                if (item.FaqSortID > 0)
                {
                    var faqsortdata = basedb.FaqSort.Where(p => p.ID > 0);
                    var faqitem = faqsortdata.Where(p => p.ID == item.FaqSortID).First();
                    oModel.FaqSortName = faqitem.Name;
                }
                oFaq_Data.Add(oModel);

            }
            return oFaq_Data;
        }

        //新增FaqItem
        public void Create(DbContent.FaqItem model)
        {
            if (model.ID > 0)
            {
                //修改FaqItem
                var data = basedb.FaqItem.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.Title = model.Title;
                    data.TitleEn = model.TitleEn;
                    data.FaqSortID = model.FaqSortID;
                    data.Detail = FileLib.ImgPathToSQL(model.Detail);
                    data.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;

                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //參數處理
                model.CreateDate = DateTime.Now;

                //加入新增資料
                basedb.FaqItem.Add(model);
                //存檔
                basedb.SaveChanges();
            }


        }

        //刪除FaqItem
        public void Remove(int id)
        {
            var data = basedb.FaqItem.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.FaqItem.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}