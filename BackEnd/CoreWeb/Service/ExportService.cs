﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class ExportService : Service.BaseService
    {
        public static void SaveExport(object data)
        {
            HttpContext.Current.Session["record"] = data;
        }
        public static object GetExport()
        {
            if(HttpContext.Current.Session["record"] != null)
            {
                return HttpContext.Current.Session["record"];
            }
            else
            {
                return null;
            }
        }
        //Channel 名稱匯出用
        public string GetChannelName(string channelID)
        {
            List<string> NameList = new List<string>();
            if (!string.IsNullOrEmpty(channelID))
            {
                var ChannelList = channelID.Split(',');
                foreach (var str in ChannelList)
                {
                    int _id = 0;
                    if (int.TryParse(str, out _id))
                    {
                        var c = basedb.Channel.Where(p => p.ID == _id).FirstOrDefault();
                        if (c != null)
                        {
                            NameList.Add(c.Name);
                        }
                    }
                }

                return String.Join(",", NameList);
            }
            else
            {
                return "";
            }
        }
    }
}
