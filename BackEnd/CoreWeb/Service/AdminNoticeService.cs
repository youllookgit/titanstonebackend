﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
using CoreWeb.Lib;
using CoreWeb.Areas.App.Models;

namespace CoreWeb.Service
{
    public class AdminNoticeService: Service.BaseService
    {
        // 查詢AdminNotice
        public List<DbContent.AdminNotice> GetList(bool? status)
        {
            var data = basedb.AdminNotice.Where(p => p.ID > 0);
            if (status != null)
            {
                data = data.Where(p => p.Status == status); ;
            }
            data = data.OrderByDescending(p => p.ID);
            return data.ToList();
        }
        // 新增AdminNotice
        public void Create(DbContent.AdminNotice model)
        {
            var addr = new System.Net.Mail.MailAddress(model.Email);
            if (addr.Address != model.Email)
                throw new Exception("Email wrong format"); //請輸入正確的Email格式

            // 新增資料
            basedb.AdminNotice.Add(model);
            basedb.SaveChanges();
        }
        // 修改AdminNotice
        public void Modify(DbContent.AdminNotice model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.AdminNotice.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.PushLog = model.PushLog;
                    data.Activity = model.Activity;
                    data.ContactUs = model.ContactUs;
                    data.Status = model.Status;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }
        // 刪除AdminNotice
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.AdminNotice.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.AdminNotice.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
        //取得 收件人管理通知寄送列表 type : 1 联络我们 2 活动报名 3 推播结果通知
        public string GetAdminMail(string type)
        {
            var maillist = new List<string>();
            var mail = basedb.AdminNotice.Where(p => p.Status == true);
            if (type == "1")
            {
                mail = mail.Where(p => p.ContactUs == true);
            }
            if (type == "2")
            {
                mail = mail.Where(p => p.Activity == true);
            }
            if (type == "3")
            {
                mail = mail.Where(p => p.PushLog == true);
            }
            foreach (var m in mail.ToList())
            {
                maillist.Add(m.Email);
            }
            return String.Join(",", maillist);
        }
        //寄送 收件人管理通知寄送 type : 1 联络我们 2 活动报名 3 推播结果通知
        public void SendNotice(string type)
        {
            string msg = "";
            string title = "巨石后台通知";
            if (type == "1")
            {
                msg = msg + " - 联络我们";
            }
            if (type == "2")
            {
                msg = msg + " - 活动报名";
            }
            if (type == "3")
            {
                msg = msg + " - 推播结果通知";
            }

        }

        //寄送 type : 1 联络我们
        public void SendContactUs(DbContent.ContactUs model)
        {
            OptionService opsr = new OptionService();
            var ContactType = opsr.GetListByCode("Contact");
            var TimeType = opsr.GetListByCode("Time");

            string title = "巨石后台通知 - 联络我们";
            var Html = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/notice_contactUs.html");
            Html = Html.Replace("{CreateDate}", model.CreateDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            Html = Html.Replace("{Name}", model.Name);
            Html = Html.Replace("{Subject}", OptionService.GetOptName(ContactType, model.Subject));
            Html = Html.Replace("{ContactTime}", OptionService.GetOptName(TimeType, model.ContactTime));
            Html = Html.Replace("{Email}", model.Email);
            Html = Html.Replace("{Phone}", model.Phone);
            if (!string.IsNullOrEmpty(model.Detail))
            {
                Html = Html.Replace("{Detail}", model.Detail.Replace("\n", "<br/>"));
            }
            else
            {
                Html = Html.Replace("{Detail}","");
            }
            
            var mailto = GetAdminMail("1");
            MailLib.SendMail(Html, title, mailto);
        }
        //寄送 type : 2 活动报名
        public void SendActSign(SignPost model,DateTime singDate)
        {
            OptionService opsr = new OptionService();
            var ContactType = opsr.GetListByCode("Contact");
            var TimeType = opsr.GetListByCode("Time");

            string title = "巨石后台通知 - 活动报名";
            var Html = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/notice_actSign.html");
            Html = Html.Replace("{CreateDate}", singDate.ToString("yyyy/MM/dd HH:mm:ss"));
            Html = Html.Replace("{NAME}", model.Name);
            Html = Html.Replace("{ActName}", model.ActName);
            string time =
                model.ActDate.ToString("yyyy/MM/dd") +
                " (" + DateLib.DateToWeekName(model.ActDate) + ") " +
                model.ActDate.ToString("HH:mm");
            Html = Html.Replace("{Time}", time);
            Html = Html.Replace("{Phone}", model.Prefix + " " + model.Phone);
            Html = Html.Replace("{Addr}", model.ActAddr);
            Html = Html.Replace("{Count}", model.Count.ToString());
            if (!string.IsNullOrEmpty(model.Note))
            {
                Html = Html.Replace("{Note}", model.Note.Replace("\n", "<br/>"));
            }
            else
            {
                Html = Html.Replace("{Note}", "");
            }
            
            var mailto = GetAdminMail("2");
            MailLib.SendMail(Html, title, mailto);
        }
        //寄送 type : 3 推播结果通知
        public void SendPushNotice(int PushLogID, bool success)
        {
            OptionService opsr = new OptionService();
            var data = basedb.PushLog.Where(p => p.ID == PushLogID).FirstOrDefault();
            var pushType = opsr.GetListByCode("Noitce");
            string result = (success) ? "发送成功" : "发送失败";
            string tagetName = (data.TargetType == "1") ? "所有用户" : ((data.TargetType == "2") ? "会员用户" : "自订");
            if (data != null)
            {
                string title = "巨石后台通知 - 推播结果通知";
                var Html = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/notice_push.html");
                Html = Html.Replace("{Time}", data.CreateDate.ToString("yyyy/MM/dd HH:mm:ss"));
                Html = Html.Replace("{Type}", OptionService.GetOptName(pushType, data.PType));
                Html = Html.Replace("{Result}", result);
                Html = Html.Replace("{Target}", tagetName);
                Html = Html.Replace("{Txt}", data.Detail);
                var mailto = GetAdminMail("3");
                MailLib.SendMail(Html, title, mailto);
            }
        }
    }
}