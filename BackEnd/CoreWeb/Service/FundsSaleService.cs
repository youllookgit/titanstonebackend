﻿using CoreWeb.Lib;
using CoreWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FundsSaleService: Service.BaseService
    {
        private FundsDetailService fundsDetailSrv = new FundsDetailService();

        // 查詢FundsSale
        public List<FundsSaleModel> GetList(string keyword, string fundType, string buyType, string startDate, string endDate,double? sum, double? sumlitmit)
        {
            var data = basedb.FundsSale.Where(p => p.ID > 0);
            //if (!string.IsNullOrEmpty(keyword))
            //{
            //    data = data.Where(
            //        p => p.FundsNo.Contains(keyword) || 
            //        p.UserIdentityNo.Contains(keyword) || 
            //        p.BuyerName.Contains(keyword) ||
            //        p.FundsName.Contains(keyword) ||
            //        p.Channel != null
            //   );
            //}
            if (!string.IsNullOrEmpty(fundType))
            {
                data = data.Where(p => p.FundType == fundType);
            }
            if (!string.IsNullOrEmpty(buyType))
            {
                data = data.Where(p => p.BuyType == buyType);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            List<FundsSaleModel> list = new List<FundsSaleModel>();
            foreach (var item in data)
            {
                // 累積申購金額
                var purchaseTotal = this.fundsDetailSrv.FundsSaleSum(item.ID);
                FundsSaleModel model = new FundsSaleModel()
                {
                    ID = item.ID,
                    FundsNo = item.FundsNo,
                    UserIdentityNo = item.UserIdentityNo,
                    Channel = item.Channel,
                    BuyerName = item.BuyerName,
                    FundsID = item.FundsID,
                    FundsName = item.FundsName,
                    FundType = item.FundType,
                    BuyType = item.BuyType,
                    Note = item.Note,
                    CreateDate = item.CreateDate,
                    PurchaseTotal = purchaseTotal
                };
                if (!string.IsNullOrEmpty(item.Channel))
                {
                    var clist = item.Channel.Split(',');
                    ChannelService cs = new ChannelService();
                    var cdata = cs.GetChannelName(clist);
                    model.ChannelAct = String.Join(",", cdata.Select(p => p.Act));
                    model.ChannelName = String.Join(",", cdata.Select(p => p.Name));
                }
                else
                {
                    model.ChannelAct = "";
                    model.ChannelName = "";
                }
                list.Add(model);
            }
            //keyword塞選條件 調整位置
            if (!string.IsNullOrEmpty(keyword))
            {
                list = list.Where(
                    p => p.FundsNo.Contains(keyword) ||
                    p.UserIdentityNo.Contains(keyword) ||
                    p.BuyerName.Contains(keyword) ||
                    p.FundsName.Contains(keyword) ||
                    p.ChannelAct.Contains(keyword) ||
                    p.ChannelName.Contains(keyword)                    
               ).ToList();
            };
            

            ////回補Channel篩選
            //list = list.Where(p => p.ChannelAct.Contains(keyword) ||
            //                p.ChannelName.Contains(keyword)).ToList();

            if (sum != null)
            {
                list = list.Where(p => p.PurchaseTotal >= sum).ToList();
            }
            
            if (sumlitmit != null)
            {
                list = list.Where(p => p.PurchaseTotal <= sumlitmit).ToList();
            }


            //若為登入身分為渠道 在過濾一次
            string _type = AdminService.GetUserType();
            DbContent.Channel cuser = null;
            if (_type == "channel")
            {
                cuser = (DbContent.Channel)AdminService.GetUserData();
                string channelid = cuser.ID.ToString();
                List<FundsSaleModel> filter = new List<FundsSaleModel>();
                foreach (var uitem in list)
                {
                    if (!string.IsNullOrEmpty(uitem.Channel))
                    {
                        var carray = uitem.Channel.Split(',');
                        if (carray.Contains(channelid))
                        {
                            filter.Add(uitem);
                        }
                    }
                }
                return filter;
            }


            return list;
        }

        // 新增FundsSale
        public void Create(DbContent.FundsSale model)
        {
            // 檢查資料設定單號
            var data = basedb.FundsSale.Where(p => p.FundsNo == model.FundsNo).FirstOrDefault();
            if (data == null)
            {
                // 處理數據
                model.CreateDate = DateTime.Now;
                // 新增資料
                basedb.FundsSale.Add(model);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Assigning Funds No. Duplicated");
            }
        }

        // 修改FundsSale
        public void Modify(DbContent.FundsSale model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.FundsSale.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.UserIdentityNo = model.UserIdentityNo;
                    data.Channel = model.Channel;
                    data.BuyerName = model.BuyerName;
                    data.BuyType = model.BuyType;
                    data.Note = model.Note;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除FundsSale
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.FundsSale.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.FundsSale.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void Import(List<ImportModel.FundSale> data)
        {
            //檢查內容是否有重複物件編號
            var Nos = data.Select(p => p.FundsNo).ToList();
            var groups = Nos.GroupBy(v => v);
            foreach (var group in groups)
            {
                var k = group.Key;
                if (string.IsNullOrEmpty(k))
                    throw new Exception("汇入基金单号不可为空白");
                var c = group.Count();
                if (c > 1)
                    throw new Exception("已有重复基金单号");
            }

            var FundList = basedb.Funds.Where(p => p.Status == true).ToList();
            var IDs = basedb.FundsSale.Where(p => p.ID > 0).Select(p => p.FundsNo).ToList();

            var ChannelList = basedb.Channel.ToList();
            int importCnt = 0;

            if (data.Count() == 0)
            {
                throw new Exception("汇入资料为空");
            }

            foreach (var item in data)
            {
                if (IDs.Contains(item.FundsNo))
                    throw new Exception("已有重复基金单号");

                if (
                    string.IsNullOrEmpty(item.UserIdentityNo) ||
                    string.IsNullOrEmpty(item.FundsName) ||
                    string.IsNullOrEmpty(item.BuyerName)
                    )
                {
                    throw new Exception("汇入格式错误");
                }

                DbContent.FundsSale _save = new DbContent.FundsSale()
                {
                    FundsNo = item.FundsNo,
                    UserIdentityNo = item.UserIdentityNo,
                    FundsName = item.FundsName,
                    BuyerName = item.BuyerName,
                    BuyType = item.BuyType,
                    Note = item.Note,

                    FundsID = 0,
                    FundType = "",
                    Channel = "",
                    CreateDate = DateTime.Now
                };

                string DataName = item.FundsName;
                // 查詢EstateObj
                var _BindData = FundList.Where(p => p.Name == item.FundsName).FirstOrDefault();
                if (_BindData != null)
                {
                    _save.FundsID = _BindData.ID;
                    _save.FundType = _BindData.FundType;
                }
                else
                {
                    throw new Exception("错误基金名称");
                }

                if (!string.IsNullOrEmpty(item.Channel))
                {
                    string _channel = "";
                    var cArray = item.Channel.Split(',');

                    foreach (var cdata in cArray)
                    {
                        var isChannel = ChannelList.Where(p => p.Name == cdata).FirstOrDefault();
                        if (isChannel != null)
                            _channel = isChannel.ID.ToString() + ",";
                    }
                    _save.Channel = _channel.TrimEnd(',');
                }

                basedb.FundsSale.Add(_save);
                importCnt++;
            }
            if (importCnt > 0)
                basedb.SaveChanges();
        }
    }
}