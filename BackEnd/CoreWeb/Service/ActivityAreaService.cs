﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class ActivityAreaService : Service.BaseService
    {
        //取得列表
        public List<DbContent.Option> GetActivityArea(string keyword, bool? enabled)
        {
            var data = basedb.Option.Where(p => p.ID > 0 && p.Code == "ActArea");
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where( p =>                 
                    p.Name.Contains(keyword)
               );
            }

            if (enabled != null)
            {
                data = data.Where(p => p.Enabled == enabled);
            }
					
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        //新增Option
        public void Create(DbContent.Option model)
        {
            if (model.ID > 0)
            {
                //修改Option
                var data = basedb.Option.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.Name = model.Name;
                    data.NameEn = model.NameEn;
                    data.Enabled = model.Enabled;
                    data.OrderNo = model.OrderNo;                    
                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增Option
                //驗證是否重複標題
                var isExist = basedb.Option.Where(p => p.Name.Contains(model.Name) && p.Code.Contains("ActArea")).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Name Exist");
                }

                //參數處理
                model.CreateDate = DateTime.Now;
                model.Code = "ActArea";
                model.Enabled = true;
                model.Name = model.Name;
                model.NameEn = model.NameEn;
                model.OptionValue = "";
                basedb.Option.Add(model);
                basedb.SaveChanges();
                model.OptionValue = model.ID.ToString();
                basedb.SaveChanges();
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.Option.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.Option.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}
