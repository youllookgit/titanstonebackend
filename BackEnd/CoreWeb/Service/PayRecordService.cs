﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ExcelDataReader;
using CoreWeb.Models;
namespace CoreWeb.Service
{
    public class PayRecordService: Service.BaseService
    {
        // 查詢PayRecord
        public List<DbContent.PayRecord> GetList(string keyword, string startDate, string endDate, string type, int? EstateObjID, int? EstateSaleID)
        {
            var data = basedb.PayRecord.Where(p => p.ID > 0);
            if (EstateObjID != null)
            {
                data = data.Where(p => p.EstateObjID == EstateObjID);
            }
            if (EstateSaleID != null)
            {
                data = data.Where(p => p.EstateSaleID == EstateSaleID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                
            }
            if (!string.IsNullOrEmpty(type))
            {
                data = data.Where(p => p.PayStatus == type);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            return data.ToList();
        }

        // 新增PayRecord
        public void Create(DbContent.PayRecord model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.PayRecord.Add(model);
            basedb.SaveChanges();
        }

        // 修改PayRecord
        public void Modify(DbContent.PayRecord model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.PayRecord.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.EstateScheduleID = model.EstateScheduleID;
                    data.Amount = model.Amount;
                    data.PayStatus = model.PayStatus;
                    data.Note = model.Note;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除PayRecord
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.PayRecord.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.PayRecord.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void Import(int EstateObjID, int EstateSaleID,List<ImportModel.PayRecordData> data)
        {
            if (EstateObjID <= 0 || EstateSaleID <= 0 || data == null || data.Count() == 0)
            {
                throw new Exception("汇入格式不正确");
            }
            //防呆
            var ScheduleList = basedb.EstateSchedule.Where(p => p.EstateObjID == EstateObjID).ToList();
            foreach (var item in data)
            {
                DbContent.PayRecord model = new DbContent.PayRecord
                {
                    EstateObjID = EstateObjID,
                    EstateSaleID = EstateSaleID,
                    EstateScheduleID = 0,
                    CreateDate = DateTime.Now,
                    Amount = 0,
                    PayStatus = "1",
                    Note = item.Note
                };

                var isExist = ScheduleList.Where(p => p.SchNo == item.SchNo).FirstOrDefault();
                if (isExist != null)
                {
                    model.EstateScheduleID = isExist.ID;
                }
                else
                    throw new Exception("查无对应工期");

                if (item.Amount >= 0)
                {
                    model.Amount = item.Amount;
                }
                else
                    throw new Exception("金额错误");

                if (item.PayStatus == "1" || item.PayStatus == "2")
                {
                    model.PayStatus = item.PayStatus;
                }
                else
                    throw new Exception("缴款状态错误");

                basedb.PayRecord.Add(model);
            }

            basedb.SaveChanges();
        }
    }
}