﻿using CoreWeb.Lib;
using CoreWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FaqSortService : Service.BaseService
    {
        //取得列表
        public List<DbContent.FaqSort> GetFaqSort(string keyword, bool? status)
        {
            var data = basedb.FaqSort.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword)
               );
            }

            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        //新增FaqSort
        public void Create(DbContent.FaqSort model)
        {
            if (model.ID > 0)
            {
                //修改Admin
                var data = basedb.FaqSort.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.ImageUrl = FileLib.ImgPathToUrl(model.ImageUrl);
                    data.Name = model.Name;
                    data.NameEn = model.NameEn;
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;
                    
                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //參數處理
                model.CreateDate = DateTime.Now;
                model.ImageUrl = FileLib.ImgPathToUrl(model.ImageUrl);
                //加入新增資料
                basedb.FaqSort.Add(model);
                //存檔
                basedb.SaveChanges();
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.FaqSort.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.FaqSort.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}
