﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class ChannelService : Service.BaseService
    {
        public SysPrmisnService syserv = new SysPrmisnService();
        //取得列表
        public List<ChannelEditModel> GetChannel(string keyword, string location, bool? status)
        {
            var data = basedb.Channel.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Act.Contains(keyword) ||
                    p.Name.Contains(keyword) ||
                    p.Lacation.Contains(keyword) ||
                    p.Unit.Contains(keyword) ||
                    p.Email.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(location))
            {
                data = data.Where(p => p.Lacation == location);
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            var datalist = data.ToList();

            List<ChannelEditModel> model = new List<ChannelEditModel>();
            foreach (var d in datalist)
            {

                ChannelEditModel item = new ChannelEditModel()
                {
                    ID = d.ID,
                    Act = d.Act,
                    Name = d.Name,
                    Lacation = d.Lacation,
                    Unit = d.Unit,
                    Phone = d.Phone,
                    Email = d.Email,
                    Status = d.Status.Value,
                    CreateDate = d.CreateDate
                };
                item.Pwd = Security.Decrypt(d.Pwd);
                //item.Permission = syserv.GetPermission("Channel", d.ID);
                model.Add(item);
            }
            return model;
        }

        //新增
        public void Create(ChannelEditModel model)
        {
            if (model.ID > 0)
            {
                //修改Channel
                var data = basedb.Channel.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.Act = model.Act;
                    data.Name = model.Name;
                    if (!string.IsNullOrEmpty(model.Pwd))
                    {
                        data.Pwd = Security.Encrypt(model.Pwd);
                    }
                    data.Lacation = model.Lacation;
                    data.Unit = model.Unit;
                    data.Phone = model.Phone;
                    data.Email = model.Email;
                    data.Status = model.Status;
                    //直接存檔
                    basedb.SaveChanges();
                    //儲存權限
                    syserv.SavePermission("Channel", data.ID, model.Permission);
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增Channel
                //驗證是否重複帳號
                var isExist = basedb.Channel.Where(p => p.Act == model.Act).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Account Exist");
                }
                //參數處理
                DbContent.Channel data = new DbContent.Channel()
                {
                    Act = model.Act,
                    Name = model.Name,
                    Email = model.Email,
                    Unit = model.Unit,
                    Lacation = model.Lacation,
                    Phone = model.Phone,
                    Status = model.Status,
                    CreateDate = DateTime.Now
                };
                data.Pwd = Security.Encrypt(model.Pwd);
                data.CreateDate = DateTime.Now;
                //加入新增資料
                basedb.Channel.Add(data);
                //存檔
                basedb.SaveChanges();
                //儲存權限
                syserv.SavePermission("Channel", data.ID, model.Permission);
            }


        }

        //刪除
        public void Remove(int id)
        {
            var data = basedb.Channel.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.Channel.Remove(data);
                //直接存檔
                basedb.SaveChanges();
                //刪除權限
                syserv.DeletePermission("Channel", data.ID);
            }
            else
            {
                throw new Exception("Not Found");
            }
        }

        public List<DbContent.Channel> GetChannelName(string[] ChannelList)
        {
            List<DbContent.Channel> model = new List<DbContent.Channel>();
            foreach (var str in ChannelList)
            {
                int _id = 0;
                if (int.TryParse(str,out _id))
                {
                   var c = basedb.Channel.Where(p => p.ID == _id).FirstOrDefault();
                    if (c != null)
                    {
                        model.Add(c);
                    }
                }
            }
            return model;
        }
    }
}