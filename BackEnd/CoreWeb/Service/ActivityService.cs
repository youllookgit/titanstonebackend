﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class ActivityService : Service.BaseService
    {
        //取得列表
        public List<DbContent.Activity> GetActivity(string keyword, bool? status, string acttypeid, string actareaid, string estateObjID)
        {
            var data = basedb.Activity.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword) ||
                    p.EstateObjIDs.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(estateObjID))
            {
                data = data.ToList().FindAll(x => {
                    return !string.IsNullOrEmpty(x.EstateObjIDs) && x.EstateObjIDs.Split(',').Contains(estateObjID);
                }).AsQueryable();
            }
            if (string.IsNullOrEmpty(acttypeid) == false)
            {
                data = data.Where(
                    p => p.ActType.Contains(acttypeid));
            }

            if (string.IsNullOrEmpty(actareaid) == false)
            {
                data = data.Where(
                    p => p.ActArea.Contains(actareaid));
            }


            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
			
            data = data.OrderByDescending(p => p.ActDate);
            return data.ToList();
        }

        public string GetActTypeName(string ActType)
        {
            string ActTypeName = string.Empty;

            OptionLib.ActTypeDic.TryGetValue(ActType, out ActTypeName);

            return ActTypeName;
        }

        public string GetActAreaName(string ActAreaValue)
        {
            var data = basedb.Option.Where(p => p.Code == "ActArea" && p.OptionValue.Contains(ActAreaValue)).FirstOrDefault();
            return (data == null) ? "" : data.Name;            
        }

        public List<DbContent.Option> GetActArea()
        {
            var data = basedb.Option.Where(p => p.ID > 0 && p.Code.Contains("ActArea") && p.Enabled == true);

            data = data.OrderByDescending(p => p.CreateDate);

            return data.ToList();
        }

        //新增Activity
        public void Create(DbContent.Activity model)
        {
            if (model.ID > 0)
            {
                //修改Activity
                var data = basedb.Activity.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.ActAddr = model.ActAddr;
                    data.ActAddrEn = model.ActAddrEn;
                    data.ActArea = model.ActArea;
                    data.ActDate = model.ActDate;
                    data.ActPosition = model.ActPosition;
                    data.ActPositionEn = model.ActPositionEn;
                    data.ActType = model.ActType;
                    data.BeginDate = model.BeginDate;
                    data.Brief = model.Brief;
                    data.BriefEn = model.BriefEn;
                    data.Detail = FileLib.ImgPathToSQL(model.Detail);
                    data.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                    data.Process = FileLib.ImgPathToSQL(model.Process);
                    data.ProcessEn = FileLib.ImgPathToSQL(model.ProcessEn);
                    data.Traffic = FileLib.ImgPathToSQL(model.Traffic);
                    data.TrafficEn = FileLib.ImgPathToSQL(model.TrafficEn);
                    data.EndDate = model.EndDate;
                    data.EstateObjIDs = model.EstateObjIDs;
                    data.Img = FileLib.ImgPathToSQL(model.Img);
                    data.Name = model.Name;
                    data.NameEn = model.NameEn;
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;

                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增Activity
                //驗證是否活動名稱
                var isExist = basedb.Activity.Where(p => p.Name == model.Name).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Name Exist");
                }

                //參數處理
                model.CreateDate = DateTime.Now;
                model.Detail = FileLib.ImgPathToSQL(model.Detail);
                model.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                model.Img = FileLib.ImgPathToSQL(model.Img);

                model.Process = FileLib.ImgPathToSQL(model.Process);
                model.ProcessEn = FileLib.ImgPathToSQL(model.ProcessEn);
                model.Traffic = FileLib.ImgPathToSQL(model.Traffic);
                model.TrafficEn = FileLib.ImgPathToSQL(model.TrafficEn);
                //加入新增資料
                basedb.Activity.Add(model);
                //存檔
                basedb.SaveChanges();

                if (model.Status)
                {
                    //發理财趋势推播
                    PushService psr = new PushService();
                    psr.SeminarPush(model.ID);
                }
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.Activity.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.Activity.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}
