﻿using CoreWeb.Lib;
using CoreWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class EmailLogService : Service.BaseService
    {
        //取得列表
        public List<DbContent.EmailLog> GetEmailLog(string keyword, string startDate, string endDate, bool? status, string type)
        {
            var data = basedb.EmailLog.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Email.Contains(keyword) 
               );
            }
            if (!string.IsNullOrEmpty(type))
            {
                data = data.Where(p => p.TypeCode == type);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CheckDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CheckDate < eDate);
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        // 刪除EmailLog
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.EmailLog.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.EmailLog.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}
