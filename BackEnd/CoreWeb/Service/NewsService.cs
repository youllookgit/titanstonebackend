﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class NewsService : Service.BaseService
    {
        //取得列表
        public List<DbContent.News> GetNews(string keyword,  bool? status, DateTime? publicstartdate, DateTime? publicenddate, DateTime? beginstartdate, DateTime? beginenddate, DateTime? startdate, DateTime? enddate)
        {
            var data = basedb.News.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Title.Contains(keyword)
               );
            }

            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            if (publicstartdate != null)
            {
                data = data.Where(p => p.PublicDate >= publicstartdate);
            }

            if (publicenddate != null)
            {
                data = data.Where(p => p.PublicDate <= publicenddate);
            }

            if (beginstartdate != null)
            {
                data = data.Where(p => p.BeginDate >= beginstartdate);
            }

            if (beginenddate != null)
            {
                data = data.Where(p => p.BeginDate <= beginenddate);
            }

            if (startdate != null)
            {
                data = data.Where(p => p.CreateDate >= startdate);
            }

            if (enddate != null)
            {
                data = data.Where(p => p.CreateDate <= enddate);
            }
            
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        //新增News
        public void Create(DbContent.News model)
        {
            if (model.ID > 0)
            {
                //修改News
                var data = basedb.News.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.Title = model.Title;
                    data.TitleEn = model.TitleEn;
                    data.PublicDate = model.PublicDate;
                    data.Brief = model.Brief;
                    data.BriefEn = model.BriefEn;
                    data.ImageUrl = FileLib.ImgPathToSQL(model.ImageUrl);
                    data.Detail = FileLib.ImgPathToSQL(model.Detail);
                    data.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                    data.BeginDate = model.BeginDate;
                    data.EndDate = model.EndDate;
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;
                    data.UpdDate = DateTime.Now;
                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增News
                //驗證是否重複標題
                var isExist = basedb.News.Where(p => p.Title == model.Title).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Title Exist");
                }

                //參數處理
                model.CreateDate = DateTime.Now;
                model.UpdDate = DateTime.Now;
                model.ImageUrl = FileLib.ImgPathToSQL(model.ImageUrl);
                model.Detail = FileLib.ImgPathToSQL(model.Detail);
                model.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                //加入新增資料
                basedb.News.Add(model);
                //存檔
                basedb.SaveChanges();

                if (model.Status)
                {
                    //發官方消息推播
                    PushService psr = new PushService();
                    psr.NewsPush(model.ID);
                }
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.News.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.News.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}
