﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class RentDataService: Service.BaseService
    {
        // 查詢RentData
        public List<DbContent.RentData> GetList(string startDate, string endDate, int? EstateSaleID, int? CharterDataID)
        {
            var data = basedb.RentData.Where(p => p.ID > 0);
            if (EstateSaleID != null && CharterDataID != null)
            {
                data = data.Where(p => p.EstateSaleID == EstateSaleID & p.CharterDataID == CharterDataID);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.RentDeal = item.RentDeal != null ? FileLib.ImgPathToUrl(item.RentDeal) : item.RentDeal;
            });
            return list;
        }

        // 新增RentData
        public void Create(DbContent.RentData model)
        {
            // 處理數據
            model.RentDeal = model.RentDeal != null ? FileLib.ImgPathToSQL(model.RentDeal) : model.RentDeal;
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.RentData.Add(model);
            basedb.SaveChanges();
        }

        // 修改RentData
        public void Modify(DbContent.RentData model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.RentData.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.RentStatus = model.RentStatus;
                    data.RentStart = model.RentStart;
                    data.RentEnd = model.RentEnd;
                    data.RenterName = model.RenterName;
                    data.RenterGender = model.RenterGender;
                    data.RenterIdNo = model.RenterIdNo;
                    data.RenterBirth = model.RenterBirth;
                    data.RentDeal = model.RentDeal != null ? FileLib.ImgPathToSQL(model.RentDeal) : model.RentDeal;
                    data.RentAmount = model.RentAmount;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除RentData
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.RentData.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.RentData.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        // 查詢最新一筆有效租客
        public DbContent.RentData GetLatestRenter(int EstateSaleID) 
        {
            var data = basedb.RentData.Where(
                p => p.ID > 0 && 
                p.EstateSaleID == EstateSaleID && 
                p.RentStatus == "1"
            );
            data = data.OrderByDescending(p => p.RentEnd);
            return data.FirstOrDefault();
        }
    }
}