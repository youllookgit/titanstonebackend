﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class InsuranceService : Service.BaseService
    {
        // 查詢保險商品
        public List<DbContent.Insurance> GetList(string keyword, bool? status)
        {
            var data = basedb.Insurance.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(p =>
                p.Name.Contains(keyword) ||
                p.TypeName.Contains(keyword));
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }
        // 新增保險商品
        public void Create(DbContent.Insurance model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.Insurance.Add(model);
            basedb.SaveChanges();
        }
        // 修改保險商品
        public void Modify(DbContent.Insurance model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.Insurance.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Name = model.Name;
                    data.NameEn = model.NameEn;
                    data.TypeName = model.TypeName;
                    data.TypeNameEn = model.TypeNameEn;
                    data.Status = model.Status;
                    data.Note = model.Note;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }
        // 刪除保險商品
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.Insurance.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.Insurance.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}