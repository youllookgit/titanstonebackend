﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
using CoreWeb.Lib;
using CoreWeb.Service;
namespace CoreWeb.Service
{
    public class AdminService : Service.BaseService
    {
        public SysPrmisnService syserv = new SysPrmisnService();
        //Login
        public LoginData LogIn(Login data)
        {
            LoginData model;
            string pwdEncode = Lib.Security.Encrypt(data.Pwd);            
            if (data.Type == "admin")
            {
                var entity = basedb.Admin.Where(p => p.Status == true
                && p.Act == data.Act
                && p.Pwd == pwdEncode).FirstOrDefault();
                if (entity != null)
                {
                    string token = Lib.Security.Encrypt((entity.ID.ToString() + entity.Pwd));
                    model = new LoginData()
                    {
                        Act = entity.Act,
                        Name = entity.Name,
                        Token = token,
                        LoginType = "admin"
                    };
                    model.Permission = syserv.GetPermission("Admin", entity.ID);
                    //set Token
                    entity.Token = token;
                    basedb.SaveChanges();
                    
                }
                else
                    throw new Exception("查无该使用者");

                return model;
            }
            else
            {
                var entity = basedb.Channel.Where(p => p.Status == true
                && p.Act == data.Act
                && p.Pwd == pwdEncode).FirstOrDefault();
                if (entity != null)
                {
                    string token = Lib.Security.Encrypt((entity.ID.ToString() + entity.Pwd));
                    model = new LoginData()
                    {
                        Act = entity.Act,
                        Name = entity.Name,
                        Token = token,
                        LoginType = "channel"
                    };
                    model.Permission = syserv.GetPermission("Channel", entity.ID);
                    entity.Token = token;
                    basedb.SaveChanges();
                }
                else
                    throw new Exception("查无该使用者");

                return model;
            }
        }
        //取得列表
        public List<AdminModel> GetAdmin(string keyword,bool? status)
        {
            var data = basedb.Admin.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Act.Contains(keyword) ||
                    p.Name.Contains(keyword)
               );
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status); ;
            }
            data = data.OrderByDescending(p => p.CreateDate);
            var datalist = data.ToList();

            List<AdminModel> model = new List<AdminModel>();
            foreach (var d in datalist)
            {
                AdminModel item = new AdminModel()
                {
                    ID = d.ID,
                    Act = d.Act,
                    Status = d.Status,
                    Name = d.Name,
                    CreateDate = d.CreateDate
                };
                string pwd = "";
                item.Pwd = Security.Decrypt(d.Pwd);
                //item.Permission = syserv.GetPermission("Admin", d.ID);

                model.Add(item);
            }
            return model;
        }
        //新增Admin
        public void Create(AdminModel model)
        {

            if (model.ID > 0)
            {
                //修改前都先檢查是否重複帳號
                //驗證是否重複帳號
                var isExist = basedb.Admin.Where(p => p.ID != model.ID && p.Act == model.Act).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Account Exist");
                }

                //修改Admin
                var data = basedb.Admin.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.Act = model.Act;
                    data.Name = model.Name;
                    if (!string.IsNullOrEmpty(model.Pwd))
                        data.Pwd = Security.Encrypt(model.Pwd);
                    data.Status = model.Status;
                    //存檔
                    basedb.SaveChanges();
                    //儲存權限
                    syserv.SavePermission("Admin",data.ID,model.Permission);
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增Admin
                //驗證是否重複帳號
                var isExist = basedb.Admin.Where(p => p.Act == model.Act).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Account Exist");
                }
                //參數處理
                DbContent.Admin data = new DbContent.Admin()
                {
                    Act = model.Act,
                    Name = model.Name,
                    Status = model.Status,
                    CreateDate = DateTime.Now
                };
                data.Pwd = Security.Encrypt(model.Pwd);
                //加入新增資料
                basedb.Admin.Add(data);
                //存檔
                basedb.SaveChanges();
                //儲存權限
                syserv.SavePermission("Admin", data.ID, model.Permission);
            }

            
        }
        //刪除Admin
        public void Remove(int id)
        {
            var data = basedb.Admin.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.Admin.Remove(data);
                //直接存檔
                basedb.SaveChanges();
                //刪除權限
                syserv.DeletePermission("Admin", data.ID);
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
        //驗證管理者
        public object CheckManager(string token,string loginType)
        {
            if (loginType == "admin")
            {
                var user = basedb.Admin.Where(p => p.Status == true && p.Token == token).FirstOrDefault();
                return user;
            }
            else
            {
                var user = basedb.Channel.Where(p => p.Status == true && p.Token == token).FirstOrDefault();
                return user;
            }
        }
        //設定登入使用者
        public static void SetAdmin(object manager,string loginType)
        {
            HttpContext.Current.Session["LoginType"] = loginType;
            HttpContext.Current.Session["TokenUser"] = manager;
        }
        //取得使用者登入類別
        public static string GetUserType()
        {
            if (HttpContext.Current.Session["LoginType"] != null)
            {
                return (string)HttpContext.Current.Session["LoginType"];
            }
            else
            {
                return null;
            }
        }
        public static object GetUserData()
        {
            if (HttpContext.Current.Session["TokenUser"] != null)
            {
                return (object)HttpContext.Current.Session["TokenUser"];
            }
            else
            {
                return null;
            }
        }
    }
}