﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Models;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class SysPrmisnService : Service.BaseService
    {
        //預設值
        public List<SysPermissionItem> GetDefault()
        {
            List<SysPermissionItem> model = new List<SysPermissionItem>();
            var keys = OptionLib.PermissionDic.Keys.ToArray();
            foreach (var key in keys)
            {
                var item = new SysPermissionItem()
                {
                    FuncCode = key,
                    Name = OptionLib.PermissionDic[key],
                    IsAdd = false,
                    IsEdit = false,
                    IsDelete = false,
                    IsView = false
                };
                model.Add(item);
            }
            return model;
        }
        //預設值
        public List<SysPermissionItem> GetChannelDefault()
        {
            List<SysPermissionItem> model = new List<SysPermissionItem>();
            var keys = OptionLib.ChannelPermissionDic.Keys.ToArray();
            foreach (var key in keys)
            {
                var item = new SysPermissionItem()
                {
                    FuncCode = key,
                    Name = OptionLib.PermissionDic[key],
                    IsAdd = false,
                    IsEdit = false,
                    IsDelete = false,
                    IsView = false
                };
                model.Add(item);
            }
            return model;
        }
        //取得列表
        public List<SysPermissionItem> GetPermission(string PType,int UserID)
        {
            var list = basedb.SysPermission.Where(p => p.SType == PType && p.RelationID == UserID).OrderBy(p => p.FuncCode).ToList();

            var defaults = GetDefault();
            if (PType == "Channel")
            {
                defaults = GetChannelDefault();
            }
            if (list.Count() == 0)
            {
                return defaults;
            }
            else
            {
                List<SysPermissionItem> model = new List<SysPermissionItem>();
                
                foreach (var item in defaults)
                {
                    var data = list.Where(p => p.FuncCode == item.FuncCode).FirstOrDefault();
                    var set = new SysPermissionItem()
                    {
                        FuncCode = item.FuncCode,
                        Name = OptionLib.PermissionDic[item.FuncCode],
                        IsAdd = false,
                        IsEdit = false,
                        IsDelete = false,
                        IsView = false
                    };
                    if (data != null)
                    {
                        set.IsAdd = data.IsAdd;
                        set.IsEdit = data.IsEdit;
                        set.IsDelete = data.IsDelete;
                        set.IsView = data.IsView;
                    }
                    model.Add(set);
                }
                return model;
            }
        }
        //儲存列表
        public void SavePermission(string PType, int UserID,List<SysPermissionItem> model)
        {
            //Delete All
            string cmd = $"DELETE SysPermission WHERE SType = '{PType}' AND RelationID = {UserID}";
            int DeleteCount = basedb.Database.ExecuteSqlCommand(cmd);
            foreach (var data in model)
            {
                var item = new DbContent.SysPermission()
                {
                    SType = PType,
                    FuncCode = data.FuncCode,
                    RelationID = UserID,
                    IsAdd = data.IsAdd,
                    IsEdit = data.IsEdit,
                    IsDelete = data.IsDelete,
                    IsView = data.IsView
                };
                basedb.SysPermission.Add(item);
                basedb.SaveChanges();
            }
        }
        //儲存列表
        public void DeletePermission(string PType, int UserID)
        {
            //Delete All
            string cmd = $"DELETE SysPermission WHERE SType = '{PType}' AND RelationID = {UserID}";
            int DeleteCount = basedb.Database.ExecuteSqlCommand(cmd);
        }

    }
}
