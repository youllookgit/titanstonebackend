﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Lib;
using System.Web;

namespace CoreWeb.Service
{
    public class ContactUsService: Service.BaseService
    {
        // 查詢ContacUs
        public List<DbContent.ContactUs> GetList(string keyword, string startDate, string endDate, string country, string subject, string status)
        {
            var data = basedb.ContactUs.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword) || 
                    p.Email.Contains(keyword) || 
                    p.Country.Contains(keyword) || 
                    p.Subject.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(country))
            {
                data = data.Where(p => p.Country == country);
            }
            if (!string.IsNullOrEmpty(subject))
            {
                data = data.Where(p => p.Subject == subject);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.ReplyTime >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.ReplyTime < eDate);
            }
            if (!string.IsNullOrEmpty(status))
            {
                data = data.Where(
                    p => p.Status == status
               );
            }
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        // 新增ContactUs
        public void Create(DbContent.ContactUs model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.ContactUs.Add(model);
            basedb.SaveChanges();
        }

        // 修改ContactUs
        public void Modify(DbContent.ContactUs model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.ContactUs.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    if (string.IsNullOrEmpty(model.Reply))
                    {
                        data.Status = model.Status;
                        data.Handler = model.Handler;
                        data.Note = model.Note;
                        basedb.SaveChanges();
                    }
                    else
                    {
                        data.Reply = model.Reply;
                        data.Status = model.Status;
                        data.Handler = model.Handler;
                        data.ReplyTime = DateTime.Now;
                        data.Note = model.Note;
                        basedb.SaveChanges();

                        #region 寄送回複訊息給User
                        var opser = new OptionService();
                        var c = opser.GetByCode("Country", data.Country);
                        var s = opser.GetByCode("Contact", data.Subject);
                        var t = opser.GetByCode("Time", data.ContactTime);

                        string HtmlContent = "";
                        string MailTitle = "联络回覆";

                        string langCode = LangLib.cn;
                        var userData = basedb.User.Where(p => p.Act == data.Email).FirstOrDefault();
                        if (userData != null && !string.IsNullOrEmpty(userData.Lang))
                        {
                            langCode = userData.Lang;
                        }

                        HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/contactUs.html");

                        if (langCode == LangLib.en)
                        {
                            MailTitle = "Titan Stone customer service";
                            HtmlContent = FileLib.ReadFile(FileLib.rootSite + "/Content/mail_template/contactUsEn.html");
                        }
                        if (langCode == LangLib.cn)
                        {
                            HtmlContent = HtmlContent.Replace("{NAME}", data.Name);
                            HtmlContent = HtmlContent.Replace("{CName}", (c == null) ? "" : c.Name);
                            HtmlContent = HtmlContent.Replace("{Phone}", data.Phone);
                            HtmlContent = HtmlContent.Replace("{TypeName}", (s == null) ? "" : s.Name);
                        }
                        else
                        {
                            HtmlContent = HtmlContent.Replace("{NAME}", data.Name);
                            HtmlContent = HtmlContent.Replace("{CName}", (c == null) ? "" : c.NameEn);
                            HtmlContent = HtmlContent.Replace("{Phone}", data.Phone);
                            HtmlContent = HtmlContent.Replace("{TypeName}", (s == null) ? "" : s.NameEn);
                        }
                        

                        //加入建案名稱
                        if (data.Subject == "1")
                        {
                            string typeName = (s == null) ? "" : s.Name;
                            if (langCode == LangLib.en)
                            {
                                typeName = (s == null) ? "" : s.NameEn;
                            }

                            int eid = 0;
                            if (int.TryParse(data.SeletedItem, out eid))
                            {
                                var estateser = new EstateObjService();
                                string eName = estateser.GetNameByID(eid, langCode);
                                typeName = typeName + " " + eName;
                            }
                        }
                        else
                        {
                            HtmlContent = HtmlContent.Replace("{TypeName}", (s == null) ? "" : (langCode == LangLib.cn) ? s.Name : s.NameEn);
                        }
                        HtmlContent = HtmlContent.Replace("{TimeName}", (t == null) ? "" :  (langCode == LangLib.cn) ? t.Name : t.NameEn);
                        if (!string.IsNullOrEmpty(data.Detail))
                        {
                            HtmlContent = HtmlContent.Replace("{Detail}", data.Detail.Replace("\n", "<br/>"));
                        }
                        else
                        {
                            HtmlContent = HtmlContent.Replace("{Detail}","");
                        }
                        if (!string.IsNullOrEmpty(data.Reply))
                        {
                            HtmlContent = HtmlContent.Replace("{Reply}", data.Reply.Replace("\n", "<br/>"));
                        }
                        else
                        {
                            HtmlContent = HtmlContent.Replace("{Reply}","");
                        }

                       
                        MailLib.SendMail(HtmlContent, MailTitle, data.Email);
                        #endregion
                    }

                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除ContactUs
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.ContactUs.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.ContactUs.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}