﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FundsReportService: Service.BaseService
    {
        // 查詢FundsReport
        public List<DbContent.FundsReport> GetList(string keyword, string startDate, string endDate, int? FundsID)
        {
            var data = basedb.FundsReport.Where(p => p.ID > 0);
            if (FundsID != null)
            {
                data = data.Where(p => p.FundsID == FundsID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(p => (p.Year + "/" + p.Month).Contains(keyword));
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            return data.ToList();
        }

        // 新增FundsReport
        public void Create(DbContent.FundsReport model)
        {
            // 檢查資料設定日期
            var reportDate = model.Year + "/" + model.Month;
            var data = basedb.FundsReport.Where(p => (p.Year + "/" + p.Month) == reportDate && p.FundsID == model.FundsID).FirstOrDefault();
            if (data == null)
            {
                // 處理數據
                model.FileUrl.ToSQL();
                model.CreateDate = DateTime.Now;
                // 新增資料
                basedb.FundsReport.Add(model);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("已存在相同区间资料");
            }
        }

        // 修改FundsReport
        public void Modify(DbContent.FundsReport model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.FundsReport.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Year = model.Year;
                    data.Month = model.Month;
                    data.FileUrl = model.FileUrl.ToSQL();
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除FundsReport
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.FundsReport.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.FundsReport.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}