﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class EstateRoomService: Service.BaseService
    {
        // 查詢EstateRoom
        public List<DbContent.EstateRoom> GetList(string keyword, string startDate, string endDate, int? EstateObjID)
        {
            var data = basedb.EstateRoom.Where(p => p.ID > 0);
            if (EstateObjID != null)
            {
                data = data.Where(p => p.EstateObjID == EstateObjID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword) ||
                    p.FloorDesc.Contains(keyword) ||
                    p.Area.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.Img = item.Img != null ? FileLib.ImgPathToUrl(item.Img) : item.Img;
            });
            return list;
        }

        // 新增EstateRoom
        public void Create(DbContent.EstateRoom model)
        {
            // 處理數據
            model.Img = model.Img != null ? FileLib.ImgPathToSQL(model.Img) : model.Img;
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.EstateRoom.Add(model);
            basedb.SaveChanges();
        }

        // 修改EstateRoom
        public void Modify(DbContent.EstateRoom model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.EstateRoom.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Name = model.Name;
                    data.Brief = model.Brief;
                    data.FloorDesc = model.FloorDesc;
                    data.Area = model.Area;
                    data.Img = model.Img != null ? FileLib.ImgPathToSQL(model.Img) : model.Img;
                    data.OrderNo = model.OrderNo;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除EstateRoom
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.EstateRoom.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.EstateRoom.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}