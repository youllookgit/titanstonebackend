﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class EstateScheduleService: Service.BaseService
    {
        // 查詢EstateSchedule
        public List<DbContent.EstateSchedule> GetList(string keyword, int? EstateObjID)
        {
            var data = basedb.EstateSchedule.Where(p => p.ID > 0);
            if (EstateObjID != null)
            {
                data = data.Where(p => p.EstateObjID == EstateObjID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
               // data = data.Where(
               //     // TODO: 其他搜尋欄位
               //     p => p.Name.Contains(keyword) || 
               //     p.Email.Contains(keyword) || 
               //     p.Country.Contains(keyword) || 
               //     p.Subject.Contains(keyword)
               //);
            }
            data = data.OrderByDescending(p => p.CreateDate);
            return data.ToList();
        }

        // 新增EstateSchedule
        public void Create(DbContent.EstateSchedule model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.EstateSchedule.Add(model);
            basedb.SaveChanges();

            var DebugMode = Lib.AppSetting.DebugMode();
            //20200527 調整 正式站移除繳款推播 說繳款規則客戶端異動
            if (DebugMode == false)
            {
                if (model.ScheduleType == "3")
                {
                    //發工程缴款推播
                    PushService psr = new PushService();
                    psr.SchedulePush(model.ID);
                }
            }
        }

        // 修改EstateSchedule
        public void Modify(DbContent.EstateSchedule model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.EstateSchedule.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.SchNo = model.SchNo;
                    data.Name = model.Name;
                    data.Brief = model.Brief;
                    data.SchNoEn = model.SchNoEn;
                    data.NameEn = model.NameEn;
                    data.BriefEn = model.BriefEn;
                    data.ScheduleType = model.ScheduleType;
                    data.CompleteDate = model.CompleteDate;
                    data.RealComplete = model.RealComplete;
                    data.OrderNo = model.OrderNo;
                    basedb.SaveChanges();

                    var DebugMode = Lib.AppSetting.DebugMode();
                    //20200527 調整 正式站移除繳款推播 說繳款規則客戶端異動
                    if (DebugMode == false)
                    {
                        if (model.ScheduleType == "3")
                        {
                            //發工程缴款推播
                            PushService psr = new PushService();
                            psr.SchedulePush(model.ID);
                        }
                    }
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除EstateSchedule
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.EstateSchedule.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.EstateSchedule.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}