﻿using CoreWeb.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class ScheduleRecordService: Service.BaseService
    {
        // 查詢ScheduleRecord
        public List<DbContent.ScheduleRecord> GetList(string keyword, int? EstateObjID, int? EstateSID)
        {

            var data = basedb.ScheduleRecord.Where(p => p.ID > 0);
            if (EstateObjID != null && EstateSID != null)
            {
                data = data.Where(p => p.EstateObjID == EstateObjID & p.EstateSID == EstateSID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
               // data = data.Where(
               //     // TODO: 其他搜尋欄位
               //     p => p.Name.Contains(keyword) || 
               //     p.Email.Contains(keyword) || 
               //     p.Country.Contains(keyword) || 
               //     p.Subject.Contains(keyword)
               //);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.Img = item.Img != null ? FileLib.ImgPathToUrl(item.Img) : item.Img;
            });
            return list;
        }

        // 新增ScheduleRecord
        public void Create(DbContent.ScheduleRecord model)
        {
            // 處理數據
            model.Img = model.Img != null ? FileLib.ImgPathToSQL(model.Img) : model.Img;
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.ScheduleRecord.Add(model);
            basedb.SaveChanges();
        }

        // 修改ScheduleRecord
        public void Modify(DbContent.ScheduleRecord model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.ScheduleRecord.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Img = model.Img != null ? FileLib.ImgPathToSQL(model.Img) : model.Img;
                    data.ImgTitle = model.ImgTitle;
                    data.ImgDesc = model.ImgDesc;
                    data.ImgTitleEn = model.ImgTitleEn;
                    data.ImgDescEn = model.ImgDescEn;
                    data.OrderNo = model.OrderNo;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除ScheduleRecord
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.ScheduleRecord.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.ScheduleRecord.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}