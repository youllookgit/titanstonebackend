﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class EstateObjService: Service.BaseService
    {
        // 查詢EstateObj
        public List<DbContent.EstateObj> GetList(string keyword, bool? status, string startDate, string endDate, string type)
        {
            var data = basedb.EstateObj.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword) ||
                    p.SaleType.Contains(keyword) ||
                    p.Price.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(type))
            {
                data = data.Where(p => p.SaleType == type);
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.MainImg = item.MainImg != null ? FileLib.ImgPathToUrl(item.MainImg) : item.MainImg;
                item.FloorImg = item.FloorImg != null ? FileLib.ImgPathToUrl(item.FloorImg) : item.FloorImg;
                item.CaseSpec = item.CaseSpec != null ? FileLib.ImgPathToUrl(item.CaseSpec) : item.CaseSpec;
                item.FileUrl = item.FileUrl != null ? FileLib.ImgPathToUrl(item.FileUrl) : item.FileUrl;
                item.Around = item.Around != null ? FileLib.ImgPathToUrl(item.Around) : item.Around;
                item.Device = item.Device != null ? FileLib.ImgPathToUrl(item.Device) : item.Device;
                item.Service = item.Service != null ? FileLib.ImgPathToUrl(item.Service) : item.Service;
                item.Album = item.Album != null ? FileLib.ImgPathToUrl(item.Album) : item.Album;
                item.VideoImg = item.VideoImg != null ? FileLib.ImgPathToUrl(item.VideoImg) : item.VideoImg;
                //多語系新增
                item.CaseSpecEn = item.CaseSpecEn != null ? FileLib.ImgPathToUrl(item.CaseSpecEn) : item.CaseSpecEn;
                item.AroundEn = item.AroundEn != null ? FileLib.ImgPathToUrl(item.AroundEn) : item.AroundEn;
                item.DeviceEn = item.DeviceEn != null ? FileLib.ImgPathToUrl(item.DeviceEn) : item.DeviceEn;
                item.ServiceEn = item.ServiceEn != null ? FileLib.ImgPathToUrl(item.ServiceEn) : item.ServiceEn;
            });
            return list;
        }

        // 新增EstateObj
        public void Create(DbContent.EstateObj model)
        {
            // 處理數據
            model.MainImg = model.MainImg != null ? FileLib.ImgPathToSQL(model.MainImg) : model.MainImg;
            model.FloorImg = model.FloorImg != null ? FileLib.ImgPathToSQL(model.FloorImg) : model.FloorImg;
            model.CaseSpec = model.CaseSpec != null ? FileLib.ImgPathToSQL(model.CaseSpec) : model.CaseSpec;
            model.FileUrl = model.FileUrl != null ? FileLib.ImgPathToSQL(model.FileUrl) : model.FileUrl;
            model.Around = model.Around != null ? FileLib.ImgPathToSQL(model.Around) : model.Around;
            model.Device = model.Device != null ? FileLib.ImgPathToSQL(model.Device) : model.Device;
            model.Service = model.Service != null ? FileLib.ImgPathToSQL(model.Service) : model.Service;
            model.Album = model.Album != null ? FileLib.ImgPathToSQL(model.Album) : model.Album;
            model.VideoImg = model.VideoImg != null ? FileLib.ImgPathToSQL(model.VideoImg) : model.VideoImg;
            model.VideoUrl = model.VideoUrl;
            //多語系新增
            model.CaseSpecEn = model.CaseSpecEn != null ? FileLib.ImgPathToSQL(model.CaseSpecEn) : model.CaseSpecEn;
            model.AroundEn = model.AroundEn != null ? FileLib.ImgPathToSQL(model.AroundEn) : model.AroundEn;
            model.DeviceEn = model.DeviceEn != null ? FileLib.ImgPathToSQL(model.DeviceEn) : model.DeviceEn;
            model.ServiceEn = model.ServiceEn != null ? FileLib.ImgPathToSQL(model.ServiceEn) : model.ServiceEn;

            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.EstateObj.Add(model);
            basedb.SaveChanges();

            if (model.Status)
            {
                //發物业产品推播
                PushService psr = new PushService();
                psr.EstatePush(model.ID);
            }
        }

        // 修改EstateObj
        public void Modify(DbContent.EstateObj model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.EstateObj.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Name = model.Name;
                    data.SaleType = model.SaleType;
                    data.MainImg = model.MainImg != null ? FileLib.ImgPathToSQL(model.MainImg) : model.MainImg;
                    data.Brief = model.Brief;
                    data.Price = model.Price;
                    data.SpecTag = model.SpecTag;
                    data.VideoUrl = model.VideoUrl;
                    data.VideoImg = model.VideoImg != null ? FileLib.ImgPathToSQL(model.VideoImg) : model.VideoImg;
                    data.Country = model.Country;
                    data.EstateType = model.EstateType;
                    data.Address = model.Address;
                    data.FloorDesc = model.FloorDesc;
                    data.FloorImg = model.FloorImg != null ? FileLib.ImgPathToSQL(model.FloorImg) : model.FloorImg;
                    data.CompleteDate = model.CompleteDate;
                    data.CaseSpec = model.CaseSpec != null ? FileLib.ImgPathToSQL(model.CaseSpec) : model.CaseSpec;
                    data.FileUrl = model.FileUrl != null ? FileLib.ImgPathToSQL(model.FileUrl) : model.FileUrl;
                    data.OnShelfDate = model.OnShelfDate;
                    data.OffShelfDate = model.OffShelfDate;
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;
                    data.Around = model.Around != null ? FileLib.ImgPathToSQL(model.Around) : null;
                    data.Device = model.Device != null ? FileLib.ImgPathToSQL(model.Device) : null;
                    data.Service = model.Service != null ? FileLib.ImgPathToSQL(model.Service) : null;
                    data.Album = model.Album != null ? FileLib.ImgPathToSQL(model.Album) : null;
                    //多語系新增
                    data.NameEn = model.NameEn;
                    data.BriefEn = model.BriefEn;
                    data.SpecTagEn = model.SpecTagEn;
                    data.CountryEn = model.CountryEn;
                    data.EstateTypeEn = model.EstateTypeEn;
                    data.CaseSpecEn = model.CaseSpecEn != null ? FileLib.ImgPathToSQL(model.CaseSpecEn) : null;
                    data.AroundEn = model.AroundEn != null ? FileLib.ImgPathToSQL(model.AroundEn) : null;
                    data.DeviceEn = model.DeviceEn != null ? FileLib.ImgPathToSQL(model.DeviceEn) : null;
                    data.ServiceEn = model.ServiceEn != null ? FileLib.ImgPathToSQL(model.ServiceEn) : null;

                    basedb.SaveChanges();

                    if (model.Status)
                    {
                        //發物业产品推播
                        PushService psr = new PushService();
                        psr.EstatePush(model.ID);
                    }
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除EstateObj
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.EstateObj.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.EstateObj.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        // 刪除EstateObj
        public string GetNameByID(int? id, string lang = "zh_CN")
        {
            if (id == null)
            {
                return "";
            }
            // 取得指定資料
            var data = basedb.EstateObj.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                if(lang == LangLib.cn)
                {
                    return data.Name;
                }
                else
                {
                    return data.NameEn;
                }
            }
            else
            {
                return "";
            }
        }
        
    }
}