﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class CharterDataService: Service.BaseService
    {
        // 查詢CharterData
        public List<DbContent.CharterData> GetList(string startDate, string endDate, string type, int? EstateID)
        {
            var data = basedb.CharterData.Where(p => p.ID > 0);
            if (EstateID != null)
            {
                data = data.Where(p => p.EstateID == EstateID);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (!string.IsNullOrEmpty(type))
            {
                data = data.Where(p => p.ManagerType == type);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.ManageDeal = item.ManageDeal != null ? FileLib.ImgPathToUrl(item.ManageDeal) : item.ManageDeal;
            });
            return list;
        }

        // 新增CharterData
        public void Create(DbContent.CharterData model)
        {
            // 處理數據
            model.ManageDeal = model.ManageDeal != null ? FileLib.ImgPathToSQL(model.ManageDeal) : model.ManageDeal;
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.CharterData.Add(model);
            basedb.SaveChanges();
        }

        // 修改CharterData
        public void Modify(DbContent.CharterData model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.CharterData.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.ManagerType = model.ManagerType;
                    data.StartDate = model.StartDate;
                    data.EndDate = model.EndDate;
                    data.ManageDeal = model.ManageDeal != null ? FileLib.ImgPathToSQL(model.ManageDeal) : model.ManageDeal;
                    data.CharterAmount = model.CharterAmount;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除CharterData
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.CharterData.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.CharterData.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void Import(int EstateSaleID, List<CoreWeb.Models.ImportModel.CharterData> data)
        {
            if (EstateSaleID <= 0 || data == null || data.Count() == 0)
            {
                throw new Exception("汇入格式不正确");
            }
            //先防呆
            //先建立管理合約 在建立租賃合約
            foreach (var item in data)
            {
                //建立管理合約
                DbContent.CharterData model = new DbContent.CharterData
                {
                    EstateID = EstateSaleID,
                    ManagerType = "",
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    ManageDeal = null,
                    CharterAmount = item.RentAmount,
                    CreateDate = DateTime.Now,
                };

                if (item.ManagerType == "1" || item.ManagerType == "2")
                {
                    model.ManagerType = item.ManagerType;
                }
                else
                    throw new Exception("代管类型错误");

                //建立租賃合約
                DbContent.RentData rData = new DbContent.RentData
                {
                    EstateSaleID = EstateSaleID,
                    CharterDataID = model.ID,
                    RentStart = item.RentStart,
                    RentEnd = item.RentEnd,
                    RenterName = item.RenterName,
                    RenterBirth = item.RenterBirth,
                    RenterGender = "",
                    RenterIdNo = item.RenterIdNo,
                    RentAmount = item.RentAmount,
                    CreateDate = DateTime.Now,
                    RentStatus = "1",
                };

                if (item.RenterGender == "male" || item.RenterGender == "female")
                    rData.RenterGender = item.RenterGender;
                else
                    throw new Exception("租客性别错误");
            }

            //這邊要建立兩層
            //先建立管理合約 在建立租賃合約
            foreach (var item in data)
            {
                //建立管理合約
                DbContent.CharterData model = new DbContent.CharterData
                {
                    EstateID = EstateSaleID,
                    ManagerType = "",
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    ManageDeal = null,
                    CharterAmount = item.RentAmount,
                    CreateDate = DateTime.Now,
                };

                if (item.ManagerType == "1" || item.ManagerType == "2")
                {
                    model.ManagerType = item.ManagerType;
                }
                else
                    throw new Exception("代管类型错误");

                basedb.CharterData.Add(model);
                basedb.SaveChanges();

                //建立租賃合約
                DbContent.RentData rData = new DbContent.RentData
                {
                    EstateSaleID = EstateSaleID,
                    CharterDataID = model.ID,
                    RentStart = item.RentStart,
                    RentEnd = item.RentEnd,
                    RenterName = item.RenterName,
                    RenterBirth = item.RenterBirth,
                    RenterGender = "",
                    RenterIdNo = item.RenterIdNo,
                    RentAmount = item.RentAmount,
                    CreateDate = DateTime.Now,
                    RentStatus = "1",
                };

                if (item.RenterGender == "male" || item.RenterGender == "female")
                    rData.RenterGender = item.RenterGender;
                else
                    throw new Exception("租客性别错误");

                basedb.RentData.Add(rData);
                basedb.SaveChanges();
            }


        }
    }
}