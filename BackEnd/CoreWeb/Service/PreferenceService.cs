﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Models;

namespace CoreWeb.Service
{
    public class PreferenceService : Service.BaseService
    {
        //取得列表
        public List<PreferenceModel> GetPreference(string keyword, string startDate, string endDate, int? UserId)
        {
            List<PreferenceModel> l_Data = new List<PreferenceModel>();
            

            var data = from d in basedb.Preference
                       join ud in basedb.User on d.UserID equals ud.ID                       
                       select new
                       {
                           d.ID,
                           d.UserID,
                           d.UpdateDate,
                           d.CreateDate,
                           ud.Act,
                           ud.Name,
                           ud.IdentityNo
                       };


            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(p => p.Name.Contains(keyword) ||
                                p.Act.Contains(keyword) ||
                                p.IdentityNo.Contains(keyword));

            }
            if (UserId != null)
            {
                data = data.Where(p => p.UserID == UserId);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.UpdateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.UpdateDate < eDate);
            }
            data = data.OrderByDescending(p => p.UpdateDate);

            foreach (var item in data)
            {
                
                var preitem = basedb.PreferenceItem.Where(p => p.PreferenceID == item.ID);
                

                PreferenceModel oItem = new PreferenceModel
                {
                    Email = item.Act,
                    Name = item.Name,
                    IdentityNo = item.IdentityNo,
                    UpdateDate = item.UpdateDate,
                    UserID = item.UserID
                };
                

                foreach (var itemSub in preitem)
                {
                    PreferenceItem oItemSub = new PreferenceItem();
                    oItemSub.PrefernceItemID = itemSub.ID;
                    oItemSub.Ans = itemSub.PAnswer;
                    oItemSub.PrefernceName = itemSub.PType;
                    oItem.ItemData.Add(oItemSub);
                }

                l_Data.Add(oItem);
            }

            var result = l_Data.ToList();

            //若為渠道 在過濾一次
            string _type = AdminService.GetUserType();
            DbContent.Channel cuser = null;
            if (_type == "channel")
            {
                cuser = (DbContent.Channel)AdminService.GetUserData();
                string channelid = cuser.ID.ToString();
                var list = basedb.User.Where(p => p.Channel != null && p.Channel.Contains(channelid)).ToList();
                cuser = (DbContent.Channel)AdminService.GetUserData();
                List<DbContent.User> filter = new List<DbContent.User>();
                foreach (var uitem in list)
                {
                    if (!string.IsNullOrEmpty(uitem.Channel))
                    {
                        var carray = uitem.Channel.Split(',');
                        if (carray.Contains(channelid))
                        {
                            filter.Add(uitem);
                        }
                    }
                }
                var ChannelValidUserIDs = filter.Select(p => p.ID).ToList();
                List<PreferenceModel> _filter = new List<PreferenceModel>();
                foreach (var r in result)
                {
                    if (ChannelValidUserIDs.Contains(r.UserID))
                    {
                        _filter.Add(r);
                    }
                }
                return _filter;
            }

            return result;
        }
    }
}
