﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CoreWeb.Lib;

namespace CoreWeb.Service
{
    public class TradeNewsService : Service.BaseService
    {
        //取得列表
        public List<DbContent.TradeNews> GetTradeNews(string keyword, bool? status, DateTime? displaystartdate, DateTime? displayenddate, DateTime? beginstartdate, DateTime? beginenddate, DateTime? startdate, DateTime? enddate, string tradetypeid)
        {
            var data = basedb.TradeNews.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(p=>                
                    p.Name.Contains(keyword) ||
                    p.Author.Contains(keyword)
                    
               );
            }

            if (string.IsNullOrEmpty(tradetypeid) == false)
            {
                data = data.Where(p => p.TradeType.Contains(tradetypeid));
            }

            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            if (displaystartdate != null)
            {
                data = data.Where(p => p.DisplayDate >= displaystartdate);
            }

            if (displayenddate != null)
            {
                data = data.Where(p => p.DisplayDate <= displayenddate);
            }

            if (beginstartdate != null)
            {
                data = data.Where(p => p.BeginDate >= beginstartdate);
            }

            if (beginenddate != null)
            {
                data = data.Where(p => p.BeginDate <= beginenddate);
            }

            if (startdate != null)
            {
                data = data.Where(p => p.CreateDate >= startdate);
            }

            if (enddate != null)
            {
                data = data.Where(p => p.CreateDate <= enddate);
            }
          					
            data = data.OrderByDescending(p => p.DisplayDate);
            return data.ToList();
        }

        public List<DbContent.Activity> GetActivity()
        {

            var data = basedb.Activity.Where(p => p.ID > 0 && p.Status == true);
            data = data.OrderBy(p => p.OrderNo);

            return data.ToList();
        }

        public string TradeTypeName(string value)
        {
            var TradeTypeData = basedb.Option.Where(p => p.Code == "TradeType" && p.OptionValue == value).FirstOrDefault();

            return TradeTypeData.Name;
        }

        //新增TradeNews
        public void Create(DbContent.TradeNews model)
        {
            if (model.ID > 0)
            {
                //修改TradeNews
                var data = basedb.TradeNews.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    //參數處理
                    data.TradeType = model.TradeType;
                    data.Name = model.Name;
                    data.NameEn = model.NameEn;
                    data.DisplayDate = model.DisplayDate;
                    data.Brief = model.Brief;
                    data.BriefEn = model.BriefEn;
                    data.Img = FileLib.ImgPathToSQL(model.Img);
                    data.Author = model.Author;
                    data.AuthorEn = model.AuthorEn;
                    data.Detail = FileLib.ImgPathToSQL(model.Detail);
                    data.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                    data.Recommend = model.Recommend;
                    data.BeginDate = model.BeginDate;
                    data.EndDate = model.EndDate;
                    data.OrderNo = model.OrderNo;
                    data.Status = model.Status;
                    
                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增TradeNews
                //驗證是否重複標題
                var isExist = basedb.TradeNews.Where(p => p.Name == model.Name).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Name Exist");
                }

                //參數處理
                model.CreateDate = DateTime.Now;
                model.Img = FileLib.ImgPathToSQL(model.Img);
                model.Detail = FileLib.ImgPathToSQL(model.Detail);
                model.DetailEn = FileLib.ImgPathToSQL(model.DetailEn);
                //加入新增資料
                basedb.TradeNews.Add(model);
                //存檔
                basedb.SaveChanges();

                if (model.Status)
                {
                    //發理财趋势推播
                    PushService psr = new PushService();
                    psr.TrendPush(model.ID);
                }
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.TradeNews.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.TradeNews.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }
    }
}
