﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ExcelDataReader;

namespace CoreWeb.Service
{
    public class UserService : Service.BaseService
    {
        //取得列表
        public List<DbContent.User> GetUser(string keyword, string startDate, string endDate, string country, string channel, bool? valid, bool? status)
        {
            
            var data = basedb.User.Where(p => p.ID > 0);
            
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Act.Contains(keyword) ||
                    p.Name.Contains(keyword) ||
                    p.IdentityNo.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(country))
            {
                data = data.Where(p => p.Country == country);
            }
            if (!string.IsNullOrEmpty(channel))
            {
                data = data.Where(p => p.Channel.Contains(channel));
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (valid != null)
            {
                data = data.Where(p => p.Valid == valid);
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            // 轉換URL網域
            var list = data.ToList();
            list.ForEach(item =>
            {
                item.ImageUrl = item.ImageUrl != null ? FileLib.ImgPathToUrl(item.ImageUrl) : item.ImageUrl;
            });

            //若為渠道 在過濾一次
            string _type = AdminService.GetUserType();
            DbContent.Channel cuser = null;
            if (_type == "channel")
            {
                cuser = (DbContent.Channel)AdminService.GetUserData();
                string channelid = cuser.ID.ToString();
                List<DbContent.User> filter = new List<DbContent.User>();
                foreach (var uitem in list)
                {
                    if (!string.IsNullOrEmpty(uitem.Channel))
                    {
                        var carray = uitem.Channel.Split(',');
                        if (carray.Contains(channelid))
                        {
                            filter.Add(uitem);
                        }
                    }
                }
                return filter;
            }

            return list;
        }

        //新增User
        public void Create(DbContent.User model)
        {
            //新增修改前都先檢查帳號是否符合Email格式
            // 驗證EMAIL帳號
            try
            {
                var addr = new System.Net.Mail.MailAddress(model.Act);
                if (addr.Address != model.Act)
                    throw new Exception("Email wrong format"); //請輸入正確的Email格式
            }
            catch (Exception ex)
            {
                throw new Exception("Email wrong format"); //請輸入正確的Email格式
            }



            if (model.ID > 0)
            {

                //改前驗證是否重複帳號
                var isExist = basedb.User.Where(p => p.ID != model.ID && p.Act == model.Act).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Account Exist");
                }

                //修改Admin
                var data = basedb.User.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {

                    //參數處理
                    data.Act = model.Act;
                    if (!string.IsNullOrEmpty(model.Pwd))
                        data.Pwd = Security.Encrypt(model.Pwd);
                    data.Name = model.Name;
                    data.Valid = model.Valid;
                    data.IdentityNo = model.IdentityNo;
                    data.Birth = model.Birth;
                    data.Country = model.Country;
                    data.Phone = model.Phone;
                    data.ImageUrl = model.ImageUrl != null ? FileLib.ImgPathToSQL(model.ImageUrl) : model.ImageUrl;
                    data.Status = model.Status;
                    data.Channel = model.Channel;
                    //直接存檔
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Not Found");
                }
            }
            else
            {
                //新增User
                //驗證是否重複帳號
                var isExist = basedb.User.Where(p => p.Act == model.Act).FirstOrDefault();
                if (isExist != null)
                {
                    throw new Exception("Account Exist");
                }

                //判斷密碼格式
                if (DataLib.isPwdForamt(model.Pwd))
                {
                    model.Pwd = Security.Encrypt(model.Pwd);
                }
                else
                {
                    throw new Exception("Password wrong format");
                }

                //參數處理
                model.Pwd = model.Pwd;
                model.ImageUrl = model.ImageUrl != null ? FileLib.ImgPathToSQL(model.ImageUrl) : model.ImageUrl;
                model.CreateDate = DateTime.Now;
                model.UpdateDate = DateTime.Now;
                model.DeviceID = "";
                model.Platform = "";
                //加入新增資料
                basedb.User.Add(model);
                //存檔
                basedb.SaveChanges();
            }


        }

        //刪除User
        public void Remove(int id)
        {
            var data = basedb.User.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                //加入刪除項目
                basedb.User.Remove(data);
                //直接存檔
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Not Found");
            }
        }

        //更改User狀態
        public void ChangeStatus(int Id)
        {
            var user = basedb.User.Where(p => p.ID == Id).FirstOrDefault();
            if (user != null)
            {
                var enabled = user.Status;
                user.Status = !user.Status;
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("資料錯誤!");
            }
        }

        public void ChangeValid(int Id)
        {
            var user = basedb.User.Where(p => p.ID == Id).FirstOrDefault();
            if (user != null)
            {
                //var enabled = user.Valid;
                user.Valid = !user.Valid;
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("資料錯誤!");
            }
        }

        public string IsVerify(int UserID)
        {
            var user = basedb.User.Where(p => p.ID == UserID).FirstOrDefault();
            if (user == null)
            {
                return "此帳號不存在";
            }
            if (user.Status == false)
            {
                return "此帳號停用";
            }
            if (user.Valid == true)
            {
                return "此帳號已驗證";
            }
            else
            {
                if (user.UpdateDate.AddDays(1) > DateTime.Now)
                {
                    user.Valid = true;
                    user.UpdateDate = DateTime.Now;
                    basedb.SaveChanges();

                    //標記MailLog以驗證
                    CoreWeb.Areas.App.Service.apiUserService apiusr = new Areas.App.Service.apiUserService();
                    apiusr.CheckMailLog("1",user.Act);

                    return "";
                }
                else
                {
                    return "此網址認證期限已逾時！";
                }
            }
        }

        public DbContent.User GetByID(int UserID)
        {
            var user = basedb.User.Where(p => p.ID == UserID).FirstOrDefault();
            if (user == null)
                throw new Exception("此帳號不存在");

            return user;
        }
        public string ResetPwd(int UserID, string newPwd)
        {
            var user = basedb.User.Where(p => p.ID == UserID).FirstOrDefault();
            if (user == null)
            {
                return "此帳號不存在";
            }
            if (user.Status == false)
            {
                return "此帳號停用";
            }

            else
            {
                if (user.UpdateDate.AddDays(1) > DateTime.Now)
                {
                    user.Pwd = newPwd;
                    var token = Guid.NewGuid().ToString();
                    user.Token = token;
                    user.UpdateDate = DateTime.Now;
                    basedb.SaveChanges();
                    return "";
                }
                else
                {
                    return "此密碼變更期限已逾時！";
                }
            }
        }

        public void UserImport(string filepath)
        {

            //FileStream sample = System.IO.File.Open(FileLib.rootPath + "/Content/Sample.xlsx", FileMode.Open, FileAccess.Read);
            FileStream sample = System.IO.File.Open(filepath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(sample);
            System.Data.DataSet result = excelReader.AsDataSet();
            excelReader.Close();

            var sourceData = result.Tables[0].Rows;
            int row_count = sourceData.Count;
            var column_count = result.Tables[0].Columns.Count;

            var firstColume = new List<string>();
            int rowindex = 2;

            List<List<string>> Table = new List<List<string>>();
            while (rowindex < row_count)
            {
                int columnStart = 0;
                List<string> columnData = new List<string>();
                while (columnStart < column_count)
                {
                    string content = sourceData[rowindex][columnStart].ToString();
                    //可為空的欄位 1(密碼) 8(渠道)
                    if (columnStart == 1 || columnStart == 8)
                    {
                        if(columnStart == 1)
                        {
                            //若密碼為空 填手機(不含國碼)
                            if (string.IsNullOrEmpty(content))
                            {
                                string pwd = sourceData[rowindex][7].ToString();
                                content = pwd;
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(content))
                            throw new Exception("汇入格式错误");
                    }

                    //驗證生日
                    DateTime birth;
                    if (columnStart == 4)
                    {
                        if (!DateTime.TryParse(content, out birth))
                        {
                            throw new Exception("汇入格式错误");
                        }
                    }
                    
                    columnData.Add(content);
                    columnStart++;
                }
                Table.Add(columnData);
                rowindex++;
            }

            //匯入格式
            //信箱重複不匯入 身分證字號重複不匯入
            foreach (var item in Table)
            {
                var Act = item[0];//帐号(信箱)
                var Pwd = item[1];//密码
                var Name = item[2];//姓名
                var IDNo = item[3];//ID/身分证字号
                var birth = item[4];// 生日
                DateTime _birth = DateTime.Parse(birth);
                var Country = item[5];//国码
                var PhonePrefix = item[6];//国码
                var Phone = item[7];//电话
                var channel = item[8];//渠道名称

                var isExist = basedb.User.Where(p => p.Act == Act || p.IdentityNo == IDNo).FirstOrDefault();

                OptionService ops = new OptionService();
                var CountryList = ops.GetListByCode("Country");
                var ChannelList = basedb.Channel.ToList();

                int SaveSum = 0;
                if (isExist == null)
                {
                    DbContent.User u = new DbContent.User()
                    {
                        Act = Act,
                        Pwd = Security.Encrypt(Pwd),
                        Name = Name,
                        IdentityNo = IDNo,
                        Birth = _birth,
                        Country = Country,
                        PhonePrefix = PhonePrefix,
                        Phone = Phone,
                        ImageUrl = null,
                        Channel = channel,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Valid = true,
                        Status = true,
                        DeviceID = "Import",
                        Platform = "Import",
                        Version = null,
                        PushID = null,
                        Lang = "zh_CN"
                    };
                    //國家轉換
                    var isCountry = CountryList.Where(p => p.Name == u.Country).FirstOrDefault();
                    if (isCountry != null)
                        u.Country = isCountry.OptionValue;
                    else
                        u.Country = "";

                    if (!string.IsNullOrEmpty(u.Channel))
                    {
                        string _channel = "";
                        var cArray = u.Channel.Split(',');

                        foreach (var cdata in cArray)
                        {
                            var isChannel = ChannelList.Where(p => p.Name == cdata).FirstOrDefault();
                            if (isChannel != null)
                                _channel = isChannel.ID.ToString() + ",";
                        }
                        u.Channel = _channel.TrimEnd(',');
                    }
                    basedb.User.Add(u);
                    SaveSum++;
                }

                if(SaveSum > 0)
                {
                    basedb.SaveChanges();
                }
            }


        }
    }
}
