﻿using CoreWeb.Lib;
using CoreWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CoreWeb.Service
{

    public class EstateSaleService : BaseService
    {
        // 查詢EstateSale
        public List<EstateSaleModel> GetList(string keyword, string startDate, string endDate, string type, string floor, string room, bool? status)
        {
            var data = basedb.EstateSale.Where(p => p.ID > 0);            
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (!string.IsNullOrEmpty(floor))
            {
                data = data.Where(p => p.Floor.Contains(floor));
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }

            data = data.OrderByDescending(p => p.CreateDate);

            OptionService ops = new OptionService();
            var parktype = ops.GetListByCode("ParkingType");

            List<EstateSaleModel> list = new List<EstateSaleModel>();
            DateTime today = DateTime.Now;

            // 查詢EstateObj
            var estateObjList = basedb.EstateObj.ToList();
            // 查詢EstateRoom
            var estateRoomList = basedb.EstateRoom.ToList();
            // 查詢Charter
            var CharterDataList = basedb.CharterData.Where(p => p.EndDate >= today & p.StartDate <= today).ToList();

            foreach (var item in data)
            {
                // 查詢EstateObj
                var estateObj = estateObjList.Where(p => p.ID == item.EstateObjID).FirstOrDefault();
                // 查詢EstateRoom
                var estateRoom = estateRoomList.Where(p => p.ID == item.EstateRoomID).FirstOrDefault();
                // 查詢Charter
                var CharterData = CharterDataList.Where(p => p.EstateID == item.ID).FirstOrDefault();
                // 查詢RentData
                DbContent.RentData rentData = null;
                if (CharterData != null)
                {
                    rentData = basedb.RentData.Where(p => p.EstateSaleID == item.ID & p.CharterDataID == CharterData.ID & p.RentEnd >= today & p.RentStart <= today).FirstOrDefault();
                };

                EstateSaleModel model = new EstateSaleModel();
                model.ID = item.ID;
                model.IsFinish = (!string.IsNullOrEmpty(item.IsFinish)) ? item.IsFinish : "1";
                model.EstateNo = !string.IsNullOrEmpty(item.EstateNo) ? item.EstateNo : "";
                model.UserIdentityNo = !string.IsNullOrEmpty(item.UserIdentityNo) ? item.UserIdentityNo : "";
                model.Channel = !string.IsNullOrEmpty(item.Channel) ? item.Channel : "";
                model.BuyerName = !string.IsNullOrEmpty(item.BuyerName) ? item.BuyerName : "";
                model.EstateObjID = item.EstateObjID;
                model.EstateRoomID = item.EstateRoomID;
                model.Addr = item.Addr;
                model.Floor = item.Floor;
                model.UnitArea = item.UnitArea;
                model.RoomArea = item.RoomArea;
                model.PublicArea = item.PublicArea;
                model.PriceSum = item.PriceSum;
                model.DealFile = item.DealFile != null ? FileLib.ImgPathToUrl(item.DealFile) : item.DealFile;
                model.SignDate = item.SignDate;
                model.ParkingType = item.ParkingType;
                model.ParkingTypeName = OptionService.GetOptName(parktype,item.ParkingType);
                model.ParkingPrice = item.ParkingPrice;
                model.ManagerType = item.ManagerType;
                model.ManagerDataID = item.ManagerDataID;
                model.IsPush = item.IsPush;
                model.CreateDate = item.CreateDate;
                model.UpdDate = item.UpdDate;
                model.Status = item.Status;
                if(estateObj != null)
                {
                    model.EstateObjName = !string.IsNullOrEmpty(estateObj.Name) ? estateObj.Name : "";
                }
                else
                {
                    model.EstateObjName = "";
                }
                if (estateRoom != null)
                {
                    model.EstateRoomName = !string.IsNullOrEmpty(estateRoom.Name) ? estateRoom.Name : "";
                }
                else
                {
                    model.EstateRoomName = "";
                }
                
                // 查無對應租賃紀錄時，預設值為2(待租中)
                //出租中 1 待租中 2 未簽約0
                model.RentStatus = "0";
                if (CharterData != null)
                {
                    model.RentStatus = "2";
                    if (rentData != null)
                    {
                        model.RentStatus = "1";
                    }
                }

                list.Add(model);
            }
            // 租賃狀態
            if (!string.IsNullOrEmpty(type))
            {
                list = list.Where(p => p.RentStatus == type).ToList();
            }
            // 關鍵字搜尋
            if (!string.IsNullOrEmpty(keyword))
            {
                list = list.Where(
                    p => p.EstateNo.Contains(keyword) ||
                    p.UserIdentityNo.Contains(keyword) ||
                    p.Channel.Contains(keyword) ||
                    p.BuyerName.Contains(keyword) ||
                    p.EstateObjName.Contains(keyword) ||
                    p.EstateRoomName.Contains(keyword)
               ).ToList();
            }
            //房型搜尋
            if (!string.IsNullOrEmpty(room))
            {
                list = list.Where(p => p.EstateRoomName.Contains(room)).ToList();
            }


            //若為登入身分為渠道 在過濾一次
            string _type = AdminService.GetUserType();
            DbContent.Channel cuser = null;
            if (_type == "channel")
            {
                cuser = (DbContent.Channel)AdminService.GetUserData();
                string channelid = cuser.ID.ToString();
                List<EstateSaleModel> filter = new List<EstateSaleModel>();
                foreach (var uitem in list)
                {
                    if (!string.IsNullOrEmpty(uitem.Channel))
                    {
                        var carray = uitem.Channel.Split(',');
                        if (carray.Contains(channelid))
                        {
                            filter.Add(uitem);
                        }
                    }
                }
                return filter;
            }

            return list;
        }

        // 新增EstateSale
        public void Create(DbContent.EstateSale model)
        {
            //不能存重複ID
            var isExist = basedb.EstateSale.Where(p => p.EstateNo == model.EstateNo).FirstOrDefault();
            if (isExist != null)
            {
                throw new Exception("已存在相同物件ID " + model.EstateNo);
            }
            // 處理數據
            model.DealFile = model.DealFile != null ? FileLib.ImgPathToSQL(model.DealFile) : model.DealFile;
            model.CreateDate = DateTime.Now;
            model.UpdDate = DateTime.Now;
            // 新增資料
            basedb.EstateSale.Add(model);
            basedb.SaveChanges();
        }

        // 修改EstateSale
        public void Modify(DbContent.EstateSale model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.EstateSale.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.UserIdentityNo = model.UserIdentityNo;
                    data.IsFinish = model.IsFinish;
                    data.Channel = model.Channel;
                    data.BuyerName = model.BuyerName;
                    data.EstateObjID = model.EstateObjID;
                    data.EstateRoomID = model.EstateRoomID;
                    data.Addr = model.Addr;
                    data.Floor = model.Floor;
                    data.UnitArea = model.UnitArea;
                    data.RoomArea = model.RoomArea;
                    data.PublicArea = model.PublicArea;
                    data.PriceSum = model.PriceSum;
                    data.DealFile = model.DealFile != null ? FileLib.ImgPathToSQL(model.DealFile) : model.DealFile;
                    data.SignDate = model.SignDate;
                    data.ParkingType = model.ParkingType;
                    data.ParkingPrice = model.ParkingPrice;
                    data.IsPush = model.IsPush;
                    data.Status = model.Status;
                    data.UpdDate = DateTime.Now;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除EstateSale
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.EstateSale.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.EstateSale.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void EstateSaleImport(List<ImportModel.EstateData> data)
        {
            //檢查內容是否有重複物件編號
            var Nos = data.Select(p => p.EstateNo).ToList();
            var groups = Nos.GroupBy(v => v);
            foreach (var group in groups)
            {
                var k = group.Key;
                if (string.IsNullOrEmpty(k))
                    throw new Exception("汇入重复物件ID不正确");
                var c = group.Count();
                if(c > 1)
                    throw new Exception("已有重复物件ID");
            }



            var SeleIDs = basedb.EstateSale.Where(p => p.Status == true).Select(p => p.EstateNo).ToList();

            var ChannelList = basedb.Channel.ToList();
            int importCnt = 0;

            if (data.Count() == 0)
            {
                throw new Exception("汇入格式不正确");
            }

            foreach (var item in data)
            {
                if(SeleIDs.Contains(item.EstateNo))
                    throw new Exception("已有重复物件ID");
                //购买总价
                DateTime sDate;
                if (!DateTime.TryParse(item.SignDate, out sDate))
                {
                    throw new Exception("汇入格式错误");
                }
                //购买总价
                Double psum;
                if (!Double.TryParse(item.PriceSum, out psum))
                {
                    throw new Exception("汇入格式错误");
                }

                Double? _tempPrice = null;
                Double pprice;
                if (Double.TryParse(item.ParkingPrice, out pprice))
                {
                    _tempPrice = pprice;
                }

                if (string.IsNullOrEmpty(item.EstateNo) || string.IsNullOrEmpty(item.UserIdentityNo) ||
                    string.IsNullOrEmpty(item.BuyerName) || string.IsNullOrEmpty(item.EstateObjName) ||
                    string.IsNullOrEmpty(item.Room) || string.IsNullOrEmpty(item.Addr) ||
                    string.IsNullOrEmpty(item.Floor) || string.IsNullOrEmpty(item.IsPush) ||
                    string.IsNullOrEmpty(item.ParkingType) || string.IsNullOrEmpty(item.IsFinish))
                {
                    throw new Exception("汇入格式错误");
                }

                DbContent.EstateSale es = new DbContent.EstateSale()
                {
                    EstateNo = item.EstateNo,
                    IsFinish = item.IsFinish,
                    UserIdentityNo = item.UserIdentityNo,
                    BuyerName = item.BuyerName,
                    Addr = item.Addr,
                    Floor = item.Floor,
                    UnitArea = item.UnitArea,
                    RoomArea = item.RoomArea,
                    PublicArea = item.PublicArea,
                    PriceSum = psum,
                    SignDate = sDate,
                    IsPush = item.IsPush.Contains("TRUE") ? true : false,
                    ParkingType = item.ParkingType,
                    ParkingPrice = (_tempPrice == null) ? (double?)null : _tempPrice.Value,
                    Status = item.Status.Contains("TRUE") ? true : false,
                    UpdDate = DateTime.Now,
                    CreateDate =DateTime.Now
            };
                string esName = item.EstateObjName;
                // 查詢EstateObj
                var estateObj = basedb.EstateObj.Where(p => p.Name.Equals(esName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (estateObj == null)
                {
                    throw new Exception("汇入格式错误");                    
                }
                
                es.EstateObjID = estateObj.ID;

                string room = item.Room;
                // 查詢EstateRoom
                var estateRoom = basedb.EstateRoom.Where(p => p.Name.Equals(room, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (estateRoom == null)
                {
                    throw new Exception("汇入格式错误");
                }
                es.EstateRoomID = estateRoom.ID;


                if (!string.IsNullOrEmpty(item.Channel))
                {
                    string _channel = "";
                    var cArray = item.Channel.Split(',');

                    foreach (var cdata in cArray)
                    {
                        var isChannel = ChannelList.Where(p => p.Name == cdata).FirstOrDefault();
                        if (isChannel != null)
                            _channel = isChannel.ID.ToString() + ",";
                    }
                    es.Channel = _channel.TrimEnd(',');
                }

                basedb.EstateSale.Add(es);
                importCnt++;
            }
            if (importCnt >0)
                basedb.SaveChanges();
        }
    }
}
