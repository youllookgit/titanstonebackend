﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreWeb.Models;
namespace CoreWeb.Service
{
    public class InsuranceSaleService : Service.BaseService
    {
        // 查詢保險商品
        public List<InsuranceSaleModel> GetList(
            string keyword, string Status,
            DateTime? validStart, DateTime? validEnd,
            DateTime? startDate, DateTime? endDate
            )
        {
            InsuranceService isr = new InsuranceService();
            var InsList = isr.GetList("",null);
            OptionService opsr = new OptionService();
            var statusList = opsr.GetListByCode("InsuranceValid");

            var data = basedb.InsuranceSale.Where(p => p.ID > 0);

            if (!string.IsNullOrEmpty(Status))
                data = data.Where(p => p.InsStatus == Status);

            if (validStart != null)
            {
                validStart = validStart.Value.AddSeconds(-1);
                data = data.Where(p => p.ValidStart >= validStart.Value);
            }
            if (validEnd != null)
            {
                validEnd = validEnd.Value.AddDays(1);
                data = data.Where(p => p.ValidEnd <= validEnd.Value);
            }
            if (startDate != null)
            {
                data = data.Where(p => p.CreateDate >= startDate.Value);
            }
            if (endDate != null)
            {
                endDate = endDate.Value.AddDays(1);
                data = data.Where(p => p.CreateDate <= endDate.Value);
            }

            data = data.OrderByDescending(p => p.CreateDate);

            var result = data.ToList();
            List<InsuranceSaleModel> model = new List<InsuranceSaleModel>();
            foreach (var item in result)
            {
                InsuranceSaleModel d = new InsuranceSaleModel()
                {
                    ID = item.ID,
                    InsID = item.InsID,
                    InsNo = (string.IsNullOrEmpty(item.InsNo)) ? "" : item.InsNo,
                    UserIdentityNo =  (string.IsNullOrEmpty(item.UserIdentityNo)) ? "" : item.UserIdentityNo,
                    Channel = item.Channel,
                    BuyerName = (string.IsNullOrEmpty(item.BuyerName)) ? "" : item.BuyerName,
                    BenefitName = (string.IsNullOrEmpty(item.BenefitName)) ? "" : item.BenefitName,
                    SaleName = (string.IsNullOrEmpty(item.SaleName)) ? "" : item.SaleName,
                    SalePhone = item.SalePhone,
                    InsStatus = item.InsStatus,
                    ValidDate = (item.ValidDate != null) ? item.ValidDate.Value.ToString("yyyy/MM/dd") : "",
                    ValidStart = (item.ValidStart != null) ? item.ValidStart.Value.ToString("yyyy/MM/dd") : "",
                    ValidEnd = (item.ValidEnd != null)  ? item.ValidEnd.Value.ToString("yyyy/MM/dd") : "",
                    PayPeriod = item.PayPeriod,
                    PayType = item.PayType,
                    Currency = item.Currency,
                    CurrencyEn = item.CurrencyEn,
                    Amount = item.Amount,
                    InsAmount = item.InsAmount,
                    InsContent = item.InsContent,
                    InsContentEn = item.InsContentEn,
                    InsNote = item.InsNote,
                    InsNoteEn = item.InsNoteEn,
                    Recommend = (item.Recommend == null) ? "" : item.Recommend.ToSiteUrl(),
                    DealFile = (item.DealFile == null) ? "" : item.DealFile.ToSiteUrl(),
                    Note = item.Note,
                    CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                };
                //組前端顯示欄位
                if (!string.IsNullOrEmpty(item.Channel))
                {
                    var clist = item.Channel.Split(',');
                    ChannelService cs = new ChannelService();
                    var cdata = cs.GetChannelName(clist);
                    d.ChannelName = String.Join(",", cdata.Select(p => p.Name));
                }
                else
                {
                    d.ChannelName = "";
                }
                d.InsFee = d.Currency + " " + d.Amount.ToString();
                d.ValidRange = d.ValidStart + " - " + d.ValidEnd;
                d.InsName = "";
                var InsData = InsList.Where(p => p.ID == d.InsID).FirstOrDefault();
                if (InsData != null)
                {
                    d.InsName = InsData.Name;
                    d.InsSortName = InsData.TypeName;
                }
                    

                var statusData = statusList.Where(p => p.OptionValue == d.InsStatus).FirstOrDefault();
                d.StatusName = "";
                if (statusData != null)
                {
                    d.StatusName = statusData.Name;
                }
                model.Add(d);
            }

            //補篩選保險名稱 渠道
            if (!string.IsNullOrEmpty(keyword))
            {
                model = model.Where(p =>                
                p.ChannelName.Contains(keyword) ||
                p.InsName.Contains(keyword) ||
                p.InsNo.Contains(keyword) ||
                p.UserIdentityNo.Contains(keyword) ||
                p.BuyerName.Contains(keyword) ||
                p.SaleName.Contains(keyword) ||
                p.BenefitName.Contains(keyword) 
                ).ToList();
            }
            ////補篩選保險名稱 渠道
            //if (!string.IsNullOrEmpty(keyword))
            //{
            //    model = model.Where(p =>
            //    p.ChannelName.Contains(keyword) ||
            //    p.InsName.Contains(keyword)
            //    ).ToList();
            //}

            //若為登入身分為渠道 在過濾一次
            string _type = AdminService.GetUserType();
            DbContent.Channel cuser = null;
            if (_type == "channel")
            {
                cuser = (DbContent.Channel)AdminService.GetUserData();
                string channelid = cuser.ID.ToString();
                List<InsuranceSaleModel> filter = new List<InsuranceSaleModel>();
                foreach (var uitem in model)
                {
                    if (!string.IsNullOrEmpty(uitem.Channel))
                    {
                        var carray = uitem.Channel.Split(',');
                        if (carray.Contains(channelid))
                        {
                            filter.Add(uitem);
                        }
                    }
                }
                return filter;
            }

            return model;
        }
        // 新增保險商品
        public void Create(DbContent.InsuranceSale model)
        {
            //驗證
            var isExist = basedb.InsuranceSale.Where(p => p.InsNo == model.InsNo).FirstOrDefault();
            if (isExist != null)
            {
                throw new Exception("保单号码重复");
            }
            // 處理數據
            model.CreateDate = DateTime.Now;

            if (!string.IsNullOrEmpty(model.DealFile))
                model.DealFile = model.DealFile.ToSQL();

            if (!string.IsNullOrEmpty(model.Recommend))
                model.DealFile = model.Recommend.ToSQL();
            // 新增資料
            basedb.InsuranceSale.Add(model);
            basedb.SaveChanges();
        }
        // 修改保險商品
        public void Modify(DbContent.InsuranceSale model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.InsuranceSale.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.InsID = model.InsID;
                    data.UserIdentityNo = model.UserIdentityNo;
                    data.Channel = model.Channel;
                    data.BuyerName = model.BuyerName;
                    data.BenefitName = model.BenefitName;
                    data.SaleName = model.SaleName;
                    data.SalePhone = model.SalePhone;
                    data.InsStatus = model.InsStatus;
                    data.ValidDate = model.ValidDate;
                    data.ValidStart = model.ValidStart;
                    data.ValidEnd = model.ValidEnd;
                    data.PayPeriod = model.PayPeriod;
                    data.PayType = model.PayType;
                    data.Currency = model.Currency;
                    data.CurrencyEn = model.CurrencyEn;
                    data.Amount = model.Amount;
                    data.InsAmount = model.InsAmount;
                    data.InsContent = model.InsContent;
                    data.InsContentEn = model.InsContentEn;
                    data.InsNote = model.InsNote;
                    data.InsNoteEn = model.InsNoteEn;

                    data.Note = model.Note;

                    if (!string.IsNullOrEmpty(model.DealFile))
                        data.Recommend = model.Recommend.ToSQL();
                    if (!string.IsNullOrEmpty(model.Recommend))
                        data.DealFile = model.DealFile.ToSQL();
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }
        // 刪除保險商品
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.InsuranceSale.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.InsuranceSale.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }

        public void Import(List<CoreWeb.Models.ImportModel.InsSale> data)
        {
            //檢查內容是否有重複物件編號
            var Nos = data.Select(p => p.InsNo).ToList();
            var groups = Nos.GroupBy(v => v);
            foreach (var group in groups)
            {
                var k = group.Key;
                if (string.IsNullOrEmpty(k))
                    throw new Exception("汇入保单号码不可为空白");
                var c = group.Count();
                if (c > 1)
                    throw new Exception("已有重复保单号码");
            }

            if (data.Count() == 0)
            {
                throw new Exception("汇入资料为空");
            }

            var InsData = basedb.Insurance.Where(p => p.ID > 0).ToList();
            var IDs = basedb.InsuranceSale.Where(p => p.ID > 0).Select(p => p.InsNo).ToList();
            var ChannelList = basedb.Channel.ToList();

            int importCnt = 0;

            foreach (var item in data)
            {
                if (IDs.Contains(item.InsNo))
                    throw new Exception("已有重复保单号码");

                if (
                    string.IsNullOrEmpty(item.UserIdentityNo) ||
                    string.IsNullOrEmpty(item.UserIdentityNo) ||
                    string.IsNullOrEmpty(item.BuyerName) ||
                    string.IsNullOrEmpty(item.BenefitName) ||
                    string.IsNullOrEmpty(item.InsName) ||
                    string.IsNullOrEmpty(item.InsStatus) ||
                    string.IsNullOrEmpty(item.PayPeriod) ||
                    string.IsNullOrEmpty(item.PayType) ||
                    string.IsNullOrEmpty(item.Currency) ||
                    string.IsNullOrEmpty(item.CurrencyEn)
                    )
                {
                    throw new Exception("汇入格式错误");
                }

                //處理保險名称
                var _Ins = InsData.Where(p => p.Name == item.InsName).FirstOrDefault();
                if (_Ins == null)
                    throw new Exception("汇入保險名称错误");

                item.InsID = _Ins.ID;

                if (!string.IsNullOrEmpty(item.Channel))
                {
                    string _channel = "";
                    var cArray = item.Channel.Split(',');

                    foreach (var cdata in cArray)
                    {
                        var isChannel = ChannelList.Where(p => p.Name == cdata).FirstOrDefault();
                        if (isChannel != null)
                            _channel = isChannel.ID.ToString() + ",";
                    }
                    item.Channel = _channel.TrimEnd(',');
                }

                var _save = new DbContent.InsuranceSale()
                {
                    InsNo = item.InsNo,
                    InsID = item.InsID,
                    UserIdentityNo = item.UserIdentityNo,
                    Channel = item.Channel,
                    BuyerName = item.BuyerName,
                    BenefitName = item.BenefitName,
                    SaleName = item.SaleName,
                    SalePhone = item.SalePhone,
                    InsStatus = item.InsStatus,
                    ValidDate = item.ValidDate,
                    ValidStart = item.ValidStart,
                    ValidEnd = item.ValidEnd,
                    PayPeriod = item.PayPeriod,
                    PayType = item.PayType,
                    Currency = item.Currency,
                    CurrencyEn = item.CurrencyEn,
                    Amount = item.Amount,
                    InsAmount = item.InsAmount,
                    Note = item.Note,
                    CreateDate = DateTime.Now
                };

                basedb.InsuranceSale.Add(_save);
                importCnt++;
            }
            if (importCnt > 0)
                basedb.SaveChanges();
        }
    }
}