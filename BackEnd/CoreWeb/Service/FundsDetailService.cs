﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FundsDetailService: Service.BaseService
    {
        // 查詢FundsDetail
        public List<DbContent.FundsDetail> GetList(string keyword, string startDate, string endDate, 
            double? redemptionMin, double? redemptionMax, double? purchaseMin, double? purchaseMax, 
            string txnType, string txnStartDate, string txnEndDate, int? FundsSaleID)
        {
            var data = basedb.FundsDetail.Where(p => p.ID > 0);
            if (FundsSaleID != null)
            {
                data = data.Where(p => p.FundsSaleID == FundsSaleID);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.TxnNo.Contains(keyword)
               );
            }
            if (!string.IsNullOrEmpty(txnType))
            {
                data = data.Where(p => p.TxnType == txnType);
            }
            if (!string.IsNullOrEmpty(txnStartDate))
            {
                DateTime tsDate = DateTime.Parse(txnStartDate);
                data = data.Where(p => p.TxnDate >= tsDate);
            }
            if (!string.IsNullOrEmpty(txnEndDate))
            {
                DateTime teDate = DateTime.Parse(txnEndDate).AddDays(1);
                data = data.Where(p => p.TxnDate < teDate);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime sDate = DateTime.Parse(startDate);
                data = data.Where(p => p.CreateDate >= sDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime eDate = DateTime.Parse(endDate).AddDays(1);
                data = data.Where(p => p.CreateDate < eDate);
            }
            if (redemptionMin != null)
            {
                data = data.Where(p => p.RedemptionVal >= redemptionMin);
            }
            if (redemptionMax != null)
            {
                data = data.Where(p => p.RedemptionVal <= redemptionMax);
            }
            if (purchaseMin != null)
            {
                data = data.Where(p => p.PurchaseVal >= purchaseMin);
            }
            if (purchaseMax != null)
            {
                data = data.Where(p => p.PurchaseVal <= purchaseMax);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            var list = data.ToList();

            return list;
        }

        // 新增FundsDetail
        public void Create(DbContent.FundsDetail model)
        {
            // 檢查資料設定單號
            var data = basedb.FundsDetail.Where(p => p.TxnNo == model.TxnNo && model.TxnType != "3").FirstOrDefault();
            if (data == null)
            {
                // 處理數據
                model.CreateDate = DateTime.Now;
                // 新增資料
                basedb.FundsDetail.Add(model);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Assigning Transaction No. Duplicated");
            }
        }

        // 修改FundsDetail
        public void Modify(DbContent.FundsDetail model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.FundsDetail.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.TxnDate = model.TxnDate;
                    data.Unit = model.Unit;
                    data.RedemptionVal = model.RedemptionVal;
                    data.RedemptionAmt = model.RedemptionAmt;
                    data.PurchaseVal = model.PurchaseVal;
                    data.PurchaseAmt = model.PurchaseAmt;
                    data.DividendAmt = model.DividendAmt;
                    data.PrefFee = model.PrefFee;
                    data.HandlingFee = model.HandlingFee;
                    data.PayStatus = model.PayStatus;
                    data.Note = model.Note;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除FundsDetail
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.FundsDetail.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.FundsDetail.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
        //Bryan0414：
        //累積申購金額，要扣掉贖回
        //投資成本加總 (申購淨值 - 贖回淨值 加總)
        public double FundsSaleSum(int FundsSaleID)
        {
            //投資成本加總(申購金額加總)
            string cost_sql = $"SELECT ISNULL((SELECT SUM(PurchaseAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 1),Cast(0 as float)) as Total";
            var cost_cmd = basedb.Database.SqlQuery<double>(cost_sql);
            var cost = cost_cmd.First();

            //(贖回淨值加總)
            string earn_sql = $"SELECT ISNULL((SELECT SUM(RedemptionAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID} AND TxnType = 2),Cast(0 as float)) as Total";
            var earn_cmd = basedb.Database.SqlQuery<double>(earn_sql);
            var earn = earn_cmd.First();

            return cost - earn;
        }
        //// 依據FundsSaleID統計申購總金額
        //public double FundsSaleSum(int FundsSaleID)
        //{
        //    string sumsql = $"SELECT ISNULL((SELECT SUM(PurchaseAmt) FROM FundsDetail WHERE FundsSaleID = {FundsSaleID}),Cast(0 as float)) as Total";
        //    var sqlcmd = basedb.Database.SqlQuery<double>(sumsql);
        //    var result = sqlcmd.First();
        //    return result;
        //}
        
        public void Import(List<CoreWeb.Models.ImportModel.FundSaleDetail> data)
        {
            //檢查內容是否有重複物件編號
            var Nos = data.Select(p => p.TxnNo).ToList();
            var groups = Nos.GroupBy(v => v);
            foreach (var group in groups)
            {
                var k = group.Key;
                if (string.IsNullOrEmpty(k))
                    throw new Exception("汇入交易单号不可为空白");
                var c = group.Count();
                if (c > 1)
                    throw new Exception("已有重复交易单号");
            }

            if (data.Count() == 0)
            {
                throw new Exception("汇入资料为空");
            }

            var FSaleID = data[0].FundsSaleID;
            var IDs = basedb.FundsDetail.Where(p => p.FundsSaleID == FSaleID).Select(p => p.TxnNo).ToList();

            int importCnt = 0;

            foreach (var item in data)
            {
                if (IDs.Contains(item.TxnNo))
                    throw new Exception("已有重复交易单号");

                if (
                    string.IsNullOrEmpty(item.TxnType) ||
                    item.TxnDate == null
                    )
                {
                    throw new Exception("汇入格式错误");
                }
                if (item.TxnType != "1" && item.TxnType != "2" && item.TxnType != "3")
                    throw new Exception("交易类型格式错误");
                //申購判斷
                if (item.TxnType == "1")
                {
                    if (
                        item.Unit == null ||
                        item.PurchaseVal == null ||
                        item.PurchaseAmt == null ||
                         string.IsNullOrEmpty(item.PayStatus)
                        )
                    {
                        throw new Exception("汇入申購格式错误");
                    }
                }
                //贖回判斷
                if (item.TxnType == "2")
                {
                    if (
                        item.Unit == null ||
                        item.RedemptionVal == null ||
                        item.RedemptionAmt == null
                        )
                        throw new Exception("汇入贖回格式错误");
                }
                //分紅判斷
                if (item.TxnType == "3")
                {
                    if (item.DividendAmt == null)
                        throw new Exception("汇入分紅格式错误");
                }

                DbContent.FundsDetail _save = new DbContent.FundsDetail()
                {
                    //ID
                    FundsSaleID = item.FundsSaleID,
                    TxnNo = item.TxnNo,
                    TxnType = item.TxnType,
                    TxnDate = item.TxnDate,
                    Unit = item.Unit,
                    RedemptionVal = item.RedemptionVal,
                    RedemptionAmt = item.RedemptionAmt,
                    PurchaseVal = item.PurchaseVal,
                    PurchaseAmt = item.PurchaseAmt,
                    DividendAmt = item.DividendAmt,
                    PrefFee = item.PrefFee,
                    HandlingFee = item.HandlingFee,
                    PayStatus = item.PayStatus,
                    Note = item.Note,
                    CreateDate = DateTime.Now
                };

                basedb.FundsDetail.Add(_save);
                importCnt++;
            }
            if (importCnt > 0)
                basedb.SaveChanges();
        }
    }
}