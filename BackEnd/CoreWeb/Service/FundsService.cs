﻿using CoreWeb.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreWeb.Service
{
    public class FundsService: Service.BaseService
    {
        // 查詢Funds
        public List<DbContent.Funds> GetList(string keyword, bool? status, string fundType, string buyType)
        {
            var data = basedb.Funds.Where(p => p.ID > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(
                    p => p.Name.Contains(keyword)
               );
            }
            if (status != null)
            {
                data = data.Where(p => p.Status == status);
            }
            if (!string.IsNullOrEmpty(fundType))
            {
                data = data.Where(p => p.FundType == fundType);
            }
            if (!string.IsNullOrEmpty(buyType))
            {
                data = data.Where(p => p.BuyType == buyType);
            }
            data = data.OrderByDescending(p => p.CreateDate);

            return data.ToList();
        }

        // 新增Funds
        public void Create(DbContent.Funds model)
        {
            // 處理數據
            model.CreateDate = DateTime.Now;
            // 新增資料
            basedb.Funds.Add(model);
            basedb.SaveChanges();
        }

        // 修改Funds
        public void Modify(DbContent.Funds model)
        {
            if (model.ID > 0)
            {
                // 取得指定資料
                var data = basedb.Funds.Where(p => p.ID == model.ID).FirstOrDefault();
                if (data != null)
                {
                    // 處理可修改數據
                    data.Name = model.Name;
                    data.FundType = model.FundType;
                    data.BuyType = model.BuyType;
                    data.Status = model.Status;
                    basedb.SaveChanges();
                }
                else
                {
                    throw new Exception("Data Not Found");
                }
            }
        }

        // 刪除Funds
        public void Delete(int id)
        {
            // 取得指定資料
            var data = basedb.Funds.Where(p => p.ID == id).FirstOrDefault();
            if (data != null)
            {
                // 刪除資料
                basedb.Funds.Remove(data);
                basedb.SaveChanges();
            }
            else
            {
                throw new Exception("Data Not Found");
            }
        }
    }
}