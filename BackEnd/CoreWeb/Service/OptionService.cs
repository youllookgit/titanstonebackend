﻿using CoreWeb.Lib;
using CoreWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace CoreWeb.Service
{
    public class OptionService : Service.BaseService
    {
        public List<DbContent.Option> GetListByCode(string code,bool? Enabled = null)
        {
            var data = basedb.Option.Where(p => p.Code == code);
            if (Enabled != null)
            {
                data = data.Where(p => p.Enabled == Enabled);
            }
            return data.OrderBy(p => p.OrderNo).ToList();
        }
        public DbContent.Option GetByCode(string code,string value)
        {
            var data = basedb.Option.Where(p => p.Code == code && p.OptionValue == value).FirstOrDefault();
            return data;
        }
        public static string GetOptName(List<DbContent.Option> data, string value, string lang = "")
        {
            var obj = data.Where(p => p.OptionValue == value).FirstOrDefault();
            if (obj == null)
            {
                return "";
            }
            else
            {
                if (lang != LangLib.en || lang == "")
                {
                    return obj.Name;
                }
                else
                {
                    return obj.NameEn;
                }
            }
        }
        
    }
}